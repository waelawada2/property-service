package com.sothebys.propertydb.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 *
 * @author nagaraju
 *
 */
@Data
public class ArtistCatalogueRaisoneeDTO {

  private Long id;

  @Size(max = 250, message = "Author for catalogue raisonee can not be greater than 250 characters")
  @NotNull(message = "Author for catalogue raisonee can not be null")
  private String author;

  @Size(max = 250, message = "Type for catalogue rasionee can not be greater than 250 characters")
  private String type;

  @Size(max = 10, message = "Volumes for catalogue rasionee can not be greater than 10 characters")
  private String volumes;

  private Integer year;

  private Integer sortId;

}
