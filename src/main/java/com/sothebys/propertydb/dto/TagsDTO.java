package com.sothebys.propertydb.dto;

import java.math.BigDecimal;
import java.util.List;

import com.sothebys.propertydb.model.Orientation;
import com.sothebys.propertydb.model.Scale;

import lombok.Data;

/**
 * This class used to hold Tags information
 *
 * @author Minhtri Tran
 */
@Data
public class TagsDTO {

  private String imageText;

  private String imageFigure;

  private String expertiseLocation;

  private List<String> imageSubjects;

  private List<String> imageGenders;

  private List<String> imagePositions;

  private List<String> imageAnimals;

  private List<String> imageMainColours;

  private List<ExpertiseSitterDTO> expertiseSitters;

  private List<String> otherTags;

  private Orientation autoOrientation;

  private boolean autoImage;

  private Scale autoScale;

  private BigDecimal autoVolume;
}
