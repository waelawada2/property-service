package com.sothebys.propertydb.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum SnsMessageOperation {
  CREATE(),
  DELETE(),
  MERGE();
}
