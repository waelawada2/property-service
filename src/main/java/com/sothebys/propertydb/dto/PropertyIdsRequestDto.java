package com.sothebys.propertydb.dto;

import java.util.List;

import lombok.Data;

/**
 * @author SrinivasaRao Batthula
 *
 */
@Data
public class PropertyIdsRequestDto {

  private List<String> externalIds;

}
