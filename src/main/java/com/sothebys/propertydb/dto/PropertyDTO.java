package com.sothebys.propertydb.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class PropertyDTO {

  private List<ArchiveNumberDTO> archiveNumbers;
  private int artistProofTotal;
  private List<ArtistDTO> artists = null;
  private Boolean authentication;
  private int bonATirerTotal;
  private Boolean castYearCirca;
  private Integer castYearStart;
  private Integer castYearEnd;
  private Boolean certificate;
  private Boolean yearCirca;
  private BigDecimal depthCm;
  private String editionName;
  private String editionNumber;

  private String foundry;
  private BigDecimal heightCm;
  private int horsCommerceProofTotal;
  private String id;
  private String imagePath;
  private String medium;
  private String originalTitle;
  private CategoryDTO parentCategory;
  private Integer parts;
  private Boolean posthumous;
  private String presizeName;
  private PresizeTypeDTO presizeType;
  private int printersProofTotal;
  private List<PropertyCatalogueRaisoneeDTO> propertyCatalogueRaisonees = null;
  @JsonFormat(pattern = "MM-dd-yyyy")
  @DateTimeFormat(pattern = "MM-dd-yyyy")
  private Date reminderDate;
  private String reminderEmployee;
  private String reminderNotesText;
  private String researcherName;
  @JsonFormat(pattern = "MM-dd-yyyy")
  @DateTimeFormat(pattern = "MM-dd-yyyy")
  private Date researcherNotesDate;
  private String researcherNotesText;
  private Boolean researchCompleteCheck;
  private int seriesTotal;
  private String signature;
  private Boolean signed;
  private String sortCode;
  private Boolean stamped;
  private CategoryDTO subCategory;
  private String title;
  private int trialProofTotal;
  private Boolean uniqueEdition;
  private String uploadReference;
  private BigDecimal weight;
  private BigDecimal widthCm;
  private Integer yearStart;
  private Integer yearEnd;
  private String yearText;
  private Boolean unknownEditionSizeType;
  private String editionSizeNotes;
  private String sizeText;
  private String editionSizeTypeName;
  private int editionSizeTotal;
  private int cancellationProofTotal;
  private int colorTrialProofTotal;
  private int dedicatedProofTotal;
  private int museumProofTotal;
  private int progressiveProofTotal;
  private int rightToProduceProofTotal;
  private int workingProofTotal;
  private String editionOverride;
  private String currentOwnerId;
  private FlagsDTO flags;
  private TagsDTO tags;
   @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
   @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
   private Date createdDate;
   @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
   @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
   private Date updatedDate;
   private UserDTO createdBy;
   private UserDTO updatedBy;
   private List<String> objectCodes;
   private Boolean isPropertyDeleted = false;
}
