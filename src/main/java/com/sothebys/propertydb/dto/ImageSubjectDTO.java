package com.sothebys.propertydb.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * @author roopa.rnair
 *
 */
@Data
public class ImageSubjectDTO {

  private Long id;

  @NotNull(message = "Sitter name can not be empty")
  @Size(max = 30, message = "Sitter Name text can not be greater than 30 characters")
  private String name;

}
