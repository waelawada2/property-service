package com.sothebys.propertydb.dto.response;

import java.io.Serializable;
import lombok.Data;

@Data
public class SavePropertiesResponseDto implements Serializable {

  private int totalFound;
  private int totalInserted;

}
