// Copyright (c) 2018 Sotheby's, Inc.
package com.sothebys.propertydb.dto.response;

import java.io.Serializable;
import lombok.Data;

/**
 * @author jsilva.
 */
@Data
public class ClientInfoDTO implements Serializable {

  private String entityId;
}
