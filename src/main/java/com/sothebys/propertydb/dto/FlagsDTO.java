package com.sothebys.propertydb.dto;

import lombok.Data;

/**
 * This class used to hold Flags information
 *
 * @author VenkataPrasad Tammineni
 *
 */
@Data
public class FlagsDTO {
  private Boolean flagAuthenticity;

  private Boolean flagCondition;

  private Boolean flagMedium;

  private Boolean flagRestitution;

  private Boolean flagSfs;

  private String flagsCountry;


}
