package com.sothebys.propertydb.dto.request;

import com.sothebys.propertydb.search.PropertySearchInputDTO;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class SaveAllPropertiesRequestDto {

  List<String> ignoredIds;

  @NotNull
  PropertySearchInputDTO searchCriteria;

}
