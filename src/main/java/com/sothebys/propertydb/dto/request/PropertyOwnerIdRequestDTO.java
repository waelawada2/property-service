package com.sothebys.propertydb.dto.request;

import java.util.List;
import lombok.Data;

/**
 * @author jsilva.
 */

@Data
public class PropertyOwnerIdRequestDTO {
  private List<String> externalIds;
  private String currentOwnerId;
}
