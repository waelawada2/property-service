package com.sothebys.propertydb.dto.request;

import lombok.Data;

/**
 * This class used to hold Flags request information
 *
 * @author VenkataPrasad Tammineni
 *
 */

@Data
public class FlagsRequestDTO {

  private Boolean flagAuthenticity;

  private Boolean flagCondition;

  private Boolean flagMedium;

  private Boolean flagRestitution;

  private Boolean flagSfs;

  private String flagsCountry;


}
