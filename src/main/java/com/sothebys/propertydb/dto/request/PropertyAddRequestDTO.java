package com.sothebys.propertydb.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sothebys.propertydb.dto.ArchiveNumberDTO;
import com.sothebys.propertydb.validation.HasArtistIdentifier;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.Size;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Created by wael.awada on 3/22/17.
 */
@HasArtistIdentifier
@Data
public class PropertyAddRequestDTO {

  private List<ArchiveNumberDTO> archiveNumbers;

  private List<String> artistIds;
  
  private List<Long> ulanIds;

  private int artistProofTotal;

  private Boolean authentication;

  private int bonATirerTotal;

  private Boolean castYearCirca;

  private Integer castYearStart;

  private Integer castYearEnd;

  @Size(max = 250, message = "Category can not be greater than 250 characters")
  private String category;

  private Boolean certificate;

  private BigDecimal depthCm;

  private String editionName; // TODO: Can be removed?

  private String editionNumber;

  @Size(max = 250, message = "Foundry can not be greater than 250 characters")
  private String foundry;

  private BigDecimal heightCm;

  private int horsCommerceProofTotal;

  @Size(max = 1000, message = "Medium can not be greater than 1000 characters")
  private String medium;

  @Size(max = 500, message = "Original title can not be greater than 500 characters")
  private String originalTitle;

  private Integer parts;

  private Boolean posthumous;

  @Size(max = 250, message = "Presize name can not be greater than 250 characters")
  private String presizeName;

  private int printersProofTotal;

  private List<PropertyAddRequestCatalogueRaisoneeDTO> propertyCatalogueRaisonees;

  @JsonFormat(pattern = "MM-dd-yyyy")
  @DateTimeFormat(pattern = "MM-dd-yyyy")
  private Date reminderDate;

  @Size(max = 200, message = "Reminder employee can not be greater than 200 characters")
  private String reminderEmployee;

  @Size(max = 200, message = "Reminder notes text can not be greater than 200 characters")
  private String reminderNotesText;

  @Size(max = 200, message = "Researcher name can not be greater than 200 characters")
  private String researcherName;

  @JsonFormat(pattern = "MM-dd-yyyy")
  @DateTimeFormat(pattern = "MM-dd-yyyy")
  private Date researcherNotesDate;

  @Size(max = 1000, message = "Researcher notes text can not be greater than {max} characters")
  private String researcherNotesText;
  
  private Boolean researchCompleteCheck;

  private int seriesTotal;

  @Size(max = 1000, message = "Signature can not be greater than 1000 characters")
  private String signature;

  private Boolean signed;

  @Size(max = 200, message = "Sort code can not be greater than 200 characters")
  private String sortCode;

  private Boolean stamped;

  @Size(max = 250, message = "Sub-category can not be greater than 250 characters")
  private String subCategory;

  @Size(max = 500, message = "Title can not be greater than 500 characters")
  private String title;

  private int trialProofTotal;

  private Boolean uniqueEdition;

  private String uploadReference;

  private BigDecimal weight;

  private BigDecimal widthCm;

  private Boolean yearCirca;

  private Integer yearStart;

  private Integer yearEnd;

  @Size(max = 200, message = "Year text can not be greater than 200 characters")
  private String yearText;

  private Boolean unknownEditionSizeType;

  @Size(max = 200, message = "Edition size notes can not be greater than 200 characters")
  private String editionSizeNotes;

  @Size(max = 500, message = "Size text can not be greater than 500 characters")
  private String sizeText;

  private String editionSizeTypeName;

  private int editionSizeTotal;

  private int cancellationProofTotal;

  private int colorTrialProofTotal;

  private int dedicatedProofTotal;

  private int museumProofTotal;

  private int progressiveProofTotal;

  private int rightToProduceProofTotal;

  private int workingProofTotal;

  @Size(max = 200, message = "Edition override can not be greater than 200 characters")
  private String editionOverride;

  private FlagsRequestDTO flags;
  
  private List<String> objectCodes;
  
  private String propertyImage;

  private TagsRequestDTO tags;

  private String imagePath;

}
