package com.sothebys.propertydb.dto.request;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * @author SrinivasaRao Batthula
 *
 */
@Data
public class SaveListRequestDTO {

  @NotNull
  private String name;

  @NotNull
  private String notes;
  
  @NotNull
  private String user;

  @NotNull
  private boolean openSaveList;

  @JsonFormat(pattern = "MM-dd-yyyy")
  @DateTimeFormat(pattern = "MM-dd-yyyy")
  private Date date;

  @NotNull
  @Size(min = 1)
  private List<String> propertyIds;

  @NotNull
  @Size(min = 1)
  private List<String> sharedUsersList;

}