package com.sothebys.propertydb.dto.request;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sothebys.propertydb.dto.SaveSearchCriteriaDTO;

import lombok.Data;

/**
 * @author SrinivasaRao Batthula
 *
 */
@Data
public class SaveSearchRequestDTO {

  @NotNull
  private String name;

  @NotNull
  private String notes;

  @JsonFormat(pattern = "MM-dd-yyyy")
  @DateTimeFormat(pattern = "MM-dd-yyyy")
  private Date date;

  @NotNull
  private String searchType;

  private List<SaveSearchCriteriaDTO> searchCriterias;
}
