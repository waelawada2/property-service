package com.sothebys.propertydb.dto.request;

import com.sothebys.propertydb.search.PropertySearchInputDTO;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Data
public class SaveAllPropertiesFromSavelistRequestDto {

  private List<String> ignoredIds;

  @NotEmpty
  private String fromSavedListId;

}
