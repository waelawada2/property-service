package com.sothebys.propertydb.dto.request;

import lombok.Data;

/**
 * Created by waelawada on 3/26/17.
 */
@Data
public class PropertyAddRequestCatalogueRaisoneeDTO {

  private Long id;
  private Long artistCatalogueRaisoneeId;
  private String volume;
  private String number;

}
