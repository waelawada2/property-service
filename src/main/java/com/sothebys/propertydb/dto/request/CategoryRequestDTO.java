package com.sothebys.propertydb.dto.request;

import javax.validation.constraints.Size;
import lombok.Data;

/**
 * Created by waelawada on 4/8/17.
 */
@Data
public class CategoryRequestDTO {
  private String description;

  @Size(max = 30, message = "Category text can not be greater than 30 characters")
  private String name;
}
