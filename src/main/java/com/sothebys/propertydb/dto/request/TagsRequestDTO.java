package com.sothebys.propertydb.dto.request;

import java.util.List;
import com.sothebys.propertydb.dto.ExpertiseSitterDTO;
import lombok.Data;

/**
 * Created by Minhtri Tran on 11/15/17.
 */
@Data
public class TagsRequestDTO {

  private String imageText;

  private String imageFigure;

  private String expertiseLocation;

  private List<String> imageSubjects;

  private List<String> imageGenders;

  private List<String> imagePositions;

  private List<String> imageAnimals;

  private List<String> imageMainColours;

  private List<ExpertiseSitterDTO> expertiseSitters;

  private List<String> otherTags;

}
