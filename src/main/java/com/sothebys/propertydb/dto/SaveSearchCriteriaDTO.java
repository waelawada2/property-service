package com.sothebys.propertydb.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * @author SrinivasaRao Batthula
 *
 */
@Data
public class SaveSearchCriteriaDTO {

  private Long id;

  @NotNull
  private String paramName;

  @NotNull
  private String paramValue;

}
