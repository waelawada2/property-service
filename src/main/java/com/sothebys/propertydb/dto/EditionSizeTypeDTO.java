package com.sothebys.propertydb.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * @author SrinivasaRao Batthula
 *
 */
@Data
public class EditionSizeTypeDTO {

  private Long id;

  @NotNull(message = "Edition size name can not be empty or null")
  @Size(max = 30, message = "EditionSize Type Name text can not be greater than 30 characters")
  private String name;

}
