/**
 * 
 */
package com.sothebys.propertydb.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * @author Manju.Rana
 *
 */
@Data
public class UserDTO {

  @Size(max = 250, message = "First name can not exceed 250 characters")
  @NotNull(message = "First name can not be null")
  private String firstName;

  @Size(max = 250, message = "Username can not exceed 250 characters")
  @NotNull(message = "Username can not be null")
  private String userName;

  @Size(max = 250, message = "Last name can not exceed 250 characters")
  @NotNull(message = "Last name can not be null")
  private String lastName;
  
  @Size(max = 250, message = "Email can not exceed 250 characters")
  @NotNull(message = "Email can not be null")
  private String email;

}
