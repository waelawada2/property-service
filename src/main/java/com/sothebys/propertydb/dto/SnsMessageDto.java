package com.sothebys.propertydb.dto;

import lombok.Data;

@Data
public class SnsMessageDto {

  private String id;

  private String mergedTo;

}
