/**
 * 
 */
package com.sothebys.propertydb.dto;

import javax.validation.constraints.Size;

import lombok.Data;

/**
 * @author roopa.rnair
 *
 */

@Data
public class OtherTagDTO {

	private Long id;

	@Size(max = 250, message = "Name can not be greater than 250 characters")
	private String name;
}
