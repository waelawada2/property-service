package com.sothebys.propertydb.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author SrinivasaRao Batthula
 *
 */
@Data
public class SaveListDTO {

  private String id;

  @NotNull(message = "Saved list name can not be null")
  @Size(min = 1, max = 250, message = "save list name must be between 1 and 250 characters long")
  private String name;

  @NotNull(message = "Saved list notes can not be null")
  private String notes;

  @NotNull(message = "Saved list user can not be null")
  private String user;

  @NotNull(message = "Open saved list should be specified as true or false")
  private boolean openSaveList;

  @JsonFormat(pattern = "MM-dd-yyyy")
  @DateTimeFormat(pattern = "MM-dd-yyyy")
  private Date date;

  private List<String> propertyIds;

  @NotNull(message = "Shared users list can not be empty")
  private List<String> sharedUsersList;
  
  private boolean saveListCompleteStatus = false;

  private boolean saveListResearchCompleteStatus = false;

}
