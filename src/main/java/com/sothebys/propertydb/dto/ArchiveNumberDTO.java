package com.sothebys.propertydb.dto;

import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class ArchiveNumberDTO {

  private int id;

  @Size(max = 250, message = "Archive Number can not be greater than 250 characters")
  private String archiveNumber;

}
