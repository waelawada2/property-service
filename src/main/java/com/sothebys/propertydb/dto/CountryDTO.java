package com.sothebys.propertydb.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * @author SrinivasaRao Batthula
 *
 */
@Data
public class CountryDTO {

  private Long id;

  @NotNull(message = "Country name can not be empty")
  @Size(max = 60, message = "Country Name text can not be greater than 60 characters")
  private String countryName;

}
