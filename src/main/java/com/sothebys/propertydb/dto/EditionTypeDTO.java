package com.sothebys.propertydb.dto;

import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * @author SrinivasaRao Batthula
 *
 */
@Data
public class EditionTypeDTO {

  private Long id;

  @NotNull(message = "Edition-type description can not be null")
  private String description;

  @NotNull(message = "Edition-type name can not be null")
  private String name;

  @NotNull(message = "Edition-type sort code can not be null")
  private Long sortCode;

}
