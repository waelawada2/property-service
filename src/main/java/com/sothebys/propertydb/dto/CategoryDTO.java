package com.sothebys.propertydb.dto;

import lombok.Data;

/**
 * @author SrinivasaRao Batthula
 *
 */
@Data
public class CategoryDTO {
  private String description;
  private Long id;
  private String name;
}
