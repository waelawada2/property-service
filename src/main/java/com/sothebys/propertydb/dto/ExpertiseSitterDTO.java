package com.sothebys.propertydb.dto;

import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.StringUtils.stripAll;

import javax.validation.constraints.Size;
import lombok.Data;

/**
 * @author aneesha.l
 */
@Data
public class ExpertiseSitterDTO {

  private Long id;

  @Size(max = 100, message = "ExpertiseSitter First Name text can not be greater than 100 characters")
  private String firstName;

  @Size(max = 100, message = "ExpertiseSitter Last Name text can not be greater than 100 characters")
  private String lastName;

  private String displayName;

  public String fullName() {
    return join(stripAll(firstName, lastName), ' ').trim();
  }

}
