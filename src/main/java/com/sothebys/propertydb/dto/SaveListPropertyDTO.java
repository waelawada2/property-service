package com.sothebys.propertydb.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author SrinivasaRao Batthula
 *
 */
@Data
public class SaveListPropertyDTO {

  private String id;

  @NotNull(message = "Saved list name can not be null")
  private String name;

  @NotNull(message = "Saved list notes can not be null")
  private String notes;

  @NotNull(message = "User can not be null")
  private String user;

  @NotNull(message = "Open saved list should be specified as true or false")
  private boolean openSaveList;

  @JsonFormat(pattern = "MM-dd-yyyy")
  @DateTimeFormat(pattern = "MM-dd-yyyy")
  private Date date;

  private List<PropertyDTO> properties;

  private List<String> sharedUsers;
  

}
