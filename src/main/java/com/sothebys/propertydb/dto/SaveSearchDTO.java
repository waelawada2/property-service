package com.sothebys.propertydb.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author SrinivasaRao Batthula
 *
 */
@Data
public class SaveSearchDTO {

  private String id;

  @NotNull(message = "Saved search name can not be null")
  private String name;

  @NotNull(message = "Saved search notes can not be null")
  private String notes;

  @JsonFormat(pattern = "MM-dd-yyyy")
  @DateTimeFormat(pattern = "MM-dd-yyyy")
  private Date date;

  @NotNull(message = "Saved search search type can not be null")
  private String searchType;

  private List<SaveSearchCriteriaDTO> searchCriterias;
}
