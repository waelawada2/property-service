package com.sothebys.propertydb.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author nagaraju
 *
 */

@Data
public class ArtistDTO {

  private String id;

  private Boolean approved;

  private List<ArtistCatalogueRaisoneeDTO> artistCatalogueRaisonees = new ArrayList<>();

  private Integer birthYear;

  private List<CategoryDTO> categories = new ArrayList<>();

  private Integer deathYear;

  @Size(max = 200, message = "Display name can not be greater than 200 characters")
  private String display;

  @Size(max = 100, message = "First name can not be greater than 100 characters")
  private String firstName;

  @Size(max = 10, message = "Gender can not be greater than 10 characters")
  private String gender;

  @Size(max = 100, message = "Last name can not be greater than 100 characters")
  @NotNull(message = "Last name can not be empty")
  private String lastName;

  @Size(max = 250, message = "Notes can not be greater than 250 characters")
  private String notes;

  private Long ulanId;
  
  private List<CountryDTO> countries = new ArrayList<>();
  
  @JsonFormat(pattern = "dd-MMM-yyyy")
  @DateTimeFormat(pattern = "dd-MMM-yyyy")
  private Date createdDate;
  @JsonFormat(pattern = "dd-MMM-yyyy")
  @DateTimeFormat(pattern = "dd-MMM-yyyy")
  private Date updatedDate;
  private UserDTO createdBy;
  private UserDTO updatedBy;

}
