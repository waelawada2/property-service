package com.sothebys.propertydb.dto;

import lombok.Data;

/**
 *
 * @author nagaraju
 *
 */
@Data
public class PropertyCatalogueRaisoneeDTO {

  private Long id;
  private Long artistCatalogueRaisoneeId;
  private String artistCatalogueRaisoneeType;
  private String artistCatalogueRaisoneeAuthor;
  private String number;
  private String volume;

}
