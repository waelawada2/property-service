package com.sothebys.propertydb.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * @author SrinivasaRao Batthula
 *
 */
@Data
public class PresizeTypeDTO {

  private Long id;

  @NotNull(message = "Presize-type description can not be null")
  private String description;

  @NotNull(message = "Presize-type name can not be null")
  @Size(max = 60, message = "Presize-type text can not be greater than 60 characters")
  private String name;

}
