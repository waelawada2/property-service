package com.sothebys.propertydb.exception;

/**
 * Exception thrown when an entity is too large.
 *
 * @author Gregor Zurowski
 */
public class TooLargeException extends PropertyServiceException {

  private static final long serialVersionUID = 1L;

  public TooLargeException() {
    super();
  }

  public TooLargeException(String message) {
    super(message);
  }

  public TooLargeException(String message, Throwable cause) {
    super(message, cause);
  }

}
