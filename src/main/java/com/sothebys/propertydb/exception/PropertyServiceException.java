package com.sothebys.propertydb.exception;

/**
 * Base exception for property service.
 *
 * @author Gregor Zurowski
 * @author SrinivasaRao Batthula
 */
public class PropertyServiceException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public PropertyServiceException() {
    super();
  }

  public PropertyServiceException(String message) {
    super(message);
  }

  public PropertyServiceException(String message, Throwable cause) {
    super(message, cause);
  }

}
