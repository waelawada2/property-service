package com.sothebys.propertydb.exception;

public class BadRequestException extends PropertyServiceException {

  private static final long serialVersionUID = 1L;

  public BadRequestException() {
    super();
  }

  public BadRequestException(String message) {
    super(message);
  }

  public BadRequestException(String message, Throwable cause) {
    super(message, cause);
  }

}
