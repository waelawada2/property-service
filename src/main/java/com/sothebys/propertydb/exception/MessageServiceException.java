package com.sothebys.propertydb.exception;

/**
 * Exception thrown when aws messaging process fails.
 */
public class MessageServiceException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public MessageServiceException() {
    super();
  }

  public MessageServiceException(String message) {
    super(message);
  }

  public MessageServiceException(String message, Throwable cause) {
    super(message, cause);
  }

}
