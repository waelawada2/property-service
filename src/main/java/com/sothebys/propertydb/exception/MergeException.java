package com.sothebys.propertydb.exception;

/**
 * Exception is thrown when an entity cannot be added, read or merged, if the object status
 * "Merged".
 *
 * @author SrinivasaRao Batthula
 */
public class MergeException extends PropertyServiceException {

  private static final long serialVersionUID = 1L;

  private String mergedToId;

  public MergeException() {
    super();
  }

  public MergeException(String message, String mergedToId) {
    super(message);
    this.mergedToId = mergedToId;
  }

  public MergeException(String message, String mergedToId, Throwable cause) {
    super(message, cause);
    this.mergedToId = mergedToId;
  }

  public String getMergedToId() {
    return mergedToId;
  }

  public void setMergedToId(String mergedToId) {
    this.mergedToId = mergedToId;
  }
}
