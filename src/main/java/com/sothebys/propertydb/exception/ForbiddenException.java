package com.sothebys.propertydb.exception;

/**
 * Exception thrown when the user access denied.
 *
 * @author SrinivasaRao Batthula
 */
public class ForbiddenException extends PropertyServiceException {

  private static final long serialVersionUID = 1L;

  public ForbiddenException() {
    super();
  }

  public ForbiddenException(String message) {
    super(message);
  }

  public ForbiddenException(String message, Throwable cause) {
    super(message, cause);
  }

}
