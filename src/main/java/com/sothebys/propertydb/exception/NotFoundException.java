package com.sothebys.propertydb.exception;

/**
 * Exception thrown when some entity was not found.
 *
 * @author Gregor Zurowski
 */
public class NotFoundException extends PropertyServiceException {

  private static final long serialVersionUID = 1L;

  public NotFoundException() {
    super();
  }

  public NotFoundException(String message) {
    super(message);
  }

  public NotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

}
