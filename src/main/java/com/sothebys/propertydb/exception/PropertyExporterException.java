package com.sothebys.propertydb.exception;

/**
 * Exception is thrown when fail to export property data into csv file.
 *
 * @author VenkataPrasad Tammineni
 */

public class PropertyExporterException extends PropertyServiceException {

  private static final long serialVersionUID = 1L;

  public PropertyExporterException() {
    super();
  }

  public PropertyExporterException(String message) {
    super(message);
  }

  public PropertyExporterException(String message, Throwable cause) {
    super(message, cause);
  }

}
