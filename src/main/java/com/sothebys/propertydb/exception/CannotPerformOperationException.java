package com.sothebys.propertydb.exception;

/**
 * Exception is thrown when an entity cannot be added, updated or deleted.
 *
 * @author Gregor Zurowski
 */
public class CannotPerformOperationException extends PropertyServiceException {

  private static final long serialVersionUID = 1L;

  public CannotPerformOperationException() {
    super();
  }

  public CannotPerformOperationException(String message) {
    super(message);
  }

  public CannotPerformOperationException(String message, Throwable cause) {
    super(message, cause);
  }

}
