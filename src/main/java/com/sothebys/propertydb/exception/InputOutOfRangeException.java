// Copyright (c) 2018 Sotheby's, Inc.
package com.sothebys.propertydb.exception;

/**
 *  Exception thrown when input data is out of range and cant be handled by the service.
 * @author jsilva.
 */
public class InputOutOfRangeException extends PropertyServiceException {

  public InputOutOfRangeException(String message) {
    super(message);
  }
}
