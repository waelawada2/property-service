package com.sothebys.propertydb.exception;

/**
 * Exception is thrown when an entity already exists.
 *
 * @author SrinivasaRao Batthula
 */

public class AlreadyExistsException extends PropertyServiceException {

  private static final long serialVersionUID = 1L;

  public AlreadyExistsException() {
    super();
  }

  public AlreadyExistsException(String message) {
    super(message);
  }

  public AlreadyExistsException(String message, Throwable cause) {
    super(message, cause);
  }

}
