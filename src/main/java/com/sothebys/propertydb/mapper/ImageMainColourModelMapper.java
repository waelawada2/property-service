package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.ImageMainColourDTO;
import com.sothebys.propertydb.model.ImageMainColour;

/**
 * @author aneesha.l
 *	
 */
@Component
public class ImageMainColourModelMapper extends AbstractModelMapper {

	@Autowired
	public ImageMainColourModelMapper(ModelMapper modelMapper) {
		super(modelMapper);
	};

	public Page<ImageMainColourDTO> mapEntityPageIntoDTOPage(Pageable pageRequest, Page<ImageMainColour> source) {
		List<ImageMainColourDTO> dtos = this.mapToImageMainColourDTOs(source.getContent());
		return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
	}

	/**
	 * @param imageMainColour
	 * @return list
	 */
	public List<ImageMainColourDTO> mapToImageMainColourDTOs(final List<ImageMainColour> imageMainColour) {
		Type listType = new TypeToken<List<ImageMainColourDTO>>() {
		}.getType();
		return modelMapper.map(imageMainColour, listType);
	}

	/**
	 * Map the given ImageMainColour DTO list to a list of imageMainColours.
	 *
	 * @param imageMainColourDTOS The source list of DTOs to be mapped.
	 * @return A list of ImagemainColours.
	 */
	public List<ImageMainColour> mapToEntity(final List<ImageMainColourDTO> imageMainColourDTOS) {
		Type listType = new TypeToken<List<ImageMainColour>>() {
		}.getType();
		return modelMapper.map(imageMainColourDTOS, listType);
	}
}
