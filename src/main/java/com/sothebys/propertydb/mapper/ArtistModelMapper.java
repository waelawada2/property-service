package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.sothebys.propertydb.dto.ArtistCatalogueRaisoneeDTO;
import com.sothebys.propertydb.dto.ArtistDTO;
import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.ArtistCatalogueRaisonee;
import com.sothebys.propertydb.search.ArtistSearchDTO;

/**
 * Provides mapping methods to convert between {@link Artist} and {@link ArtistDTO}.
 *
 * @author SrinivasaRao.Batthula
 * @author Gregor Zurowski
 */
@Component
public class ArtistModelMapper extends AbstractModelMapper {

  @Autowired
  public ArtistModelMapper(ModelMapper modelMapper) {
    super(modelMapper);
  };

  /**
   * Transforms {@code Page<ENTITY>} objects into {@code Page<DTO>} objects.
   *
   * @param pageRequest The information of the requested page.
   * @param source The {@code Page<ENTITY>} object.
   * @return The created {@code Page<DTO>} object.
   */
  public Page<ArtistSearchDTO> mapEntityPageIntoDTOPageWithIdsOnly(Pageable pageRequest,
      Page<Artist> source) {
    List<ArtistSearchDTO> dtos = mapToArtistIdList(source.getContent());
    return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
  }

  public Page<ArtistDTO> mapEntityPageIntoDTOPage(Pageable pageRequest, Page<Artist> source) {
    List<ArtistDTO> dtos = this.mapToArtistDTOs(source.getContent());
    return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
  }

  /**
   * to map the ArtistId from fetch Artist list
   *
   * @param artists
   * @return
   */
  public List<ArtistSearchDTO> mapToArtistIdList(List<Artist> artists) {
    List<ArtistSearchDTO> artistdtos = new ArrayList<ArtistSearchDTO>();

    for (Artist artist : artists) {
      ArtistSearchDTO artistdto = new ArtistSearchDTO();
      artistdto.setId(artist.getExternalId());
      artistdtos.add(artistdto);
    }
    return artistdtos;
  }

  /**
   * Method for CatalogueRaisonee Entity to CatalogueRaisoneeDTO conversion
   *
   * @return catalogueRaisoneeDTO
   */
  public ArtistCatalogueRaisoneeDTO mapToDTO(ArtistCatalogueRaisonee artistCatalogueRaisonee) {
    ArtistCatalogueRaisoneeDTO artistCatalogueRaisoneeDTO =
        modelMapper.map(artistCatalogueRaisonee, ArtistCatalogueRaisoneeDTO.class);
    return artistCatalogueRaisoneeDTO;
  }

  /**
   * @param artists
   * @return list
   */
  public List<ArtistDTO> mapToArtistDTOs(final List<Artist> artists) {
    Type listType = new TypeToken<List<ArtistDTO>>() {}.getType();
    return modelMapper.map(artists, listType);
  }

  /**
   * @param artistCatalogueRaisoneeDTO
   * @return catalogueRaisonee
   * @throws ParseException
   */
  public ArtistCatalogueRaisonee mapToEntity(
      ArtistCatalogueRaisoneeDTO artistCatalogueRaisoneeDTO) {
    ArtistCatalogueRaisonee artistCatalogueRaisonee =
        modelMapper.map(artistCatalogueRaisoneeDTO, ArtistCatalogueRaisonee.class);
    return artistCatalogueRaisonee;
  }

  /**
   * Converts a list of {@link ArtistCatalogueRaisonee} to the corresponding DTO representation.
   * 
   * @param artistCatalogueRaisonee The list of catalogue raisonee entities.
   * @return A list of catalogue raisonee entities in the DTO representation.
   */
  public List<ArtistCatalogueRaisoneeDTO> mapToDTO(
      List<ArtistCatalogueRaisonee> artistCatalogueRaisonee) {
    Type listType = new TypeToken<List<ArtistCatalogueRaisoneeDTO>>() {}.getType();
    return modelMapper.map(artistCatalogueRaisonee, listType);
  }

  /**
   * Converts a list of {@link ArtistCatalogueRaisoneeDTO} to the corresponding entity
   * representation.
   * 
   * @param artistCatalogueRaisoneeDTOs A list of catalogue raisonee DTO objects.
   * @return A list of catalogue raisonee DTO objects.
   */
  public List<ArtistCatalogueRaisonee> mapToEntity(
      List<ArtistCatalogueRaisoneeDTO> artistCatalogueRaisoneeDTOs) {
    Type listType = new TypeToken<List<ArtistCatalogueRaisonee>>() {}.getType();
    return modelMapper.map(artistCatalogueRaisoneeDTOs, listType);
  }
  
  /**
   * Method for artistDTO to Entity Artist conversion
   *
   * @return Artist
   */
  public Artist mapToEntity(ArtistDTO artistDTO) {
    List<ArtistCatalogueRaisoneeDTO> artistCatalogueRaisoneeDTOs =
        artistDTO.getArtistCatalogueRaisonees();
    if (!CollectionUtils.isEmpty(artistCatalogueRaisoneeDTOs)) {
      for (int i = 0; i < artistCatalogueRaisoneeDTOs.size(); i++) {
        artistCatalogueRaisoneeDTOs.get(i).setSortId(i + 1);
      }
    }
    return modelMapper.map(artistDTO, Artist.class);
  }

}
