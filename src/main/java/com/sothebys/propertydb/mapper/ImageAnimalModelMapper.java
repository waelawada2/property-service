/**
 * 
 */
package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.ImageAnimalDTO;
import com.sothebys.propertydb.model.ImageAnimal;

/**
 * @author aneesha.l
 *
 */
@Component
public class ImageAnimalModelMapper extends AbstractModelMapper {

	@Autowired
	public ImageAnimalModelMapper(ModelMapper modelMapper) {
		super(modelMapper);
	};

	public Page<ImageAnimalDTO> mapEntityPageIntoDTOPage(Pageable pageRequest, Page<ImageAnimal> source) {
		List<ImageAnimalDTO> dtos = this.mapToImageAnimalDTOs(source.getContent());
		return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
	}

	/**
	 * @param imageAnimals
	 * @return list
	 */
	public List<ImageAnimalDTO> mapToImageAnimalDTOs(final List<ImageAnimal> imageAnimals) {
		Type listType = new TypeToken<List<ImageAnimalDTO>>() {
		}.getType();
		return modelMapper.map(imageAnimals, listType);
	}
}
