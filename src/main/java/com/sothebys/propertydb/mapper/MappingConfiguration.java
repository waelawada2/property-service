package com.sothebys.propertydb.mapper;

import com.sothebys.propertydb.dto.ArtistDTO;
import com.sothebys.propertydb.dto.PropertyDTO;
import com.sothebys.propertydb.dto.request.PropertyAddRequestDTO;
import com.sothebys.propertydb.mapper.converter.TagsToDTOConverter;
import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.Edition;
import com.sothebys.propertydb.model.EditionSize;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.search.ArtistSearchDTO;
import com.sothebys.propertydb.search.PropertySearchDTO;
import org.modelmapper.AbstractProvider;
import org.modelmapper.PropertyMap;
import org.modelmapper.Provider;

/**
 * Created by waelawada on 4/7/17.
 */
public class MappingConfiguration {

  public static final PropertyMap<Property, PropertyDTO> PROPERTY_TO_PROPERTY_DTO_PROPERTY_MAP =
      new PropertyMap<Property, PropertyDTO>() {
        protected void configure() {
          map().setId(source.getExternalId());

          // Map Edition Size
          map().setArtistProofTotal(source.getEdition().getEditionSize().getArtistProofTotal());
          map().setBonATirerTotal(source.getEdition().getEditionSize().getBonATirerTotal());
          map().setTrialProofTotal(source.getEdition().getEditionSize().getTrialProofTotal());
          map().setPrintersProofTotal(source.getEdition().getEditionSize().getPrintersProofTotal());
          map().setHorsCommerceProofTotal(
              source.getEdition().getEditionSize().getHorsCommerceProofTotal());
          map().setSeriesTotal(source.getEdition().getEditionSize().getSeriesTotal());
          map().setCancellationProofTotal(
              source.getEdition().getEditionSize().getCancellationProofTotal());
          map().setColorTrialProofTotal(
              source.getEdition().getEditionSize().getColorTrialProofTotal());
          map().setDedicatedProofTotal(
              source.getEdition().getEditionSize().getDedicatedProofTotal());
          map().setMuseumProofTotal(source.getEdition().getEditionSize().getMuseumProofTotal());
          map().setProgressiveProofTotal(
              source.getEdition().getEditionSize().getProgressiveProofTotal());
          map().setRightToProduceProofTotal(
              source.getEdition().getEditionSize().getRightToProduceProofTotal());
          map().setWorkingProofTotal(source.getEdition().getEditionSize().getWorkingProofTotal());
          map().setUnknownEditionSizeType(
              source.getEdition().getEditionSize().getUnknownEditionSizeType());
          map().setEditionSizeTotal(source.getEdition().getEditionSize().getEditionSizeTotal());
          map().setEditionSizeNotes(source.getEdition().getEditionSize().getEditionSizeNotes());
          map().setEditionOverride(source.getEdition().getEditionSize().getEditionOverride());

          // Map Edition
          map().setEditionNumber(source.getEdition().getEditionNumber());
          map().setUniqueEdition(source.getEdition().getUniqueEdition());
          map().setEditionName(source.getEdition().getEditionType().getName());
          map().setEditionSizeTypeName(source.getEdition().getEditionSizeType().getName());

          // Map Authenticity
          map().setAuthentication(source.getAuthenticity().getAuthentication());
          map().setCertificate(source.getAuthenticity().getCertificate());
          map().setSigned(source.getAuthenticity().getSigned());
          map().setStamped(source.getAuthenticity().getStamped());

          // Map Date Details
          map().setCastYearCirca(source.getDateDetails().getCastYearCirca());
          map().setCastYearEnd(source.getDateDetails().getCastYearEnd());
          map().setCastYearStart(source.getDateDetails().getCastYearStart());

          map().setYearCirca(source.getDateDetails().getYearCirca());
          map().setYearEnd(source.getDateDetails().getYearEnd());
          map().setYearStart(source.getDateDetails().getYearStart());
          map().setYearText(source.getDateDetails().getYearText());

          // Map Dimension Details
          map().setDepthCm(source.getDimensions().getDepth());
          map().setHeightCm(source.getDimensions().getHeight());
          map().setWidthCm(source.getDimensions().getWidth());
          map().setWeight(source.getDimensions().getWeight());

          // Map Reminder Notes
          map().setReminderNotesText(source.getReminderNotes().getText());
          map().setReminderEmployee(source.getReminderNotes().getReminderEmployee());

          // Map Researcher Notes
          map().setResearcherName(source.getResearcherNotes().getResearcherName());
          map().setResearcherNotesDate(source.getResearcherNotes().getDate());
          map().setResearcherNotesText(source.getResearcherNotes().getText());
          map().setResearchCompleteCheck(source.getResearcherNotes().getResearchCompleteCheck());

          // Map Flag details
          map().getFlags().setFlagAuthenticity(source.getFlags().getFlagAuthenticity());
          map().getFlags().setFlagCondition(source.getFlags().getFlagCondition());
          map().getFlags().setFlagMedium(source.getFlags().getFlagMedium());
          map().getFlags().setFlagRestitution(source.getFlags().getFlagRestitution());
          map().getFlags().setFlagSfs(source.getFlags().getFlagSfs());
          map().getFlags().setFlagsCountry(source.getFlags().getFlagsCountry().getCountryName());

          // Map Tag details
          using(new TagsToDTOConverter()).map(source.getTags()).setTags(null);

        }
      };

  public static final PropertyMap<Property, PropertySearchDTO> PROPERTY_TO_PROPERTY_SEARCH_DTO_PROPERTY_MAP =
      new PropertyMap<Property, PropertySearchDTO>() {
        protected void configure() {
          map().setId(source.getExternalId());
        }
      };

  public static final PropertyMap<PropertyAddRequestDTO, Property> PROPERTY_ADD_REQUEST_DTO_PROPERTY_TO_PROPERTY_MAP =
      new PropertyMap<PropertyAddRequestDTO, Property>() {
        protected void configure() {

          // Map Edition Size
          map().getEdition().getEditionSize().setArtistProofTotal(source.getArtistProofTotal());
          map().getEdition().getEditionSize().setBonATirerTotal(source.getBonATirerTotal());
          map().getEdition().getEditionSize().setTrialProofTotal(source.getTrialProofTotal());
          map().getEdition().getEditionSize().setPrintersProofTotal(source.getPrintersProofTotal());
          map().getEdition().getEditionSize()
              .setHorsCommerceProofTotal(source.getHorsCommerceProofTotal());
          map().getEdition().getEditionSize().setSeriesTotal(source.getSeriesTotal());
          map().getEdition().getEditionSize()
              .setCancellationProofTotal(source.getCancellationProofTotal());
          map().getEdition().getEditionSize()
              .setColorTrialProofTotal(source.getColorTrialProofTotal());
          map().getEdition().getEditionSize()
              .setDedicatedProofTotal(source.getDedicatedProofTotal());
          map().getEdition().getEditionSize().setMuseumProofTotal(source.getMuseumProofTotal());
          map().getEdition().getEditionSize()
              .setProgressiveProofTotal(source.getProgressiveProofTotal());
          map().getEdition().getEditionSize()
              .setRightToProduceProofTotal(source.getRightToProduceProofTotal());
          map().getEdition().getEditionSize().setWorkingProofTotal(source.getWorkingProofTotal());
          map().getEdition().getEditionSize().setEditionSizeNotes(source.getEditionSizeNotes());
          map().getEdition().getEditionSize().setEditionSizeTotal(source.getEditionSizeTotal());
          map().getEdition().getEditionSize().setEditionOverride(source.getEditionOverride());

          // Map Edition
          map().getEdition().setEditionNumber(source.getEditionNumber());
          map().getEdition().setUniqueEdition(source.getUniqueEdition());
          map().getEdition().getEditionType().setName(source.getEditionName());
          map().getEdition().getEditionSizeType().setName(source.getEditionSizeTypeName());

          // Map Authenticity
          map().getAuthenticity().setAuthentication(source.getAuthentication());
          map().getAuthenticity().setCertificate(source.getCertificate());
          map().getAuthenticity().setSigned(source.getSigned());
          map().getAuthenticity().setStamped(source.getStamped());

          // Map Date Details
          map().getDateDetails().setCastYearCirca(source.getCastYearCirca());
          map().getDateDetails().setCastYearEnd(source.getCastYearEnd());
          map().getDateDetails().setCastYearStart(source.getCastYearStart());

          map().getDateDetails().setYearCirca(source.getYearCirca());
          map().getDateDetails().setYearEnd(source.getYearEnd());
          map().getDateDetails().setYearStart(source.getYearStart());
          map().getDateDetails().setYearText(source.getYearText());

          // Map Dimensions
          map().getDimensions().setDepth(source.getDepthCm());
          map().getDimensions().setHeight(source.getHeightCm());
          map().getDimensions().setWidth(source.getWidthCm());
          map().getDimensions().setWeight(source.getWeight());

          // Map Reminder Notes
          map().getReminderNotes().setText(source.getReminderNotesText());
          map().getReminderNotes().setReminderEmployee(source.getReminderEmployee());

          // Map Researcher Notes
          map().getResearcherNotes().setResearcherName(source.getResearcherName());
          map().getResearcherNotes().setDate(source.getResearcherNotesDate());
          map().getResearcherNotes().setText(source.getResearcherNotesText());
          map().getResearcherNotes().setResearchCompleteCheck(source.getResearchCompleteCheck());

          // Map Flag details
          map().getFlags().setFlagAuthenticity(source.getFlags().getFlagAuthenticity());
          map().getFlags().setFlagCondition(source.getFlags().getFlagCondition());
          map().getFlags().setFlagMedium(source.getFlags().getFlagMedium());
          map().getFlags().setFlagRestitution(source.getFlags().getFlagRestitution());
          map().getFlags().setFlagSfs(source.getFlags().getFlagSfs());
          map().getFlags().getFlagsCountry().setCountryName(source.getFlags().getFlagsCountry());
        }
      };

  public static final PropertyMap<Artist, ArtistDTO> ARTIST_TO_ARTIST_DTO_PROPERTY_MAP =
      new PropertyMap<Artist, ArtistDTO>() {
        protected void configure() {
          map().setId(source.getExternalId());
        }
      };

  public static final PropertyMap<ArtistDTO, Artist> ARTIST_DTO_TO_ARTIST_PROPERTY_MAP =
      new PropertyMap<ArtistDTO, Artist>() {
        protected void configure() {
          map().setExternalId(source.getId());
        }
      };

  public static final PropertyMap<Artist, ArtistSearchDTO> ARTIST_TO_ARTIST_SEARCH_DTO_PROPERTY_MAP =
      new PropertyMap<Artist, ArtistSearchDTO>() {
        protected void configure() {
          map().setId(source.getExternalId());
        }
      };

  static Provider<Edition> editionProvider = new AbstractProvider<Edition>() {
    public Edition get() {
      Edition edition = new Edition();
      EditionSize editionSize = new EditionSize();
      edition.setEditionSize(editionSize);
      return new Edition();
    }
  };
}
