package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.ExpertiseSitterDTO;
import com.sothebys.propertydb.model.ExpertiseSitter;

/**
 * @author aneesha.l
 *
 */
@Component
public class ExpertiseSitterModelMapper extends AbstractModelMapper {

	@Autowired
	public ExpertiseSitterModelMapper(ModelMapper modelMapper) {
		super(modelMapper);
	};

	public Page<ExpertiseSitterDTO> mapEntityPageIntoDTOPage(Pageable pageRequest, Page<ExpertiseSitter> source) {
		List<ExpertiseSitterDTO> dtos = this.mapToExpertiseSitterDTOs(source.getContent());
		return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
	}

	/**
	 * @param expertiseSitters
	 * @return list
	 */
	public List<ExpertiseSitterDTO> mapToExpertiseSitterDTOs(final List<ExpertiseSitter> expertiseSitters) {
		Type listType = new TypeToken<List<ExpertiseSitterDTO>>() {
		}.getType();
		
		List<ExpertiseSitterDTO> expertiseSitterDTOs = modelMapper.map(expertiseSitters, listType);
		for(ExpertiseSitterDTO expertiseSitterDTO : expertiseSitterDTOs) {
			String firstName = expertiseSitterDTO.getFirstName() != null ? expertiseSitterDTO.getFirstName() : "";
			String lastName = expertiseSitterDTO.getLastName() != null ? expertiseSitterDTO.getLastName() : "";
			String displayName = firstName + " " + lastName;
			expertiseSitterDTO.setDisplayName(displayName.trim());
		}
		return expertiseSitterDTOs;
	}

	/**
	 * Map the given ExpertiseSitter DTO list to a list of ExpertiseSitters.
	 *
	 * @param expertiseSitterDTOS The source list of DTOs to be mapped.
	 * @return A list of ExpertiseSitters.
	 */
	public List<ExpertiseSitter> mapToEntity(final List<ExpertiseSitterDTO> expertiseSitterDTOS) {
		Type listType = new TypeToken<List<ExpertiseSitter>>() {
		}.getType();
		return modelMapper.map(expertiseSitterDTOS, listType);
	}
}
