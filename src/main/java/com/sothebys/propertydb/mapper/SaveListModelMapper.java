package com.sothebys.propertydb.mapper;

import com.sothebys.propertydb.dto.PropertyDTO;
import com.sothebys.propertydb.dto.SaveListDTO;
import com.sothebys.propertydb.dto.SaveListPropertyDTO;
import com.sothebys.propertydb.dto.request.SaveListRequestDTO;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.SaveList;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.service.PropertyService;
import com.sothebys.propertydb.service.SaveListService;
import com.sothebys.propertydb.service.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

/**
 * Provides mapping methods to convert between {@link SaveList} and {@link SaveListDTO}.
 *
 * @author SrinivasaRao Batthula
 */
@Slf4j
@Component
public class SaveListModelMapper extends AbstractModelMapper {

  PropertyService propertyService;

  UserService userService;

  SaveListService saveListService;

  @Autowired
  public SaveListModelMapper(ModelMapper modelMapper, PropertyService propertyService,
      UserService userService, SaveListService saveListService) {
    super(modelMapper);
    this.propertyService = propertyService;
    this.userService = userService;
    this.saveListService = saveListService;

  }

  /**
   * Method for SaveList to SaveListDTO conversion
   *
   * @param saveListObject The instance of {@code SaveList} to be mapped
   * @return A saveListDTO object populated with the the values passed by the SaveList Object
   */
  public SaveListDTO mapToDTO(SaveList saveListObject) {
    SaveListDTO saveListDTO = modelMapper.map(saveListObject, SaveListDTO.class);
    saveListDTO.setId(saveListObject.getExternalId());

    final List<String> propertyIds = Optional.ofNullable(saveListObject.getProperties())
        .orElseGet(Collections::emptyList).stream()
        .map(Property::getExternalId)
        .collect(Collectors.toList());
    saveListDTO.setPropertyIds(propertyIds);

    if (!CollectionUtils.isEmpty(saveListObject.getSharedUsers())) {
      List<String> userNames = new ArrayList<String>();
      for (User userObj : saveListObject.getSharedUsers()) {
        userNames.add(userObj.getUserName());
      }
      saveListDTO.setSharedUsersList(userNames);
    }
    return saveListDTO;
  }

  /**
   * Maps a {@code saveListRequestDTO} to a SaveList entity
   *
   * @param saveListRequestDTO refers to the {@code saveListRequestDTO} object to be mapped
   * @return A saveList object populated with the the values passed by the DTO
   */
  public SaveList mapToEntity(SaveListRequestDTO saveListRequestDTO) {
    
    SaveList saveListObject = map(saveListRequestDTO, SaveList.class);
    List<Property> properties = new ArrayList<>();
    if (!CollectionUtils.isEmpty(saveListRequestDTO.getPropertyIds())) {
      for (String propertyId : saveListRequestDTO.getPropertyIds()) {
        Property property = propertyService.getPropertyByExternalId(propertyId);
        properties.add(property);
      }
      saveListObject.setProperties(properties);
    }
    if (!CollectionUtils.isEmpty(saveListRequestDTO.getSharedUsersList())) {
      List<User> sharedUsers = new ArrayList<>();
      for (String sharedUserName : saveListRequestDTO.getSharedUsersList()) {
        User sharedUserObj = userService.findByUsername(sharedUserName);
        sharedUsers.add(sharedUserObj);
      }
      saveListObject.setSharedUsers(sharedUsers);
    }
    if (StringUtils.isNotBlank(saveListRequestDTO.getUser())) {
      saveListObject.setUser(userService.findByUsername(saveListRequestDTO.getUser()));
    }
    return saveListObject;
  }


  /**
   * Transforms {@code Page<ENTITY>} objects into {@code Page<DTO>} objects.
   *
   * @param pageRequest The information of the requested page.
   * @param source The {@code Page<ENTITY>} object.
   * @return The created {@code Page<DTO>} object.
   */
  public Page<SaveListDTO> mapEntityPageIntoDTOPageWithIdsOnly(Pageable pageRequest,
      Page<SaveList> source) {
    List<SaveListDTO> dtos = mapToSaveListIdList(source.getContent());
    return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
  }

  public Page<SaveListDTO> mapEntityPageIntoDTOPage(Pageable pageRequest, Page<SaveList> source) {
    List<SaveListDTO> dtos = this.mapToSaveListDTOsWithResearchStatus(source.getContent());
    return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
  }

  /**
   * Retrieves the research complete status of each {@code SaveList}
   *
   * @param dtos
   * @return
   */
  private List<SaveListDTO> mapResearchStatus(List<SaveListDTO> dtos){
    dtos.stream().forEach(dto -> {
      dto.setSaveListResearchCompleteStatus(saveListService.getReseachStatus(dto.getId()));
    });
    return dtos;
  }

  /**
   * to map the SaveListId from fetch saveList object list
   *
   * @param saveLists refers to the list of SaveList Objects to be converted to DTO
   * @return list refers to the list of SaveListDTO populated with the Ids from List of Save List
   *     Objects
   */
  public List<SaveListDTO> mapToSaveListIdList(List<SaveList> saveLists) {
    List<SaveListDTO> saveListdtos = new ArrayList<SaveListDTO>();
    for (SaveList saveList : saveLists) {
      SaveListDTO saveListdto = new SaveListDTO();
      saveListdto.setId(saveList.getExternalId());
      saveListdtos.add(saveListdto);
    }
    return saveListdtos;
  }

  /**
   * to map the SaveListId from fetch saveList object list
   *
   * @param saveLists refers to the list of SaveList Objects to be converted to DTO
   * @return list refers to the list of SaveListDTO mapped with the values from List of Save List
   *     Objects
   */
  public List<SaveListDTO> mapToSaveListDTOs(List<SaveList> saveLists) {
    List<SaveListDTO> saveListdtos = new ArrayList<SaveListDTO>();

    for (SaveList saveList : saveLists) {
      SaveListDTO saveListDto = mapToSaveListDTO(saveList);
      if (!CollectionUtils.isEmpty(saveListDto.getPropertyIds())) {
        saveListdtos.add(saveListDto);
      }
    }

    return saveListdtos;
  }

  /**
   * Method for SaveList to SaveListDTO conversion
   *
   * @param saveListObject The instance of {@code SaveList} to be mapped
   * @return A SaveListDTO object populated with SaveList object values
   */
  public SaveListDTO mapToSaveListDTO(SaveList saveListObject) {
    SaveListDTO saveListDTO = modelMapper.map(saveListObject, SaveListDTO.class);
    saveListDTO.setId(saveListObject.getExternalId());
    if (!CollectionUtils.isEmpty(saveListObject.getSharedUsers())) {
      List<String> userNames = new ArrayList<String>();
      for (User userObj : saveListObject.getSharedUsers()) {
        userNames.add(userObj.getUserName());
      }
      saveListDTO.setSharedUsersList(userNames);
    }
    List<Property> propertiesList = saveListObject.getProperties();
    if (!CollectionUtils.isEmpty(propertiesList)) {
      saveListDTO.setSaveListCompleteStatus(propertiesList.stream().filter(Objects::nonNull)
          .allMatch(p -> p.getResearcherNotes() != null
              && p.getResearcherNotes().getResearchCompleteCheck() != null
              && p.getResearcherNotes().getResearchCompleteCheck()));
      List<String> propertiesIdsList = new ArrayList<String>();
      for (Property propertyobj : propertiesList) {
        propertiesIdsList.add(propertyobj.getExternalId());
      }
      saveListDTO.setPropertyIds(propertiesIdsList);
    }
    return saveListDTO;
  }

  /**
   * Method for SaveList to SaveListDTO conversion
   *
   * @param saveListObject refers to the SaveList object to be converted
   * @return SaveListPropertyDTO populated with Save List Object values
   */
  public SaveListPropertyDTO mapToSavelistPropertyDTO(SaveList saveListObject) {
    SaveListPropertyDTO saveListPropertyDTO =
        modelMapper.map(saveListObject, SaveListPropertyDTO.class);
    saveListPropertyDTO.setId(saveListObject.getExternalId());
    if (!CollectionUtils.isEmpty(saveListObject.getProperties())) {
      List<PropertyDTO> properties = new ArrayList<PropertyDTO>();
      for (Property propertyObj : saveListObject.getProperties()) {
        try {
          propertyService.getPropertyByExternalId(propertyObj.getExternalId());
          properties.add(modelMapper.map(propertyObj, PropertyDTO.class));
        } catch (NotFoundException ex) {
          log.error("Property doesn't exist or inactive for saveList propertyId={} ",
              propertyObj.getExternalId(), ex);
          continue;
        }
      }
      saveListPropertyDTO.setProperties(properties);
    }
    return saveListPropertyDTO;
  }
  
  /**
   * Method for list of SaveList to list of SaveListDTO conversion
   * 
   * @param allSaveLists list of saveLists
   * @return SaveListDTO list populated with SaveList DTO Object values
   */
  public List<SaveListDTO> mapToSaveListDtoList(List<SaveList> allSaveLists) {
    List<SaveListDTO> saveListDtoList = new ArrayList<SaveListDTO>();
    for (SaveList saveListObj : allSaveLists) {
      saveListDtoList.add(mapToSaveListDTO(saveListObj));
    }
    return saveListDtoList;
  }
  
  /**
   * to map the SaveListId from fetch saveList object list
   *
   * @param saveLists refers to the list of SaveList Objects to be converted to DTO
   * @return list refers to the list of SaveListDTO mapped with the values from List of Save List
   *     Objects
   */
  public SaveListDTO mapToSaveListDTOs(SaveList saveListObject) {
    SaveListDTO saveListDTO = modelMapper.map(saveListObject, SaveListDTO.class);
    saveListDTO.setId(saveListObject.getExternalId());
    if (!CollectionUtils.isEmpty(saveListObject.getSharedUsers())) {
      List<String> userNames = new ArrayList<String>();
      for (User userObj : saveListObject.getSharedUsers()) {
        userNames.add(userObj.getUserName());
      }
      saveListDTO.setSharedUsersList(userNames);
    }

    List<Property> propertiesList = saveListObject.getProperties();
    if (!CollectionUtils.isEmpty(propertiesList)) {
      List<String> propertiesIdsList = new ArrayList<String>();
      for (Property propertyobj : propertiesList) {
        propertiesIdsList.add(propertyobj.getExternalId());
      }
      saveListDTO.setPropertyIds(propertiesIdsList);
    }
    return saveListDTO;
  }
  
  public Page<SaveListDTO> mapEntityPageIntoDTOPageWithOutPropertyIds(Pageable pageRequest,
      Page<SaveList> source) {
    List<SaveListDTO> dtos = this.mapToSaveListDTOsWithOutPropertyIds(source.getContent());
    return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
  }

  /**
   * to map the SaveListId from fetch saveList object list
   *
   * @param saveLists refers to the list of SaveList Objects to be converted to DTO
   * @return list refers to the list of SaveListDTO mapped with the values from List of Save List
   *         Objects
   */
  private List<SaveListDTO> mapToSaveListDTOsWithOutPropertyIds(List<SaveList> saveLists) {
    List<SaveListDTO> saveListdtos = new ArrayList<SaveListDTO>();

    for (SaveList saveListObject : saveLists) {
      SaveListDTO saveListDTO = modelMapper.map(saveListObject, SaveListDTO.class);
      saveListDTO.setId(saveListObject.getExternalId());
      if (!CollectionUtils.isEmpty(saveListObject.getSharedUsers())) {
        List<String> userNames = new ArrayList<String>();
        for (User userObj : saveListObject.getSharedUsers()) {
          userNames.add(userObj.getUserName());
        }
        saveListDTO.setSharedUsersList(userNames);
      }
      saveListdtos.add(saveListDTO);
    }
    return saveListdtos;
  }
  
  /**
   * to map the SaveListId from fetch saveList object list
   *
   * @param saveLists refers to the list of SaveList Objects to be converted to DTO
   * @return list refers to the list of SaveListDTO mapped with the values from List of Save List
   *         Objects
   */
  public List<SaveListDTO> mapToSaveListDTOsWithResearchStatus(List<SaveList> saveLists) {
    List<SaveListDTO> saveListdtos = new ArrayList<SaveListDTO>();
    for (SaveList saveList : saveLists) {
      SaveListDTO saveListDto = mapToSaveListDTOsWithResearchStatusCheck(saveList);
      saveListdtos.add(saveListDto);
    }
    return saveListdtos;
  }

  /**
   * Method for SaveList to SaveListDTO conversion
   *
   * @param saveListObject The instance of {@code SaveList} to be mapped
   * @return A SaveListDTO object populated with SaveList object values
   */
 private SaveListDTO mapToSaveListDTOsWithResearchStatusCheck(SaveList saveListObject) {
    SaveListDTO saveListDTO = modelMapper.map(saveListObject, SaveListDTO.class);
    saveListDTO.setId(saveListObject.getExternalId());
    if (!CollectionUtils.isEmpty(saveListObject.getSharedUsers())) {
      List<String> userNames = new ArrayList<String>();
      for (User userObj : saveListObject.getSharedUsers()) {
        userNames.add(userObj.getUserName());
      }
      saveListDTO.setSharedUsersList(userNames);
    }
    saveListDTO.setSaveListResearchCompleteStatus(
        saveListService.findSaveListResearchStatusById(saveListDTO.getId()));
    return saveListDTO;
  }
}
