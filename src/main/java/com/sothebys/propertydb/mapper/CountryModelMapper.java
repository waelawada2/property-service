package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.CountryDTO;
import com.sothebys.propertydb.model.Country;

/**
 * Provides mapping methods to convert between {@link Country} and {@link CountryDTO}.
 *
 * @author SrinivasaRao Batthula
 */
@Component
public class CountryModelMapper extends AbstractModelMapper {

  @Autowired
  public CountryModelMapper(ModelMapper modelMapper) {
    super(modelMapper);
  }

  /**
   * Map the given country entity list to a list of country DTOs.
   *
   * @param country The source list of entities to be mapped.
   * @return A list of country DTOs.
   */
  public List<CountryDTO> mapToDTO(final List<Country> countries) {
    Type listType = new TypeToken<List<CountryDTO>>() {}.getType();
    return modelMapper.map(countries, listType);
  }

  /**
   * Map the given country DTO list to a list of countries.
   *
   * @param countryDTOs The source list of DTOs to be mapped.
   * @return A list of countries.
   */
  public List<Country> mapToEntity(final List<CountryDTO> countryDTOs) {
    Type listType = new TypeToken<List<Country>>() {}.getType();
    return modelMapper.map(countryDTOs, listType);
  }

}
