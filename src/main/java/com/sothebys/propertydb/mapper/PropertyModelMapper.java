package com.sothebys.propertydb.mapper;

import static org.apache.commons.lang3.StringUtils.defaultIfBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.sothebys.propertydb.dto.CategoryDTO;
import com.sothebys.propertydb.dto.ExpertiseSitterDTO;
import com.sothebys.propertydb.dto.PropertyDTO;
import com.sothebys.propertydb.dto.request.PropertyAddRequestCatalogueRaisoneeDTO;
import com.sothebys.propertydb.dto.request.PropertyAddRequestDTO;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.exception.PropertyServiceException;
import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.ArtistCatalogueRaisonee;
import com.sothebys.propertydb.model.Category;
import com.sothebys.propertydb.model.Country;
import com.sothebys.propertydb.model.EditionSizeType;
import com.sothebys.propertydb.model.EditionType;
import com.sothebys.propertydb.model.ExpertiseSitter;
import com.sothebys.propertydb.model.ImageAnimal;
import com.sothebys.propertydb.model.ImageFigure;
import com.sothebys.propertydb.model.ImageGender;
import com.sothebys.propertydb.model.ImageMainColour;
import com.sothebys.propertydb.model.ImagePosition;
import com.sothebys.propertydb.model.ImageSubject;
import com.sothebys.propertydb.model.Orientation;
import com.sothebys.propertydb.model.OtherTag;
import com.sothebys.propertydb.model.PresizeType;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.PropertyCatalogueRaisonee;
import com.sothebys.propertydb.model.Scale;
import com.sothebys.propertydb.model.Tags;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.search.PropertySearchDTO;
import com.sothebys.propertydb.service.ArtistCatalogueRaisoneeService;
import com.sothebys.propertydb.service.ArtistService;
import com.sothebys.propertydb.service.CategoryService;
import com.sothebys.propertydb.service.CountryService;
import com.sothebys.propertydb.service.EditionSizeTypeService;
import com.sothebys.propertydb.service.EditionTypeService;
import com.sothebys.propertydb.service.ExpertiseSitterService;
import com.sothebys.propertydb.service.ImageAnimalService;
import com.sothebys.propertydb.service.ImageFigureService;
import com.sothebys.propertydb.service.ImageGenderService;
import com.sothebys.propertydb.service.ImageMainColourService;
import com.sothebys.propertydb.service.ImagePositionService;
import com.sothebys.propertydb.service.ImageSubjectService;
import com.sothebys.propertydb.service.OtherTagService;
import com.sothebys.propertydb.service.PresizeTypeService;
import com.sothebys.propertydb.service.UserService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

/**
 * Provides mapping code for {@link Property} objects.
 */
@Slf4j
@Component
public class PropertyModelMapper extends AbstractModelMapper {

  private ArtistCatalogueRaisoneeService artistCatalogueRaisoneeService;

  private ArtistService artistService;

  private CategoryService categoryService;

  private EditionSizeTypeService editionSizeTypeService;

  private EditionTypeService editionTypeService;

  private PresizeTypeService presizeTypeService;

  private CountryService countryService;

  private ImageFigureService imageFigureService;

  private ImageSubjectService imageSubjectService;

  private ImageGenderService imageGenderService;

  private ImagePositionService imagePositionService;

  private ImageAnimalService imageAnimalService;

  private ImageMainColourService imageMainColourService;

  private ExpertiseSitterService expertiseSitterService;

  private OtherTagService otherTagService;
  
  private UserService userService;  
  

  @Autowired
  public PropertyModelMapper(ModelMapper modelMapper, ArtistService artistService,
      CategoryService categoryService, EditionTypeService editionTypeService,
      EditionSizeTypeService editionSizeTypeService, PresizeTypeService presizeTypeService,
      ArtistCatalogueRaisoneeService artistCatalogueRaisoneeService, CountryService countryService,
      ImageFigureService imageFigureService, ImageSubjectService imageSubjectService,
      ImageGenderService imageGenderService, ImagePositionService imagePositionService,
      ImageAnimalService imageAnimalService, ImageMainColourService imageMainColourService,
      ExpertiseSitterService expertiseSitterService, OtherTagService otherTagService,
      UserService userService) {
    super(modelMapper);
    this.artistCatalogueRaisoneeService = artistCatalogueRaisoneeService;
    this.artistService = artistService;
    this.categoryService = categoryService;
    this.editionTypeService = editionTypeService;
    this.editionSizeTypeService = editionSizeTypeService;
    this.presizeTypeService = presizeTypeService;
    this.countryService = countryService;
    this.imageFigureService = imageFigureService;
    this.imageSubjectService = imageSubjectService;
    this.imageGenderService = imageGenderService;
    this.imagePositionService = imagePositionService;
    this.imageAnimalService = imageAnimalService;
    this.imageMainColourService = imageMainColourService;
    this.expertiseSitterService = expertiseSitterService;
    this.otherTagService = otherTagService;
    this.userService = userService;
  }

  /**
   * Maps the EntityPage into PropertyDTO
   *
   * @param pageRequest
   * @param source
   * @return
   */
  public Page<PropertyDTO> mapEntityPageIntoDTOPage(Pageable pageRequest, Page<Property> source) {
    List<PropertyDTO> dtos = this.mapToDTO(source.getContent());
    return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
  }

  /**
   * Transforms {@code Page<Property>} objects into {@code Page<PropertySearchDTO>} objects.
   *
   * @param pageRequest The information of the requested page.
   * @param source The {@code Page<Property>} object.
   * @return The created {@code Page<PropertySearchDTO>} object.
   */
  public Page<PropertySearchDTO> mapEntityPageIntoDTOPageWithIdsOnly(Pageable pageRequest,
      Page<Property> source) {
    List<PropertySearchDTO> dtos = mapToPropertyIdList(source.getContent());
    return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
  }

  /**
   * Map a list of {@link Property} into a list of {@link PropertyDTO}.
   *
   * @param properties The list of {@link Property}.
   * @return A list of {@link PropertyDTO}.
   */
  public List<PropertyDTO> mapToDTO(final List<Property> properties) {
    Objects.requireNonNull(properties, "Properties list must not be null");
    log.debug("Mapping propertyListToDto");
    return properties.stream().map(this::mapToDTO).collect(Collectors.toList());
  }

  /**
   * Method for Artist Property to PropertyDTO conversion
   */
  public PropertyDTO mapToDTO(Property propertyObject) {
    CategoryDTO categoryDto = null;
    PropertyDTO propertyDTO = modelMapper.map(propertyObject, PropertyDTO.class);
    Category category = propertyObject.getCategory();
    if (category != null) {
      categoryDto = modelMapper.map(category, CategoryDTO.class);
      if (category.getParent() == null) {
        propertyDTO.setParentCategory(categoryDto);
      } else {
        categoryDto = modelMapper.map(category.getParent(), CategoryDTO.class);
        CategoryDTO subcategoryDTO = new CategoryDTO();
        propertyDTO.setParentCategory(categoryDto);
        subcategoryDTO.setId(category.getId());
        subcategoryDTO.setName(category.getName());
        subcategoryDTO.setDescription(category.getDescription());
        propertyDTO.setSubCategory(subcategoryDTO);
      }
    }
    return propertyDTO;
  }
  
  /**
   * Maps a {@code propertyAddRequestDTO} to a Property entity
   *
   * @return A property object populated with the the values passed by the DTO
   */
  public Property mapToEntity(PropertyAddRequestDTO propertyAddRequestDTO) {
	  List<String> errorMeassgesList = new ArrayList<>();
    if (!CollectionUtils.isEmpty(propertyAddRequestDTO.getArchiveNumbers())) {
      propertyAddRequestDTO.setArchiveNumbers(propertyAddRequestDTO.getArchiveNumbers().stream()
          .distinct().collect(Collectors.toList()));
    }
    if (!CollectionUtils.isEmpty(propertyAddRequestDTO.getObjectCodes())) {
      propertyAddRequestDTO.setObjectCodes(propertyAddRequestDTO.getObjectCodes().stream()
          .distinct().collect(Collectors.toList()));
    }
    // Filtering CatalogueRaisonees for empty Number and Volume vlaues
    propertyAddRequestDTO = mapCatalogueRaisonees(propertyAddRequestDTO);

    Property propertyObject = map(propertyAddRequestDTO, Property.class);

    if (null != propertyAddRequestDTO.getCategory()) {
      String parentCategoryName;
      String categoryName;
      if (isNotBlank(propertyAddRequestDTO.getSubCategory())) {
        categoryName = propertyAddRequestDTO.getSubCategory();
        parentCategoryName = propertyAddRequestDTO.getCategory();
      } else {
        categoryName = propertyAddRequestDTO.getCategory();
        parentCategoryName = null;
      }
      Category category =
          categoryService.findByCategoryNameAndParentCategoryName(categoryName, parentCategoryName);
      if (category == null) {
    	  errorMeassgesList.add("Specified category doesn't exist");
      } else if (isNotBlank(propertyAddRequestDTO.getSubCategory())) {
        if (!category.getName().equalsIgnoreCase(categoryName)
            || !category.getParent().getName().equalsIgnoreCase(parentCategoryName)) {
        	errorMeassgesList.add(String.format("Specified category %s and sub-category %s don't match",
                    parentCategoryName, categoryName));         
        }
      } else if (!category.getName().equalsIgnoreCase(categoryName)) {
    	  errorMeassgesList.add(String.format("Specified category %s don't match", categoryName));
        
      }
      propertyObject.setCategory(category);
    }

    if (null != propertyAddRequestDTO.getEditionName()) {
      EditionType editionType =
          editionTypeService.getEditionTypeByName(propertyAddRequestDTO.getEditionName());
      if (editionType == null
          || !editionType.getName().equalsIgnoreCase(propertyAddRequestDTO.getEditionName())) {
    	  errorMeassgesList.add(String.format("Specified Edition Name %s doesn't exist",
    	                    propertyAddRequestDTO.getEditionName()));        
      }
      propertyObject.getEdition().setEditionType(editionType);
    }

    if (null != propertyAddRequestDTO.getEditionSizeTypeName()) {
      EditionSizeType editionSizeType = editionSizeTypeService
          .findEditionSizeTypeByName(propertyAddRequestDTO.getEditionSizeTypeName());
      if (editionSizeType == null || !editionSizeType.getName()
          .equalsIgnoreCase(propertyAddRequestDTO.getEditionSizeTypeName())) {
    	  errorMeassgesList.add(String.format(
    	            "Specified EditionSizeType Name %s doesn't exist",
    	            propertyAddRequestDTO.getEditionSizeTypeName()));
       
      }
      propertyObject.getEdition().setEditionSizeType(editionSizeType);
    }

    if (null != propertyAddRequestDTO.getPresizeName()) {
      PresizeType presizeType =
          presizeTypeService.getPresizeTypeByName(propertyAddRequestDTO.getPresizeName());
      if (presizeType == null
          || !presizeType.getName().equalsIgnoreCase(propertyAddRequestDTO.getPresizeName())) {
    	  errorMeassgesList.add(String.format("Specified Pre-Size type %s doesn't exist",
                  propertyAddRequestDTO.getPresizeName()));       
      }
      propertyObject.setPresizeType(presizeType);
    }

    if (null != propertyAddRequestDTO.getFlags()
        && null != propertyAddRequestDTO.getFlags().getFlagsCountry()) {
      String countryName = propertyAddRequestDTO.getFlags().getFlagsCountry();
      Country flagsCountry = countryService.findByCountryName(countryName);
      if (flagsCountry == null || !flagsCountry.getCountryName().equalsIgnoreCase(countryName)) {
    	  errorMeassgesList.add(String.format("Specified country name %s doesn't exist", countryName));       
      }
      propertyObject.getFlags().setFlagsCountry(flagsCountry);
    }
    
    if (isNotBlank(propertyAddRequestDTO.getResearcherName())) {
      User userObj = userService.findByUsername(propertyAddRequestDTO.getResearcherName());
      if (userObj == null) {
    	  errorMeassgesList.add(String.format("Specified Researcher name '%s' doesn't exist",
                  propertyAddRequestDTO.getResearcherName()));
      }
      propertyObject.getResearcherNotes()
          .setResearcherName(propertyAddRequestDTO.getResearcherName());
    }

    mapArtistDetails(propertyAddRequestDTO, propertyObject, errorMeassgesList);

    if (!CollectionUtils.isEmpty(propertyObject.getPropertyCatalogueRaisonees())) {
      for (PropertyCatalogueRaisonee propertyCatalogueRaisonee : propertyObject
          .getPropertyCatalogueRaisonees()) {
        if (null != propertyCatalogueRaisonee.getArtistCatalogueRaisonee()) {
          ArtistCatalogueRaisonee artistCatalogueRaisonee =
              artistCatalogueRaisoneeService.findArtistCatalogueRaisoneeById(
                  propertyCatalogueRaisonee.getArtistCatalogueRaisonee().getId());
          propertyCatalogueRaisonee.setArtistCatalogueRaisonee(artistCatalogueRaisonee);
        }
      }
    }

    propertyObject.setImagePath(defaultIfBlank(propertyAddRequestDTO.getPropertyImage(), null));
    editionSizeTypeService.calculateEditionSize(propertyObject.getEdition());
    mapTags(propertyAddRequestDTO, propertyObject, errorMeassgesList);

    if (!errorMeassgesList.isEmpty()) {
      StringBuffer strBufErrorMsgLst = new StringBuffer();
      strBufErrorMsgLst.append("Unable to insert property. ");
      for (String error : errorMeassgesList) {
        strBufErrorMsgLst.append(error).append(" | ");
      }
      throw new PropertyServiceException(isNotBlank(strBufErrorMsgLst) ? strBufErrorMsgLst
          .substring(0, strBufErrorMsgLst.length() - 2) : "");
    }
    return propertyObject;
  }

  /**
   * Maps CatalogueRaisonees to PropertyAddRequestDTO,
   * Filters CatalogueRaisonees for null entries of Number and Volume
   */
  PropertyAddRequestDTO mapCatalogueRaisonees(PropertyAddRequestDTO propertyAddRequestDTO) {
    if (!CollectionUtils.isEmpty(propertyAddRequestDTO.getPropertyCatalogueRaisonees())) {
      List<PropertyAddRequestCatalogueRaisoneeDTO> propertyCatalogueRaisoneeDTOList =
          new ArrayList<>();
      for (PropertyAddRequestCatalogueRaisoneeDTO propertyCatalogueRaisoneeDTO : propertyAddRequestDTO
          .getPropertyCatalogueRaisonees()) {
        if (propertyCatalogueRaisoneeDTO != null
            && (isNotBlank(propertyCatalogueRaisoneeDTO.getNumber())
            || isNotBlank(propertyCatalogueRaisoneeDTO.getVolume()))) {
          propertyCatalogueRaisoneeDTOList.add(propertyCatalogueRaisoneeDTO);
        }
      }
      propertyAddRequestDTO.setPropertyCatalogueRaisonees(propertyCatalogueRaisoneeDTOList);
    }
    return propertyAddRequestDTO;
  }

  /**
   * Maps the artist details from the given {@link PropertyAddRequestDTO} into the {@link Property}
   * instance. <br/> <b>Note:</b> The list of artist IDs in the DTO has preference over the list of
   * ULAN Ids.
   *
   * @param dto The DTO object that contains a list of artist IDs or ULAND IDs.
   * @param property The target property object that will be updated.
   */
  private void mapArtistDetails(PropertyAddRequestDTO dto, Property property, List<String> errorMeassgesList) {
    List<Artist> artists = new ArrayList<>();

    if (!CollectionUtils.isEmpty(dto.getArtistIds())) {
      dto.getArtistIds().forEach(artistId -> {
        Artist artist = null;
        try {
          artist = artistService.findArtistByExternalId(artistId);
        } catch(NotFoundException ex) {
          artist = null;
        }
        if (artist == null) {
        	errorMeassgesList.add(String.format("Artist with ID=%s does not exist", artistId));         
        }
        artists.add(artist);
      });
    } else if (!CollectionUtils.isEmpty(dto.getUlanIds())) {
      dto.getUlanIds().stream().forEach(ulandId -> {
        Artist artist = artistService.findArtistByUlanId(ulandId);
        if (artist == null) {
        	errorMeassgesList.add(String.format("Artist with ULAN ID=%s does not exist", ulandId));         
        }
        artists.add(artist);
      });
    }

    property.setArtists(artists);
  }


  /**
   * to map the PropertyId from fetch Property list
   *
   * @return propertydtos
   */
  public List<PropertySearchDTO> mapToPropertyIdList(List<Property> properties) {
    List<PropertySearchDTO> propertydtos = new ArrayList<PropertySearchDTO>();
    for (Property property : properties) {
      PropertySearchDTO propertydto = new PropertySearchDTO();
      propertydto.setId(property.getExternalId());
      propertydtos.add(propertydto);
    }
    return propertydtos;
  }

  /**
   * Maps the tags details from the given {@link PropertyAddRequestDTO} into the {@link Property}
   * instance.
   *
   * @param dto The DTO object
   * @param property The target property object that will be updated.
   */
  private void mapTags(PropertyAddRequestDTO dto, Property property, List<String> errorMeassgesList) {
    if (property.getTags() == null) {
      property.setTags(new Tags());
    }

    if (dto.getTags() != null) {
      if (dto.getTags().getExpertiseLocation() != null
          && dto.getTags().getExpertiseLocation().trim().length() > 250) {
        errorMeassgesList
            .add(String.format("Expertise Location \"%s\" exceeds maximum limit of 250 characters",
                dto.getTags().getExpertiseLocation().trim()));
      }
      
      if (dto.getTags().getImageFigure() != null) {
        ImageFigure imageFigure = imageFigureService.findByName(dto.getTags().getImageFigure());
        property.getTags().setImageFigure(imageFigure);
      }

      List<ImageSubject> imageSubjects = Optional.ofNullable(dto.getTags().getImageSubjects())
          .orElseGet(Collections::emptyList)
          .stream()
          .map(imageSubjectService::findByName)
          .collect(Collectors.toList());
      property.getTags().setImageSubjects(imageSubjects);

      List<ImageGender> imageGenders = Optional.ofNullable(dto.getTags().getImageGenders())
          .orElseGet(Collections::emptyList)
          .stream()
          .map(imageGenderService::findByName)
          .collect(Collectors.toList());
      property.getTags().setImageGenders(imageGenders);

      List<ImagePosition> imagePositions = Optional.ofNullable(dto.getTags().getImagePositions())
          .orElseGet(Collections::emptyList)
          .stream()
          .map(imagePositionService::findByName)
          .collect(Collectors.toList());
      property.getTags().setImagePositions(imagePositions);

      List<ImageMainColour> imageMainColours = Optional
          .ofNullable(dto.getTags().getImageMainColours())
          .orElseGet(Collections::emptyList)
          .stream()
          .map(imageMainColourService::findByName)
          .collect(Collectors.toList());
      property.getTags().setImageMainColours(imageMainColours);

      List<ImageAnimal> imageAnimals = Optional.ofNullable(dto.getTags().getImageAnimals())
          .orElseGet(Collections::emptyList)
          .stream()
          .map(imageAnimalService::findByNameCreateIfNotFound)
          .collect(Collectors.toList());
      property.getTags().setImageAnimals(imageAnimals);

      List<ExpertiseSitterDTO> expertiseSittersDTO = Optional
          .ofNullable(dto.getTags().getExpertiseSitters()).orElseGet(Collections::emptyList);

      List<ExpertiseSitter> expertiseSitters = null;
      List<Long> expertiseSitterIdList =
          expertiseSittersDTO.stream().filter(sitterDTO -> sitterDTO.getId() != null)
              .map(ExpertiseSitterDTO::getId).collect(Collectors.toList());

      List<String> expertiseSitterNameList =
          expertiseSittersDTO.stream().filter(sitterDTO -> sitterDTO.getDisplayName() != null)
              .distinct().map(ExpertiseSitterDTO::getDisplayName).collect(Collectors.toList());

      if (expertiseSitterIdList != null && !expertiseSitterIdList.isEmpty()) {
        expertiseSitters = expertiseSitterIdList.stream()
            .map(expertiseSitterService::findExpertiseSitterById).collect(Collectors.toList());
      } else if (expertiseSitterNameList != null && !expertiseSitterNameList.isEmpty()) {
        expertiseSitters = expertiseSitterNameList.stream().map(expertiseSitterService::findByName)
            .collect(Collectors.toList());
      }
      property.getTags().setExpertiseSitters(expertiseSitters);

      List<OtherTag> otherTags = Optional.ofNullable(dto.getTags().getOtherTags())
          .orElseGet(Collections::emptyList)
          .stream()
          .map(otherTagService::findByNameCreateIfNotFound)
          .collect(Collectors.toList());
      property.getTags().setOtherTags(otherTags);
    }

    property.getTags()
        .setAutoOrientation(calculateOrientationTag(dto.getHeightCm(), dto.getWidthCm()));
    property.getTags().setAutoImage(isNotBlank(dto.getImagePath()));
    property.getTags().setAutoScale(calculateScaleTag(dto.getHeightCm(), dto.getWidthCm()));
    property.getTags()
        .setAutoVolume(calculateVolumeTag(dto.getHeightCm(), dto.getWidthCm(), dto.getDepthCm()));
  }

  private Orientation calculateOrientationTag(BigDecimal height, BigDecimal width) {
    if (height == null || width == null) {
      return null;
    }

    Orientation orientation;
    if (height.compareTo(width) > 0) {
      orientation = Orientation.VERTICAL;
    } else if (height.compareTo(width) < 0) {
      orientation = Orientation.HORIZONTAL;
    } else {
      orientation = Orientation.EQUAL;
    }
    return orientation;
  }

  private Scale calculateScaleTag(BigDecimal height, BigDecimal width) {
    if (height == null || width == null) {
      return null;
    }

    Scale scale;
    BigDecimal area = height.multiply(width);
    if (area.compareTo(Scale.XXS.getUpperBound()) < 0) {
      scale = Scale.XXS;
    } else if (area.compareTo(Scale.XS.getUpperBound()) < 0) {
      scale = Scale.XS;
    } else if (area.compareTo(Scale.S.getUpperBound()) < 0) {
      scale = Scale.S;
    } else if (area.compareTo(Scale.M.getUpperBound()) < 0) {
      scale = Scale.M;
    } else if (area.compareTo(Scale.L.getUpperBound()) < 0) {
      scale = Scale.L;
    } else if (area.compareTo(Scale.XL.getUpperBound()) < 0) {
      scale = Scale.XL;
    } else {
      scale = Scale.XXL;
    }
    return scale;
  }

  private BigDecimal calculateVolumeTag(BigDecimal height, BigDecimal width, BigDecimal depth) {
    if ((height == null && width == null && depth == null) || (height != null && width == null && depth == null)
    		|| (height == null && width != null && depth == null) || (height == null && width == null && depth != null)) {
      return null;
    }
    height = height != null ? height : new BigDecimal(1);
    width = width != null ? width : new BigDecimal(1);
    depth = depth != null ? depth : new BigDecimal(1);
    
    return height.multiply(width).multiply(depth);
  }
}
