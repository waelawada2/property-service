/**
 * 
 */
package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.UserDTO;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.search.UserSearchDTO;

/**
 * This class provides various methods to map User Entity to User DTO.
 * 
 * @author Manju.Rana
 *
 */
@Component
public class UserModelMapper extends AbstractModelMapper{
  
  @Autowired
  public UserModelMapper(ModelMapper modelMapper) {
    super(modelMapper);
  };
  
  /**
   * Transforms {@code Page<ENTITY>} objects into {@code Page<DTO>} objects.
   *
   * @param pageRequest The information of the requested page.
   * @param source The {@code Page<ENTITY>} object.
   * @return The created {@code Page<DTO>} object.
   */
  public Page<UserSearchDTO> mapEntityPageIntoDTOPageWithUserNameOnly(Pageable pageRequest,
      Page<User> source) {
    List<UserSearchDTO> dtos = mapToUserNameList(source.getContent());
    return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
  }

  public Page<UserDTO> mapEntityPageIntoDTOPage(Pageable pageRequest, Page<User> source) {
    List<UserDTO> dtos = this.mapToUserDTO(source.getContent());
    return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
  }
  
  /**
   * This method maps the User-name to user name when lod = MIN
   *
   * @param users
   * @return
   */
  public List<UserSearchDTO> mapToUserNameList(List<User> users) {
    List<UserSearchDTO> userSearchDTOsList = new ArrayList<UserSearchDTO>();

    for (User user : users) {
      UserSearchDTO userSearchDto = new UserSearchDTO();
      userSearchDto.setUserName(user.getUserName());
      userSearchDTOsList.add(userSearchDto);
    }
    return userSearchDTOsList;
  }
  
  /**
   * This method maps the User Entity to UserDTO.
   * 
   * @param users
   * @return list
   */
  public List<UserDTO> mapToUserDTO(final List<User> users) {
    Type listType = new TypeToken<List<UserDTO>>() {}.getType();
    return modelMapper.map(users, listType);
  }

}
