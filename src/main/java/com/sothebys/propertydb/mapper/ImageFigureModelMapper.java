package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.ImageFigureDTO;
import com.sothebys.propertydb.model.ImageFigure;

/**
 * @author aneesha.l
 *	
 */
@Component
public class ImageFigureModelMapper extends AbstractModelMapper {

	@Autowired
	public ImageFigureModelMapper(ModelMapper modelMapper) {
		super(modelMapper);
	};

	public Page<ImageFigureDTO> mapEntityPageIntoDTOPage(Pageable pageRequest, Page<ImageFigure> source) {
		List<ImageFigureDTO> dtos = this.mapToImageFigureDTOs(source.getContent());
		return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
	}

	/**
	 * @param imageFigures
	 * @return list
	 */
	public List<ImageFigureDTO> mapToImageFigureDTOs(final List<ImageFigure> imageFigures) {
		Type listType = new TypeToken<List<ImageFigureDTO>>() {
		}.getType();
		return modelMapper.map(imageFigures, listType);
	}
}
