package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.EditionSizeTypeDTO;
import com.sothebys.propertydb.model.EditionSizeType;

/**
 * Provides mapping methods to convert between {@link EditionSizeType} and
 * {@link EditionSizeTypeDTO}.
 *
 * @author SrinivasaRao Batthula
 */
@Component
public class EditionSizeTypeModelMapper extends AbstractModelMapper {

  @Autowired
  public EditionSizeTypeModelMapper(ModelMapper modelMapper) {
    super(modelMapper);
  }

  /**
   * Map the given edition size type entity list to a list of edition type DTOs.
   *
   * @param editionSizeTypes The source list of entities to be mapped.
   * @return A list of edition size type DTOs.
   */
  public List<EditionSizeTypeDTO> mapToDTO(final List<EditionSizeType> editionSizeTypes) {
    Type listType = new TypeToken<List<EditionSizeTypeDTO>>() {}.getType();
    return modelMapper.map(editionSizeTypes, listType);
  }

  /**
   * Map the given edition size type DTO list to a list of edition size types.
   *
   * @param editionSizeTypeDTOs The source list of DTOs to be mapped.
   * @return A list of edition size types.
   */
  public List<EditionSizeType> mapToEntity(final List<EditionSizeTypeDTO> editionSizeTypeDTOs) {
    Type listType = new TypeToken<List<EditionSizeType>>() {}.getType();
    return modelMapper.map(editionSizeTypeDTOs, listType);
  }

}
