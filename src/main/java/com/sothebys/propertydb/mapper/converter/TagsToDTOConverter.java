package com.sothebys.propertydb.mapper.converter;

import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.AbstractConverter;

import com.sothebys.propertydb.dto.ExpertiseSitterDTO;
import com.sothebys.propertydb.dto.TagsDTO;
import com.sothebys.propertydb.model.ImageAnimal;
import com.sothebys.propertydb.model.ImageGender;
import com.sothebys.propertydb.model.ImageMainColour;
import com.sothebys.propertydb.model.ImagePosition;
import com.sothebys.propertydb.model.ImageSubject;
import com.sothebys.propertydb.model.OtherTag;
import com.sothebys.propertydb.model.Tags;

public class TagsToDTOConverter extends AbstractConverter<Tags, TagsDTO> {

  @Override
  protected TagsDTO convert(Tags tags) {
    if (tags == null) {
      return null;
    }

    TagsDTO tagsDTO = new TagsDTO();

    tagsDTO.setImageText(tags.getImageText());
    tagsDTO.setExpertiseLocation(tags.getExpertiseLocation());
    tagsDTO.setAutoOrientation(tags.getAutoOrientation());
    tagsDTO.setAutoImage(tags.isAutoImage());
    tagsDTO.setAutoScale(tags.getAutoScale());
    tagsDTO.setAutoVolume(tags.getAutoVolume());

    if (tags.getImageFigure() != null) {
      tagsDTO.setImageFigure(tags.getImageFigure().getName());
    }

    tagsDTO.setImageGenders(Optional.ofNullable(tags.getImageGenders())
        .orElseGet(Collections::emptyList)
        .stream()
        .map(ImageGender::getName)
        .collect(Collectors.toList()));
    tagsDTO.setImageSubjects(Optional.ofNullable(tags.getImageSubjects())
        .orElseGet(Collections::emptyList)
        .stream()
        .map(ImageSubject::getName)
        .collect(Collectors.toList()));
    tagsDTO.setImagePositions(Optional.ofNullable(tags.getImagePositions())
        .orElseGet(Collections::emptyList)
        .stream()
        .map(ImagePosition::getName)
        .collect(Collectors.toList()));
    tagsDTO.setImageAnimals(Optional.ofNullable(tags.getImageAnimals())
        .orElseGet(Collections::emptyList)
        .stream()
        .map(ImageAnimal::getName)
        .collect(Collectors.toList()));
    tagsDTO.setImageMainColours(Optional.ofNullable(tags.getImageMainColours())
        .orElseGet(Collections::emptyList)
        .stream()
        .map(ImageMainColour::getName)
        .collect(Collectors.toList()));
    tagsDTO.setExpertiseSitters(Optional.ofNullable(tags.getExpertiseSitters())
        .orElseGet(Collections::emptyList)
        .stream()
    	.map(sitter -> {
    			ExpertiseSitterDTO sitterDTO = new ExpertiseSitterDTO();
    			sitterDTO.setId(sitter.getId());
    			sitterDTO.setFirstName(sitter.getFirstName());
    			sitterDTO.setLastName(sitter.getLastName());
    			return sitterDTO;
    	})
        .collect(Collectors.toList()));
    tagsDTO.setOtherTags(Optional.ofNullable(tags.getOtherTags())
        .orElseGet(Collections::emptyList)
        .stream()
        .map(OtherTag::getName)
        .collect(Collectors.toList()));

    return tagsDTO;
  }
}
