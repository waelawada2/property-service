package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.ImageSubjectDTO;
import com.sothebys.propertydb.model.ImageSubject;

/**
 * @author aneesha.l
 *
 */
@Component
public class ImageSubjectModelMapper extends AbstractModelMapper {

	@Autowired
	public ImageSubjectModelMapper(ModelMapper modelMapper) {
		super(modelMapper);
	};

	public Page<ImageSubjectDTO> mapEntityPageIntoDTOPage(Pageable pageRequest, Page<ImageSubject> source) {
		List<ImageSubjectDTO> dtos = this.mapToImageSubjectDTOs(source.getContent());
		return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
	}

	/**
	 * @param ImageSubjects
	 * @return list
	 */
	public List<ImageSubjectDTO> mapToImageSubjectDTOs(final List<ImageSubject> ImageSubjects) {
		Type listType = new TypeToken<List<ImageSubjectDTO>>() {
		}.getType();
		return modelMapper.map(ImageSubjects, listType);
	}

	/**
	 * Map the given ImageSubject DTO list to a list of imageSubjects.
	 *
	 * @param imageSubjectDTOS The source list of DTOs to be mapped.
	 * @return A list of ImageSubjects.
	 */
	public List<ImageSubject> mapToEntity(final List<ImageSubjectDTO> imageSubjectDTOS) {
		Type listType = new TypeToken<List<ImageSubject>>() {
		}.getType();
		return modelMapper.map(imageSubjectDTOS, listType);
	}
}
