package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.CategoryDTO;
import com.sothebys.propertydb.model.Category;

/**
 * Provides mapping methods to convert between {@link Category} and {@link CategoryDTO}.
 *
 * @author Gregor Zurowski
 */
@Component
public class CategoryModelMapper extends AbstractModelMapper {

  @Autowired
  public CategoryModelMapper(ModelMapper modelMapper) {
    super(modelMapper);
  }

  /**
   * Map the given category entity list to a list of category DTOs.
   *
   * @param categories The source list of entities to be mapped.
   * @return A list of category DTOs.
   */
  public List<CategoryDTO> mapToDTO(final List<Category> categories) {
    Type listType = new TypeToken<List<CategoryDTO>>() {}.getType();
    return modelMapper.map(categories, listType);
  }

  /**
   * Map the given category DTO list to a list of categories.
   *
   * @param categoryDTOs The source list of DTOs to be mapped.
   * @return A list of categories.
   */
  public List<Category> mapToEntity(final List<CategoryDTO> categoryDTOs) {
    Type listType = new TypeToken<List<Category>>() {}.getType();
    return modelMapper.map(categoryDTOs, listType);
  }
}
