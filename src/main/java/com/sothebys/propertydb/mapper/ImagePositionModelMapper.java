/**
 * 
 */
package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.ImagePositionDTO;
import com.sothebys.propertydb.model.ImagePosition;

/**
 * @author aneesha.l
 *
 */
@Component
public class ImagePositionModelMapper extends AbstractModelMapper {
	@Autowired
	public ImagePositionModelMapper(ModelMapper modelMapper) {
		super(modelMapper);
	};

	public Page<ImagePositionDTO> mapEntityPageIntoDTOPage(Pageable pageRequest, Page<ImagePosition> source) {
		List<ImagePositionDTO> dtos = this.mapToImagePositionDTOs(source.getContent());
		return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
	}

	/**
	 * @param imagePositions
	 * @return list
	 */
	public List<ImagePositionDTO> mapToImagePositionDTOs(final List<ImagePosition> imagePositions) {
		Type listType = new TypeToken<List<ImagePositionDTO>>() {
		}.getType();
		return modelMapper.map(imagePositions, listType);
	}

	/**
	 * Map the given country DTO list to a list of ImagePositions.
	 *
	 * @param imagePositionDTOS The source list of DTOs to be mapped.
	 * @return A list of ImagePositionDTOs.
	 */
	public List<ImagePosition> mapToEntity(final List<ImagePositionDTO> imagePositionDTOS) {
		Type listType = new TypeToken<List<ImagePosition>>() {
		}.getType();
		return modelMapper.map(imagePositionDTOS, listType);
	}
}
