/**
 * 
 */
package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.OtherTagDTO;
import com.sothebys.propertydb.model.OtherTag;

/**
 * @author aneesha.l
 *
 */
@Component
public class OtherTagModelMapper extends AbstractModelMapper {

	@Autowired
	public OtherTagModelMapper(ModelMapper modelMapper) {
		super(modelMapper);
	};

	public Page<OtherTagDTO> mapEntityPageIntoDTOPage(Pageable pageRequest, Page<OtherTag> source) {
		List<OtherTagDTO> dtos = this.mapToOtherTagDTOs(source.getContent());
		return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
	}
	
	/**
	 * @param otherTags
	 * @return list
	 */
	public List<OtherTagDTO> mapToOtherTagDTOs(final List<OtherTag> otherTags) {
		Type listType = new TypeToken<List<OtherTagDTO>>() {
		}.getType();
		return modelMapper.map(otherTags, listType);
	}
}
