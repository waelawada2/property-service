package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.SaveSearchDTO;
import com.sothebys.propertydb.model.SaveSearch;

/**
 * Provides mapping methods to convert between {@link SaveSearch} and {@link SaveSearchDTO}.
 *
 * @author SrinivasaRao Batthula
 */
@Component
public class SaveSearchModelMapper extends AbstractModelMapper {

  @Autowired
  public SaveSearchModelMapper(ModelMapper modelMapper) {
    super(modelMapper);
  }

  /**
   * Map the given SaveSearch entity list to a list of SaveSearch DTOs.
   *
   * @param SaveSearch The source list of entities to be mapped.
   * @return A list of SaveSearch DTOs.
   */
  public List<SaveSearchDTO> mapToDTO(final List<SaveSearch> saveSearches) {
    Type listType = new TypeToken<List<SaveSearchDTO>>() {}.getType();
    return modelMapper.map(saveSearches, listType);
  }

  /**
   * Map the given SaveSearch DTO list to a list of saveSearches.
   *
   * @param SaveSearchDTOs The source list of DTOs to be mapped.
   * @return A list of saveSearches.
   */
  public List<SaveSearch> mapToEntity(final List<SaveSearchDTO> saveSearchDTOs) {
    Type listType = new TypeToken<List<SaveSearch>>() {}.getType();
    return modelMapper.map(saveSearchDTOs, listType);
  }

  /**
   * Method for SaveSearch to SaveSearchDTO conversion
   *
   * @param saveSearchObject
   * @return
   */
  public SaveSearchDTO mapToDTO(SaveSearch saveSearchObject) {
    SaveSearchDTO saveSearchDTO = modelMapper.map(saveSearchObject, SaveSearchDTO.class);
    saveSearchDTO.setId(saveSearchObject.getExternalId());
    return saveSearchDTO;
  }

  /**
   * Transforms {@code Page<ENTITY>} objects into {@code Page<DTO>} objects.
   *
   * @param pageRequest The information of the requested page.
   * @param source The {@code Page<ENTITY>} object.
   * @return The created {@code Page<DTO>} object.
   */
  public Page<SaveSearchDTO> mapEntityPageIntoDTOPageWithIdsOnly(Pageable pageRequest,
      Page<SaveSearch> source) {
    List<SaveSearchDTO> dtos = mapToSaveSearchIdList(source.getContent());
    return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
  }

  public Page<SaveSearchDTO> mapEntityPageIntoDTOPage(Pageable pageRequest,
      Page<SaveSearch> source) {
    List<SaveSearchDTO> dtos = this.mapToSaveSearchDTOs(source.getContent());
    return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
  }

  /**
   * to map the SaveSearchId from fetch saveSearch list
   *
   * @param saveSearches
   * @return
   */
  public List<SaveSearchDTO> mapToSaveSearchIdList(List<SaveSearch> saveSearches) {
    List<SaveSearchDTO> saveSearchdtos = new ArrayList<SaveSearchDTO>();

    for (SaveSearch saveSearch : saveSearches) {
      SaveSearchDTO saveSearchdto = new SaveSearchDTO();
      saveSearchdto.setId(saveSearch.getExternalId());
      saveSearchdtos.add(saveSearchdto);
    }
    return saveSearchdtos;
  }

  /**
   * @param saveSearches
   * @return list
   */
  public List<SaveSearchDTO> mapToSaveSearchDTOs(final List<SaveSearch> saveSearches) {
    Type listType = new TypeToken<List<SaveSearchDTO>>() {}.getType();
    return modelMapper.map(saveSearches, listType);
  }

}
