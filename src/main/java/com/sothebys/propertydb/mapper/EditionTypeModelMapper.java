package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.EditionTypeDTO;
import com.sothebys.propertydb.model.EditionType;

/**
 * Provides mapping methods to convert between {@link EditionType} and {@link EditionTypeDTO}.
 *
 * @author Gregor Zurowski
 */
@Component
public class EditionTypeModelMapper extends AbstractModelMapper {

  @Autowired
  public EditionTypeModelMapper(ModelMapper modelMapper) {
    super(modelMapper);
  }

  /**
   * Map the given edition type entity list to a list of edition type DTOs.
   *
   * @param editionTypes The source list of entities to be mapped.
   * @return A list of edition type DTOs.
   */
  public List<EditionTypeDTO> mapToDTO(final List<EditionType> editionTypes) {
    Type listType = new TypeToken<List<EditionTypeDTO>>() {}.getType();
    return modelMapper.map(editionTypes, listType);
  }

  /**
   * Map the given edition type DTO list to a list of edition types.
   *
   * @param editionTypeDTOs The source list of DTOs to be mapped.
   * @return A list of edition types.
   */
  public List<EditionType> mapToEntity(final List<EditionTypeDTO> editionTypeDTOs) {
    Type listType = new TypeToken<List<EditionType>>() {}.getType();
    return modelMapper.map(editionTypeDTOs, listType);
  }

}
