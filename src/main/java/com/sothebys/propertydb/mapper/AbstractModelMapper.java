package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;

import org.modelmapper.ModelMapper;

/**
 * A base class for model mapping classes.
 *
 * @author Gregor Zurowski
 */
public abstract class AbstractModelMapper {

  protected ModelMapper modelMapper;

  public AbstractModelMapper(ModelMapper modelMapper) {
    this.modelMapper = modelMapper;
  }

  /**
   * Map {@code source} object to an instance of {@code destinationType}.
   *
   * @param source Source object to map from.
   * @param destinationType Destination type to map to.
   * @param <T> Destination type.
   * @return
   */
  public <T> T map(Object source, Type destinationType) {
    return modelMapper.map(source, destinationType);
  }

}
