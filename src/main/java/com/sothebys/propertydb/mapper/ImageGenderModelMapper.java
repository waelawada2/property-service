package com.sothebys.propertydb.mapper;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.dto.ImageGenderDTO;
import com.sothebys.propertydb.model.ImageGender;

/**
 * @author aneesha.l
 *
 */
@Component
public class ImageGenderModelMapper extends AbstractModelMapper {

	@Autowired
	public ImageGenderModelMapper(ModelMapper modelMapper) {
		super(modelMapper);
	};

	public Page<ImageGenderDTO> mapEntityPageIntoDTOPage(Pageable pageRequest, Page<ImageGender> source) {
		List<ImageGenderDTO> dtos = this.mapToImageGenderDTOs(source.getContent());
		return new PageImpl<>(dtos, pageRequest, source.getTotalElements());
	}

	/**
	 * @param imageGender
	 * @return list
	 */
	public List<ImageGenderDTO> mapToImageGenderDTOs(final List<ImageGender> imageGender) {
		Type listType = new TypeToken<List<ImageGenderDTO>>() {
		}.getType();
		return modelMapper.map(imageGender, listType);
	}
}
