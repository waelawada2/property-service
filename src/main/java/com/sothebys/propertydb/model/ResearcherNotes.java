package com.sothebys.propertydb.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Data;

/**
 * Created by waelawada on 4/15/17.
 */
@Embeddable
@Data
public class ResearcherNotes {

  @Column(name = "researcher_name", length = 200)
  private String researcherName;
  
  @Column(name = "research_complete_check")
  private Boolean researchCompleteCheck;

  @Column(name = "researcher_notes_text", length = 1000)
  private String text;

  @Column(name = "researcher_notes_date", length = 20)
  private Date date;

}
