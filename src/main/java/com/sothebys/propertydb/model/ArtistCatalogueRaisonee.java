/*
 * -------------------------------------------------------------------------
 *
 * (C) Copyright / Sotheby's. All rights reserved. The contents of this file represent Sotheby's
 * trade secrets and are confidential. Use outside of Sotheby's is prohibited and in violation of
 * copyright law.
 *
 * -------------------------------------------------------------------------
 */
package com.sothebys.propertydb.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sothebys.propertydb.search.Status;

import lombok.Data;
import lombok.ToString;

/**
 * This class used to hold CatalogueRaisonee information
 *
 * @author VenkataPrasad Tammineni
 *
 */
@Entity
@Data
@ToString(callSuper = true, exclude = {"artists", "propertyCatalogueRaisonees"})
public class ArtistCatalogueRaisonee {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @JsonIgnore
  @ManyToMany(mappedBy = "artistCatalogueRaisonees")
  private List<Artist> artists;

  @Column(name = "author", nullable = false, length = 250)
  private String author;

  @Column(name = "sort_id", length = 10)
  private Integer sortId;

  @OneToMany(mappedBy = "artistCatalogueRaisonee")
  @Where(clause = "status='Active'")
  private List<PropertyCatalogueRaisonee> propertyCatalogueRaisonees;

  @Column(name = "cr_type", length = 250)
  private String type;

  @Column(name = "volumes", length = 10)
  private String volumes;

  @Column(name = "year", length = 10)
  private Integer year;

  @Column(name = "status")
  @Enumerated(EnumType.STRING)
  private Status status;

  @PrePersist
  public void prePersistArtistCatalogueRaisonee() {
    status = Status.Active;
  }

}
