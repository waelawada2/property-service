package com.sothebys.propertydb.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.ToString;

/**
 * Created by SrinivasaRao.Batthula
 */
@ToString(callSuper = true, exclude = {"searchCriterias"})
@Entity
@Data
public class SaveSearch {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "name", unique = false, nullable = false, length = 250)
  private String name;

  @Column(name = "notes", unique = false, nullable = false, length = 250)
  private String notes;

  @Temporal(value = TemporalType.TIMESTAMP)
  private Date date;

  @Column(name = "external_id", length = 200)
  private String externalId;

  @Column(name = "user_id", unique = false, nullable = false, length = 250)
  private long userId;

  @Column(name = "search_type", unique = false, nullable = false, length = 250)
  private String searchType;

  @JsonIgnore
  @OneToMany(mappedBy = "saveSearches", cascade = CascadeType.ALL)
  private List<SaveSearchCriteria> searchCriterias;

  @PrePersist
  public void prePersistSaveSearch() {
    date = new Date();
  }

  @PreUpdate
  public void preUpdateSaveSearch() {
    date = new Date();
  }

}
