package com.sothebys.propertydb.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import lombok.Data;
import lombok.ToString;

@ToString(callSuper = true, exclude = {"properties"})
@Data
@Entity
public class ArchiveNumber {

  @Column(name = "archive_number", unique = false, nullable = false, length = 250)
  private String archiveNumber;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  @ManyToMany(mappedBy = "archiveNumbers")
  private List<Property> properties;

}
