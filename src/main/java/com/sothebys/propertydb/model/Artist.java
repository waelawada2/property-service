/*
 * -------------------------------------------------------------------------
 *
 * (C) Copyright / Sotheby's. All rights reserved. The contents of this file represent Sotheby's
 * trade secrets and are confidential. Use outside of Sotheby's is prohibited and in violation of
 * copyright law.
 *
 * -------------------------------------------------------------------------
 */
package com.sothebys.propertydb.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Where;

import com.sothebys.propertydb.search.Status;

import lombok.Data;
import lombok.ToString;

/**
 * This class used to hold Artist information
 *
 * @author VenkataPrasad Tammineni
 */
@ToString(callSuper = true,
    exclude = {"artistCatalogueRaisonees", "countries", "property", "categories"})
@Entity
@Data
public class Artist {

  private boolean approved;

  @ManyToMany(cascade = CascadeType.ALL)
  @JoinTable(name = "artist_artist_catalogue_raisonee",
      joinColumns = @JoinColumn(name = "artist_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "catalogue_raisonee_id", referencedColumnName = "id"))
  @Where(clause = "status='Active'")
  private List<ArtistCatalogueRaisonee> artistCatalogueRaisonees;

  @Column(name = "birth_year", length = 10)
  private Integer birthYear;

  @ManyToMany
  @JoinTable(name = "artist_category",
      joinColumns = @JoinColumn(name = "artist_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id"))
  private List<Category> categories;

  @Temporal(value = TemporalType.TIMESTAMP)
  private Date createdDate;

  @Column(name = "death_year", length = 10)
  private Integer deathYear;

  @Column(name = "display", length = 200)
  private String display;

  @Column(name = "external_id", length = 200)
  private String externalId;

  @Column(name = "first_name", length = 100)
  private String firstName;

  @Column(name = "gender", length = 10)
  private String gender;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "last_name", nullable = false, length = 100)
  private String lastName;

  @ManyToMany
  @JoinTable(name = "artist_country",
      joinColumns = @JoinColumn(name = "artist_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "country_id", referencedColumnName = "id"))
  private List<Country> countries;

  @ManyToMany(mappedBy = "artists")
  private List<Property> property;

  @Column(name = "ulan_id", length = 20)
  private Long ulanId;

  @ManyToOne
  @JoinColumn(name = "created_by")
  private User createdBy;

  @ManyToOne
  @JoinColumn(name = "updated_by")
  private User updatedBy;

  @Temporal(value = TemporalType.TIMESTAMP)
  private Date updatedDate;

  @PrePersist
  public void prePersistArtist() {
    createdDate = new Date();
    status = Status.Active;
  }

  @PreUpdate
  public void preUpdateArtist() {
    updatedDate = new Date();
  }

  @Column(name = "status")
  @Enumerated(EnumType.STRING)
  private Status status;

  @Column(name = "notes", length = 250)
  private String notes;

}
