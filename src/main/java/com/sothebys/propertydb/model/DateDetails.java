package com.sothebys.propertydb.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Data;

/**
 * Created by waelawada on 4/15/17.
 */
@Embeddable
@Data
public class DateDetails {
  @Column(name = "cast_year_circa", length = 10)
  private Boolean castYearCirca;

  @Column(name = "cast_year_start", length = 10)
  private Integer castYearStart;

  @Column(name = "cast_year_end", length = 10)
  private Integer castYearEnd;

  @Column(name = "year_start", length = 10)
  private Integer yearStart;

  @Column(name = "year_end", length = 10)
  private Integer yearEnd;

  @Column(name = "year_text", length = 200)
  private String yearText;

  @Column(name = "year_circa", length = 10)
  private Boolean yearCirca;
}
