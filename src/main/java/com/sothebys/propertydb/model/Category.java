/*
 * -------------------------------------------------------------------------
 *
 * (C) Copyright / Sotheby's. All rights reserved. The contents of this file represent Sotheby's
 * trade secrets and are confidential. Use outside of Sotheby's is prohibited and in violation of
 * copyright law.
 *
 * -------------------------------------------------------------------------
 */
package com.sothebys.propertydb.model;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

/**
 * This class used to hold Category information
 *
 * @author SrinivasaRao Batthula
 *
 */
@Entity
@Data
@ToString(exclude = {"children", "properties"})
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"parent_id", "name"})})
public class Category {

  @Column(name = "description", unique = false, length = 250)
  private String description;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "name", unique = true, nullable = false, length = 30)
  private String name;

  @JsonIgnore
  @OneToMany(mappedBy = "category")
  private List<Property> properties;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "parent_id")
  private Category parent;

  @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
  private List<Category> children;

}
