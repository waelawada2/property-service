package com.sothebys.propertydb.model;

import lombok.Data;


@Data
public class AddPropertiesToSavelistResult {

  private SaveList saveList;
  private int totalFound;
  private int totalInserted;

}
