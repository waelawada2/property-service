package com.sothebys.propertydb.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Data;

/**
 * Created by waelawada on 4/15/17.
 */
@Data
@Embeddable
public class Authenticity {

  @Column(name = "signed", length = 10)
  private Boolean signed;

  @Column(name = "certificate", length = 10)
  private Boolean certificate;

  @Column(name = "stamped", length = 10)
  private Boolean stamped;

  @Column(name = "authentication", length = 10)
  private Boolean authentication;

}
