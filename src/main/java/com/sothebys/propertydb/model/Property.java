/*
 * -------------------------------------------------------------------------
 *
 * (C) Copyright / Sotheby's. All rights reserved. The contents of this file represent Sotheby's
 * trade secrets and are confidential. Use outside of Sotheby's is prohibited and in violation of
 * copyright law.
 *
 * -------------------------------------------------------------------------
 */
package com.sothebys.propertydb.model;

import com.sothebys.propertydb.search.PropertyUtil;
import com.sothebys.propertydb.search.Status;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Where;

import lombok.Data;
import lombok.ToString;


/**
 * This class used to hold Object information
 *
 * @author SrinivasaRao Batthula
 */
@ToString(callSuper = true, exclude = {"propertyCatalogueRaisonees", "category", "artists",
    "archiveNumbers", "objectCodes"})
@Entity
@Data
public class Property {

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinTable(name = "property_archiveNumber",
      joinColumns = @JoinColumn(name = "property_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "archiveNumber_id", referencedColumnName = "id"))
  private List<ArchiveNumber> archiveNumbers;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "property_artist",
      joinColumns = @JoinColumn(name = "property_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "artist_id", referencedColumnName = "id"))
  @Size(min = 1)
  private List<Artist> artists;

  @ManyToOne(fetch = FetchType.LAZY)
  private Category category;

  @Column(unique = true, name = "external_id", length = 200)
  private String externalId;

  @Embedded
  private Flags flags;

  @Column(name = "foundry", length = 250)
  private String foundry;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "image_path", length = 50)
  private String imagePath;

  @Column(name = "medium", length = 1000)
  private String medium;

  @Column(name = "parts", length = 10)
  private Integer parts;

  @ManyToOne(fetch = FetchType.LAZY)
  private PresizeType presizeType;

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinTable(name = "property_property_catalogue_raisonee",
      joinColumns = @JoinColumn(name = "property_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "catalogue_raisonee_id", referencedColumnName = "id"))
  @Where(clause = "status='Active'")
  private List<PropertyCatalogueRaisonee> propertyCatalogueRaisonees;

  @Column(name = "signature", length = 1000)
  private String signature;

  @Column(name = "sort_code", length = 200)
  private String sortCode;

  @Column(name = "title", length = 500)
  private String title;

  @Column(name = "original_title", length = 500)
  private String originalTitle;

  @Column(name = "posthumous", length = 10)
  private Boolean posthumous;

  @Temporal(value = TemporalType.TIMESTAMP)
  private Date createdDate;

  @Column(name = "upload_reference", length = 250)
  private String uploadReference;

  @Embedded
  private Dimensions dimensions;

  @Embedded
  private Authenticity authenticity;

  @Embedded
  private Edition edition;

  @Embedded
  private ResearcherNotes researcherNotes;

  @Embedded
  private ReminderNotes reminderNotes;

  @Embedded
  private DateDetails dateDetails;

  @Column(name = "size_text", length = 500)
  private String sizeText;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "created_by")
  private User createdBy;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "updated_by")
  private User updatedBy;

  @Column(name = "status")
  @Enumerated(EnumType.STRING)
  private Status status;

  @Column(name = "merged_to_id", length = 200)
  private String mergedToId;

  @Column(name = "current_owner_id", length = 250)
  private String currentOwnerId;


  @ElementCollection(fetch = FetchType.LAZY)
  @CollectionTable(name = "ObjectCode", joinColumns = @JoinColumn(name = "property_id"))
  @Column(name = "object_code")
  private List<String> objectCodes;

  @Temporal(value = TemporalType.TIMESTAMP)
  private Date updatedDate;

  @ManyToMany(mappedBy = "properties", fetch = FetchType.LAZY)
  private List<SaveList> saveList;

  @Embedded
  private Tags tags;


  @PrePersist
  public void prePersistProperty() {
    createdDate = new Date();
    status = Status.Active;
    if (researcherNotes != null && researcherNotes.getDate() != null) {
      Date researcherNoteDate = PropertyUtil.dateFormat(researcherNotes.getDate());
      researcherNotes.setDate(researcherNoteDate);
    }
  }

  @PreUpdate
  public void preUpdateProperty() {
    if (researcherNotes != null && researcherNotes.getDate() != null) {
      Date researcherNoteDate = PropertyUtil.dateFormat(researcherNotes.getDate());
      researcherNotes.setDate(researcherNoteDate);
    }
    updatedDate = new Date();
  }

}
