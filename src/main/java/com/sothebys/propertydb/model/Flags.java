package com.sothebys.propertydb.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import lombok.Data;

/**
 * This class used to hold Flags information
 *
 * @author VenkataPrasad Tammineni
 *
 */

@Data
@Embeddable
public class Flags {

  @Column(name = "flag_authenticity")
  private Boolean flagAuthenticity;

  @Column(name = "flag_condition")
  private Boolean flagCondition;

  @Column(name = "flag_medium")
  private Boolean flagMedium;

  @Column(name = "flag_restitution")
  private Boolean flagRestitution;

  @Column(name = "flag_sfs")
  private Boolean flagSfs;

  @ManyToOne
  private Country flagsCountry;

}
