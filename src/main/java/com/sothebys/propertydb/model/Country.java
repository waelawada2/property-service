package com.sothebys.propertydb.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

/**
 * Created by SrinivasaRao.Batthula
 */
@ToString(callSuper = true, exclude = {"properties","artists"})
@Entity
@Data
public class Country {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "name", unique = true, nullable = false, length = 100)
  private String countryName;

  @JsonIgnore
  @OneToMany(mappedBy = "flags.flagsCountry")
  private List<Property> properties;
  
  @JsonIgnore
  @ManyToMany(mappedBy = "countries")
  private List<Artist> artists;



}
