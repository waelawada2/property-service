package com.sothebys.propertydb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

/**
 * Created by Minhtri Tran on 11/15/17.
 */
@Entity
@Data
public class ExpertiseSitter {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "first_name", length = 100)
  private String firstName;
  
  @Column(name = "last_name", length = 100)
  private String lastName;
}
