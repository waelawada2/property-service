package com.sothebys.propertydb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.ToString;

/**
 * Created by SrinivasaRao.Batthula
 */
@ToString(callSuper = true)
@Entity
@Data
public class SaveSearchCriteria {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "param_name", unique = false, nullable = false, length = 250)
  private String paramName;

  @Column(name = "param_value", unique = false, nullable = false, length = 250)
  private String paramValue;

  @ManyToOne
  @JoinColumn(name = "save_search_id", referencedColumnName = "id")
  private SaveSearch saveSearches;

}
