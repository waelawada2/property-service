/*
 * -------------------------------------------------------------------------
 *
 * (C) Copyright / Sotheby's. All rights reserved. The contents of this file represent Sotheby's
 * trade secrets and are confidential. Use outside of Sotheby's is prohibited and in violation of
 * copyright law.
 *
 * -------------------------------------------------------------------------
 */
package com.sothebys.propertydb.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sothebys.propertydb.search.Status;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import lombok.Data;

/**
 * This class used to hold CatalogueRaisonee information
 *
 * @author VenkataPrasad Tammineni
 */
@Entity
@Data
public class PropertyCatalogueRaisonee {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "number", length = 20)
  private String number;

  @ManyToOne
  @JoinColumn(name = "artist_catalogue_raisonee_id")
  private ArtistCatalogueRaisonee artistCatalogueRaisonee;

  @JsonIgnore
  @ManyToMany(mappedBy = "propertyCatalogueRaisonees")
  private List<Property> property;

  @Column(name = "volume", length = 10)
  private String volume;

  @Column(name = "status")
  @Enumerated(EnumType.STRING)
  private Status status;

  @PrePersist
  public void prePersistPropertyCatalogueRaisonee() {
    status = Status.Active;
  }

}
