package com.sothebys.propertydb.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Data;

/**
 * Created by waelawada on 4/15/17.
 */
@Embeddable
@Data
public class EditionSize {

  @Column(name = "artist_proof_total", length = 10)
  private int artistProofTotal;

  @Column(name = "trial_proof_total", length = 10)
  private int trialProofTotal;

  @Column(name = "bon_a_tirer_total", length = 10)
  private int bonATirerTotal;

  @Column(name = "printers_proof_total", length = 10)
  private int printersProofTotal;

  @Column(name = "hors_commerce_proof_total", length = 10)
  private int horsCommerceProofTotal;

  @Column(name = "series_total", length = 100)
  private int seriesTotal;

  @Column(name = "cancellation_proof_total", length = 10)
  private int cancellationProofTotal;

  @Column(name = "color_trial_proof_total", length = 10)
  private int colorTrialProofTotal;

  @Column(name = "dedicated_proof_total", length = 10)
  private int dedicatedProofTotal;

  @Column(name = "museum_proof_total", length = 10)
  private int museumProofTotal;

  @Column(name = "progressive_proof_total", length = 10)
  private int progressiveProofTotal;

  @Column(name = "right_to_produce_proof_total", length = 10)
  private int rightToProduceProofTotal;

  @Column(name = "working_proof_total", length = 10)
  private int workingProofTotal;

  @Column(name = "unkown_edition_type_size")
  private Boolean unknownEditionSizeType;

  @Column(name = "edition_size_notes")
  private String editionSizeNotes;

  @Column(name = "edition_size_total", length = 10)
  private int editionSizeTotal;

  @Column(name = "edition_override", length = 200)
  private String editionOverride;

}
