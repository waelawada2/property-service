package com.sothebys.propertydb.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Data;

/**
 * Created by waelawada on 4/15/17.
 */
@Data
@Embeddable
public class Dimensions {

  @Column(name = "width", length = 10)
  private BigDecimal width;

  @Column(name = "height", length = 10)
  private BigDecimal height;

  @Column(name = "depth", length = 10)
  private BigDecimal depth;

  @Column(name = "weight", length = 10)
  private BigDecimal weight;

}
