package com.sothebys.propertydb.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

/**
 * Created by waelawada on 4/15/17.
 */
@Embeddable
@Data
public class ReminderNotes {
  @Column(name = "reminder_employee", length = 200)
  private String reminderEmployee;

  @Column(name = "reminder_notes_text", length = 200)
  private String text;

}
