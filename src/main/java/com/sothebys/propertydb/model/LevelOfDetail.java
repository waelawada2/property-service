package com.sothebys.propertydb.model;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Enum that represents the level of detail, e.g. for requesting a representation of data with a
 * specific of details.
 *
 * @author Gregor Zurowski
 */
public enum LevelOfDetail {
  MAX, MIN;
}
