package com.sothebys.propertydb.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

/**
 * Created by wael.awada on 5/15/17.
 */
@Entity
@Data
@ToString(callSuper = true, exclude = {"createdArtists", "updatedArtists", "createdProperties",
    "updatedProperties", "sharedSaveLists", "saveLists"})
public class User implements Serializable{
  
  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String keycloakUserId;

  private String firstName;

  private String lastName;

  @Column(unique = true)
  private String userName;

  @Column(unique = true)
  private String email;
  
  @JsonIgnore
  @OneToMany(mappedBy = "createdBy")
  private List<Artist> createdArtists;
  
  @JsonIgnore
  @OneToMany(mappedBy = "updatedBy")
  private List<Artist> updatedArtists;
  
  @JsonIgnore
  @OneToMany(mappedBy = "createdBy")
  private List<Property> createdProperties;
  
  @JsonIgnore
  @OneToMany(mappedBy = "updatedBy")
  private List<Property> updatedProperties;
  
  @JsonIgnore
  @ManyToMany(mappedBy = "sharedUsers")
  private List<SaveList> sharedSaveLists;

  @JsonIgnore
  @OneToMany(mappedBy = "user")
  private List<SaveList> saveLists;

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }

    if (!(object instanceof User)) {
      return false;
    }
    final User user = (User) object;

    if (!user.getUserName().equals(getUserName())) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    return prime * 7 + getUserName().hashCode();
  }

}
