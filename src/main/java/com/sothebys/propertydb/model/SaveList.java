package com.sothebys.propertydb.model;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.Where;

/**
 * Created by SrinivasaRao.Batthula
 */
@ToString(callSuper = true, exclude = {"properties", "sharedUsers"})
@Entity
@Data
public class SaveList {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "name", unique = false, nullable = false, length = 250)
  private String name;

  @Column(name = "notes", unique = false, nullable = false, length = 250)
  private String notes;

  @Temporal(value = TemporalType.TIMESTAMP)
  private Date date;

  @Column(name = "external_id", length = 200)
  private String externalId;

  @ManyToOne
  private User user;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "save_list_property",
      joinColumns = @JoinColumn(name = "save_list_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "property_id", referencedColumnName = "id"))
  @Where(clause = "status='Active'")
  private List<Property> properties;

  @Column(name = "open_save_list", length = 10)
  private Boolean openSaveList;

  @ManyToMany
  @JoinTable(name = "save_list_user",
      joinColumns = @JoinColumn(name = "save_list_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "shared_user_id", referencedColumnName = "id"))
  private List<User> sharedUsers;

  @PrePersist
  public void prePersistSaveList() {
    date = new Date();
  }

  @PreUpdate
  public void preUpdateSaveList() {
    date = new Date();
  }

}
