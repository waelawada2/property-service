package com.sothebys.propertydb.model;

public enum Orientation {
  VERTICAL, HORIZONTAL, EQUAL
}
