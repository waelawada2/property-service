package com.sothebys.propertydb.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

/**
 * Created by SrinivasaRao.Batthula
 */
@ToString(callSuper = true, exclude = {"properties"})
@Entity
@Data
public class EditionSizeType {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "name", unique = true, nullable = false, length = 30)
  private String name;

  @JsonIgnore
  @OneToMany(mappedBy = "edition.editionSizeType")
  private List<Property> properties;

}
