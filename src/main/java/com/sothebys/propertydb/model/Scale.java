package com.sothebys.propertydb.model;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Scale {
  XXS(BigDecimal.valueOf(625)),
  XS(BigDecimal.valueOf(2500)),
  S(BigDecimal.valueOf(10000)),
  M(BigDecimal.valueOf(22500)),
  L(BigDecimal.valueOf(40000)),
  XL(BigDecimal.valueOf(90000)),
  XXL(null);

  private BigDecimal upperBound;
}
