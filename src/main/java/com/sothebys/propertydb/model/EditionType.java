/*
 * -------------------------------------------------------------------------
 *
 * (C) Copyright / Sotheby's. All rights reserved. The contents of this file represent Sotheby's
 * trade secrets and are confidential. Use outside of Sotheby's is prohibited and in violation of
 * copyright law.
 *
 * -------------------------------------------------------------------------
 */
package com.sothebys.propertydb.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

/**
 * This class used to hold EditionTypes information
 *
 * @author SrinivasaRao Batthula
 *
 */
@ToString(callSuper = true, exclude = {"properties"})
@Entity
@Data
public class EditionType {
  @Column(name = "description", unique = false, nullable = false, length = 250)
  private String description;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "name", unique = true, nullable = false, length = 250)
  private String name;

  @JsonIgnore
  @OneToMany(mappedBy = "edition.editionType")
  private List<Property> properties;

  @Column(name = "sort_code", length = 200)
  private Long sortCode;
}
