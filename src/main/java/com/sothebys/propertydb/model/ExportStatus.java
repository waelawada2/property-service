package com.sothebys.propertydb.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

/**
 * @author SrinivasaRao.Batthula
 */
@Entity
@Data
public class ExportStatus {
  
  @Id
  @Column(name = "export_id", length = 50)
  private String exportId;
  
  @Column(name = "status", length = 20)
  private String status;
  
}
