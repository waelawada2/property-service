package com.sothebys.propertydb.model;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import lombok.Data;

/**
 * Created by Minhtri Tran on 11/14/17.
 */
@Data
@Embeddable
public class Tags {

  @Column(name = "image_text", length = 300)
  private String imageText;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "image_figure_id")
  private ImageFigure imageFigure;

  @Column(name = "expertise_location", length = 250)
  private String expertiseLocation;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "property_image_subject",
      joinColumns = @JoinColumn(name = "property_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "image_subject_id", referencedColumnName = "id"))
  private List<ImageSubject> imageSubjects;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "property_image_gender",
      joinColumns = @JoinColumn(name = "property_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "image_gender_id", referencedColumnName = "id"))
  private List<ImageGender> imageGenders;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "property_image_position",
      joinColumns = @JoinColumn(name = "property_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "image_position_id", referencedColumnName = "id"))
  private List<ImagePosition> imagePositions;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "property_image_animal",
      joinColumns = @JoinColumn(name = "property_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "image_animal_id", referencedColumnName = "id"))
  private List<ImageAnimal> imageAnimals;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "property_image_main_colour",
      joinColumns = @JoinColumn(name = "property_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "image_main_colour_id", referencedColumnName = "id"))
  private List<ImageMainColour> imageMainColours;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "property_expertise_sitter",
      joinColumns = @JoinColumn(name = "property_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "expertise_sitter_id", referencedColumnName = "id"))
  private List<ExpertiseSitter> expertiseSitters;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "property_other_tag",
      joinColumns = @JoinColumn(name = "property_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "other_tag_id", referencedColumnName = "id"))
  private List<OtherTag> otherTags;

  @Enumerated(EnumType.STRING)
  @Column(name = "auto_orientation", length = 20)
  private Orientation autoOrientation;

  @Column(name = "auto_image")
  private boolean autoImage;

  @Enumerated(EnumType.STRING)
  @Column(name = "auto_scale", length = 10)
  private Scale autoScale;

  @Column(name = "auto_volume", length = 17)
  private BigDecimal autoVolume;
}
