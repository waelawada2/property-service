package com.sothebys.propertydb.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.ManyToOne;
import lombok.Data;

/**
 * Created by waelawada on 4/15/17.
 */
@Embeddable
@Data
public class Edition {

  @Embedded
  private EditionSize editionSize;

  @Column(name = "unique_edition", length = 10)
  private Boolean uniqueEdition;

  @ManyToOne
  private EditionType editionType;

  @Column(name = "edition_number", length = 50)
  private String editionNumber;

  @ManyToOne
  private EditionSizeType editionSizeType;

}
