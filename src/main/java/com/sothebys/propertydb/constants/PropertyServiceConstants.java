package com.sothebys.propertydb.constants;

/**
 * Contains constants of the property database service.
 * @author SrinivasaRao Batthula
 *
 */
public class PropertyServiceConstants {

  public static final String CSV_EXTENSION = ".csv";
  
  public static final String TEMP_PATH = "java.io.tmpdir";

  public static final String BACK_SLASH = "/";
  
  public static final String UTF_8 = "UTF-8";
  
  public static final String JPG_EXTENSION = ".jpg";
  
  public static final String DELETED = "DELETED";
  
  public static final String UNDERSCORE = "_";

  
  private PropertyServiceConstants() {
    throw new AssertionError();
  }
}
