package com.sothebys.propertydb.util;

/**
 * Created by wael.awada on 5/1/17.
 */
public class AmazonS3CSVHandler extends AmazonS3FileHandler {

  private static final String EXTENSION = ".csv";

  public AmazonS3CSVHandler(String accessKey, String secretKey, String bucketName, String region) {
    super(accessKey, secretKey, bucketName, region);
  }

  @Override
  String getExtension() {
    return EXTENSION;
  }
}
