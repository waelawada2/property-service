package com.sothebys.propertydb.util;

import com.google.common.base.Strings;
import org.springframework.stereotype.Component;

/**
 * Created by wael.awada on 3/9/17.
 */
@Component
public class AlphaNumericIdGenerator {

  private static final String BASE29_CHARATCTERS = "23456789BCDFGHJKLMNPQRSTVWXYZ";

  public static String getAlphaNumbericId(Long number, String prefix) {
    StringBuilder sb = new StringBuilder();
    int j = (int) Math.ceil(Math.log(number) / Math.log(BASE29_CHARATCTERS.length()));
    for (int i = 0; i < j; i++) {
      // i goes to log base code.length() of num (using change of base formula)
      sb.append(BASE29_CHARATCTERS.charAt(number.intValue() % BASE29_CHARATCTERS.length()));
      number /= BASE29_CHARATCTERS.length();
    }

    return prefix.concat(Strings.padStart(sb.toString(), 8, '0'));
  }
}