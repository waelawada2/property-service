package com.sothebys.propertydb.util;

import com.amazonaws.services.s3.model.ObjectMetadata;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;

/**
 * Created by wael.awada on 3/24/17.
 */
@Slf4j
public class AmazonS3ImageHandler extends AmazonS3FileHandler {

  private static final String EXTENSION = ".jpg";

  public AmazonS3ImageHandler(String accessKey, String secretKey, String bucketName, String folder,
      String region) {
    super(accessKey, secretKey, bucketName, region);
    this.folder = folder;
  }

  @Override
  protected String getExtension() {
    return EXTENSION;
  }

  @Override
  protected ObjectMetadata createObjectMetadata(final long contentLength) {
    final ObjectMetadata objectMetadata = new ObjectMetadata();
    objectMetadata.setContentLength(contentLength);
    objectMetadata.setContentType(MediaType.IMAGE_JPEG_VALUE);
    objectMetadata.setCacheControl(CacheControl.noCache().getHeaderValue());
    return objectMetadata;
  }

}
