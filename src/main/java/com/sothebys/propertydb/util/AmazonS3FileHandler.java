package com.sothebys.propertydb.util;

import static java.time.format.DateTimeFormatter.ofPattern;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.sothebys.propertydb.constants.PropertyServiceConstants;
import com.sothebys.propertydb.exception.AlreadyExistsException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.exception.PropertyServiceException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.CacheControl;

/**
 * Created by wael.awada on 5/1/17.
 */
@Slf4j
public abstract class AmazonS3FileHandler {

  private final AmazonS3 s3Client;

  private final TransferManager transferManager;

  private String bucketName;

  String folder = "";

  public AmazonS3FileHandler(String accessKey, String secretKey, String bucketName, String region) {
    this.bucketName = bucketName;
    AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
    AWSCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(credentials);
    s3Client = AmazonS3ClientBuilder.standard()
        .withRegion(region).withCredentials(credentialsProvider).build();
    transferManager = TransferManagerBuilder.standard()
        .withS3Client(s3Client).withShutDownThreadPools(false).build();
  }

  /**
   * Uploads the given {@code file} into S3, using {@link TransferManager}.
   *
   * @param file The file to upload.
   * @return The generated file name.
   * @throws AlreadyExistsException if there already exists another object with the generated name.
   */
  public String uploadFile(File file) {
    String fileName = getFileName();
    if (s3Client.doesObjectExist(bucketName, folder + fileName)) {
      throw new AlreadyExistsException(String
          .format("A file with name=[%s] in S3 folder=[%s] already exists.", fileName, folder));
    }
    try {
      log.info("Uploading file with name=[{}] to S3 folder=[{}]", fileName, folder);
      transferManager.upload(new PutObjectRequest(bucketName, folder + fileName, file)
          .withMetadata(createObjectMetadata(file.length()))).waitForCompletion();
      transferManager.shutdownNow(false);
      return fileName;
    } catch (Exception e) {
      throw new PropertyServiceException(
          String.format("Uploading file=[%s] in S3 folder=[%s] failed.", fileName, folder), e);
    }
  }

  /**
   * Uploads the given {@code inputStream} into S3, using {@link TransferManager}.
   *
   * @param inputStream The file as a stream.
   * @param inputSize The file size in bytes.
   * @return The generated filename of the uploaded object.
   * @throws AlreadyExistsException if there already exists another object with the generated name.
   */
  public String uploadFile(InputStream inputStream, long inputSize) {
    String fileName = getFileName();
    if (s3Client.doesObjectExist(bucketName, folder + fileName)) {
      throw new AlreadyExistsException(String
          .format("A file with name=[%s] in S3 folder=[%s] already exists.", fileName, folder));
    }
    return uploadFile(inputStream, inputSize, fileName);
  }

  /**
   * Uploads the given {@code inputStream} into S3, using {@link TransferManager}
   * (may already exist).
   *
   * @param inputStream The file as a stream.
   * @param inputSize The file size in bytes.
   * @param fileName The file name to save.
   * @return The file name of the uploaded object.
   */
  public String uploadFile(InputStream inputStream, long inputSize, String fileName) {
    try {
      log.info("Uploading file with name=[{}] to S3 folder=[{}]", fileName, folder);
      transferManager
          .upload(bucketName, folder + fileName, inputStream, createObjectMetadata(inputSize))
          .waitForCompletion();
      transferManager.shutdownNow(false);
      return fileName;
    } catch (Exception e) {
      throw new PropertyServiceException(
          String.format("Uploading file=[%s] in S3 folder=[%s] failed.", fileName, folder), e);
    } finally {
      IOUtils.closeQuietly(inputStream);
    }
  }

  protected ObjectMetadata createObjectMetadata(final long contentLength) {
    final ObjectMetadata objectMetadata = new ObjectMetadata();
    objectMetadata.setContentLength(contentLength);
    objectMetadata.setCacheControl(CacheControl.noCache().getHeaderValue());
    return objectMetadata;
  }

  /**
   * Delete an object identified by the provided {@code key} in S3.
   *
   * @param key The key of the object.
   * @throws IllegalStateException If the object could not be deleted.
   */
  public void deleteFile(String key) {
    try {
      s3Client.deleteObject(bucketName, folder + key);
      log.info("Removed image " + folder + key + " successfully from the S3 bucket");
    } catch (SdkClientException e) {
      throw new IllegalStateException(String.format("Image '%s' could not be deleted in S3", key),
          e);
    }
  }

  public Pair<InputStream, Long> getFileContent(String imageName) {
    try {
      S3Object s3Object = s3Client.getObject(bucketName, folder + imageName);
      long imageSizeInBytes = s3Object.getObjectMetadata().getContentLength();
      return Pair.of(s3Object.getObjectContent(), imageSizeInBytes);
    } catch (SdkClientException e) {
      log.error("SDK Client Exception", e);
      throw new NotFoundException(String.format("File named '%s' doesn't exist", imageName), e);
    }
  }

  private String getFileName() {
    return UUID.randomUUID() + getExtension();
  }

  public String getCSVFileName(String fileType, String originalFileName) {
    StringBuilder fileName = new StringBuilder();
    if (originalFileName.contains(".csv")) {
      originalFileName = originalFileName.replaceAll(".csv$", "-");
    }
    fileName.append(fileType).append('-').append(originalFileName)
        .append(LocalDateTime.now().format(ofPattern("yyyyMMddHHmmss"))).append(getExtension());
    return fileName.toString();
  }

  abstract String getExtension();

  /**
   * This method used to read the csv file from tmp and stores file into S3
   *
   * @param fileName of the export data csv file name
   * @throws PropertyServiceException If the file could not be store into s3.
   */
  public void uploadExportDataFileIntoS3(final String fileName) {
    String tmpPath = System.getProperty(PropertyServiceConstants.TEMP_PATH);
    File exportCSVDataFile = null;
    if (!tmpPath.endsWith(File.separator)) {
      tmpPath = tmpPath + File.separator;
    }
    if (StringUtils.isBlank(fileName)) {
      throw new PropertyServiceException("Property export file name not valid");
    }
    try {
      exportCSVDataFile = new File(new File(tmpPath), fileName);
      if (exportCSVDataFile.exists()) {
        s3Client.putObject(new PutObjectRequest(bucketName, folder + fileName, exportCSVDataFile)
            .withCannedAcl(CannedAccessControlList.PublicRead));
        log.info(String.format("Successfully uploaded file '%s'", fileName));
      } else {
        log.error(String.format("Export CSV File '%s' doesn't exist", exportCSVDataFile));
      }
    } catch (Exception exp) {
      throw new PropertyServiceException(
          String.format("Exception occured while store csv file '%s' on S3 bucket", fileName), exp);
    } finally {
      if (exportCSVDataFile != null) {
        log.info(String.format("Remove file '%s' from tmp directory", exportCSVDataFile));
        exportCSVDataFile.delete();
      }
    }

  }

  /**
   * This Method used to download image from website s3
   *
   * @param imageName of the image
   * @return InputStream of the image
   */
  public Pair<InputStream, Long> downLoadS3Image(String imageName) {
    try {
      S3Object s3Object = s3Client.getObject(bucketName, folder + imageName);
      long imageSizeInBytes = s3Object.getObjectMetadata().getContentLength();
      if (imageSizeInBytes <= 0) {
        s3Object.close();
        throw new PropertyServiceException("Image file not valid");
      }
      return Pair.of(s3Object.getObjectContent(), imageSizeInBytes);
    } catch (SdkClientException e) {
      log.error("SDK Client Exception", e);
      throw new NotFoundException(
          String.format("Website Image named '%s' doesn't exist", imageName), e);
    } catch (IOException e) {
      log.error("IO Exception", e);
      throw new NotFoundException(String
          .format("Website Image named '%s' cannot be downloaded due to IO exception", imageName),
          e);
    }
  }

}
