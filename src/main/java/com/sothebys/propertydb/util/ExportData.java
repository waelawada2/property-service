package com.sothebys.propertydb.util;

import static com.sothebys.propertydb.configuration.views.ArtistCsvView.ARTIST_HEADER;
import static com.sothebys.propertydb.configuration.views.PropertyCsvView.PROPERTY_HEADER;

import com.sothebys.propertydb.configuration.views.ArtistCsvView;
import com.sothebys.propertydb.configuration.views.PropertyCsvView;
import com.sothebys.propertydb.constants.PropertyServiceConstants;
import com.sothebys.propertydb.dto.ArtistDTO;
import com.sothebys.propertydb.dto.PropertyDTO;
import com.sothebys.propertydb.mapper.ArtistModelMapper;
import com.sothebys.propertydb.mapper.PropertyModelMapper;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import liquibase.util.csv.CSVWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ExportData {

  private static final int[] UTF_BOM = new int[]{0xef, 0xbb, 0xbf};

  private static final String[] DELETE_SAVE_LIST_OBJECT_HEADER = {"Object_Code", "EOS_Object_ID"};
  
  private static final String DELETED = "DELETED";
    
  @Autowired
  PropertyModelMapper propertyModelMapper;

  @Autowired
  ArtistModelMapper artistModelMapper;

  /**
   * This method used to write the property data into csv file and stores file into tmp
   *
   * @param properties list of active properties
   * @param fileName name of the property csv file
   * @throws Exception java.lang.Exception When write fails property data into csv file.
   */
  public void buildPropertyCsvDocument(List<PropertyDTO> properties, String fileName)
      throws Exception {
    String tmpPath = System.getProperty(PropertyServiceConstants.TEMP_PATH);
    if (!tmpPath.endsWith(File.separator)) {
      tmpPath = tmpPath + File.separator;
    }
    log.info(String
        .format(" Building property CSV Document FileOutputStream to path '%s' to file '%s' : ",
            tmpPath, fileName));
    try (FileOutputStream fileOutputStream = new FileOutputStream(
        tmpPath + fileName); CSVWriter csvWriter = new CSVWriter(
        new OutputStreamWriter(fileOutputStream), CSVWriter.DEFAULT_SEPARATOR,
        CSVWriter.DEFAULT_QUOTE_CHARACTER, CSVWriter.DEFAULT_QUOTE_CHARACTER)) {
      fileOutputStream.write(UTF_BOM[0]);
      fileOutputStream.write(UTF_BOM[1]);
      fileOutputStream.write(UTF_BOM[2]);
      csvWriter.writeNext(PROPERTY_HEADER);
      properties.stream().map(PropertyCsvView::exportPropertyData).forEach(csvWriter::writeNext);
    }
  }

  /**
   * This method used to write the Artist data into csv file and stores file into tmp
   *
   * @param artists list of active Artists
   * @param fileName name of the artist csv file
   * @throws Exception java.lang.Exception When write fails property data into csv file.
   */
  public void buildArtistCsvDocument(List<ArtistDTO> artists, String fileName)
      throws Exception {
    String tmpPath = System.getProperty(PropertyServiceConstants.TEMP_PATH);
    if (!tmpPath.endsWith(File.separator)) {
      tmpPath = tmpPath + File.separator;
    }
    log.info(String
        .format(" Building Artist CSV Document FileOutputStream to path '%s' to file '%s' : ",
            tmpPath, fileName));
    try (FileOutputStream fileOutputStream = new FileOutputStream(tmpPath + fileName);
        CSVWriter csvWriter = new CSVWriter(new OutputStreamWriter(fileOutputStream))) {
      fileOutputStream.write(UTF_BOM[0]);
      fileOutputStream.write(UTF_BOM[1]);
      fileOutputStream.write(UTF_BOM[2]);
      csvWriter.writeNext(ARTIST_HEADER);
      artists.stream().map(ArtistCsvView::exportArtistData).forEach(csvWriter::writeNext);
    }
  }
  
  /**
   * This method used to write the SavedList property data into csv file and stores file into tmp
   * 
   * @param savedList list of active saveList properties
   * @param fileName name of the savedList csv file
   * @throws Exception java.lang.Exception When write fails property data into csv file.
   */
  public void buildDeleteSaveListObjectCsvDocument(List<String> saveListPropertyIds,
      String fileName) throws Exception {
    String tmpPath = System.getProperty(PropertyServiceConstants.TEMP_PATH);
    if (!tmpPath.endsWith(File.separator)) {
      tmpPath = tmpPath + File.separator;
    }
    log.info(
        String.format(" Building Artist CSV Document FileOutputStream to path '%s' to file '%s' : ",
            tmpPath, fileName));

    try (FileOutputStream fileOutputStream = new FileOutputStream(tmpPath + fileName);
        CSVWriter csvWriter = new CSVWriter(new OutputStreamWriter(fileOutputStream))) {
      csvWriter.writeNext(DELETE_SAVE_LIST_OBJECT_HEADER);
      saveListPropertyIds.stream().map(ExportData::buldDeleteSaveListPropetyIdsData)
          .forEach(csvWriter::writeNext);
    }
  }

  private static String[] buldDeleteSaveListPropetyIdsData(String propertyId) {
    int index = 0;
    String[] data = new String[DELETE_SAVE_LIST_OBJECT_HEADER.length];
    data[index++] = propertyId;
    data[index++] = DELETED;
    return data;
  }

}
