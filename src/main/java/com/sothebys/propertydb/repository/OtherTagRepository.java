package com.sothebys.propertydb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.sothebys.propertydb.model.OtherTag;

@Repository("otherTagRepository")
public interface OtherTagRepository extends JpaRepository<OtherTag, Long>, JpaSpecificationExecutor<OtherTag> {

  OtherTag findByName(String name);

	List<OtherTag> findAllByName(String name);

	OtherTag findById(Long id);

	List<OtherTag> findByNameContaining(String name);

	List<OtherTag> findAllByOrderByNameAsc();
}
