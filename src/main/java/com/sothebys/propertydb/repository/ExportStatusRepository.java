package com.sothebys.propertydb.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sothebys.propertydb.model.ExportStatus;

/**
 * This interface is used to perform all CRUD operations by using JPA repository
 *
 * @author Srinivasarao.Batthula
 *
 */
@Repository("exportStatusRepository")
public interface ExportStatusRepository extends JpaRepository<ExportStatus, Long> {

  ExportStatus findByExportId(String exportId);
}
