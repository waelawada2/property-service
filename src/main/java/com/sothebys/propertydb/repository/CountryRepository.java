package com.sothebys.propertydb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sothebys.propertydb.model.Country;

/**
 * This interface is used to perform all CRUD operations by using JPA repository
 *
 * @author Srinivasarao.Batthula
 *
 */
@Repository("countryRepository")
public interface CountryRepository extends JpaRepository<Country, Long> {
  @Query("Select country from Country country where country.countryName like :countryName%")
  List<Country> findByCountryNameContaining(@Param("countryName") String countryName);

  Country findByCountryName(String countryName);

  @Query(value = "select c.* from country c left join artist_country a_c on a_c.country_id = c.id "
      + "where a_c.country_id is not null and a_c.country_id = ?1", nativeQuery = true)
  Country findCountryByCountryId(Long countryId);

  @Query(value = "select c.* from country c left join property p on p.flags_country_id = c.id "
      + "where p.flags_country_id is not null and p.flags_country_id=?1", nativeQuery = true)
  Country findCountryByFlagsCountryId(Long countryId);

  @Modifying(clearAutomatically = true)
  @Query(
      value = "update artist_country set country_id=:mergeToCountryId where country_id = :mergeFromCountryId and artist_id not in (select a.artist_id from (select artist_id from artist_country where country_id = :mergeToCountryId) a)",
      nativeQuery = true)
  void updateArtistCountry(@Param("mergeToCountryId") Long mergeToCountryId,
      @Param("mergeFromCountryId") Long mergeFromCountryId);
  
  @Modifying(clearAutomatically = true)
  @Query(
      value = "update property set flags_country_id =:mergeToCountryId where flags_country_id = :mergeFromCountryId",
      nativeQuery = true)
  void updatePropertyCountry(@Param("mergeToCountryId") Long mergeToCountryId,
      @Param("mergeFromCountryId") Long mergeFromCountryId);
  
  @Modifying(clearAutomatically = true)
  @Query(
      value = "delete from artist_country where country_id =:mergeFromCountryId",
      nativeQuery = true)
  void deleteArtistCountry(
      @Param("mergeFromCountryId") Long mergeFromCountryId);
}
