package com.sothebys.propertydb.repository.support;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CustomRepository<T> {
  void refresh(T t);
}
