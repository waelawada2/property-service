package com.sothebys.propertydb.repository;

import com.sothebys.propertydb.model.ImagePosition;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("imagePositionRepository")
public interface ImagePositionRepository extends JpaRepository<ImagePosition, Long> {

	ImagePosition findByName(String name);

	List<ImagePosition> findAllByOrderByNameAsc();

	@Query(value = "select i.* from image_position i left join property_image_position p_i on p_i.image_position_id = i.id "
			+ "where p_i.image_position_id is not null and p_i.image_position_id = ?1", nativeQuery = true)
	ImagePosition findByProperty(Long imagePostionId);
	
    List<ImagePosition> findByNameContaining(String name);
    
    @Modifying
    @Query(
        value = "update property_image_position set image_position_id =:imagePositionToId where image_position_id =:imagePositionFromId",
        nativeQuery = true)
    int updatePropertyImagePosition(@Param("imagePositionToId") Long imagePositionToId,
        @Param("imagePositionFromId") Long imagePositionFromId);
}
