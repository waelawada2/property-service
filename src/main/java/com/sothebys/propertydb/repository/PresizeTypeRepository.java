/**
 *
 */
package com.sothebys.propertydb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sothebys.propertydb.model.PresizeType;

/**
 * This interface is used to perform all CRUD operations by using JPA repository
 *
 * @author Srinivasarao.Batthula
 *
 */
@Repository("presizeTypeRepository")
public interface PresizeTypeRepository extends JpaRepository<PresizeType, Long> {
  public PresizeType findByName(String name);
  
  List<PresizeType> findAllByOrderByNameAsc();
}
