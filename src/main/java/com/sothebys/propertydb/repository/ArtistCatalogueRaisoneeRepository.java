package com.sothebys.propertydb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sothebys.propertydb.model.ArtistCatalogueRaisonee;

/**
 * Created by waelawada on 3/26/17.
 */
public interface ArtistCatalogueRaisoneeRepository
    extends JpaRepository<ArtistCatalogueRaisonee, Long> {
  ArtistCatalogueRaisonee findArtistCatalogueRaisoneesById(long catalogueRaisoneeId);
}
