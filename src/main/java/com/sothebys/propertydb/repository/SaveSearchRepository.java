package com.sothebys.propertydb.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.sothebys.propertydb.model.SaveSearch;

/**
 * This interface is used to perform all CRUD operations by using JPA repository
 *
 * @author Srinivasarao.Batthula
 *
 */
@Repository("saveSearchRepository")
public interface SaveSearchRepository
    extends JpaRepository<SaveSearch, Long>, JpaSpecificationExecutor<SaveSearch> {

  Page<SaveSearch> findSaveSearchesByUserId(Pageable pageRequest, Long userId);

  SaveSearch findByUserIdAndExternalId(long userId, String externalId);
  
  SaveSearch findByExternalId(String externalId);

  void deleteById(long id);

}
