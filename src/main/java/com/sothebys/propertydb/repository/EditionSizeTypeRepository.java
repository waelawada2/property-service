package com.sothebys.propertydb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sothebys.propertydb.model.EditionSizeType;

/**
 * This interface is used to perform all CRUD operations by using JPA repository
 *
 * @author Srinivasarao.Batthula
 *
 */
@Repository("editionSizeTypeRepository")
public interface EditionSizeTypeRepository extends JpaRepository<EditionSizeType, Long> {
  public EditionSizeType findByName(String name);

  List<EditionSizeType> findAllByOrderByIdAsc();
}
