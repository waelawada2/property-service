/**
 *
 */
package com.sothebys.propertydb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sothebys.propertydb.model.Category;

/**
 * This interface is used to perform all CRUD operations by using JPA repository
 *
 * @author Srinivasarao.Batthula
 */
@Repository
public interface CategoryRepositry extends JpaRepository<Category, Long> {

  void deleteById(Long childId);

  List<Category> findByParentId(Long parentId);

  Category findByIdAndParentId(Long id, Long parentId);

  List<Category> findByName(String categoryName);

  List<Category> findByNameAndParentIsNull(String name);

  List<Category> findByNameAndParentName(String name, String parentName);

  @Query(value = "select c.* from category c left join artist_category a_c on a_c.category_id = c.id where a_c.category_id is not null and a_c.category_id = ?1", nativeQuery = true)
  List<Category> findByArtist(Long categoryId);
  
  List<Category> findDistinctByNameContaining(String categoryName);
  
  @Modifying
  @Query(
      value = "update artist_category a_c set a_c.category_id =:mergeToCategoryId where a_c.category_id = :mergeFromCategoryId and artist_id not in (select a.artist_id from (select artist_id from artist_category where category_id = :mergeToCategoryId) a)",
      nativeQuery = true)
  void updateArtistCategory(@Param("mergeToCategoryId") Long mergeToCategoryId,
      @Param("mergeFromCategoryId") Long mergeFromCategoryId);
  
  @Modifying(clearAutomatically = true)
  @Query(
      value = "delete from artist_category where category_id =:mergeFromCategoryId",
      nativeQuery = true)
  void deleteArtistCategory(
      @Param("mergeFromCategoryId") Long mergeFromCategoryId);

}
