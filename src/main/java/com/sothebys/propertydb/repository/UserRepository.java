package com.sothebys.propertydb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.sothebys.propertydb.model.User;

/**
 * Created by wael.awada on 5/15/17.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> ,JpaSpecificationExecutor<User> {

  User findByEmail(String email);

  User findById(Long id);

  User findByKeycloakUserId(String keycloakUserId);

  User findByUserName(String userName);
}