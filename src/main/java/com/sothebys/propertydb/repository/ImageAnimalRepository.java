package com.sothebys.propertydb.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sothebys.propertydb.model.ImageAnimal;

@Repository("imageAnimalRepository")
public interface ImageAnimalRepository extends JpaRepository<ImageAnimal, Long>, JpaSpecificationExecutor<ImageAnimal> {

	ImageAnimal findByName(String name);

	List<ImageAnimal> findAllByName(String name);

	ImageAnimal findById(Long id);

	Page<ImageAnimal> findAllByOrderByNameAsc(Specification<ImageAnimal> spec, Pageable pageable);

	List<ImageAnimal> findAllByOrderByNameAsc();

	List<ImageAnimal> findByNameContaining(String name);
	
  @Modifying
	@Query(value = "update property_image_animal set image_animal_id =:imageAnimalToId where image_animal_id =:imageAnimalFromId", nativeQuery = true)
  int updatePropertyImageAnimals(@Param("imageAnimalToId") Long imageAnimalToId,
      @Param("imageAnimalFromId") Long imageAnimalFromId);

	@Query(value = "select count(1) from property_image_animal pia where pia.image_animal_id = ?1", nativeQuery = true)
	int countImageAnimals(Long id);
}
