package com.sothebys.propertydb.repository;

import com.sothebys.propertydb.model.ImageFigure;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("imageFigureRepository")
public interface ImageFigureRepository extends JpaRepository<ImageFigure, Long> {

	ImageFigure findByName(String name);

	List<ImageFigure> findAllByOrderByNameAsc();
}
