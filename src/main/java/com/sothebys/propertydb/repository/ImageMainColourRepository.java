package com.sothebys.propertydb.repository;

import com.sothebys.propertydb.model.ImageMainColour;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("imageMainColourRepository")
public interface ImageMainColourRepository extends JpaRepository<ImageMainColour, Long> {

	ImageMainColour findByName(String name);

	List<ImageMainColour> findAllByOrderByNameAsc();

	@Query(value =
			"select i.* from image_main_colour i left join property_image_main_colour p_i on p_i.image_main_colour_id = i.id "
					+ "where p_i.image_main_colour_id is not null and p_i.image_main_colour_id = ?1", nativeQuery = true)
	ImageMainColour findByProperty(Long imageMainColourId);
	
  @Modifying
  @Query(
      value = "update property_image_main_colour set image_main_colour_id =:imageMainColourToId where image_main_colour_id =:imageMainColourFromId",
      nativeQuery = true)
  int updatePropertyImageMainColor(@Param("imageMainColourToId") Long imageMainColourToId,
      @Param("imageMainColourFromId") Long imageMainColourFromId);
  
}