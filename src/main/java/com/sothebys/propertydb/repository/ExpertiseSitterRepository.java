package com.sothebys.propertydb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sothebys.propertydb.model.ExpertiseSitter;

@Repository("expertiseSitterRepository")
public interface ExpertiseSitterRepository
		extends JpaRepository<ExpertiseSitter, Long>, JpaSpecificationExecutor<ExpertiseSitter> {


	ExpertiseSitter findById(Long id);


	@Query(value = "select count(1) from property_expertise_sitter pes where pes.expertise_sitter_id = ?1", nativeQuery = true)
	int countExpertiseSitters(Long id);

	List<ExpertiseSitter> findAll();

	@Query(value = "select e.* from expertise_sitter e left join property_expertise_sitter p_e on p_e.expertise_sitter_id = e.id where p_e.expertise_sitter_id is not null and p_e.expertise_sitter_id = ?1", nativeQuery = true)
	ExpertiseSitter findByProperty(Long expertiseSitterId);

	List<ExpertiseSitter> findDistinctExpertiseSitterByLastNameAndFirstName(String lastName, String firstName);

	ExpertiseSitter findByLastNameAndFirstNameIn(String lastName, String firstName);
	
  @Modifying
  @Query(
      value = "update property_expertise_sitter set expertise_sitter_id =:expertSitterToId where expertise_sitter_id =:expertSitterFromId",
      nativeQuery = true)
  int updatePropertyExpertiseSitter(@Param("expertSitterToId") Long expertSitterToId,
      @Param("expertSitterFromId") Long expertSitterFromId);

}
