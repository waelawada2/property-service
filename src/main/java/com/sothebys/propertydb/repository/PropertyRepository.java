package com.sothebys.propertydb.repository;

import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.search.Status;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyRepository
    extends JpaRepository<Property, Long>, JpaSpecificationExecutor<Property> {

  List<Property> findByCategoryId(Long categoryId);

  Property findByExternalId(String externalId);

  Page<Property> findPropertiesByStatus(Pageable pageable,
      @Param(value = "status") Status status);

  List<Property> findPropertiesByStatus(@Param(value = "status") Status status);

  Page<Property> findPropertiesByExternalIdIn(List<String> propertyIds, Pageable pageable);
  
  long countPropertiesByStatus(Status status);

  Long countByArtistsInAndStatus(List<Artist> artists, Status status);
  
  @Modifying
  @Query(value = "update property set status =:status where external_id in :ids",
      nativeQuery = true)
  int updatePropertyIds(@Param("ids") List<String> ids, @Param("status") String status);

  @Query(value = "SELECT image_path FROM property WHERE external_id =:id", nativeQuery = true)
  String findPropertyImagePath(@Param("id") String id);

  List<Property> findByPresizeTypeId(Long presizeTypeId);

  List<Property> findByEditionEditionSizeTypeId(Long editionSizeTypeId);
  
  @Modifying(clearAutomatically = true)
  @Query(
      value = "update property set category_id =:categoryToId where category_id =:categoryFromId",
      nativeQuery = true)
  int updatePropertyCategories(@Param("categoryToId") Long categoryToId,
      @Param("categoryFromId") Long categoryFromId);
  
  @Modifying(clearAutomatically = true)
  @Query(
      value = "update property set presize_type_id =:presizeTypeToId where presize_type_id =:presizeTypeFromId",
      nativeQuery = true)
  int updatePropertyPresizeTypes(@Param("presizeTypeToId") Long presizeTypeToId,
      @Param("presizeTypeFromId") Long presizeTypeFromId);
  
  @Modifying(clearAutomatically = true)
  @Query(
      value = "update property set edition_size_type_id =:editionSizeTypeToId where edition_size_type_id =:editionSizeTypeFromId",
      nativeQuery = true)
  int updatePropertyEditionSizeTypes(@Param("editionSizeTypeToId") Long editionSizeTypeToId,
      @Param("editionSizeTypeFromId") Long editionSizeTypeFromId);
  
  List<Property> findPropertiesByExternalIdIn(List<String> propertyIds);
}
