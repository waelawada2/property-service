/**
 *
 */
package com.sothebys.propertydb.repository;

import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.search.Status;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.sothebys.propertydb.model.Country;

/**
 * This interface is used to perform all CRUD operations by using JPA repository
 *
 * @author Srinivasarao.Batthula
 *
 */
@Repository("artistRepository")
public interface ArtistRepository
    extends JpaRepository<Artist, Long>, JpaSpecificationExecutor<Artist> {

  Artist findByExternalId(String externalId);

  Artist findByUlanId(Long ulanId);

  Page<Artist> findDistinctArtistsByStatus(Pageable pageable, Status status);

  List<Artist> findDistinctArtistsByStatus(Status status);

  long countDistinctArtistsByStatus(Status status);
  
  List<Artist> findByExternalIdIsNotNull();
  
  List<Artist> findDistinctArtistByStatusAndLastNameAndFirstNameAndBirthYearAndDeathYearAndGenderAndCountriesIn(Status status,
      String lastName, String firstName, int birthYear, int deathYear, String gender,
      List<Country> countries);
  
  List<Artist> findDistinctArtistByStatusAndLastNameAndFirstNameAndGenderAndCountriesIn(
      Status status, String lastName, String firstName, String gender, List<Country> countries);
  
  List<Artist> findDistinctArtistByStatusAndLastNameAndFirstNameAndBirthYearAndGender(Status status,
      String lastName, String firstName, int birthYear, String gender);
  
  List<Artist> findDistinctArtistByStatusAndLastNameAndFirstNameAndBirthYearAndGenderAndCountriesIn(
      Status status, String lastName, String firstName, int birthYear, String gender,
      List<Country> countries);
}
