package com.sothebys.propertydb.repository;

import com.sothebys.propertydb.model.ImageSubject;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("imageSubjectRepository")
public interface ImageSubjectRepository extends JpaRepository<ImageSubject, Long> {

	ImageSubject findByName(String name);

	ImageSubject findById(Long id);

	List<ImageSubject> findAllByName(String name);

	List<ImageSubject> findByNameContaining(String name);

	List<ImageSubject> findAllByOrderByNameAsc();

	@Query(value = "select i.* from image_subject i left join property_image_subject p_i on p_i.image_subject_id = i.id "
			+ "where p_i.image_subject_id is not null and p_i.image_subject_id = ?1", nativeQuery = true)
	ImageSubject findByProperty(Long imagePostionId);
	
  @Modifying
  @Query(
      value = "update property_image_subject set image_subject_id =:imageSubjectToId where image_subject_id =:imageSubjectFromId",
      nativeQuery = true)
  int updatePropertyImageSubject(@Param("imageSubjectToId") Long imageSubjectToId,
      @Param("imageSubjectFromId") Long imageSubjectFromId);
}
