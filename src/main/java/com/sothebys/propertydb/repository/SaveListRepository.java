package com.sothebys.propertydb.repository;

import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.SaveList;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.repository.support.CustomRepository;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * This interface is used to perform all CRUD operations by using JPA repository
 *
 * @author Srinivasarao.Batthula
 */
@Repository("saveListRepository")
public interface SaveListRepository
    extends JpaRepository<SaveList, Long>, JpaSpecificationExecutor<SaveList>,
    CustomRepository<SaveList> {

  SaveList findByUserAndExternalId(User user, String externalId);

  Page<SaveList> findDistinctByUserOrSharedUsers(User user, User users, Pageable pageRequest);

  SaveList findByExternalIdAndSharedUsers(String externalId, User user);

  Page<SaveList> findDistinctByNameContainingAndUserOrNameContainingAndSharedUsers(
      String saveListName, User user, String name,
      User users, Pageable pageRequest);

  Page<SaveList> findAll(Pageable pageable);
  
  @Query(
      value = "Select saveList.* from save_list saveList where saveList.name like %:name% and saveList.id in (select save_list_id from save_list_property where property_id in (select id from property where property_id= id and status='Active'))",
      nativeQuery = true)
  List<SaveList> findDistinctByNameContaining(@Param("name") String name);

  SaveList findByExternalId(String externalId);

  SaveList findSharedUsersByExternalId(String externalId);

  @Modifying
  @Query(value = "INSERT INTO save_list_property "
      + "SELECT :saveListId, id FROM property "
      + "WHERE external_id IN :propertyExternalIds "
      + "AND id NOT IN (SELECT property_id FROM save_list_property WHERE save_list_id = :saveListId)",
      nativeQuery = true)
  int addPropertiesToSaveList(@Param("saveListId") Long saveListId,
      @Param("propertyExternalIds") List<String> propertyExternalIds);

  @Modifying
  @Query(value = "insert into save_list_property values (?1 ,?2)", nativeQuery = true)
  void addPropertyToSaveList(Long saveListId, Long propertyId);


  @Query(value = "select count(*) from save_list_property where save_list_id = ?1 and property_id = ?2", nativeQuery = true)
  Integer containsPropertyId(Long saveListId, Long propertyId);

  @Query(value = "select * from save_list where id in (select save_list_id from save_list_property where property_id = ?1)", nativeQuery = true)
  List<SaveList> findListsConstainingProperty(Long propertyId);


  @Modifying
  @Query(value = "update save_list_property set property_id = ?3 where save_list_id = ?1 and property_id = ?2", nativeQuery = true)
  void updateSavedListProperty(Long savedListId, Long oldPropertyId, Long newPropertyId);

  @Modifying
  @Query(value = "delete from save_list_property where property_id = ?2 and save_list_id = ?1", nativeQuery = true)
  void deletePropertyFromSavedList(Long savedListId, Long propertyId);

  List<Property> findPropertiesById(Long id);
  
  @Query(
      value = "select * from save_list where id in (select save_list_id from save_list_property where property_id in (select id from property where property_id= id and status='Active')) \n#pageable\n",
      countQuery = "select count(name) from save_list where id in (select save_list_id from save_list_property where property_id in (select id from property where property_id= id and status='Active')) \n#pageable\n",
      nativeQuery = true)
  Page<SaveList> findSaveListActveProperties(Pageable pageable);
  
  @Query(
      value = "select name from save_list where id in (select save_list_id from save_list_property where property_id in (select id from property where property_id= id and status='Active'))",
      nativeQuery = true)
  List<String> findAdminSaveListActveProperties();
  
  @Query(value= "Select saveList.* from save_list saveList where saveList.name like %:name% and saveList.id in (select save_list_id from save_list_property where property_id in (select id from property where property_id= id and status='Active'))",nativeQuery = true)
  SaveList findDistinctSaveListByNameContaining(@Param("name") String name);
  
  @Query(
      value = "select count(id) from property where status='Active' and  id in (select property_id from save_list_property where save_list_id = (select id from save_list where external_id = ?1))",
      nativeQuery = true)
  Long findSaveListPropertiesCountById(String externalId);
  
  @Query(
      value = "select count(id) from property where status='Active' and (research_complete_check IS NULL or research_complete_check in (0)) and id in (select property_id from save_list_property where save_list_id=(select id from save_list where external_id = ?1))",
      nativeQuery = true)
  Long findSaveListResearchStatusById(String externalId);

}
