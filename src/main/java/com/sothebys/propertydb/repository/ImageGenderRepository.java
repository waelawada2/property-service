package com.sothebys.propertydb.repository;

import com.sothebys.propertydb.model.ImageGender;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("imageGenderRepository")
public interface ImageGenderRepository extends JpaRepository<ImageGender, Long> {

	ImageGender findByName(String name);

	List<ImageGender> findAllByOrderByNameAsc();
}
