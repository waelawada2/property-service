/**
 *
 */
package com.sothebys.propertydb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sothebys.propertydb.model.EditionType;

/**
 * This interface is used to perform all CRUD operations by using JPA repository
 *
 * @author Srinivasarao.Batthula
 *
 */
@Repository("editionTypeRepository")
public interface EditionTypeRepository extends JpaRepository<EditionType, Long> {
  public EditionType findByName(String name);
	List<EditionType> findAllByOrderBySortCodeAsc(); 
	
	@Query("SELECT max(et.sortCode)+1 FROM EditionType et")
	Long getMaxSortCode();
}
