package com.sothebys.propertydb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.dto.CountryDTO;
import com.sothebys.propertydb.mapper.CountryModelMapper;
import com.sothebys.propertydb.model.Country;
import com.sothebys.propertydb.service.CountryService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * REST controller that exposes country details.
 *
 * @author SrinivasaRao.Batthula
 *
 */
@RestController
@RequestMapping("/countries")
@CrossOrigin(origins = "*", maxAge = 3600)
public class CountryController {
  @Autowired
  private CountryModelMapper countryModelMapper;

  @Autowired
  private CountryService countryService;

  /**
   * Get all countries.
   */
  @ApiOperation(value = "Retrieving all the countries")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping
  public ResponseEntity<List<CountryDTO>> findAllCountries() {
    final List<Country> countries = countryService.findAllCountries();
    final List<CountryDTO> countryDTOs = countryModelMapper.mapToDTO(countries);

    return new ResponseEntity<>(countryDTOs, HttpStatus.OK);
  }

  /**
   * Get all countries by name.
   */
  @ApiOperation(value = "Retrieving all the countries by name")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/name")
  public ResponseEntity<List<CountryDTO>> findAllCountriesByName(
      @RequestParam("name") String name) {
    final List<Country> countries = countryService.findAllCountriesByName(name);
    final List<CountryDTO> countryDTOs = countryModelMapper.mapToDTO(countries);

    return new ResponseEntity<>(countryDTOs, HttpStatus.OK);
  }


  /**
   * Create a new Country.
   */
  @ApiOperation(value = "Create Country")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Invalid input message"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<CountryDTO> createCountry(@RequestBody final CountryDTO countryDTO) {
    final Country country = countryModelMapper.map(countryDTO, Country.class);

    final Country createdCountry = countryService.createCountry(country);
    final CountryDTO createdCountryDTO = countryModelMapper.map(createdCountry, CountryDTO.class);
    HttpHeaders headers = RestControllerHelper.createHeadersWithLocation(createdCountryDTO.getId());
    return new ResponseEntity<>(createdCountryDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Update a country identified by {@code countryId}.
   */
  @ApiOperation(value = "Updating country object deatils")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{country-id}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<CountryDTO> updateCountry(@RequestBody final CountryDTO countryDTO,
      @PathVariable("country-id") Long countryId) {
    final Country country = countryModelMapper.map(countryDTO, Country.class);
    final Country updatedCountry = countryService.updateCountryById(countryId, country);
    final CountryDTO updatedCountryDTO = countryModelMapper.map(updatedCountry, CountryDTO.class);

    return new ResponseEntity<>(updatedCountryDTO, HttpStatus.OK);
  }

  /**
   * Delete an existing country identified by the given {@code countryId}
   */
  @ApiOperation(value = "Delete country based on given countryId")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully country deleted"),
      @ApiResponse(code = 404, message = "Country id not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/{country-id}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void deleteCountry(@PathVariable("country-id") Long countryId) {
    countryService.deleteCountryById(countryId);
  }

  /**
   * Merge a Country identified by {@code CountryId}.
   */
  @ApiOperation(value = "Merge Country")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Country not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{mergeToId}", params = {"mergeFromId"},
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<CountryDTO> mergeCountry(
      @PathVariable("mergeToId") final String mergeToId,
      @RequestParam("mergeFromId") final String mergeFromId) {
    final Country mergedCountry = 
        countryService.mergeCountries(Long.valueOf(mergeToId), Long.valueOf(mergeFromId));
    final CountryDTO mergedCountryDTO = countryModelMapper.map(mergedCountry, CountryDTO.class);
    return new ResponseEntity<>(mergedCountryDTO, HttpStatus.OK);
  }
  
}
