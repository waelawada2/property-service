/*
 * -------------------------------------------------------------------------
 *
 * (C) Copyright / Sotheby's. All rights reserved. The contents of this file represent Sotheby's
 * trade secrets and are confidential. Use outside of Sotheby's is prohibited and in violation of
 * copyright law.
 *
 * -------------------------------------------------------------------------
 */
package com.sothebys.propertydb.controller;

import com.sothebys.propertydb.controller.support.AdditionalMediaType;
import com.sothebys.propertydb.controller.support.ApplicationViews;
import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.dto.PropertyDTO;
import com.sothebys.propertydb.dto.PropertyIdsRequestDto;
import com.sothebys.propertydb.dto.request.PropertyAddRequestDTO;
import com.sothebys.propertydb.dto.request.PropertyOwnerIdRequestDTO;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.exception.PropertyServiceException;
import com.sothebys.propertydb.mapper.PropertyModelMapper;
import com.sothebys.propertydb.model.LevelOfDetail;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.search.ClientSearchInputDTO;
import com.sothebys.propertydb.search.PollStatus;
import com.sothebys.propertydb.search.PropertySearchBuilder;
import com.sothebys.propertydb.search.PropertySearchInputDTO;
import com.sothebys.propertydb.search.PropertyUtil;
import com.sothebys.propertydb.service.ExportService;
import com.sothebys.propertydb.service.PropertyService;
import com.sothebys.propertydb.service.external.client.ClientService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.InputStreamResource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = "/properties", produces = MediaType.APPLICATION_JSON_VALUE)
@Controller
public class PropertyController {

  @Autowired
  PropertySearchBuilder propertySearchBuilder;

  @Autowired
  PropertyService propertyService;

  @Autowired
  ClientService clientService;

  @Autowired
  PropertyModelMapper propertyModelMapper;

  @Autowired
  @Qualifier("propertyExportService")
  ExportService exportService;

  /**
   * Create a new property.
   */
  @ApiOperation(value = "Create Property")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Invalid input message"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<PropertyDTO> createProperty(
      @Valid @RequestBody final PropertyAddRequestDTO propertyAddRequestDTO,
          @RequestAttribute(name = "requestUser") User user) {
    final Property property = propertyModelMapper.mapToEntity(propertyAddRequestDTO);
    final Property createdProperty;
    createdProperty = propertyService.createProperty(property, user);
    final PropertyDTO createdPropertyDTO = propertyModelMapper.mapToDTO(createdProperty);
    HttpHeaders headers =
        RestControllerHelper.createHeadersWithLocation(createdPropertyDTO.getId());
    return new ResponseEntity<>(createdPropertyDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Delete a property identified by {@code externalId}.
   */
  @ApiOperation(value = "Delete Property object based on given property id")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully deleted"),
      @ApiResponse(code = 404, message = "Property id not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/{external-id}")
  public void deleteProperty(@PathVariable("external-id") String externalId) {
    propertyService.deletePropertyByExternalId(externalId);
  }

  /**
   * Search for properties. Results are either the full property entities or only the property IDs,
   * depending on the value of the provided {@code lod} parameter.
   * <p/>
   * Note: Search is case insensitive.
   */
  @ApiOperation(value = "Search for properties based on input parameters")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Input parameter validation failed"),
      @ApiResponse(code = 500, message = "Server error"),
      @ApiResponse(code = 416, message = "Too many clients to filter by")})
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Page<?>> findProperties(
      @ModelAttribute final PropertySearchInputDTO propertyDto,
      @ModelAttribute final ClientSearchInputDTO clientDto, final Pageable pageRequest,
      @Valid @RequestParam(value = "lod", required = false,
          defaultValue = "MAX") LevelOfDetail lod, Authentication auth) {
    final Page<?> resultsPage;

    List<String> clientIds = clientService.clientIdsSearch(clientDto,
        (KeycloakSecurityContext) auth.getCredentials());
    if (!CollectionUtils.isEmpty(clientIds)) {
      propertyDto.setCurrentOwnerIds(clientIds);
    }

    boolean doSearch = PropertyUtil.validatePropertyDTO(propertyDto);
    if (doSearch) {
      Specification<Property> specification =
          propertySearchBuilder.createSpecificationBuilder(propertyDto).build();
      Page<Property> searchResultPage =
          propertyService.findBySearchTerm(specification, pageRequest);
      resultsPage = lod == LevelOfDetail.MIN
          ? propertyModelMapper.mapEntityPageIntoDTOPageWithIdsOnly(pageRequest, searchResultPage)
          : propertyModelMapper.mapEntityPageIntoDTOPage(pageRequest, searchResultPage);
    } else {
      Page<Property> allPropertiesPage = propertyService.findAllProperties(pageRequest);
      resultsPage = lod == LevelOfDetail.MIN
          ? propertyModelMapper.mapEntityPageIntoDTOPageWithIdsOnly(pageRequest, allPropertiesPage)
          : propertyModelMapper.mapEntityPageIntoDTOPage(pageRequest, allPropertiesPage);
    }

    return new ResponseEntity<>(resultsPage, HttpStatus.OK);
  }

  /**
   * Search for properties. Results are either the full property entities or only the property IDs,
   * depending on the value of the provided {@code lod} parameter.
   * <p/>
   * Note: Search is case insensitive.
   */
  @GetMapping(produces = AdditionalMediaType.TEXT_CSV_VALUE)
  public String findProperties(
      @ModelAttribute final PropertySearchInputDTO propertyDto, final Pageable pageRequest,
      @Valid @RequestParam(value = "lod", required = false,
          defaultValue = "MAX") LevelOfDetail lod, HttpServletRequest request, Model model,
      Authentication auth) {
    final Page<?> resultsPage = findProperties(propertyDto, new ClientSearchInputDTO(), pageRequest,
        lod, auth).getBody();
    model.addAttribute("properties", resultsPage.getContent());
    return ApplicationViews.PROPERTIES_LIST.getViewName();
  }

  /**
   * Get a property identified by {@code externalId}.
   */
  @ApiOperation(value = "Retrieve Property details")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Property not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/{external-id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<PropertyDTO> getProperty(@PathVariable("external-id") String externalId) {
    final Property property = propertyService.getPropertyByExternalId(externalId);
    if (property == null) {
      throw new NotFoundException(String.format("No property found with ID=%s", externalId));
    }
    PropertyDTO propertyDto = propertyModelMapper.mapToDTO(property);

    return new ResponseEntity<>(propertyDto, HttpStatus.OK);
  }

  @ApiOperation(value = "Retrieve Property details in an output")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Property not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/{external-id}", produces = AdditionalMediaType.TEXT_CSV_VALUE)
  public String getProperty(@PathVariable("external-id") String externalId, Model model) {
    PropertyDTO propertyDto = getProperty(externalId).getBody();
    model.addAttribute("properties", Collections.singletonList(propertyDto));
    return ApplicationViews.PROPERTIES_LIST.getViewName();
  }

  /**
   * Update a property identified by {@code externalId}.
   */
  @ApiOperation(value = "Updating property object deatils")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{external-id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<PropertyDTO> updateProperty(
      @RequestBody final PropertyAddRequestDTO propertyAddRequestDTO,
      @PathVariable("external-id") String externalId,
      @RequestAttribute(name = "requestUser") User user, @RequestParam(value = "dropImage",
      required = false, defaultValue = "false") boolean dropImage) {
    final Property property = propertyModelMapper.mapToEntity(propertyAddRequestDTO);
    final Property updatedProperty =
        propertyService.updatePropertyByExternalId(externalId, property, user, dropImage);
    final PropertyDTO updatedPropertyDTO = propertyModelMapper.mapToDTO(updatedProperty);

    return new ResponseEntity<>(updatedPropertyDTO, HttpStatus.OK);
  }

  /**
   * Update a property identified by {@code externalId}.
   */
  @ApiOperation(value = "Updating property(s) current owner id")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @PatchMapping
  public ResponseEntity<Void> updatePropertiesOwnerId(
      @RequestBody final PropertyOwnerIdRequestDTO propertyOwnerIdRequestDTO,
      @RequestAttribute(name = "requestUser") User user) {
    propertyOwnerIdRequestDTO.getExternalIds().stream()
        .map(propertyService::getPropertyByExternalId)
        .peek(p -> p.setCurrentOwnerId(propertyOwnerIdRequestDTO.getCurrentOwnerId()))
        .forEach(
            p -> propertyService.updatePropertyByExternalId(p.getExternalId(), p, user, false));
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  /**
   * Upload default image for a property identified by {@code externalId}.
   */
  @ApiOperation(value = "Create or update the default image of a property.")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Property not found"),
      @ApiResponse(code = 413, message = "Image size exceeds limit"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(value = "/{external-id}/images", consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createImage(@RequestPart("file") MultipartFile file,
      @PathVariable("external-id") String externalId,
      @RequestAttribute("requestUser") User user) {
    if (file.isEmpty()) {
      throw new PropertyServiceException("Image file not valid");
    }
    try {
      String imageName = propertyService
          .createDefaultImage(externalId, file.getInputStream(), file.getSize(), user);
      HttpHeaders headers = RestControllerHelper.createHeadersWithLocation("default");
      return new ResponseEntity<>(Collections.singletonMap("imageName", imageName), headers,
          HttpStatus.CREATED);
    } catch (IOException e) {
      throw new PropertyServiceException(
          String.format("Error uploading image [%s]", file.getOriginalFilename()), e);
    }
  }

  /**
   * Get the default image of a property identified by {@code externalId}.
   */
  @ApiOperation(value = "Retrieve the default image of a property.")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Property or image not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/{external-id}/images/default", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<InputStreamResource> getImage(
      @PathVariable("external-id") String externalId) {
    return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG)
        .body(new InputStreamResource(propertyService.getDefaultImage(externalId).getKey()));
  }

  /**
   * Delete the default image of a property identified by {@code externalId}.
   */
  @ApiOperation(value = "Delete the default image of a property.")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error"),
      @ApiResponse(code = 404, message = "Property not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping(value = "/{external-id}/images/default")
  public void deleteImage(@PathVariable("external-id") String externalId) {
    propertyService.removePropertyImage(externalId);
  }

  /**
   * Merge a property identified by {@code externalId}.
   */
  @ApiOperation(value = "Merge property object deatils")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Property not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{mergeToId}", params = {"action",
      "mergeFromId"}, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<PropertyDTO> mergeProperty(
      @PathVariable("mergeToId") final String mergeToId,
      @Valid @RequestParam(value = "action", defaultValue = "merge") String action,
      @Valid @RequestParam("mergeFromId") final String mergeFromId,
      @RequestBody final PropertyAddRequestDTO propertyAddRequestDTO,
      @RequestAttribute(name = "requestUser") User user, @RequestParam(value = "dropImage",
      required = false, defaultValue = "false") boolean dropImage, Authentication auth) {
    final Property property = propertyModelMapper.mapToEntity(propertyAddRequestDTO);
    final Property mergedProperty = propertyService
        .mergePropertyByExternalId(mergeFromId, mergeToId, property, user, dropImage,
            (KeycloakSecurityContext) auth.getCredentials());
    final PropertyDTO updatedPropertyDTO = propertyModelMapper.mapToDTO(mergedProperty);
    return new ResponseEntity<>(updatedPropertyDTO, HttpStatus.OK);
  }

  /**
   * Creates properties based on external-id and numberOfDuplicates provided.
   * @return Property List
   */
  @ApiOperation(value = "Create duplicate Property")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Invalid input message"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(value = "/{external-id}")
  public ResponseEntity<?> createDuplicateProperty(@PathVariable("external-id") String externalId,
      @RequestParam(name = "action", defaultValue = "duplicate") String action,
      @RequestParam(name = "numberOfDuplicates", defaultValue = "1") Integer numberOfDuplicates,
      @RequestAttribute(name = "requestUser") User user) {
    List<Property> properties =
        propertyService.createDuplicateProperties(externalId, numberOfDuplicates, user);
    final List<PropertyDTO> createdPropertyDTOs = propertyModelMapper.mapToDTO(properties);
    return new ResponseEntity<>(createdPropertyDTOs, HttpStatus.CREATED);

  }

  /**
   * Export Property data into CSV file and stored file into S3
   *
   * @return String updated Property poll Export status
   */
  @ApiOperation(value = "Export Property data into CSV file and stored file into S3")
  @ApiResponses(value = {@ApiResponse(code = 202, message = "Accepted"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(value = "/export", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> exportProperties() {
    final String exportId = propertyService.exportPropertyData();
    HttpHeaders headers = RestControllerHelper.createHeadersWithLocation(exportId);
    return new ResponseEntity<>(exportId, headers, HttpStatus.ACCEPTED);
  }

  /**
   * Retrieve Property Poll Service Export Status by {@code exportId}.
   *
   * @param exportId The export ID of the property
   * @return PollStatus updated property pollExportID status
   */
  @ApiOperation(value = "Retrieve Property Poll Service Export Status")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Property export id not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/export/{id}/status", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<PollStatus> exportIdStatus(@PathVariable("id") String exportId) {
    final PollStatus status = exportService.findExportStatus(exportId);
    return new ResponseEntity<>(status, HttpStatus.OK);
  }

  /**
   * Delete all properties identified by {@code externalId}.
   */
  @ApiOperation(value = "Delete All Properties based on given property id")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully updated"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(value = "/delete/propertyIds", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Integer> deleteAllProperties(
      @Valid @RequestBody final PropertyIdsRequestDto propertyIdsRequestDto) {
    int sucessIdsCount =
        propertyService.deleteAllProperties(propertyIdsRequestDto.getExternalIds());
    propertyService.deletePropertyImagePath(propertyIdsRequestDto.getExternalIds());
    return new ResponseEntity<>(sucessIdsCount, HttpStatus.CREATED);
  }

}
