/**
 *
 */
package com.sothebys.propertydb.controller;

import com.sothebys.propertydb.controller.support.AdditionalMediaType;
import com.sothebys.propertydb.controller.support.ApplicationViews;
import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.dto.ArtistCatalogueRaisoneeDTO;
import com.sothebys.propertydb.dto.ArtistDTO;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.mapper.ArtistModelMapper;
import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.ArtistCatalogueRaisonee;
import com.sothebys.propertydb.model.LevelOfDetail;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.search.ArtistSearchBuilder;
import com.sothebys.propertydb.search.ArtistSearchInputDTO;
import com.sothebys.propertydb.search.ArtistUtil;
import com.sothebys.propertydb.search.PollStatus;
import com.sothebys.propertydb.service.ArtistCatalogueRaisoneeService;
import com.sothebys.propertydb.service.ArtistService;
import com.sothebys.propertydb.service.ExportService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author SrinivasaRao.Batthula
 *
 */
@Controller
@CrossOrigin(origins = "*", maxAge = 3600)
public class ArtistController {

  @Autowired
  ArtistModelMapper artistModelMapper;

  @Autowired
  ArtistSearchBuilder artistSearchBuilder;

  @Autowired
  ArtistService artistService;
  
  @Autowired
  @Qualifier("artistExportService")
  ExportService exportService;

  @Autowired
  ArtistCatalogueRaisoneeService artistCatalogueRaisoneeService;

  /**
   * Create artist
   *
   * @param artistDTO
   * @return Artist
   */
  @ApiOperation(value = "Create artist")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Input message validation failed"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(value = "/artists", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ArtistDTO> createArtist(@Valid @RequestBody final ArtistDTO artistDTO,
      @RequestAttribute("requestUser") User user, @Valid @RequestParam(value = "validArtist",
          required = false, defaultValue = "false") boolean validArtist) {
    
    final Artist artist = artistModelMapper.mapToEntity(artistDTO);
    final Artist createdArtist = artistService.createArtist(artist, user, validArtist);
    final ArtistDTO createdArtistDTO = artistModelMapper.map(createdArtist, ArtistDTO.class);

    HttpHeaders headers = RestControllerHelper.createHeadersWithLocation(createdArtistDTO.getId());
    return new ResponseEntity<>(createdArtistDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Create Catalogue raisonees for artist
   *
   * @param externalId
   * @param artistCatalogueRaisoneeDTOs
   * @return CatalogueRaisoneeDTO
   */
  @ApiOperation(value = "Create catalogue raisonees for artist")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Input message validation failed"),
      @ApiResponse(code = 404, message = "Artist not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(value = "/artists/{externalId}/catalogues-raisonees",
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<ArtistCatalogueRaisoneeDTO>> createCatalogueRaisoneesForArtist(
      @PathVariable("externalId") String externalId,
      @Valid @RequestBody final List<ArtistCatalogueRaisoneeDTO> artistCatalogueRaisoneeDTOs,
      @RequestAttribute(name = "requestUser") User user) {

    List<ArtistCatalogueRaisonee> artistCatalogueRaisonees =
        artistModelMapper.mapToEntity(artistCatalogueRaisoneeDTOs);
    List<ArtistCatalogueRaisonee> savedArtistCatalogueRaisonees =
        artistCatalogueRaisoneeService.saveArtistCataloguesRaisoneesForArtistByExternalId(
            externalId, artistCatalogueRaisonees, user);

    final List<ArtistCatalogueRaisoneeDTO> artistCatalogueRaisoneeDTOFinalList =
        new ArrayList<>(artistCatalogueRaisoneeDTOs.size());
    artistCatalogueRaisoneeDTOFinalList
        .addAll(artistModelMapper.mapToDTO(savedArtistCatalogueRaisonees));

    return new ResponseEntity<>(artistCatalogueRaisoneeDTOFinalList, HttpStatus.CREATED);
  }

  /**
   * Delete artist identified by the external ID.
   *
   * @param externalId
   */
  @ApiOperation(value = "Delete artist details based on given artistId")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully deleted"),
      @ApiResponse(code = 404, message = "Artist id not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/artists/{externalId}")
  public void deleteArtistByExternalId(@PathVariable("externalId") String externalId) {
    artistService.deleteArtist(externalId);
  }

  /**
   * To delete catalogueRaisonee from artist based on given catalogueRaisoneeIdId
   *
   * @param externalId
   * @param catalogueRaisoneeId
   * @return ResponseEntity
   * @throws CannotPerformOperationException if the artist catalogue raisonee still references other
   *         entities
   */
  @ApiOperation(value = "Delete catalogueRaisonee from artist based on given catalogueRaisoneeIdId")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully deleted"),
      @ApiResponse(code = 404, message = "Artist id not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @DeleteMapping("/artists/{externalId}/catalogues-raisonees/{catalogueRaisoneeId}")
  public ResponseEntity<Void> deleteCatalogueRaisoneesForArtistById(
      @PathVariable("externalId") final String externalId,
      @PathVariable("catalogueRaisoneeId") final Long catalogueRaisoneeId,
      @RequestAttribute(name = "requestUser") User user) {
    artistCatalogueRaisoneeService.deleteCatalogueRaisoneeForArtistByExternalId(externalId,
        catalogueRaisoneeId, user);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  /**
   * Finds ArtistID entries * search is case insensitive.
   *
   * @param artistDTO artistDTO
   * @param pageRequest The information of the requested page.
   * @return
   */
  @ApiOperation(value = "Search artists based on input Parameters")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Input parameter validation failed"),
      @ApiResponse(code = 500, message = "Server error")})
  @RequestMapping(value = "/artists", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Page<?>> findBySearchTerm(
      @ModelAttribute final ArtistSearchInputDTO artistDTO, final Pageable pageRequest,
      @Valid @RequestParam(value = "lod", required = false,
          defaultValue = "MAX") LevelOfDetail lod) {
    final Page<?> resultsPage;

    boolean doSearch = ArtistUtil.validateArtistDTO(artistDTO);
    if (doSearch) {
      Specification<Artist> specification =
          artistSearchBuilder.createSpecificationBuilder(artistDTO).build();
      Page<Artist> searchResultPage = artistService.findBySearchTerm(specification, pageRequest);
      resultsPage = lod == LevelOfDetail.MIN
          ? artistModelMapper.mapEntityPageIntoDTOPageWithIdsOnly(pageRequest, searchResultPage)
          : artistModelMapper.mapEntityPageIntoDTOPage(pageRequest, searchResultPage);
    } else {
      Page<Artist> allArtistsPage = artistService.findAllArtists(pageRequest);
      resultsPage = lod == LevelOfDetail.MIN
          ? artistModelMapper.mapEntityPageIntoDTOPageWithIdsOnly(pageRequest, allArtistsPage)
          : artistModelMapper.mapEntityPageIntoDTOPage(pageRequest, allArtistsPage);
    }

    return new ResponseEntity<>(resultsPage, HttpStatus.OK);
  }

  /**
   * To fetch artist details based on artistId
   *
   * @param externalId
   * @return artist
   */
  @ApiOperation(value = "Retrieve artist details")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/artists/{externalId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ArtistDTO> getArtistByExternalId(
      @PathVariable("externalId") String externalId) {
    final Artist artist = artistService.findArtistByExternalId(externalId);
    if (artist == null) {
      throw new NotFoundException(String.format("No artist found with ID=%s", externalId));
    }
    final ArtistDTO artistDTO = artistModelMapper.map(artist, ArtistDTO.class);

    return new ResponseEntity<>(artistDTO, HttpStatus.OK);
  }

  /**
   * Update artist identified by the external ID.
   *
   * @param artistDTO
   * @param externalId
   * @return
   */
  @ApiOperation(value = "Updating artist details")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Input message validation failed"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/artists/{externalId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ArtistDTO> updateArtist(@Valid @RequestBody final ArtistDTO artistDTO,
      @PathVariable("externalId") String externalId,
      @RequestAttribute(name = "requestUser") User user) {
    final Artist artist = artistModelMapper.map(artistDTO, Artist.class);
    final Artist updatedArtist = artistService.updateArtistByExternalId(externalId, artist, user);
    final ArtistDTO updatedArtistDTO = artistModelMapper.map(updatedArtist, ArtistDTO.class);

    return new ResponseEntity<>(updatedArtistDTO, HttpStatus.OK);
  }

  /**
   * Search for artists. Results are either the full artist entities or only the artist IDs,
   * depending on the value of the provided {@code lod} parameter.
   * <p/>
   * Note: Search is case insensitive.
   */
  @GetMapping(value = "artists", produces = AdditionalMediaType.TEXT_CSV_VALUE)
  public String findArtists(@ModelAttribute final ArtistSearchInputDTO artistDTO,
      final Pageable pageRequest,
      @Valid @RequestParam(value = "lod", required = false, defaultValue = "MAX") LevelOfDetail lod,
      HttpServletRequest request, Model model) {
    final Page<?> resultsPage = findBySearchTerm(artistDTO, pageRequest, lod).getBody();

    model.addAttribute("artists", resultsPage.getContent());
    return ApplicationViews.ARTISTS_LIST.getViewName();
  }

  @ApiOperation(value = "Retrieve Property details in an output")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Property not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/artists/{external-id}", produces = AdditionalMediaType.TEXT_CSV_VALUE)
  public String getProperty(@PathVariable("external-id") String externalId, Model model) {

    ArtistDTO artistDTO = getArtistByExternalId(externalId).getBody();
    model.addAttribute("artists", Arrays.asList(artistDTO));

    return ApplicationViews.ARTISTS_LIST.getViewName();
  }

  /**
   * Update catalogue raisonees for artist
   *
   * @param externalId The external ID of the artist
   * @param catalogueRaisoneeId The catalogueRaisoneeId ID of the catalogueRaisonee to be updated
   * @param artistCatalogueRaisoneeDTO of the artist to be updated
   * @return ArtistCatalogueRaisoneeDTO updated artist catalogue raisonees DTO
   * @throws NotFoundException if an artist with the specified {@code artistExternalId} is not found
   */
  @ApiOperation(value = "Update catalogue raisonees for artist")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Input message validation failed"),
      @ApiResponse(code = 404, message = "Artist or catalogue raisonee not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/artists/{externalId}/catalogues-raisonees/{catalogueRaisoneeId}",
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ArtistCatalogueRaisoneeDTO> updateCatalogueRaisoneesForArtist(
      @PathVariable("externalId") String externalId,
      @PathVariable("catalogueRaisoneeId") final Long catalogueRaisoneeId,
      @Valid @RequestBody final ArtistCatalogueRaisoneeDTO artistCatalogueRaisoneeDTO,
      @RequestAttribute(name = "requestUser") User user) {

    ArtistCatalogueRaisonee artistCatalogueRaisoneeObj =
        artistModelMapper.mapToEntity(artistCatalogueRaisoneeDTO);
    ArtistCatalogueRaisonee updatedArtistCatalogueRaisonees =
        artistCatalogueRaisoneeService.updateCatalogueRaisoneeForArtistByExternalId(externalId,
            artistCatalogueRaisoneeObj, catalogueRaisoneeId, user);

    final ArtistCatalogueRaisoneeDTO artistCatalogueRaisoneeUpdatedDTO =
        artistModelMapper.mapToDTO(updatedArtistCatalogueRaisonees);
    return new ResponseEntity<>(artistCatalogueRaisoneeUpdatedDTO, HttpStatus.OK);
  }
  
  /**
   * Retrieve Artist details in an output
   *
   * @return String updated pollExportId
   */
  @ApiOperation(value = "Retrieve Artist details in an output")
  @ApiResponses(value = {@ApiResponse(code = 202, message = "Accepted"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(value = "/artists/export", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> exportArtists(@RequestAttribute(name = "requestUser") User user) {

    final String exportId = artistService.exportArtistData();
    HttpHeaders headers = RestControllerHelper.createHeadersWithLocation(exportId);
    return new ResponseEntity<>(exportId, headers, HttpStatus.ACCEPTED);
  }
  
  /**
   * Retrieve Artist Poll Service Export Status by {@code exportId}.
   *
   * @param exportId The export ID of the artist
   * @return PollStatus updated artist pollExportID status
   */
  @ApiOperation(value = "Retrieve Artist Poll Service Export Status")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Artist export id not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/artists/export/{id}/status", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<PollStatus> exportIdStatus(
      @PathVariable("id") String exportId) {
    final PollStatus status = exportService.findExportStatus(exportId);
    return new ResponseEntity<>(status, HttpStatus.OK);
  }
}
