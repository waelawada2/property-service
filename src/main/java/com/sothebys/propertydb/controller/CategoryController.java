/**
 *
 */
package com.sothebys.propertydb.controller;

import java.util.ArrayList;
import java.util.List;
import com.sothebys.propertydb.dto.request.CategoryRequestDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.dto.CategoryDTO;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.mapper.CategoryModelMapper;
import com.sothebys.propertydb.model.Category;
import com.sothebys.propertydb.service.CategoryService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j;

/**
 * @author SrinivasaRao.Batthula
 * @author Gregor Zurowski
 */
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@Log4j
public class CategoryController {

  @Autowired
  CategoryService categoryService;

  @Autowired
  CategoryModelMapper categoryModelMapper;

  /**
   * Create a new category.
   */
  @ApiOperation(value = "Create category")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Success"),
      @ApiResponse(code = 500, message = "Server error"),
      @ApiResponse(code = 409, message = "Duplicate category entry exist in DB")})
  @PostMapping("/categories")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<CategoryDTO> createCategory(
      @RequestBody final CategoryRequestDTO categoryDTO) {
    final Category category = categoryModelMapper.map(categoryDTO, Category.class);
    final Category createdCategory = categoryService.createParentCategory(category);
    final CategoryDTO createdCategoryDTO =
        categoryModelMapper.map(createdCategory, CategoryDTO.class);

    HttpHeaders headers =
        RestControllerHelper.createHeadersWithLocation(createdCategoryDTO.getId());
    return new ResponseEntity<>(createdCategoryDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Get all parent categories.
   */
  @ApiOperation(value = "Retrieving all parent categories")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/categories")
  public ResponseEntity<List<CategoryDTO>> findAllParentCategories() {
    final List<Category> categories = categoryService.getAllParentCategories();
    final List<CategoryDTO> categoryDTOs = categoryModelMapper.mapToDTO(categories);

    return new ResponseEntity<>(categoryDTOs, HttpStatus.OK);
  }

  /**
   * Delete an existing category identified by the given {@code categoryId}
   */
  @ApiOperation(value = "Delete category based on given categoryId")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully category deleted"),
      @ApiResponse(code = 404, message = "Category id not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/categories/{category-id}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void deleteCategory(@PathVariable("category-id") Long categoryId) {
    categoryService.deleteParentCategory(categoryId);
  }

  /**
   * Get category identified by the provided {@code categoryId}.
   */
  @ApiOperation(value = "Retrieve category details based on categoryId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Category not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/categories/{categoryId}")
  public ResponseEntity<CategoryDTO> getCategoryById(@PathVariable("categoryId") Long categoryId) {
    final Category category = categoryService.findParentCategory(categoryId);
    if (category == null) {
      throw new NotFoundException(String.format("Category with ID=%d not found", categoryId));
    }
    final CategoryDTO categoryDTO = categoryModelMapper.map(category, CategoryDTO.class);

    return new ResponseEntity<>(categoryDTO, HttpStatus.OK);
  }

  /**
   * Update an existing category.
   */
  @ApiOperation(value = "Updating category details")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping("/categories/{categoryId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<CategoryDTO> updateCategory(
      @RequestBody final CategoryRequestDTO categoryDTO,
      @PathVariable("categoryId") Long categoryId) {
    final Category category = categoryModelMapper.map(categoryDTO, Category.class);
    final Category updatedCategory = categoryService.updateParentCategoryById(categoryId, category);
    final CategoryDTO updatedCategoryDTO =
        categoryModelMapper.map(updatedCategory, CategoryDTO.class);

    return new ResponseEntity<>(updatedCategoryDTO, HttpStatus.OK);
  }

  /**
   * Create a new sub category.
   */
  @ApiOperation(value = "Create Sub category")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping("/categories/{category-id}/children")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<CategoryDTO> createSubCategory(@PathVariable("category-id") Long parentId,
      @RequestBody final CategoryRequestDTO categoryDTO) {
    final Category category = categoryModelMapper.map(categoryDTO, Category.class);
    final Category createdCategory = categoryService.createCategoryForParent(parentId, category);
    final CategoryDTO createdCategoryDTO =
        categoryModelMapper.map(createdCategory, CategoryDTO.class);

    HttpHeaders headers =
        RestControllerHelper.createHeadersWithLocation(createdCategoryDTO.getId());
    return new ResponseEntity<>(createdCategoryDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Get all sub categories that belong to the category identified by the provided
   * {@code parentCategoryId}.
   */
  @ApiOperation(value = "Retrieving all child categories")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/categories/{category-id}/children")
  public ResponseEntity<List<CategoryDTO>> findAllChildCategories(
      @PathVariable("category-id") Long parentCategoryId) {
    final List<Category> categories = categoryService.getAllChildCategories(parentCategoryId);
    final List<CategoryDTO> categoriesDTO = new ArrayList<>(categories.size());
    for (Category category : categories) {
      categoriesDTO.add(categoryModelMapper.map(category, CategoryDTO.class));
    }
    return new ResponseEntity<>(categoriesDTO, HttpStatus.OK);
  }

  /**
   * Get a child category identified by {@code childId} that belong to the category identified by
   * the provided {@code parentCategoryId}.
   */
  @ApiOperation(value = "Retrieving a child category")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/categories/{category-id}/children/{childId}")
  public ResponseEntity<CategoryDTO> findChildCategory(
      @PathVariable("category-id") Long parentCategoryId, @PathVariable Long childId) {
    final Category category =
        categoryService.findByCategoryIdAndParentCategoryId(childId, parentCategoryId);
    CategoryDTO categoryDTO = categoryModelMapper.map(category, CategoryDTO.class);

    return new ResponseEntity<>(categoryDTO, HttpStatus.OK);
  }

  /**
   * Delete an existing category identified by the given {@code childId} and a
   * {@code parentCategoryId}
   */
  @ApiOperation(value = "Delete a child category based on given categoryId and parentCategoryId")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully category deleted"),
      @ApiResponse(code = 404, message = "Category id not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/categories/{category-id}/children/{childId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void deleteChildCategory(@PathVariable("category-id") Long parentCategoryId,
      @PathVariable Long childId) {
    categoryService.deleteByCategoryIdAndParentId(childId, parentCategoryId);
  }

  /**
   * Update a child category identified by {@code childId} and {@code parentId}.
   * 
   * @param categoryDTO
   * @param categoryId
   * @return
   */
  @ApiOperation(value = "Updating category details")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping("/categories/{parentId}/children/{childId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<CategoryDTO> updateChildCategory(
      @RequestBody final CategoryRequestDTO categoryDTO, @PathVariable final Long parentId,
      @PathVariable("childId") Long categoryId) {
    final Category category = categoryModelMapper.map(categoryDTO, Category.class);
    final Category updatedCategory =
        categoryService.updateByCategoryIdAndParentCategoryId(categoryId, parentId, category);
    final CategoryDTO updatedCategoryDTO =
        categoryModelMapper.map(updatedCategory, CategoryDTO.class);

    return new ResponseEntity<>(updatedCategoryDTO, HttpStatus.OK);
  }
  
  /**
   * List for Categories. Results are the full Category entities Note: List is case insensitive.
   *
   * @param categoryDTO refers to the provided search criteria
   * @return List of CategoryDTOs refers to the Category objects whose category name contains the
   *         given input name
   */
  @ApiOperation(value = "Search All categories based on name")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Input parameter validation failed"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/categories/name")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<List<CategoryDTO>> findAllCategoryByName(
      @RequestBody final CategoryRequestDTO categoryDTO) {
    List<Category> categories = categoryService.findCategoryContainingName(categoryDTO.getName());
    final List<CategoryDTO> categoryDTOs = categoryModelMapper.mapToDTO(categories);

    return new ResponseEntity<>(categoryDTOs, HttpStatus.OK);
  }

  /**
   * Retrieves all categories for admin.
   *
   * @return List of Category DTOs
   */
  @ApiOperation(value = "Retrieve All Categories")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "Categories not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/categories/all", produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<List<CategoryDTO>> getAllCategoriesforAdmin() {
    List<Category> categories = categoryService.getAllCategories();
    final List<CategoryDTO> categoryDTOs = categoryModelMapper.mapToDTO(categories);
    return new ResponseEntity<>(categoryDTOs, HttpStatus.OK);
  }

  /**
   * Merge a Category identified by {@code CategoryId}.
   */
  @ApiOperation(value = "Merge Categories")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Category not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/categories/{mergeToId}", params = {"mergeFromId"},
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<CategoryDTO> mergeCategory(
      @PathVariable("mergeToId") final String mergeToId,
      @RequestParam("mergeFromId") final String mergeFromId) {
    Category mergedCategory =
        categoryService.mergeCategories(Long.valueOf(mergeToId), Long.valueOf(mergeFromId));
    final CategoryDTO mergedCategoryDTO =
        categoryModelMapper.map(mergedCategory, CategoryDTO.class);
    return new ResponseEntity<>(mergedCategoryDTO, HttpStatus.OK);
  }
}
