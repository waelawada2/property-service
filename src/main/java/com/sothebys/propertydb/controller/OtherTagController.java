/**
 * 
 */
package com.sothebys.propertydb.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.dto.OtherTagDTO;
import com.sothebys.propertydb.mapper.OtherTagModelMapper;
import com.sothebys.propertydb.model.OtherTag;
import com.sothebys.propertydb.service.OtherTagService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author aneesha.l
 *
 */
@RestController
@RequestMapping("/otherTags")
@CrossOrigin(origins = "*", maxAge = 3600)
public class OtherTagController {

	@Autowired
	OtherTagModelMapper otherTagModelMapper;

	@Autowired
	OtherTagService otherTagService;

	/**
	 * Create other tag
	 *
	 * @param OtherTagDTO
	 * @return OtherTag
	 */
	@ApiOperation(value = "Create Other Tag")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Created"),
			@ApiResponse(code = 400, message = "Input message validation failed"),
			@ApiResponse(code = 500, message = "Server error") })
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OtherTagDTO> createOtherTag(@Valid @RequestBody final OtherTagDTO otherTagDTO) {
		final OtherTag otherTag = otherTagModelMapper.map(otherTagDTO, OtherTag.class);
		final OtherTag createdOtherTag = otherTagService.createOtherTag(otherTag);
		final OtherTagDTO createdOtherTagDTO = otherTagModelMapper.map(createdOtherTag, OtherTagDTO.class);

		HttpHeaders headers = RestControllerHelper.createHeadersWithLocation(createdOtherTagDTO.getId());
		return new ResponseEntity<>(createdOtherTagDTO, headers, HttpStatus.CREATED);

	}

	/**
	 * Get all other tags.
	 */
	@ApiOperation(value = "Retrieving all the other tags")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Server error") })
	@GetMapping
	public ResponseEntity<List<OtherTagDTO>> findAllOtherTags() {
		final List<OtherTag> otherTags = otherTagService.findAllOtherTags();
		final List<OtherTagDTO> otherTagDTOs = otherTagModelMapper.mapToOtherTagDTOs(otherTags);

		return new ResponseEntity<>(otherTagDTOs, HttpStatus.OK);
		}

	/**
	 * Get all other tags by name.
	 */
	@ApiOperation(value = "Retrieving all the other tags")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Server error") })
	@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@GetMapping("/name")
	public ResponseEntity<List<OtherTagDTO>> findAllOtherTagsByName(@RequestParam("name") String name) {
		name = name != null ? name : "";
		final List<OtherTag> otherTags = otherTagService.findAllOtherTagsByName(name);
		final List<OtherTagDTO> otherTagDTOs = otherTagModelMapper.mapToOtherTagDTOs(otherTags);

		return new ResponseEntity<>(otherTagDTOs, HttpStatus.OK);
	}

	/**
	 * Delete other tag.
	 *
	 * @param id
	 */
	@ApiOperation(value = "Delete other tag")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Successfully deleted"),
			@ApiResponse(code = 404, message = "Other tag not found"),
			@ApiResponse(code = 500, message = "Server error") })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping("/{id}")
	public void deleteOtherTagById(@PathVariable("id") Long id) {
		otherTagService.deleteOtherTag(id);
	}

	/**
	 * Update the other tag.
	 *
	 * @param otherTagDTO
	 * @return
	 */
	@ApiOperation(value = "Updating other tag details")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Input message validation failed"),
			@ApiResponse(code = 500, message = "Server error") })
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OtherTagDTO> updateOtherTag(@Valid @RequestBody final OtherTagDTO otherTagDTO) {
		final OtherTag otherTag = otherTagModelMapper.map(otherTagDTO, OtherTag.class);
		final OtherTag updatedOtherTag = otherTagService.updateOtherTag(otherTag);
		final OtherTagDTO updatedOtherTagDTO = otherTagModelMapper.map(updatedOtherTag, OtherTagDTO.class);

		return new ResponseEntity<>(updatedOtherTagDTO, HttpStatus.OK);
	}
}
