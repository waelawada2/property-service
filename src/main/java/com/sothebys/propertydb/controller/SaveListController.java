package com.sothebys.propertydb.controller;

import com.sothebys.propertydb.controller.support.AdditionalMediaType;
import com.sothebys.propertydb.controller.support.ApplicationViews;
import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.dto.SaveListDTO;
import com.sothebys.propertydb.dto.request.SaveAllPropertiesFromSavelistRequestDto;
import com.sothebys.propertydb.dto.request.SaveAllPropertiesRequestDto;
import com.sothebys.propertydb.dto.request.SaveListRequestDTO;
import com.sothebys.propertydb.dto.response.SavePropertiesResponseDto;
import com.sothebys.propertydb.mapper.PropertyModelMapper;
import com.sothebys.propertydb.mapper.SaveListModelMapper;
import com.sothebys.propertydb.model.AddPropertiesToSavelistResult;
import com.sothebys.propertydb.model.LevelOfDetail;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.SaveList;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.search.PollStatus;
import com.sothebys.propertydb.search.PropertySearchBuilder;
import com.sothebys.propertydb.search.PropertyUtil;
import com.sothebys.propertydb.search.SaveListInputDTO;
import com.sothebys.propertydb.search.SaveListUtil;
import com.sothebys.propertydb.service.ExportService;
import com.sothebys.propertydb.service.PropertyService;
import com.sothebys.propertydb.service.SaveListService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Collections;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * REST controller that exposes country details.
 *
 * @author SrinivasaRao.Batthula
 */
@Controller
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin(origins = "*", maxAge = 3600)
public class SaveListController {

  @Autowired
  private SaveListModelMapper saveListModelMapper;

  @Autowired
  private SaveListService saveListService;

  @Autowired
  private SaveListUtil saveListUtil;

  @Autowired
  private PropertyService propertyService;

  @Autowired
  PropertySearchBuilder propertySearchBuilder;

  @Autowired
  PropertyModelMapper propertyModelMapper;

  @Autowired
  @Qualifier("savedListExportService")
  ExportService exportService;

  /**
   * This API is to create a new save list identified by {@code userName}.
   *
   * @param userName refers to the logged in user's user name
   * @param saveListRequestDTO contains the details for the save list to be created
   * @param user The User {@code user} instance
   * @return saveListDTO returns the newly created save list
   */
  @ApiOperation(value = "Create Save List")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Success"),
      @ApiResponse(code = 403, message = "Access Denied"),
      @ApiResponse(code = 404, message = "User name not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(value = "/{user-name}/lists", produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("#userName == #user.userName")
  public ResponseEntity<SaveListDTO> createSaveList(@PathVariable("user-name") String userName,
      @RequestBody final SaveListRequestDTO saveListRequestDTO,
      @RequestAttribute(name = "requestUser") User user) {

    final List<String> propertyExternalIds = saveListRequestDTO.getPropertyIds();
    saveListRequestDTO.setPropertyIds(Collections.emptyList());

    final SaveList saveListObj = saveListModelMapper.mapToEntity(saveListRequestDTO);

    final SaveList createdSaveListObj = saveListService.createSaveList(saveListObj, user);
    final AddPropertiesToSavelistResult insertedResult = saveListService
        .addPropertiesToSaveList(createdSaveListObj.getId(), propertyExternalIds);
    final SaveList createdSaveListObjWithProperties = insertedResult.getSaveList();

    final SaveListDTO createdSaveListDTO = saveListModelMapper
        .mapToDTO(createdSaveListObjWithProperties);

    HttpHeaders headers =
        RestControllerHelper.createHeadersWithLocation(createdSaveListDTO.getId());

    return new ResponseEntity<>(createdSaveListDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Update a save List identified by {@code userName} and {@code externalId}.
   *
   * @param userName refers to the logged in user's user name
   * @param externalId refers to the externalId of a save list
   * @return updatedSaveListDTO refers to the updated Save List DTO Object
   */
  @ApiOperation(value = "Updating SaveList details")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{user-name}/lists/{external-id}",
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("#userName == #user.userName")
  public ResponseEntity<SaveListDTO> updateSavedListByExternalId(
      @RequestBody final SaveListRequestDTO saveListRequestDTO,
      @PathVariable("user-name") String userName, @PathVariable("external-id") String externalId,
      @RequestAttribute(name = "requestUser") User user) {
    final SaveList saveListObj = saveListModelMapper.mapToEntity(saveListRequestDTO);
    final SaveList updatedSaveListObj =
        saveListService.updateByUserAndExternalId(user, externalId, saveListObj);
    final SaveListDTO updatedSaveListDTO = saveListModelMapper.mapToDTO(updatedSaveListObj);

    return new ResponseEntity<>(updatedSaveListDTO, HttpStatus.OK);
  }


  /**
   * This API is to add users to a save List identified by {@code userName} and {@code externalId}.
   *
   * @param userName refers to the logged in user's user name
   * @param externalId refers to the externalId of a save list
   * @return saveList refers to the updated SaveList object
   */
  @ApiOperation(value = "Updating Shared SaveList details")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping("/{user-name}/lists/{external-id}/shareusers")
  @PreAuthorize("#userName == #user.userName")
  public ResponseEntity<SaveListDTO> addUsersToSaveListByUserNameAndExternalId(
      @RequestBody final List<String> newUsers, @PathVariable("user-name") String userName,
      @PathVariable("external-id") String externalId,
      @RequestAttribute(name = "requestUser") User user) {
    final SaveList updatedSaveListObj =
        saveListService.addUsersToSaveListByUserNameAndExternalId(userName, externalId, newUsers);
    final SaveListDTO updatedSaveListDTO = saveListModelMapper.mapToDTO(updatedSaveListObj);

    return new ResponseEntity<>(updatedSaveListDTO, HttpStatus.OK);
  }

  /**
   * This API is to delete an existing save list identified by {@code userName} and {@code
   * externalId}.
   *
   * @param userName refers to the logged in user's user name
   * @param externalId refers to the externalId of a save list
   * @param user The User {@code user} instance
   */
  @ApiOperation(value = "Delete saved list of the user based on given externalId")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully deleted"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/{user-name}/lists/{external-id}")
  @PreAuthorize("#userName == #user.userName")
  public void deleteSaveListByExternalId(@PathVariable("user-name") String userName,
      @PathVariable("external-id") String externalId,
      @RequestAttribute(name = "requestUser") User user) {

    saveListService.deleteByUserNameAndExternalId(userName, externalId);
  }

  /**
   * This API is to remove share users from an existing save list identified by {@code userName} and
   * {@code externalId}.
   *
   * @param userName refers to the logged in user's user name
   * @param externalId refers to the externalId of a save list
   * @param deletedUsersList refers to the list of items to be deleted
   * @param user The User {@code user} instance
   */
  @ApiOperation(value = "Delete saved list of the user based on given externalId")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully deleted"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/{user-name}/lists/{external-id}/shareusers")
  @PreAuthorize("#userName == #user.userName")
  public void deleteUsersFromSaveListByUserNameAndExternalId(
      @PathVariable("user-name") String userName, @PathVariable("external-id") String externalId,
      @RequestBody final List<String> deletedUsersList,
      @RequestAttribute(name = "requestUser") User user) {

    saveListService.deleteUsersFromSaveListByUserNameAndExternalId(userName, externalId,
        deletedUsersList);
  }

  /**
   * This API is to retrieve details of save list identified by {@code userName} and {@code
   * externalId}.
   *
   * @param user refers to the logged in user
   * @param userName refers to the logged in user's user name
   * @param externalId refers to the externalId of a save list
   * @return saveListDTO refers to the save-list object that matches the user-name & external Id
   * criteria
   */
  @ApiOperation(value = "Retrieving save lists based on userName and externalId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "userName or external ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/{user-name}/lists/{external-id}",
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("#userName == #user.userName")
  public ResponseEntity<SaveListDTO> findSaveListsByExternalId(
      @RequestAttribute(name = "requestUser") User user, @PathVariable("user-name") String userName,
      @PathVariable("external-id") String externalId) {
    final SaveList saveList = saveListService
        .findSaveListByUserNameAndExternalId(userName, externalId);
    final SaveListDTO saveListDTO = saveListModelMapper.mapToSaveListDTO(saveList);
    return new ResponseEntity<>(saveListDTO, HttpStatus.OK);
  }

  /**
   * List for save lists. Results are either the full saveList entities or only the saveList IDs,
   * depending on the value of the provided {@code lod} parameter.
   * <p/>
   * Note: List is case insensitive.
   *
   * @param saveListInputDTO refers to the provided search criteria
   * @param pageRequest The information of the requested page.
   * @param userName refers to the user-name property of the logged in user name.
   * @return saveListDTO refers to the save-list objects for the specified user-name & save list
   * name
   */
  @ApiOperation(value = "Search saveLists based on input Parameters")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Input parameter validation failed"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/{user-name}/lists", produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("#userName == #user.userName")
  public ResponseEntity<Page<?>> findSaveListByName(
      @ModelAttribute final SaveListInputDTO saveListInputDTO, final Pageable pageRequest,
      @PathVariable("user-name") String userName,
      @Valid @RequestParam(value = "lod", required = false, defaultValue = "MAX") LevelOfDetail lod,
      @RequestAttribute(name = "requestUser") User user) {
    final Page<?> resultsPage;
    boolean doList = saveListUtil.validate(saveListInputDTO);
    if (doList) {
      Page<SaveList> listResultPage =
          saveListService.findSaveListByNameAndUser(saveListInputDTO.getName(), user, pageRequest);

      resultsPage = lod == LevelOfDetail.MIN
          ? saveListModelMapper.mapEntityPageIntoDTOPageWithIdsOnly(pageRequest, listResultPage)
          : saveListModelMapper
              .mapEntityPageIntoDTOPageWithOutPropertyIds(pageRequest, listResultPage);
    } else {
      Page<SaveList> allSaveListsPage = saveListService.findSaveListsByUser(user, pageRequest);
      resultsPage = lod == LevelOfDetail.MIN
          ? saveListModelMapper.mapEntityPageIntoDTOPageWithIdsOnly(pageRequest, allSaveListsPage)
          : saveListModelMapper
              .mapEntityPageIntoDTOPageWithOutPropertyIds(pageRequest, allSaveListsPage);
    }

    return new ResponseEntity<>(resultsPage, HttpStatus.OK);
  }

  /**
   * This API is to download all the properties of an existing save list identified by {@code
   * userName} and {@code externalId} as CSV file.
   *
   * @param user refers to the logged in user
   * @param userName refers to the logged in user's user name
   * @param model The Model {@code model} instance
   * @param externalId which refers to the externalId of a save list
   * @return properties list for the specified save list in CSV format
   */
  @ApiOperation(
      value = "Download properties for the given save lists based on userName and externalId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/{user-name}/lists/{external-id}/properties",
      produces = AdditionalMediaType.TEXT_CSV_VALUE)
  @PreAuthorize("#userName == #user.userName")
  public String downloadPropertiesFromSavelistByUserName(
      @RequestAttribute(name = "requestUser") User user, @PathVariable("user-name") String userName,
      final Pageable pageRequest,
      @Valid @RequestParam(value = "lod", required = false,
          defaultValue = "MAX") LevelOfDetail lod, Model model,
      @PathVariable("external-id") String externalId) {

    final Page<?> resultsPage = findSaveListPropertiesByExternalId(user, pageRequest, userName,
        externalId, lod).getBody();
    model.addAttribute("properties", resultsPage.getContent());
    return ApplicationViews.PROPERTIES_LIST.getViewName();

  }

  /**
   * This API is to list all the properties of an existing save list identified by {@code userName}
   * and {@code externalId}.
   *
   * @param user refers to the logged in user
   * @param userName refers to the logged in user's user name
   * @param externalId which refers to the externalId of a save list
   * @param lod refers to if MIN/MAX details are requested
   * @return Page<?> refers to the properties list for the specified save list
   */
  @ApiOperation(value = "Retrieving save list propertyId's based on userName and externalId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/{user-name}/lists/{external-id}/properties",
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("#userName == #user.userName")
  public ResponseEntity<Page<?>> findSaveListPropertiesByExternalId(
      @RequestAttribute(name = "requestUser") User user, final Pageable pageRequest,
      @PathVariable("user-name") String userName, @PathVariable("external-id") String externalId,
      @Valid @RequestParam(value = "lod", required = false,
          defaultValue = "MAX") LevelOfDetail lod) {
    final Page<?> resultsPage;

    SaveList saveList = saveListService
        .findSaveListByUserNameAndExternalId(user.getUserName(), externalId);
    Specification<Property> specification =
        propertySearchBuilder.createSpecificationBuilder(saveList.getId()).build();
    Page<Property> searchResultPage =
        propertyService.findBySearchTerm(specification, pageRequest);
    resultsPage = lod == LevelOfDetail.MIN
        ? propertyModelMapper.mapEntityPageIntoDTOPageWithIdsOnly(pageRequest, searchResultPage)
        : propertyModelMapper.mapEntityPageIntoDTOPage(pageRequest, searchResultPage);

    return new ResponseEntity<>(resultsPage, HttpStatus.OK);
  }

  /**
   * This API is to add properties an existing save list identified by {@code userName} and {@code
   * externalId}.
   *
   * @param propertyExternalIds an object containing the external property ids sof the property we
   *     are adding to
   *     the saved list
   * @param userName refers to the logged in user's user name
   * @param externalId refers to the externalId of a save list
   * @param user the user currently making the request
   * @return saveList refers to the updated save list object
   */
  @ApiOperation(
      value = "This API is to add properties an existing save list identified by userName & externalId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(value = "/{user-name}/lists/{external-id}/properties",
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("#userName == #user.userName")
  public ResponseEntity addSaveListPropertyIdsByExternalId(
      @RequestBody final List<String> propertyExternalIds,
      @PathVariable("user-name") String userName, @PathVariable("external-id") String externalId,
      @RequestAttribute(name = "requestUser") User user) {

    SaveList saveList = saveListService
        .findSaveListByUserNameAndExternalId(user.getUserName(), externalId);
    AddPropertiesToSavelistResult insertedResult = saveListService
        .addPropertiesToSaveList(saveList.getId(), propertyExternalIds);

    final SavePropertiesResponseDto resultDto = new SavePropertiesResponseDto();
    resultDto.setTotalFound(insertedResult.getTotalFound());
    resultDto.setTotalInserted(insertedResult.getTotalInserted());

    return ResponseEntity.ok(resultDto);
  }


  /**
   * This API is to add properties an existing save list identified by {@code userName} and {@code
   * externalId}.
   *
   * @param requestDto an object containing the external property id of the property we are adding to
   * the saved list
   * @param userName refers to the logged in user's user name
   * @param externalId refers to the externalId of a save list
   * @param user the user currently making the request
   * @return saveList refers to the updated save list object
   */
  @ApiOperation(
      value = "This API is to add properties an existing save list identified by userName & externalId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 409, message = "Property already added to saved list"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(value = "/{user-name}/lists/{external-id}/properties",
      produces = MediaType.APPLICATION_JSON_VALUE, params = "updateAll")
  @PreAuthorize("#userName == #user.userName")
  public ResponseEntity<SavePropertiesResponseDto> updateSaveListPropertyIdsByExternalIdAndSearchCriteria(
      @RequestBody final SaveAllPropertiesRequestDto requestDto,
      @PathVariable("user-name") String userName, @PathVariable("external-id") String externalId,
      final Pageable pageRequest,
      @RequestAttribute(name = "requestUser") User user) {

    final SavePropertiesResponseDto resultDto = new SavePropertiesResponseDto();
    final Page<?> resultsPage;

    boolean doInsert = PropertyUtil.validatePropertyDTO(requestDto.getSearchCriteria());
    if (doInsert) {

      Specification<Property> specification =
          propertySearchBuilder.createSpecificationBuilder(requestDto.getSearchCriteria()).build();

      AddPropertiesToSavelistResult insertedResult = saveListService
          .addPropertiesToSaveList(externalId, specification, user, requestDto.getIgnoredIds());

      resultDto.setTotalFound(insertedResult.getTotalFound());
      resultDto.setTotalInserted(insertedResult.getTotalInserted());

    }

    return ResponseEntity.ok(resultDto);
  }


  /**
   * This API is to add properties an existing save list identified by {@code userName} and {@code
   * externalId}.
   *
   * @param requestDto an object containing the external property id of the property we are adding to
   * the saved list
   * @param userName refers to the logged in user's user name
   * @param externalId refers to the externalId of a save list
   * @param user the user currently making the request
   * @return saveList refers to the updated save list object
   */
  @ApiOperation(
      value = "This API is to add properties an existing save list identified by userName & externalId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 409, message = "Property already added to saved list"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(value = "/{user-name}/lists/{external-id}/properties",
      produces = MediaType.APPLICATION_JSON_VALUE, params = "fromSavedList")
  @PreAuthorize("#userName == #user.userName")
  public ResponseEntity<SavePropertiesResponseDto> updateSaveListPropertyIdsFromExistingSaveList(
      @Valid @RequestBody final SaveAllPropertiesFromSavelistRequestDto requestDto,
      @PathVariable("user-name") String userName, @PathVariable("external-id") String externalId,
      @RequestAttribute(name = "requestUser") User user) {

    final SavePropertiesResponseDto resultDto = new SavePropertiesResponseDto();

    AddPropertiesToSavelistResult insertedResult = saveListService
        .addPropertiesToSavelistFromSavelistId(requestDto.getFromSavedListId(), externalId,
            user, requestDto.getIgnoredIds());

    resultDto.setTotalFound(insertedResult.getTotalFound());
    resultDto.setTotalInserted(insertedResult.getTotalInserted());

    return ResponseEntity.ok(resultDto);
  }


  /**
   * This API is to delete properties from an existing save list identified by {@code userName} and
   * {@code
   * externalId}.
   *
   * @param propertyId an object containing the external property id of the property we are deleting
   * from the saved list
   * @param userName refers to the logged in user's user name
   * @param externalId refers to the externalId of a save list
   * @param user the user currently making the request
   * @return saveList refers to the updated save list object
   */
  @ApiOperation(
      value = "This API is to delete properties from an existing save list")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully deleted"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 409, message = "Property not found in saved list"),
      @ApiResponse(code = 500, message = "Server error")})
  @DeleteMapping(value = "/{user-name}/lists/{external-id}/properties/{object-id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize("#userName == #user.userName")
  public void deletePropertyIdsFromSaveListByExternalId(
      @PathVariable("user-name") String userName,
      @PathVariable("external-id") String externalId,
      @PathVariable("object-id") String propertyId,
      @RequestAttribute(name = "requestUser") User user) {

    saveListService.deletePropertyIdsFromSaveList(externalId, propertyId, user);
  }

  /**
   * This method used to Retrieve save list user property data
   *
   * @param user the user currently making the request
   * @param userName refers to the logged in user's user name
   * @param externalId refers to the externalId of a save list
   * @return String updated save list poll ExportIdupdateSaveListPropertyIdsFromExistingSaveList
   */
  @ApiOperation(
      value = "Retrieve properties for the given save lists based on userName and externalId")
  @ApiResponses(value = {@ApiResponse(code = 202, message = "Accepted"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(value = "/{user-name}/lists/{external-id}/export",
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("#userName == #user.userName")
  public ResponseEntity<String> exportPropertiesFromSavelistByUserName(
      @RequestAttribute(name = "requestUser") User user, @PathVariable("user-name") String userName,
      @PathVariable("external-id") String externalId) {

    SaveList saveList =
        saveListService.findSaveListByUserNameAndExternalId(user.getUserName(), externalId);
    Specification<Property> specification =
        propertySearchBuilder.createSpecificationBuilder(saveList.getId()).build();

    final String exportId = saveListService.exportSavedListData(specification);

    HttpHeaders headers = RestControllerHelper.createHeadersWithLocation(exportId);
    return new ResponseEntity<>(exportId, headers, HttpStatus.ACCEPTED);
  }

  /**
   * Retrieve save list Poll Service Export Status by {@code exportId}.
   *
   * @param exportId The export ID of the property
   * @param user the user currently making the request
   * @return PollStatus updated property pollExportID status
   */
  @ApiOperation(value = "Retrieve Saved list Poll Service Export Status")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "Saved list export id not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/{user-name}/lists/export/{id}/status",
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("#userName == #user.userName")
  public ResponseEntity<PollStatus> exportIdStatus(@PathVariable("id") String exportId,
      @PathVariable("user-name") String userName,
      @RequestAttribute(name = "requestUser") User user) {
    final PollStatus status = exportService.findExportStatus(exportId);
    return new ResponseEntity<>(status, HttpStatus.OK);
  }

  /**
   * Retrieves the entire save list for admin.
   *
   * @return Page<?> refers to the save list
   */
  @ApiOperation(value = "Retrieve All Save list")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "saveLists not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/lists/all", produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<Page<?>> getAllSavedListsforAdmin(final Pageable pageRequest) {
    final Page<?> resultsPage;
    Page<SaveList> allSaveLists = saveListService.getAllSaveLists(pageRequest);
    resultsPage = saveListModelMapper.mapEntityPageIntoDTOPage(pageRequest, allSaveLists);
    return new ResponseEntity<>(resultsPage, HttpStatus.OK);
  }

  /**
   * Updates the research status of a save list.
   *
   * @param externalId of a save list
   * @param status of a save list to update
   * @return research status of the updated save list
   */
  @ApiOperation(
      value = "Update Research Status of properties for the given save lists identified by external-id")
  @ApiResponses(value = {@ApiResponse(code = 202, message = "Accepted"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "external-ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/lists/{externalId}/researchStatus",
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<?> updateResearchStatus(@PathVariable("externalId") String externalId,
      @RequestParam(value = "status",
          required = true, defaultValue = "false") final boolean status) {
    boolean researchStatus = saveListService.updateResearchStatus(externalId, status);
    return new ResponseEntity<>(researchStatus, HttpStatus.OK);
  }

  /**
   * List for save lists. Results are the full saveList entities
   * Note: List is case insensitive.
   *
   * @param saveListInputDTO refers to the provided search criteria
   * @param pageRequest The information of the requested page.
   * @return saveListDTO refers to the save-list objects whose save list name contains the given input name
   */
  @ApiOperation(value = "Search All saveLists based on name")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Input parameter validation failed"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/lists/name", produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<List<SaveListDTO>> findAllSaveListByName(
      @ModelAttribute final SaveListInputDTO saveListInputDTO, final Pageable pageRequest) {
    List<SaveList> listOfObjects = saveListService.findSaveListContainingName(saveListInputDTO.getName());
    List<SaveListDTO> results = saveListModelMapper.mapToSaveListDTOsWithResearchStatus(listOfObjects);

    return new ResponseEntity<>(results, HttpStatus.OK);
  }

  /**
   * This method used to Delete a savedList property data based on given saveList externalId
   *
   * @param externalId refers to the externalId of a save list
   */
  @ApiOperation(value = "Delete properties by Admin for the given save lists based on externalId")
  @ApiResponses(value = {@ApiResponse(code = 202, message = "Accepted"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @DeleteMapping(value = "/lists/{external-id}/delete")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void deletePropertiesFromSavelistByAdmin(@PathVariable("external-id") String externalId) {
    saveListService.deleteAdminSavelistProperties(externalId);
  }

  /**
   * Retrieves the entire save list for admin.
   *
   * @return Page<?> refers to the save list
   */
  @ApiOperation(value = "Retrieve All Admin Active Save lists")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "saveLists not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/lists/admin/all", produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<List<String>> getAllActiveSavedListsforAdmin() {
    List<String> allAdminSaveListNames = saveListService.getAllAdminSaveLists();
    return new ResponseEntity<>(allAdminSaveListNames, HttpStatus.OK);
  }

  /**
   * Retrieve saveList based on name for Admin
   *
   * @param saveListInputDTO refers to the provided search criteria
   * @return saveListDTO refers to the save-list objects whose save list name contains the given
   * input name
   */
  @ApiOperation(value = "Retrieve saveList based on name for Admin")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Input parameter validation failed"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "saveList not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/lists/admin/name", produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<SaveListDTO> findSaveListByNameforAdmin(
      @ModelAttribute final SaveListInputDTO saveListInputDTO) {
    SaveList saveList = saveListService.findSaveListContainingByName(saveListInputDTO.getName());
    final SaveListDTO saveListDTO = saveListModelMapper.mapToSaveListDTOs(saveList);
    return new ResponseEntity<>(saveListDTO, HttpStatus.OK);
  }
  
  /**
   * Retrieve saveList active properties count based on externalId
   *
   * @param externalId refers to the externalId of a save list
   * @return count refers to the save-list objects active properties count
   */
  @ApiOperation(value = "Retrieve saveList active properties count based on externalId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Input parameter validation failed"),
      @ApiResponse(code = 403, message = "access denied"),
      @ApiResponse(code = 404, message = "saveList not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/{user-name}/lists/{external-id}/propertiesCount",
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("#userName == #user.userName")
  public ResponseEntity<Long> findSaveListPropertiesCountById(
      @PathVariable("user-name") String userName, @RequestAttribute(name = "requestUser") User user,
      @PathVariable("external-id") String externalId) {
    long count = saveListService.findSaveListPropertiesCountById(externalId);
    return new ResponseEntity<>(count, HttpStatus.OK);
  }
}
