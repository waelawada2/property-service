package com.sothebys.propertydb.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.dto.ExpertiseSitterDTO;
import com.sothebys.propertydb.mapper.ExpertiseSitterModelMapper;
import com.sothebys.propertydb.model.ExpertiseSitter;
import com.sothebys.propertydb.search.ExpertiseSitterSearchBuilder;
import com.sothebys.propertydb.search.ExpertiseSitterSearchInputDTO;
import com.sothebys.propertydb.search.ExpertiseSitterUtil;
import com.sothebys.propertydb.service.ExpertiseSitterService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author aneesha.l
 * @author Sankeerth.Kandru
 *
 */
@RestController
@RequestMapping("/sitters")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ExpertiseSitterController {

	@Autowired
	ExpertiseSitterModelMapper expertiseSitterModelMapper;

	@Autowired
	ExpertiseSitterService expertiseSitterService;

	@Autowired
	ExpertiseSitterSearchBuilder expertiseSitterSearchBuilder;

	/**
	 * Create Sitter
	 *
	 * @param expertiseSitterDTO
	 * @return ExpertiseSitter
	 */
	@ApiOperation(value = "Create Sitter")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Created"),
			@ApiResponse(code = 400, message = "Input message validation failed"),
			@ApiResponse(code = 500, message = "Server error") })
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ExpertiseSitterDTO> createExpertiseSitter(@Valid @RequestBody final ExpertiseSitterDTO expertiseSitterDTO) {
		final ExpertiseSitter expertiseSitter = expertiseSitterModelMapper.map(expertiseSitterDTO, ExpertiseSitter.class);
		final ExpertiseSitter createdExpertiseSitter = expertiseSitterService.createExpertiseSitter(expertiseSitter);
		final ExpertiseSitterDTO createdExpertiseSitterDTO = expertiseSitterModelMapper.map(createdExpertiseSitter,
				ExpertiseSitterDTO.class);

		HttpHeaders headers = RestControllerHelper.createHeadersWithLocation(createdExpertiseSitterDTO.getId());
		return new ResponseEntity<>(createdExpertiseSitterDTO, headers, HttpStatus.CREATED);

	}

	/**
	 * Get all sitters by name.
	 */
	@ApiOperation(value = "Search sitters based on input parameters")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Server error") })
	@GetMapping
	public ResponseEntity<List<ExpertiseSitterDTO>> findAllExpertiseSitters(
			@ModelAttribute final ExpertiseSitterSearchInputDTO expertiseSitterDTO) {
		List<ExpertiseSitterDTO> expertiseSitterDTOs = null;
		List<ExpertiseSitter> expertiseSitters = null;
		boolean doSearch = ExpertiseSitterUtil.validateSitterDTO(expertiseSitterDTO);
		if (doSearch) {
			Specification<ExpertiseSitter> specification = expertiseSitterSearchBuilder
					.createSpecificationBuilder(expertiseSitterDTO).build();
			expertiseSitters = expertiseSitterService.findBySearchTerm(specification);
			expertiseSitterDTOs = expertiseSitterModelMapper.mapToExpertiseSitterDTOs(expertiseSitters);
		} else {
			expertiseSitters = expertiseSitterService.findAllExpertiseSitters();
			expertiseSitterDTOs = expertiseSitterModelMapper.mapToExpertiseSitterDTOs(expertiseSitters);
		}

		return new ResponseEntity<>(expertiseSitterDTOs, HttpStatus.OK);
	}

	/**
	 * Delete sitter.
	 *
	 * @param id
	 */
	@ApiOperation(value = "Delete sitter")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Successfully deleted"),
			@ApiResponse(code = 404, message = "Sitter object not found"),
			@ApiResponse(code = 500, message = "Server error") })
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void deleteExpertiseSitterById(@PathVariable("id") Long id) {
		expertiseSitterService.deleteExpertiseSitter(id);
	}

	/**
	 * Update the sitter name.
	 *
	 * @param expertiseSitterDTO
	 * @return
	 */
	@ApiOperation(value = "Updating sitter details")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Input message validation failed"),
			@ApiResponse(code = 500, message = "Server error") })
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<ExpertiseSitterDTO> updateExpertiseSitter(@Valid @RequestBody final ExpertiseSitterDTO expertiseSitterDTO) {
		final ExpertiseSitter expertiseSitter = expertiseSitterModelMapper.map(expertiseSitterDTO, ExpertiseSitter.class);
		final ExpertiseSitter updatedExpertiseSitter = expertiseSitterService.updateExpertiseSitter(expertiseSitter);
		final ExpertiseSitterDTO updatedExpertiseSitterDTO = expertiseSitterModelMapper.map(updatedExpertiseSitter,
				ExpertiseSitterDTO.class);

		return new ResponseEntity<>(updatedExpertiseSitterDTO, HttpStatus.OK);
	}
	
  /**
   * Merge Expertise Sitter identified by {@code expertiseSitterId}.
   */
  @ApiOperation(value = "Merge Expertise Sitter")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Sitter not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{mergeToId}", params = {"mergeFromId"},
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<ExpertiseSitterDTO> mergeExpertiseSitter(
      @PathVariable("mergeToId") final String mergeToId,
      @RequestParam("mergeFromId") final String mergeFromId) {
    final ExpertiseSitter mergedExpertiseSitter =
        expertiseSitterService.mergeExpertiseSitter(Long.valueOf(mergeToId), Long.valueOf(mergeFromId));
    final ExpertiseSitterDTO mergedExpertiseSitterDTO = expertiseSitterModelMapper.map(mergedExpertiseSitter,
        ExpertiseSitterDTO.class);
    return new ResponseEntity<>(mergedExpertiseSitterDTO, HttpStatus.OK);
  }

}
