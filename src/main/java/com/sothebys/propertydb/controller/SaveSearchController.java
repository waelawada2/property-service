package com.sothebys.propertydb.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.dto.SaveSearchDTO;
import com.sothebys.propertydb.dto.request.SaveSearchRequestDTO;
import com.sothebys.propertydb.exception.ForbiddenException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.mapper.SaveSearchModelMapper;
import com.sothebys.propertydb.model.LevelOfDetail;
import com.sothebys.propertydb.model.SaveSearch;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.search.SaveSearchBuilder;
import com.sothebys.propertydb.search.SaveSearchInputDTO;
import com.sothebys.propertydb.search.SaveSearchUtil;
import com.sothebys.propertydb.service.SaveSearchService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * REST controller that exposes country details.
 *
 * @author SrinivasaRao.Batthula
 *
 */
@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*", maxAge = 3600)
public class SaveSearchController {
  @Autowired
  private SaveSearchModelMapper saveSearchModelMapper;

  @Autowired
  private SaveSearchService saveSearchService;

  @Autowired
  SaveSearchBuilder saveSearchBuilder;

  /**
   * Search for save searches. Results are either the full saveSearch entities or only the
   * saveSearch IDs, depending on the value of the provided {@code lod} parameter.
   * <p/>
   * Note: Search is case insensitive.
   *
   * @param saveSearchInputDTO SaveSearchInputDTO
   * @param pageRequest The information of the requested page.
   * @param userName The information of the requested page.
   * @return
   */
  @ApiOperation(value = "Search saveSearches based on input Parameters")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Input parameter validation failed"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/{user-name}/searches")
  public ResponseEntity<Page<?>> findBySearchTerm(
      @ModelAttribute final SaveSearchInputDTO saveSearchInputDTO, final Pageable pageRequest,
      @PathVariable("user-name") String userName, @Valid @RequestParam(value = "lod",
          required = false, defaultValue = "MAX") LevelOfDetail lod) {
    final Page<?> resultsPage;

    long userId = saveSearchService.findUserId(userName);
    saveSearchInputDTO.setUserId(userId);
    boolean doSearch = SaveSearchUtil.validateSaveSearchDTO(saveSearchInputDTO);
    if (doSearch) {
      Specification<SaveSearch> specification =
          saveSearchBuilder.createSpecificationBuilder(saveSearchInputDTO).build();
      Page<SaveSearch> searchResultPage =
          saveSearchService.findBySearchTerm(specification, pageRequest);
      resultsPage = lod == LevelOfDetail.MIN
          ? saveSearchModelMapper.mapEntityPageIntoDTOPageWithIdsOnly(pageRequest, searchResultPage)
          : saveSearchModelMapper.mapEntityPageIntoDTOPage(pageRequest, searchResultPage);
    } else {
      Page<SaveSearch> allSaveSearchesPage =
          saveSearchService.findSaveSearchesByUserId(pageRequest, userId);
      resultsPage = lod == LevelOfDetail.MIN
          ? saveSearchModelMapper.mapEntityPageIntoDTOPageWithIdsOnly(pageRequest,
              allSaveSearchesPage)
          : saveSearchModelMapper.mapEntityPageIntoDTOPage(pageRequest, allSaveSearchesPage);
    }

    return new ResponseEntity<>(resultsPage, HttpStatus.OK);
  }

  /**
   * Get save searches based on userName and externalId.
   */
  @ApiOperation(value = "Retrieving save searches based on userName and externalId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/{user-name}/searches/{external-id}")
  public ResponseEntity<SaveSearchDTO> findSaveSearchesByExternalId(
      @PathVariable("user-name") String userName, @PathVariable("external-id") String externalId) {

    final SaveSearch saveSearch =
        saveSearchService.findSaveSearchesByExternalId(userName, externalId);
    if (saveSearch == null) {
      throw new NotFoundException(String.format("No save search found with ID=%s", externalId));
    }
    final SaveSearchDTO saveSearchDTOs = saveSearchModelMapper.map(saveSearch, SaveSearchDTO.class);
    return new ResponseEntity<>(saveSearchDTOs, HttpStatus.OK);
  }
  

  /**
   * Get save searches based on externalId.
   */
  @ApiOperation(value = "Retrieving save searches based on externalId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/searches/{external-id}")
  public ResponseEntity<SaveSearchDTO> findSaveSearchesByExternalId(
       @PathVariable("external-id") String externalId) {

    final SaveSearch saveSearch =
        saveSearchService.findSaveSearchesByExternalId(externalId);
    if (saveSearch == null) {
      throw new NotFoundException(String.format("No save search found with ID=%s", externalId));
    }
    final SaveSearchDTO saveSearchDTOs = saveSearchModelMapper.map(saveSearch, SaveSearchDTO.class);
    return new ResponseEntity<>(saveSearchDTOs, HttpStatus.OK);
  }

  /**
   * Create a new save Search.
   */
  @ApiOperation(value = "Create Save Search")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Success"),
      @ApiResponse(code = 500, message = "Server error"),
      @ApiResponse(code = 404, message = "User name not found")})
  @PostMapping("/{user-name}/searches")
  public ResponseEntity<SaveSearchDTO> createSaveSearch(@PathVariable("user-name") String userName,
      @RequestBody final SaveSearchRequestDTO saveSearchRequestDTO,
      @RequestAttribute(name = "requestUser") User user) {

    if (user == null || !userName.equalsIgnoreCase(user.getUserName())) {
      throw new ForbiddenException(String.format(
          "userName '%s' doesn't match with user object name %s, hence create save search failed",
          userName, user.getUserName()));
    }

    final SaveSearch saveSearch = saveSearchModelMapper.map(saveSearchRequestDTO, SaveSearch.class);
    final SaveSearch createdSaveSearch =
        saveSearchService.createSaveSearch(saveSearch, user.getId().longValue());
    final SaveSearchDTO createdSaveSearchDTO = saveSearchModelMapper.mapToDTO(createdSaveSearch);

    HttpHeaders headers =
        RestControllerHelper.createHeadersWithLocation(createdSaveSearchDTO.getId());
    return new ResponseEntity<>(createdSaveSearchDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Delete a particular saved list of the user identified by {@code externalId}.
   */
  @ApiOperation(value = "Delete saved list of the user based on given externalId")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully deleted"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/{user-name}/searches/{external-id}")
  public void deleteSaveSearchListByExternalId(@PathVariable("user-name") String userName,
      @PathVariable("external-id") String externalId,
      @RequestAttribute(name = "requestUser") User user) {

    if (user == null || !userName.equalsIgnoreCase(user.getUserName())) {
      throw new ForbiddenException(String.format(
          "userName '%s' doesn't match with user object name %s, hence delete save search failed",
          userName, user.getUserName()));
    }
    saveSearchService.deleteByUserIdAndExternalId(user.getId(), externalId);
  }

  /**
   * Update a save search identified by {@code userId} and {@code externalId}.
   * 
   * @param userName
   * @param externalId
   * @return saveSearch
   */
  @ApiOperation(value = "Updating saveSearch details")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "external ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping("/{user-name}/searches/{external-id}")
  public ResponseEntity<SaveSearchDTO> updateSavedSearchByExternalId(
      @RequestBody final SaveSearchRequestDTO saveSearchRequestDTO,
      @PathVariable("user-name") String userName, @PathVariable("external-id") String externalId,
      @RequestAttribute(name = "requestUser") User user) {

    if (user == null || !userName.equalsIgnoreCase(user.getUserName())) {
      throw new ForbiddenException(String.format(
          "userName '%s' doesn't match with user object name %s, hence update save search failed",
          userName, user.getUserName()));
    }

    final SaveSearch saveSearch = saveSearchModelMapper.map(saveSearchRequestDTO, SaveSearch.class);
    final SaveSearch updatedSaveSearch =
        saveSearchService.updateByUserIdAndExternalId(user.getId(), externalId, saveSearch);
    final SaveSearchDTO updatedSaveSearchDTO =
        saveSearchModelMapper.map(updatedSaveSearch, SaveSearchDTO.class);

    return new ResponseEntity<>(updatedSaveSearchDTO, HttpStatus.OK);
  }
}
