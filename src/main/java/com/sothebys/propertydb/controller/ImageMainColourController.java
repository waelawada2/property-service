package com.sothebys.propertydb.controller;

import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.exception.NotFoundException;
import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sothebys.propertydb.dto.ImageMainColourDTO;
import com.sothebys.propertydb.mapper.ImageMainColourModelMapper;
import com.sothebys.propertydb.model.ImageMainColour;
import com.sothebys.propertydb.service.ImageMainColourService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author aneesha.l
 * @author Sankeerth.Kandru
 *
 */
@RestController
@RequestMapping("/imageMainColours")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ImageMainColourController {

	@Autowired
	ImageMainColourModelMapper imageMainColourModelMapper;
	
	@Autowired
	ImageMainColourService imageMainColourService;
	
	/**
	 * Get all main colours.
	 */
	@ApiOperation(value = "Retrieving all the image colours")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Server error") })
	@GetMapping
	public ResponseEntity<List<ImageMainColourDTO>> findAllImageMainCOlours() {
		final List<ImageMainColour> imageMainColours = imageMainColourService.findAllImageMainColours();
		final List<ImageMainColourDTO> imageMainColourDTOs = imageMainColourModelMapper.mapToImageMainColourDTOs(imageMainColours);

    return new ResponseEntity<>(imageMainColourDTOs, HttpStatus.OK);
  }

  /**
   * Retrieves the Image MainColour identified by Id.
   */
  @ApiOperation(value = "Retrieve Image Position details based on imageMainColourId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/imageMainColour/{imageMainColourId}")
  public ResponseEntity<ImageMainColourDTO> getImageMianColourById(
      @PathVariable("imageMainColourId") final Long imageMainColourId) {
    final ImageMainColour imageMainColour = imageMainColourService.getImageMainColourById(imageMainColourId);
    if (imageMainColour == null) {
      throw new NotFoundException(String.format("No ImageMainColour type found with ID=%d", imageMainColourId));
    }
    final ImageMainColourDTO imageMainColourDTO =
        imageMainColourModelMapper.map(imageMainColour, ImageMainColourDTO.class);

    return new ResponseEntity<>(imageMainColourDTO, HttpStatus.OK);
  }

  /**
   * Create a new Image MainColour.
   */
  @ApiOperation(value = "Create ImageMainColour")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Invalid input message"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping
  public ResponseEntity<ImageMainColourDTO> createImageMainColour(
      @Valid @RequestBody final ImageMainColourDTO imageMainColourDTO) {
    final ImageMainColour imageMainColour = imageMainColourModelMapper
        .map(imageMainColourDTO, ImageMainColour.class);

    final ImageMainColour careatedImageMainColour = imageMainColourService
        .createImageMainColour(imageMainColour);
    final ImageMainColourDTO createdImageMainColourDTO = imageMainColourModelMapper
        .map(careatedImageMainColour, ImageMainColourDTO.class);
    HttpHeaders headers = RestControllerHelper
        .createHeadersWithLocation(createdImageMainColourDTO.getId());
    return new ResponseEntity<>(createdImageMainColourDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Update a image main colour identified by {@code imageMainColourId}.
   */
  @ApiOperation(value = "Updating Image MainColour object deatils")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{imageMainColourId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<ImageMainColourDTO> updateImageMainColour(
      @Valid @RequestBody final ImageMainColourDTO imageMainColourDTO,
      @PathVariable("imageMainColourId") Long imageMainColourId) {
    final ImageMainColour imageMainColour = imageMainColourModelMapper
        .map(imageMainColourDTO, ImageMainColour.class);
    final ImageMainColour updatedImageMainCOlour = imageMainColourService
        .updateImageMainColourById(imageMainColourId, imageMainColour);
    final ImageMainColourDTO updatedimageMainColourDTO = imageMainColourModelMapper
        .map(updatedImageMainCOlour, ImageMainColourDTO.class);

    return new ResponseEntity<>(updatedimageMainColourDTO, HttpStatus.OK);
  }

  /**
   * Delete an existing image main colour identified by the given {@code imageMainColourId}.
   */
  @ApiOperation(value = "Delete Image MainColour based on given imageMainColourId")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully imageMainColour deleted"),
      @ApiResponse(code = 404, message = "imageMainColour id not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/{imageMainColourId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void deleteImageMainColour(@PathVariable("imageMainColourId") Long imageMainColourId) {
    imageMainColourService.deleteImageMainColourById(imageMainColourId);
  }
  
  /**
   * Merge a ImageMainColour identified by {@code ImageMainColourId}.
   */
  @ApiOperation(value = "Merge Image MainColour")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "ImageMainColour not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{mergeToId}", params = {"mergeFromId"},
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<ImageMainColourDTO> mergeImageMainColour(
      @PathVariable("mergeToId") final String mergeToId,
      @RequestParam("mergeFromId") final String mergeFromId) {

    ImageMainColour mergedImageMainColour = imageMainColourService
        .mergeImageMainColour(Long.valueOf(mergeToId), Long.valueOf(mergeFromId));
    final ImageMainColourDTO mergedImageMainColourDTO =
        imageMainColourModelMapper.map(mergedImageMainColour, ImageMainColourDTO.class);
    return new ResponseEntity<>(mergedImageMainColourDTO, HttpStatus.OK);
  }

}
