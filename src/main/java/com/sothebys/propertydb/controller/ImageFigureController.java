/**
 * 
 */
package com.sothebys.propertydb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sothebys.propertydb.dto.ImageFigureDTO;
import com.sothebys.propertydb.mapper.ImageFigureModelMapper;
import com.sothebys.propertydb.model.ImageFigure;
import com.sothebys.propertydb.service.ImageFigureService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author aneesha.l
 *
 */
@RestController
@RequestMapping("/imageFigures")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ImageFigureController {

	@Autowired
	ImageFigureModelMapper imageFigureModelMapper;

	@Autowired
	ImageFigureService imageFigureService;

	/**
	 * Get all figures.
	 */
	@ApiOperation(value = "Retrieving all the figures")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"), @ApiResponse(code = 500, message = "Server error") })
	@GetMapping
	public ResponseEntity<List<ImageFigureDTO>> findAllImageFigures() {
		final List<ImageFigure> imageFigures = imageFigureService.findAllImageFigures();
		final List<ImageFigureDTO> imageFigureDTOs = imageFigureModelMapper.mapToImageFigureDTOs(imageFigures);

		return new ResponseEntity<>(imageFigureDTOs, HttpStatus.OK);
	}
	

}
