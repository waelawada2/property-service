package com.sothebys.propertydb.controller;

import static java.util.Collections.singletonMap;

import com.sothebys.propertydb.service.FileService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author SrinivasaRao.Batthula
 */
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@Log4j
public class FileController {

  @Autowired
  FileService fileService;

  @Value("${aws.s3.imports.bucket}")
  private String bucketName;

  /**
   * To upload a file to the server with .csv format
   *
   * @param multipart The multi-part data.
   * @return file upload info.
   */
  @ApiOperation(value = "Upload csv file")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "File upload failed"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(value = "/fileUpload")
  public ResponseEntity<?> fileUpload(@RequestPart("file") MultipartFile multipart,
      @RequestParam(value = "type") String type) {
    ResponseEntity<?> result;
    if (!multipart.isEmpty()) {
      try {
        String fileName = fileService.uploadFile(multipart, type);
        log.info("File upload successful at " + fileName);
        result = ResponseEntity.ok(singletonMap("result", "File upload successful at " + fileName));
      } catch (Exception e) {
        result = ResponseEntity.badRequest().body(e);
      }
    } else {
      result = ResponseEntity.badRequest().body(singletonMap("error", "File uploaded is empty"));
    }
    return result;
  }

}
