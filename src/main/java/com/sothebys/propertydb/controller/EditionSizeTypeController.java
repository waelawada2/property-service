package com.sothebys.propertydb.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.dto.EditionSizeTypeDTO;
import com.sothebys.propertydb.mapper.EditionSizeTypeModelMapper;
import com.sothebys.propertydb.model.EditionSizeType;
import com.sothebys.propertydb.service.EditionSizeTypeService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * REST controller that exposes edition size type details.
 *
 * @author SrinivasaRao.Batthula
 *
 */
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/edition-size-types")
public class EditionSizeTypeController {

  @Autowired
  private EditionSizeTypeModelMapper editionSizeTypeModelMapper;

  @Autowired
  private EditionSizeTypeService editionSizeTypeService;

  /**
   * Get all edition Size types.
   */
  @ApiOperation(value = "Retrieving all the edition size types")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping
  public ResponseEntity<List<EditionSizeTypeDTO>> findAllEditionSizeTypes() {
    final List<EditionSizeType> editionSizeTypes = editionSizeTypeService.findAllEditionSizeTypes();
    final List<EditionSizeTypeDTO> editionSizeTypeDTOs =
        editionSizeTypeModelMapper.mapToDTO(editionSizeTypes);

    return new ResponseEntity<>(editionSizeTypeDTOs, HttpStatus.OK);
  }

  /**
   * Create edition size type.
   */
  @ApiOperation(value = "Create EditionSizeType")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Invalid input message"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<EditionSizeTypeDTO> createEditionSizeType(
      @Valid @RequestBody final EditionSizeTypeDTO editionSizeTypeDTO) {
    final EditionSizeType editionSizeType =
        editionSizeTypeModelMapper.map(editionSizeTypeDTO, EditionSizeType.class);
    final EditionSizeType createdEditionSizeType =
        editionSizeTypeService.createEditionSizeType(editionSizeType);
    final EditionSizeTypeDTO createdEditionSizeTypeDTO =
        editionSizeTypeModelMapper.map(createdEditionSizeType, EditionSizeTypeDTO.class);

    HttpHeaders headers =
        RestControllerHelper.createHeadersWithLocation(createdEditionSizeTypeDTO.getId());
    return new ResponseEntity<>(createdEditionSizeTypeDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Delete an edition size type identified by {@code editionSizeTypeId}.
   */
  @ApiOperation(value = "Delete edition size type based on given edition size type ID")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully deleted"),
      @ApiResponse(code = 404, message = "Edition size type ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/{editionSizeTypeId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void deleteEditionSizeType(@PathVariable("editionSizeTypeId") Long editionSizeTypeId) {
    editionSizeTypeService.deleteEditionSizeType(editionSizeTypeId);
  }

  /**
   * Get an edition size type identified by {@code editionSizeTypeId}.
   */
  @ApiOperation(value = "Retrieve editionSizeType details based on editionSizeTypeId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/{editionSizeTypeId}")
  public ResponseEntity<EditionSizeTypeDTO> findEditionSizeTypeById(
      @PathVariable("editionSizeTypeId") final Long editionSizeTypeId) {
    final EditionSizeType editionSizeType =
        editionSizeTypeService.findEditionSizeTypeById(editionSizeTypeId);
    final EditionSizeTypeDTO editionSizeTypeDTO =
        editionSizeTypeModelMapper.map(editionSizeType, EditionSizeTypeDTO.class);
    return new ResponseEntity<>(editionSizeTypeDTO, HttpStatus.OK);
  }

  /**
   * Update an edition size type.
   */
  @ApiOperation(value = "Updating edition size type deatils")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Invalid input message"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping("/{editionSizeTypeId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<EditionSizeTypeDTO> updateEditionSizeType(
      @Valid @RequestBody final EditionSizeTypeDTO editionSizeTypeDTO,
      @PathVariable("editionSizeTypeId") Long editionSizeTypeId) {
    final EditionSizeType editionSizeType =
        editionSizeTypeModelMapper.map(editionSizeTypeDTO, EditionSizeType.class);
    final EditionSizeType updatedEditionSizeType =
        editionSizeTypeService.updateEditionSizeType(editionSizeTypeId, editionSizeType);
    final EditionSizeTypeDTO updatedEditionSizeTypeDTO =
        editionSizeTypeModelMapper.map(updatedEditionSizeType, EditionSizeTypeDTO.class);

    return new ResponseEntity<>(updatedEditionSizeTypeDTO, HttpStatus.OK);
  }
  
  /**
   * Merge a EditionSize Type identified by {@code EdistionSizeTypeId}.
   */
  @ApiOperation(value = "Merge EditionSizeTypes")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "EditionSizeType not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{mergeToId}", params = {"mergeFromId"},
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<EditionSizeTypeDTO> mergeEditionSizeType(
      @PathVariable("mergeToId") final String mergeToId,
      @RequestParam("mergeFromId") final String mergeFromId) {

    EditionSizeType mergedEditionSizeType = editionSizeTypeService
        .mergeEditionSizeType(Long.valueOf(mergeToId), Long.valueOf(mergeFromId));
    final EditionSizeTypeDTO mergedEditionSizeTypeDTO =
        editionSizeTypeModelMapper.map(mergedEditionSizeType, EditionSizeTypeDTO.class);
    return new ResponseEntity<>(mergedEditionSizeTypeDTO, HttpStatus.OK);
  }

}
