/**
 * 
 */
package com.sothebys.propertydb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sothebys.propertydb.dto.ImageGenderDTO;
import com.sothebys.propertydb.mapper.ImageGenderModelMapper;
import com.sothebys.propertydb.model.ImageGender;
import com.sothebys.propertydb.service.ImageGenderService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author aneesha.l
 *
 */
@RestController
@RequestMapping("/imageGenders")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ImageGenderController {

	@Autowired
	ImageGenderModelMapper imageGenderModelMapper;
	
	@Autowired
	ImageGenderService imageGenderService;
	
	
	/**
	 * Get all main colours.
	 */
	@ApiOperation(value = "Retrieving all the main colours")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Server error") })
	@GetMapping
	public ResponseEntity<List<ImageGenderDTO>> findAllImageGenders() {
		final List<ImageGender> imageGenders = imageGenderService.findAllImageGenders();
		final List<ImageGenderDTO> imageGenderDTOs = imageGenderModelMapper.mapToImageGenderDTOs(imageGenders);

		return new ResponseEntity<>(imageGenderDTOs, HttpStatus.OK);
		}

}
