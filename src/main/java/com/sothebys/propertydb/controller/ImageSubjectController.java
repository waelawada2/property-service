package com.sothebys.propertydb.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.dto.ImageSubjectDTO;
import com.sothebys.propertydb.mapper.ImageSubjectModelMapper;
import com.sothebys.propertydb.model.ImageSubject;
import com.sothebys.propertydb.service.ImageSubjectService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * REST controller that exposes ImageSubject details.
 *
 * @author aneesha.l
 * @author Sankeerth.Kandru
 *
 */
@RestController
@RequestMapping("/imageSubjects")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ImageSubjectController {

	@Autowired
	ImageSubjectModelMapper imageSubjectModelMapper;
	
	@Autowired
	ImageSubjectService imageSubjectService;
	
	/**
	 * Get all subjects.
	 */
	@ApiOperation(value = "Retrieving all the subjects")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Server error") })
	@GetMapping
	public ResponseEntity<List<ImageSubjectDTO>> findAllImageSubjects() {
		final List<ImageSubject> imageSubjects = imageSubjectService.findAllImageSubjects();
		final List<ImageSubjectDTO> imageSubjectDTOs = imageSubjectModelMapper.mapToImageSubjectDTOs(imageSubjects);

		return new ResponseEntity<>(imageSubjectDTOs, HttpStatus.OK);
		}
	
	/**
	 * Get all subjects by name.
	 */
	@ApiOperation(value = "Retrieving all the subject")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Server error") })
	@GetMapping("/{name}")
	public ResponseEntity<List<ImageSubjectDTO>> findAllImageSubjectsByName(@PathVariable("name") String name) {
		name = name != null ? name : "";
		final List<ImageSubject> imageSubjects = imageSubjectService.findAllImageSubjectsByName(name);
		final List<ImageSubjectDTO> imageSubjectDTOs = imageSubjectModelMapper.mapToImageSubjectDTOs(imageSubjects);

    return new ResponseEntity<>(imageSubjectDTOs, HttpStatus.OK);
  }

  /**
   * Create a new ImageSubject.
   */
  @ApiOperation(value = "Create ImageSubject")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Invalid input message"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping
  public ResponseEntity<ImageSubjectDTO> createImageSubject(
      @Valid @RequestBody final ImageSubjectDTO ImageSubjectDTO) {
    final ImageSubject ImageSubject = imageSubjectModelMapper
        .map(ImageSubjectDTO, ImageSubject.class);

    final ImageSubject createdImageSubject = imageSubjectService
        .createImageSubject(ImageSubject);
    final ImageSubjectDTO createdImageSubjectDTO = imageSubjectModelMapper
        .map(createdImageSubject, ImageSubjectDTO.class);
    HttpHeaders headers = RestControllerHelper
        .createHeadersWithLocation(createdImageSubjectDTO.getId());
    return new ResponseEntity<>(createdImageSubjectDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Update a ImageSubject identified by {@code imageSubjectId}.
   */
  @ApiOperation(value = "Updating ImageSubject object deatils")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{imageSubjectId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<ImageSubjectDTO> updateImageSubject(
      @Valid @RequestBody final ImageSubjectDTO ImageSubjectDTO,
      @PathVariable("imageSubjectId") Long imageSubjectId) {
    final ImageSubject ImageSubject = imageSubjectModelMapper
        .map(ImageSubjectDTO, ImageSubject.class);
    final ImageSubject updatedImageSubject = imageSubjectService
        .updateImageSubjectById(imageSubjectId, ImageSubject);
    final ImageSubjectDTO updatedImageSubjectDTO = imageSubjectModelMapper
        .map(updatedImageSubject, ImageSubjectDTO.class);

    return new ResponseEntity<>(updatedImageSubjectDTO, HttpStatus.OK);
  }

  /**
   * Delete an existing ImageSubject identified by the given {@code imageSubjectId}
   */
  @ApiOperation(value = "Delete ImageSubject based on given ImageSubjectId")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully ImageSubject deleted"),
      @ApiResponse(code = 404, message = "ImageSubject id not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/{imageSubjectId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void deleteImageSubject(@PathVariable("imageSubjectId") Long imageSubjectId) {
    imageSubjectService.deleteImageSubjectById(imageSubjectId);
  }

  /**
   * Merge a Image Subject identified by {@code ImageSubjectId}.
   */
  @ApiOperation(value = "Merge Image Subject")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Image Subject not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{mergeToId}", params = {"mergeFromId"},
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<ImageSubjectDTO> mergeImageSubject(
      @PathVariable("mergeToId") final String mergeToId,
      @RequestParam("mergeFromId") final String mergeFromId) {
    final ImageSubject mergedImageSubject = 
        imageSubjectService.mergeImageSubject(Long.valueOf(mergeToId), Long.valueOf(mergeFromId));
    final ImageSubjectDTO mergedImageSubjectDTO = imageSubjectModelMapper
        .map(mergedImageSubject, ImageSubjectDTO.class);
    return new ResponseEntity<>(mergedImageSubjectDTO, HttpStatus.OK);
  }
}
