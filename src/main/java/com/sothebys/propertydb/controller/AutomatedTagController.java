package com.sothebys.propertydb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sothebys.propertydb.model.Orientation;
import com.sothebys.propertydb.model.Scale;
import com.sothebys.propertydb.service.AutomatedTagService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * REST controller that exposes automated tag details.
 *
 * @author aneeshal
 *
 */
@RestController
@RequestMapping("/automatedTags")
@CrossOrigin(origins = "*", maxAge = 3600)
public class AutomatedTagController {

	@Autowired
	AutomatedTagService automatedTagService;

	/**
	 * Get all orientations.
	 */
	@ApiOperation(value = "Retrieving all the orientations")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Server error") })
	@GetMapping("/orientations")
	public ResponseEntity<Orientation[]> getAllOrientations() {

		final Orientation[] orientations = automatedTagService.getAllOrientation();
		return new ResponseEntity<>(orientations, HttpStatus.OK);
	}

	/**
	 * Get all scales.
	 */
	@ApiOperation(value = "Retrieving all the scales")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 500, message = "Server error") })
	@GetMapping("/scales")
	public ResponseEntity<Scale[]> getAllScales() {

		final Scale[] scales = automatedTagService.getAllScales();
		return new ResponseEntity<>(scales, HttpStatus.OK);
	}

}
