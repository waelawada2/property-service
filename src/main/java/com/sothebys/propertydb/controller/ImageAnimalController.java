/**
 *
 */
package com.sothebys.propertydb.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.dto.ImageAnimalDTO;
import com.sothebys.propertydb.mapper.ImageAnimalModelMapper;
import com.sothebys.propertydb.model.ImageAnimal;
import com.sothebys.propertydb.service.ImageAnimalService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author aneesha.l
 */
@RestController
@RequestMapping("/imageAnimals")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ImageAnimalController {

  @Autowired
  ImageAnimalModelMapper imageAnimalModelMapper;

  @Autowired
  ImageAnimalService imageAnimalService;

  /**
   * Create Animal
   *
   * @return ImageAnimal
   */
  @ApiOperation(value = "Create Animal")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Input message validation failed"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ImageAnimalDTO> createImageAnimal(
      @Valid @RequestBody final ImageAnimalDTO imageAnimalDTO) {
    final ImageAnimal imageAnimal = imageAnimalModelMapper.map(imageAnimalDTO, ImageAnimal.class);
    final ImageAnimal createdImageAnimal = imageAnimalService.createImageAnimal(imageAnimal);
    final ImageAnimalDTO createdImageAnimalDTO = imageAnimalModelMapper.map(createdImageAnimal,
        ImageAnimalDTO.class);

    HttpHeaders headers = RestControllerHelper
        .createHeadersWithLocation(createdImageAnimalDTO.getId());
    return new ResponseEntity<>(createdImageAnimalDTO, headers, HttpStatus.CREATED);

  }

  /**
   * Get all animals.
   */
  @ApiOperation(value = "Retrieving all the animals")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping
  public ResponseEntity<List<ImageAnimalDTO>> findAllImageAnimals() {
    final List<ImageAnimal> imageAnimals = imageAnimalService.findAllImageAnimals();
    final List<ImageAnimalDTO> imageAnimalDTOs = imageAnimalModelMapper
        .mapToImageAnimalDTOs(imageAnimals);

    return new ResponseEntity<>(imageAnimalDTOs, HttpStatus.OK);
  }

  /**
   * Get all animals by name.
   */
  @ApiOperation(value = "Retrieving all the animals")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/{name}")
  public ResponseEntity<List<ImageAnimalDTO>> findAllImageAnimalsByName(
      @PathVariable("name") String name) {
    name = name != null ? name : "";
    final List<ImageAnimal> imageAnimals = imageAnimalService.findAllImageAnimalsByName(name);
    final List<ImageAnimalDTO> imageAnimalDTOs = imageAnimalModelMapper
        .mapToImageAnimalDTOs(imageAnimals);

    return new ResponseEntity<>(imageAnimalDTOs, HttpStatus.OK);
  }


  /**
   * Delete animal.
   */
  @ApiOperation(value = "Delete other tag")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully deleted"),
      @ApiResponse(code = 404, message = "Animal object not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/{id}")
  public void deleteImageAnimalById(@PathVariable("id") Long id) {
    imageAnimalService.deleteImageAnimal(id);
  }

  /**
   * Update the animal name.
   */
  @ApiOperation(value = "Updating animal details")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Input message validation failed"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ImageAnimalDTO> updateImageAnimal(
      @Valid @RequestBody final ImageAnimalDTO imageAnimalDTO) {
    final ImageAnimal imageAnimal = imageAnimalModelMapper.map(imageAnimalDTO, ImageAnimal.class);
    final ImageAnimal updatedImageAnimal = imageAnimalService.updateImageAnimal(imageAnimal);
    final ImageAnimalDTO updatedImageAnimalDTO = imageAnimalModelMapper.map(updatedImageAnimal,
        ImageAnimalDTO.class);

    return new ResponseEntity<>(updatedImageAnimalDTO, HttpStatus.OK);
  }
  
  /**
   * Merge a Image Animal identified by {@code ImageAnimalId}.
   */
  @ApiOperation(value = "Merge Image Animal")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Image Animal not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{mergeToId}", params = {"mergeFromId"},
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<ImageAnimalDTO> mergeImageAnimal(
      @PathVariable("mergeToId") final String mergeToId,
      @RequestParam("mergeFromId") final String mergeFromId) {
    ImageAnimal mergedImageAnimal =
        imageAnimalService.mergeImageAnimal(Long.valueOf(mergeToId), Long.valueOf(mergeFromId));
    final ImageAnimalDTO mergedImageAnimalDTO =
        imageAnimalModelMapper.map(mergedImageAnimal, ImageAnimalDTO.class);
    return new ResponseEntity<>(mergedImageAnimalDTO, HttpStatus.OK);
  }

}
