package com.sothebys.propertydb.controller.support;

import com.sothebys.propertydb.exception.AlreadyExistsException;
import com.sothebys.propertydb.exception.BadRequestException;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.ForbiddenException;
import com.sothebys.propertydb.exception.InputOutOfRangeException;
import com.sothebys.propertydb.exception.MergeException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.exception.PropertyServiceException;
import com.sothebys.propertydb.exception.TooLargeException;
import com.sothebys.propertydb.validation.HasArtistIdentifier;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * Exception handler for controllers.
 *
 * @author nagarajutalluri
 * @author Gregor Zurowski
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

  private static final String MSG_VALIDATION_FAILED = "Input validation failed";

  private static final String MSG_INVALID_PARAM_ENUM_VALUE =
      "The parameter must have one of the following values: %s";

  private static final String MSG_INVALID_PARAM_TYPE = "The parameter must be of type %s";
  private static final String JPA_SYSTEM_EXCEPTION = "JPASystemException ";
  private static final String INCORRECT_STRING_VALUE = "Incorrect string value";
  private static final String FOR_COLUMN = "for column";
  private static final String JPA_EXCEPTION_CHARACTER_NOT_SUPPORTED = "This character {0} is not supported by the importer";

  @ExceptionHandler({Exception.class})
  public ResponseEntity<ErrorDetails> exceptionHandler(Exception e) {
    ErrorDetails errorDetails = createDefaultErrorDetails(e, HttpStatus.INTERNAL_SERVER_ERROR);

    log.error("Server error", e);
    return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(PropertyServiceException.class)
  public ResponseEntity<ErrorDetails> exceptionHandler(PropertyServiceException pse) {
    ErrorDetails errorDetails = createDefaultErrorDetails(pse, HttpStatus.INTERNAL_SERVER_ERROR);

    log.error("Property service error", pse);
    return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
  }
  
  @ExceptionHandler({AlreadyExistsException.class, CannotPerformOperationException.class})
  public ResponseEntity<ErrorDetails> handle(Exception e) {
    ErrorDetails errorDetails = createDefaultErrorDetails(e, HttpStatus.CONFLICT);

    log.warn("Client error - conflict", e);
    return new ResponseEntity<>(errorDetails, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(NotFoundException.class)
  public ResponseEntity<ErrorDetails> handle(NotFoundException e) {
    ErrorDetails errorDetails = createDefaultErrorDetails(e, HttpStatus.NOT_FOUND);

    log.warn("Client error - not found", e);
    return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(ForbiddenException.class)
  public ResponseEntity<ErrorDetails> handle(ForbiddenException e) {
    ErrorDetails errorDetails = createDefaultErrorDetails(e, HttpStatus.FORBIDDEN);

    log.warn("Client error - access denied", e);
    return new ResponseEntity<>(errorDetails, HttpStatus.FORBIDDEN);
  }

  /**
   * Handles payload is too large errors.
   */
  @ExceptionHandler(TooLargeException.class)
  public ResponseEntity<ErrorDetails> handle(TooLargeException tle) {
    ErrorDetails errorDetails = createDefaultErrorDetails(tle, HttpStatus.PAYLOAD_TOO_LARGE);

    log.warn("Client error - payload too large", tle);
    return new ResponseEntity<>(errorDetails, HttpStatus.PAYLOAD_TOO_LARGE);
  }

  /**
   * Handles input out of range errors.
   */
  @ExceptionHandler(InputOutOfRangeException.class)
  public ResponseEntity<ErrorDetails> handle(InputOutOfRangeException tle) {
    ErrorDetails errorDetails = createDefaultErrorDetails(tle, HttpStatus.PRECONDITION_FAILED);

    log.warn("Client error - precondition failed: ", tle);
    return new ResponseEntity<>(errorDetails, HttpStatus.PRECONDITION_FAILED);
  }

  /**
   * Handles input validation errors
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorDetails> handleValidationError(MethodArgumentNotValidException manve) {
    ErrorDetails errorDetails =
        createDefaultErrorDetails(manve, HttpStatus.BAD_REQUEST, MSG_VALIDATION_FAILED);

    for (ObjectError objectError : manve.getBindingResult().getAllErrors()) {
      if (objectError != null
          && HasArtistIdentifier.class.getSimpleName().equals(objectError.getCode())) {
        errorDetails.setDetails(objectError.getDefaultMessage());
      }
    }

    for (FieldError fieldError : manve.getBindingResult().getFieldErrors()) {
      List<ValidationError> validationErrors =
          errorDetails.getValidationErrors().get(fieldError.getField());
      if (validationErrors == null) {
        validationErrors = new ArrayList<>();
        errorDetails.getValidationErrors().put(fieldError.getField(), validationErrors);
      }
      ValidationError validationError =
          new ValidationError(fieldError.getCode(), fieldError.getDefaultMessage());
      errorDetails.setDetails(validationError.getMessage());
      validationErrors.add(validationError);
    }

    log.warn("Client error - message validation failed", manve);
    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
  }

  /**
   * Handles parameter type errors
   */
  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public ResponseEntity<ErrorDetails> handleInvalidArgument(
      MethodArgumentTypeMismatchException matme) {
    ErrorDetails errorDetails =
        createDefaultErrorDetails(matme, HttpStatus.BAD_REQUEST, "Invalid parameter");

    Class<?> type = matme.getRequiredType();
    String message = type.isEnum()
        ? String.format(MSG_INVALID_PARAM_ENUM_VALUE,
        StringUtils.join(type.getEnumConstants(), ", "))
        : String.format(MSG_INVALID_PARAM_TYPE, type.getTypeName());

    ValidationError error = new ValidationError(type.getName(), message);
    errorDetails.getValidationErrors().put(matme.getName(), Collections.singletonList(error));

    log.warn("Client error - invalid parameter", matme);
    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
  }

  private ErrorDetails createDefaultErrorDetails(Exception e, HttpStatus httpStatus) {
    return createDefaultErrorDetails(e, httpStatus, httpStatus.getReasonPhrase());
  }

  private ErrorDetails createDefaultErrorDetails(Exception e, HttpStatus httpStatus, String title) {
    // formatter:off
    ErrorDetails errorDetails =
        ErrorDetails.builder().status(httpStatus.value()).title(title).details(e.getMessage())
            .type(e.getClass().getName()).timestamp(new Date().getTime()).build();
    // formatter:on

    return errorDetails;
  }

  @ExceptionHandler(MergeException.class)
  public ResponseEntity<ErrorDetails> handle(MergeException e) {
    ErrorDetails errorDetails = createDefaultErrorDetails(e, HttpStatus.MOVED_PERMANENTLY);
    errorDetails.setMergedTo(e.getMergedToId());

    log.warn("Client error - merged", e);
    return new ResponseEntity<>(errorDetails, HttpStatus.MOVED_PERMANENTLY);
  }


  @ExceptionHandler(JpaSystemException.class)
  public ResponseEntity<ErrorDetails> exceptionHandler(JpaSystemException e) {
    String responseMessage = JPA_SYSTEM_EXCEPTION;
    if (e.getCause().getCause() != null) {
      String effectiveMessage = e.getCause().getCause().getMessage();
      if (!StringUtils.isEmpty(effectiveMessage) && effectiveMessage
          .contains(INCORRECT_STRING_VALUE)) {
        String[] invalidChars = StringUtils
            .substringsBetween(effectiveMessage, INCORRECT_STRING_VALUE, FOR_COLUMN);
        if (!ArrayUtils.isEmpty(invalidChars)) {
          responseMessage = MessageFormat
              .format(JPA_EXCEPTION_CHARACTER_NOT_SUPPORTED, invalidChars[0]);
        }
      }
    }
    ErrorDetails errorDetails = createDefaultErrorDetails(e, HttpStatus.INTERNAL_SERVER_ERROR);
    errorDetails.setDetails(responseMessage);
    log.warn(JPA_SYSTEM_EXCEPTION, e);
    return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
  }
  
  @ExceptionHandler(BadRequestException.class)
  public ResponseEntity<ErrorDetails> handle(BadRequestException e) {
    ErrorDetails errorDetails = createDefaultErrorDetails(e, HttpStatus.BAD_REQUEST);

    log.warn("Client error - bad request", e);
    return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
  } 

}
