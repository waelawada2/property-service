package com.sothebys.propertydb.controller.support;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents a validation error
 *
 * @author Gregor Zurowski
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ValidationError {

  private String code;

  private String message;

}
