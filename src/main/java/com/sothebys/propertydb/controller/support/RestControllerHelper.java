package com.sothebys.propertydb.controller.support;

import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

/**
 * Contains utility methods for REST controllers.
 *
 * @author Gregor Zurowski
 */
public class RestControllerHelper {

  private RestControllerHelper() {
    throw new AssertionError();
  }

  /**
   * Provides a shortcut for creating {@code HttpHeader} instances with a Location header that can
   * be used when creating resources with POST calls. The Location header URI is built from the
   * current request context and appended with the provided {@code id} as a sub path.
   *
   * @param id The ID of the created resource.
   * @return {@code HttpHeader} that contains a Location header.
   */
  public static HttpHeaders createHeadersWithLocation(String id) {
    // @formatter:off
    URI location =
        ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri();
    // @formatter:on

    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.LOCATION, location.toASCIIString());
    return headers;
  }

  public static HttpHeaders createHeadersWithLocation(long id) {
    return createHeadersWithLocation(String.valueOf(id));
  }

}
