package com.sothebys.propertydb.controller.support;

import org.springframework.http.MediaType;

/**
 * @author Wael Awada
 */
public class AdditionalMediaType {

  public static final org.springframework.http.MediaType TEXT_CSV = MediaType.valueOf("text/csv");
  public static final String TEXT_CSV_VALUE = "text/csv";

}
