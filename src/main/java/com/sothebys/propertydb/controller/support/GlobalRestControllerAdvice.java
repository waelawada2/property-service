package com.sothebys.propertydb.controller.support;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.sothebys.propertydb.model.LevelOfDetail;

/**
 * @author Gregor Zurowski
 */
@RestControllerAdvice
public class GlobalRestControllerAdvice {

  @InitBinder
  public void registerCustomEditors(WebDataBinder binder) {
    binder.registerCustomEditor(LevelOfDetail.class, new LevelOfDetailPropertyEditor());
  }

}
