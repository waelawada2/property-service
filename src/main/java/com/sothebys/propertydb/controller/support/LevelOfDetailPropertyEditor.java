package com.sothebys.propertydb.controller.support;

import java.beans.PropertyEditorSupport;

import org.springframework.stereotype.Component;

import com.sothebys.propertydb.model.LevelOfDetail;

/**
 * A simple property editor that converts provided string values into {@link LevelOfDetail} enum
 * values in a case insensitive way.
 *
 * @author Gregor Zurowski
 */
@Component
public class LevelOfDetailPropertyEditor extends PropertyEditorSupport {

  @Override
  public void setAsText(String text) throws IllegalArgumentException {
    String capitalizedText = text.toUpperCase();
    LevelOfDetail levelOfDetail = LevelOfDetail.valueOf(capitalizedText);
    setValue(levelOfDetail);
  }
}
