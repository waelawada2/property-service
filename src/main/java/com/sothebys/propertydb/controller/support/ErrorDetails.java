package com.sothebys.propertydb.controller.support;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sothebys.propertydb.exception.MergeException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Represents error details that are returned to requesting clients.
 * <p/>
 * Influenced by <a href=https://tools.ietf.org/html/rfc7807>RFC 7807: Problem Details for HTTP
 * APIs</a>.
 *
 * @author Gregor Zurowski
 */
public class ErrorDetails {

  private int status;

  private String title;

  private String type;

  private String details;

  @JsonInclude(Include.NON_NULL)
  private String mergedTo;

  private long timestamp;

  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private Map<String, List<ValidationError>> validationErrors = new HashMap<>();

  public static ErrorDetailsBuilder builder() {
    return new ErrorDetailsBuilder();
  }

  public long getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }

  public String getMergedTo() {
    return mergedTo;
  }

  public void setMergedTo(String mergedTo) {
    this.mergedTo = mergedTo;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  public Map<String, List<ValidationError>> getValidationErrors() {
    return validationErrors;
  }

  public void setValidationErrors(
      Map<String, List<ValidationError>> validationErrors) {
    this.validationErrors = validationErrors;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  /**
   * Builder for creating {@link ErrorDetails} instances.
   */
  public static class ErrorDetailsBuilder {

    private int status;

    private long timestamp;

    private String title;

    private String type;

    private String details;

    private Map<String, List<ValidationError>> validationErrors;

    private String mergedTo;

    /**
     * @param status The HTTP status code.
     */
    public ErrorDetailsBuilder status(int status) {
      this.status = status;
      return this;
    }

    /**
     * @param timestamp A timestamp of when the error occured.
     */
    public ErrorDetailsBuilder timestamp(long timestamp) {
      this.timestamp = timestamp;
      return this;
    }

    /**
     * @param title A short summary of the error.
     */
    public ErrorDetailsBuilder title(String title) {
      this.title = title;
      return this;
    }

    /**
     * @param type The technical type of the error, e.g. the exception class name.
     */
    public ErrorDetailsBuilder type(String type) {
      this.type = type;
      return this;
    }

    /**
     * @param details Detailed error text, e.g. the exception message.
     */
    public ErrorDetailsBuilder details(String details) {
      this.details = details;
      return this;
    }

    /**
     * @param validationErrors A map of validation errors.
     */
    public ErrorDetailsBuilder validationErrors(
        Map<String, List<ValidationError>> validationErrors) {
      this.validationErrors = validationErrors;
      return this;
    }

    /**
     * @param mergedToId The id that the object was merged to. Specifically used for {@link
     *     MergeException}.
     */
    public ErrorDetailsBuilder mergedToId(String mergedToId) {
      this.mergedTo = mergedToId;
      return this;
    }

    /**
     * @return A {@link ErrorDetails} instance with the values created by the builder.
     */
    public ErrorDetails build() {
      ErrorDetails errorDetails = new ErrorDetails();
      errorDetails.setDetails(details);
      errorDetails.setStatus(status);
      errorDetails.setTimestamp(timestamp);
      errorDetails.setTitle(title);
      errorDetails.setType(type);
      if (validationErrors != null) {
        errorDetails.setValidationErrors(validationErrors);
      }
      errorDetails.setMergedTo(mergedTo);
      return errorDetails;
    }

  }

}
