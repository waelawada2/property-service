package com.sothebys.propertydb.controller.support;

/**
 * @author Wael Awada
 */
public enum ApplicationViews {
  PROPERTIES_LIST("properties/list"), ARTISTS_LIST("artists/list");

  private final String viewName;

  ApplicationViews(String viewName) {
    this.viewName = viewName;
  }

  public String getViewName() {
    return viewName;
  }
}
