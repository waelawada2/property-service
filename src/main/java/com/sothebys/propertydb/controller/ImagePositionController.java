package com.sothebys.propertydb.controller;

import com.sothebys.propertydb.controller.support.RestControllerHelper;
import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.sothebys.propertydb.dto.ImagePositionDTO;
import com.sothebys.propertydb.mapper.ImagePositionModelMapper;
import com.sothebys.propertydb.model.ImagePosition;
import com.sothebys.propertydb.service.ImagePositionService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * REST controller that exposes ImagePostion details.
 *
 * @author aneesha.l
 * @author Sankeerth.Kandru
 *
 */
@RestController
@RequestMapping("/imagePositions")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ImagePositionController {

	@Autowired
	ImagePositionModelMapper imagePositionModelMapper;
	
	@Autowired
	ImagePositionService imagePositionService;


  /**
   * Get all positions.
   */
  @ApiOperation(value = "Retrieving all the image positions")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping
  public ResponseEntity<List<ImagePositionDTO>> findAllImagePositions() {
    final List<ImagePosition> imagePositions = imagePositionService.findAllImagePositions();
    final List<ImagePositionDTO> imagePositionDTOs = imagePositionModelMapper
        .mapToImagePositionDTOs(imagePositions);

    return new ResponseEntity<>(imagePositionDTOs, HttpStatus.OK);
  }

  /**
   * Create a new Image Position.
   */
  @ApiOperation(value = "Create ImagePosition")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Invalid input message"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<ImagePositionDTO> createImagePosition(
      @Valid @RequestBody final ImagePositionDTO ImagePositionDTO) {
    final ImagePosition ImagePosition = imagePositionModelMapper
        .map(ImagePositionDTO, ImagePosition.class);

    final ImagePosition careatedImagePosition = imagePositionService
        .createImagePosition(ImagePosition);
    final ImagePositionDTO createdImagePositionDTO = imagePositionModelMapper
        .map(careatedImagePosition, ImagePositionDTO.class);
    HttpHeaders headers = RestControllerHelper
        .createHeadersWithLocation(createdImagePositionDTO.getId());
    return new ResponseEntity<>(createdImagePositionDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Update an Image Position identified by {@code ImagePositionId}.
   */
  @ApiOperation(value = "Updating Image Position object deatils")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{imagePositionId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<ImagePositionDTO> updateImagePosition(
      @Valid @RequestBody final ImagePositionDTO ImagePositionDTO,
      @PathVariable("imagePositionId") Long ImagePositionId) {
    final ImagePosition ImagePosition = imagePositionModelMapper
        .map(ImagePositionDTO, ImagePosition.class);
    final ImagePosition updatedImagePosition = imagePositionService
        .updateImagePositionById(ImagePositionId, ImagePosition);
    final ImagePositionDTO updatedImagePositionDTO = imagePositionModelMapper
        .map(updatedImagePosition, ImagePositionDTO.class);

    return new ResponseEntity<>(updatedImagePositionDTO, HttpStatus.OK);
  }

  /**
   * Delete an existing Image Position identified by the given {@code ImagePositionId}
   */
  @ApiOperation(value = "Delete Image Position based on given ImagePositionId")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully ImagePosition deleted"),
      @ApiResponse(code = 404, message = "ImagePosition id not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/{imagePositionId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void deleteImagePosition(@PathVariable("imagePositionId") Long ImagePositionId) {
    imagePositionService.deleteImagePositionById(ImagePositionId);
  }
  
  /**
   * Get all positions by name.
   */
  @ApiOperation(value = "Retrieving all the Image Positions by Name")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/{name}")
  public ResponseEntity<List<ImagePositionDTO>> findAllImagePositionsByName(
      @PathVariable("name") String name) {
    name = name != null ? name : "";
    final List<ImagePosition> imagePositions = imagePositionService.findAllImagePositionByName(name);
    final List<ImagePositionDTO> imagePositionDTOs = imagePositionModelMapper
        .mapToImagePositionDTOs(imagePositions);

    return new ResponseEntity<>(imagePositionDTOs, HttpStatus.OK);
  }

  /**
   * Merge a Image Position identified by {@code ImagePositionId}.
   */
  @ApiOperation(value = "Merge Image Position")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Image Position not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/{mergeToId}", params = {"mergeFromId"},
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<ImagePositionDTO> mergeImagePosition(
      @PathVariable("mergeToId") final String mergeToId,
      @RequestParam("mergeFromId") final String mergeFromId) {
    ImagePosition mergedImagePosition =
        imagePositionService.mergeImagePosition(Long.valueOf(mergeToId), Long.valueOf(mergeFromId));
    final ImagePositionDTO mergedImagePositionDTO =
        imagePositionModelMapper.map(mergedImagePosition, ImagePositionDTO.class);
    return new ResponseEntity<>(mergedImagePositionDTO, HttpStatus.OK);
  }
}
