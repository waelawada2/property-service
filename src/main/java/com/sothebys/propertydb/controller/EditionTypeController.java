/*
 * -------------------------------------------------------------------------
 *
 * (C) Copyright / Sotheby's. All rights reserved. The contents of this file represent Sotheby's
 * trade secrets and are confidential. Use outside of Sotheby's is prohibited and in violation of
 * copyright law.
 *
 * -------------------------------------------------------------------------
 */
package com.sothebys.propertydb.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.dto.EditionTypeDTO;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.mapper.EditionTypeModelMapper;
import com.sothebys.propertydb.model.EditionType;
import com.sothebys.propertydb.service.EditionTypeService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j;

/**
 * REST controller that exposes endpoints for edition type management.
 *
 * @author SrinivasaRao.Batthula
 * @author Gregor Zurowski
 *
 */
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@Log4j
public class EditionTypeController {

  @Autowired
  EditionTypeService editionTypeService;

  @Autowired
  EditionTypeModelMapper editionTypeModelMapper;

  /**
   * Create edition type.
   */
  @ApiOperation(value = "Create EditionType")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Invalid input message"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping("/edition-types")
  public ResponseEntity<EditionTypeDTO> createEditionType(
      @Valid @RequestBody final EditionTypeDTO editionTypeDTO) {
    final EditionType editionType = editionTypeModelMapper.map(editionTypeDTO, EditionType.class);
    final EditionType createdEditionType = editionTypeService.createEditionType(editionType);
    final EditionTypeDTO createdEditionTypeDTO =
        editionTypeModelMapper.map(createdEditionType, EditionTypeDTO.class);

    HttpHeaders headers =
        RestControllerHelper.createHeadersWithLocation(createdEditionTypeDTO.getId());
    return new ResponseEntity<>(createdEditionTypeDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Get all edition types.
   */
  @ApiOperation(value = "Retrieving all the edition types")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/edition-types")
  public ResponseEntity<List<EditionTypeDTO>> findAllEditionTypes() {
    final List<EditionType> editionTypes = editionTypeService.getAllEditionTypes();
    final List<EditionTypeDTO> editionTypeDTOs = editionTypeModelMapper.mapToDTO(editionTypes);

    return new ResponseEntity<>(editionTypeDTOs, HttpStatus.OK);
  }

  /**
   * Delete an edition type identified by {@code editionTypeId}.
   */
  @ApiOperation(value = "Delete edition type based on given edition type ID")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully deleted"),
      @ApiResponse(code = 404, message = "Edition type ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/edition-types/{editionTypeId}")
  public void deleteEditionType(@PathVariable("editionTypeId") Long editionTypeId) {
    editionTypeService.deleteEditionType(editionTypeId);
  }

  /**
   * Get an edition type identified by {@code editionTypeId}.
   */
  @ApiOperation(value = "Retrieve editionType details based on editionTypeId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/edition-types/{editionTypeId}")
  public ResponseEntity<EditionTypeDTO> getEditionTypeById(
      @PathVariable("editionTypeId") final Long editionTypeId) {
    final EditionType editionType = editionTypeService.getEditionTypeById(editionTypeId);
    if (editionType == null) {
      throw new NotFoundException(String.format("No edition type found with ID=%d", editionTypeId));
    }
    final EditionTypeDTO editionTypeDTO =
        editionTypeModelMapper.map(editionType, EditionTypeDTO.class);

    return new ResponseEntity<>(editionTypeDTO, HttpStatus.OK);
  }

  /**
   * Update an edition type.
   */
  @ApiOperation(value = "Updating edition type deatils")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Invalid input message"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping("/edition-types/{editionTypeId}")
  public ResponseEntity<EditionTypeDTO> updateEditionType(
      @Valid @RequestBody final EditionTypeDTO editionTypeDTO,
      @PathVariable("editionTypeId") Long editionTypeId) {
    final EditionType editionType = editionTypeModelMapper.map(editionTypeDTO, EditionType.class);
    final EditionType updatedEditionType =
        editionTypeService.updateEditionType(editionTypeId, editionType);
    final EditionTypeDTO updatedEditionTypeDTO =
        editionTypeModelMapper.map(updatedEditionType, EditionTypeDTO.class);

    return new ResponseEntity<>(updatedEditionTypeDTO, HttpStatus.OK);
  }

}
