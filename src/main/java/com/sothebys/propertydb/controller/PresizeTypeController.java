/**
 *
 */
package com.sothebys.propertydb.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sothebys.propertydb.controller.support.RestControllerHelper;
import com.sothebys.propertydb.dto.PresizeTypeDTO;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.PresizeType;
import com.sothebys.propertydb.service.PresizeTypeService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j;

/**
 * @author SrinivasaRao.Batthula
 * @author Gregor Zurowski
 *
 */
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@Log4j
public class PresizeTypeController {
  ModelMapper modelMapper = new ModelMapper();

  @Autowired
  PresizeTypeService presizeTypeService;

  /**
   * Create a presize type
   */
  @ApiOperation(value = "Create PresizeType")
  @ApiResponses(value = {@ApiResponse(code = 201, message = "Created"),
      @ApiResponse(code = 400, message = "Invalid input message"),
      @ApiResponse(code = 500, message = "Server error")})
  @PostMapping("/presize-types")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<PresizeTypeDTO> createPresizeType(
      @Valid @RequestBody final PresizeTypeDTO presizeTypeDtO) {
    final PresizeType presizeType = modelMapper.map(presizeTypeDtO, PresizeType.class);
    final PresizeType createdPresizeType = presizeTypeService.createPresizeType(presizeType);
    final PresizeTypeDTO createdPresizeTypeDTO =
        modelMapper.map(createdPresizeType, PresizeTypeDTO.class);

    HttpHeaders headers =
        RestControllerHelper.createHeadersWithLocation(createdPresizeTypeDTO.getId());
    return new ResponseEntity<>(createdPresizeTypeDTO, headers, HttpStatus.CREATED);
  }

  /**
   * Get all presize types.
   */
  @ApiOperation(value = "Retrieving all the presizeTypes")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/presize-types")
  public ResponseEntity<List<PresizeTypeDTO>> findAllPresizeTypes() {
    List<PresizeType> presizeTypeList = presizeTypeList = presizeTypeService.getAllPresizeTypes();
    List<PresizeTypeDTO> presizeTypesDTO = new ArrayList<>(presizeTypeList.size());

    for (PresizeType presizeType : presizeTypeList) {
      presizeTypesDTO.add(modelMapper.map(presizeType, PresizeTypeDTO.class));
    }

    return new ResponseEntity<>(presizeTypesDTO, HttpStatus.OK);
  }

  /**
   * Get a presize type identified by {@code presizeTypeId}.
   */
  @ApiOperation(value = "Retrieve presizeType object details based on presizeTypeId")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "Presize type not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping("/presize-types/{presizeTypeId}")
  public ResponseEntity<PresizeTypeDTO> getPresizeTypeById(
      @PathVariable("presizeTypeId") Long presizeTypeId) {
    PresizeType presizeType = presizeTypeService.getPresizeTypeById(presizeTypeId);

    if (presizeType == null) {
      throw new NotFoundException(String.format("No presize type found with ID=%d", presizeTypeId));
    }

    PresizeTypeDTO presizeTypeDTO = modelMapper.map(presizeType, PresizeTypeDTO.class);
    return new ResponseEntity<>(presizeTypeDTO, HttpStatus.OK);
  }

  /**
   * Update a presize type.
   */
  @ApiOperation(value = "Updating PresizeType deatils")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Invalid input message"),
      @ApiResponse(code = 404, message = "Presize type not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping("/presize-types/{presizeTypeId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<PresizeTypeDTO> updatePresizeType(
      @Valid @RequestBody final PresizeTypeDTO presizeTypeDTO,
      @PathVariable("presizeTypeId") Long presizeTypeId) {
    final PresizeType presizeType = modelMapper.map(presizeTypeDTO, PresizeType.class);
    presizeType.setId(presizeTypeId);
    final PresizeType updatedPresizeType = presizeTypeService.updatePresizeType(presizeType);
    final PresizeTypeDTO updatedPresizeTypeDTO =
        modelMapper.map(updatedPresizeType, PresizeTypeDTO.class);

    return new ResponseEntity<>(updatedPresizeTypeDTO, HttpStatus.OK);
  }

  /**
   * Delete a presize type.
   */
  @ApiOperation(value = "Delete presize type based on given presize type ID")
  @ApiResponses(value = {@ApiResponse(code = 204, message = "Successfully deleted"),
      @ApiResponse(code = 404, message = "Presize type ID not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("/presize-types/{presizeTypeId}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void deletePresizeType(@PathVariable("presizeTypeId") Long presizeTypeId) {
    presizeTypeService.deletePresizeType(presizeTypeId);
  }
  
  
  /**
   * Merge a PresizeType identified by {@code PresizeTypeId}.
   */
  @ApiOperation(value = "Merge PresizeType")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 404, message = "PresizeType not found"),
      @ApiResponse(code = 500, message = "Server error")})
  @PutMapping(value = "/presize-types/{mergeToId}", params = {"mergeFromId"},
      produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<PresizeTypeDTO> mergePresizeType(
      @PathVariable("mergeToId") final String mergeToId,
      @RequestParam("mergeFromId") final String mergeFromId) {
    PresizeType mergedPresizeType =
        presizeTypeService.mergePresizeType(Long.valueOf(mergeToId), Long.valueOf(mergeFromId));
    final PresizeTypeDTO mergedPresizeTypeDTO =
        modelMapper.map(mergedPresizeType, PresizeTypeDTO.class);
    return new ResponseEntity<>(mergedPresizeTypeDTO, HttpStatus.OK);
  }

}
