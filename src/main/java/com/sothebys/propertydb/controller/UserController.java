/**
 * 
 */
package com.sothebys.propertydb.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import com.sothebys.propertydb.mapper.UserModelMapper;
import com.sothebys.propertydb.model.LevelOfDetail;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.search.UserSearchBuilder;
import com.sothebys.propertydb.search.UserSearchInputDTO;
import com.sothebys.propertydb.search.UserUtil;
import com.sothebys.propertydb.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


/**
 * This class exposing end-points to related to Suer entity
 * 
 * @author Manju.Rana
 */
@Controller
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserController {

  @Autowired
  private UserService userService;

  @Autowired
  UserSearchBuilder userSearchBuilder;

  @Autowired
  UserModelMapper userModelMapper;

  /**
   * This method searches for Users. Results are either the full user entity or only the user names, depending
   * on the value of the provided {@code lod} parameter.
   * <p/>
   * Note: Search is case insensitive.
   */
  @ApiOperation(value = "Search Users")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 400, message = "Input parameter validation failed"),
      @ApiResponse(code = 500, message = "Server error")})
  @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Page<?>> findUsers(
      @ModelAttribute final UserSearchInputDTO userSearchInputDTO, final Pageable pageRequest,
      @Valid @RequestParam(value = "lod", required = false, defaultValue = "MAX") LevelOfDetail lod,
      HttpServletRequest request, Model model) {
    final Page<?> resultsPage;
    boolean performSearchByTerm = UserUtil.validateUserSearchInputDTO(userSearchInputDTO);
    if (performSearchByTerm) {
      Specification<User> specification =
          userSearchBuilder.createSpecificationBuilder(userSearchInputDTO).build();
      Page<User> searchResultPage = userService.findBySearchTerm(specification, pageRequest);
      resultsPage = lod == LevelOfDetail.MIN
          ? userModelMapper.mapEntityPageIntoDTOPageWithUserNameOnly(pageRequest, searchResultPage)
          : userModelMapper.mapEntityPageIntoDTOPage(pageRequest, searchResultPage);
    } else {
      Page<User> allSearchResultPage = userService.findAllUsers(pageRequest);
      resultsPage = lod == LevelOfDetail.MIN
          ? userModelMapper.mapEntityPageIntoDTOPageWithUserNameOnly(pageRequest,
              allSearchResultPage)
          : userModelMapper.mapEntityPageIntoDTOPage(pageRequest, allSearchResultPage);
    }
    return new ResponseEntity<>(resultsPage, HttpStatus.OK);
  }
}
