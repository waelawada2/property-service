package com.sothebys.propertydb.interceptor;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.HEAD;
import static org.springframework.http.HttpMethod.OPTIONS;

import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.service.UserService;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * Interceptor that retrieves JWT token from request and translates token
 * details into {@code User} instance that is stored in a request attribute.
 *
 * @author Wael Awada
 * @author Gregor Zurowski
 */
@Slf4j
public class PrincipalToUserInterceptor extends HandlerInterceptorAdapter {

  private static final List<HttpMethod> UNCHECKED = Arrays.asList(HEAD, OPTIONS);
  private static final List<String> CHECKED_PATHS = Arrays
      .asList("/users/**", "/artists/**", "/properties/**");

  private final PathMatcher pathMatcher = new AntPathMatcher();
  private final Function<String, Predicate<String>> pathTester = servletPath ->
      path -> pathMatcher.match(path, servletPath);

  private UserService userService;

  @Autowired
  public PrincipalToUserInterceptor(UserService userService) {
    this.userService = userService;
  }

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
      Object handler) {
    if (!shouldHandle(request)) {
      log.debug("Skip handling request.");
      return true;
    }

    KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) request
        .getUserPrincipal();

    if (authenticationToken != null) {
      AccessToken token = authenticationToken.getAccount().getKeycloakSecurityContext().getToken();

      User savedUser = userService.findByEmail(token.getEmail());

      if (savedUser == null) {
        synchronized (this) {
          User user = new User();
          user.setEmail(token.getEmail());
          user.setUserName(token.getPreferredUsername());
          user.setFirstName(token.getGivenName());
          user.setLastName(token.getFamilyName());
          user.setKeycloakUserId(token.getSubject());

          log.info("Creating user={}", user);
          savedUser = userService.createOrFindUser(user);
        }
      }

      request.setAttribute("requestUser", savedUser);
    }
    return true;
  }

  private boolean shouldHandle(HttpServletRequest request) {
    HttpMethod method = HttpMethod.resolve(request.getMethod());
    return method == GET ? pathTester.apply(request.getServletPath()).test(CHECKED_PATHS.get(0))
        : !UNCHECKED.contains(method) &&
            CHECKED_PATHS.stream().anyMatch(pathTester.apply(request.getServletPath()));
  }

}
