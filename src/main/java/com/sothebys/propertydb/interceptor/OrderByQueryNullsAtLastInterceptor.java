// Copyright (c) 2017 Sotheby's, Inc.
package com.sothebys.propertydb.interceptor;

import java.util.regex.Pattern;
import org.hibernate.EmptyInterceptor;
import org.springframework.context.annotation.Configuration;

/**
 * @author jsilva.
 */
@Configuration
public class OrderByQueryNullsAtLastInterceptor extends EmptyInterceptor {


  private static final String QUERY_REGEX = ".*order\\sby.*limit.*";
  private static final Pattern QUERY_PATTERN = Pattern.compile(QUERY_REGEX);
  private static final String IS_NULL_FUNCTION = "ISNULL";
  private static final String NULL_IF_FUNCTION = "NULLIF";
  private static final String ORDER_BY = "order by";
  private static final String LIMIT = "limit";
  private static final String SPACE = " ";

  @Override
  public String onPrepareStatement(String sql) {

    if (!sql.isEmpty() && QUERY_PATTERN.matcher(sql).matches()) {
      int orderBy = sql.toLowerCase().indexOf(ORDER_BY);
      int limitIndex = sql.toLowerCase().lastIndexOf(LIMIT);
      String limitAppend = sql.toLowerCase().substring(limitIndex);
      String orderByParameters = sql.substring(orderBy + 8, limitIndex - 1);

      return super.onPrepareStatement(new StringBuilder(sql.substring(0, orderBy))
          .append(ORDER_BY).append(SPACE).append(replaceOrderByStatement(orderByParameters))
          .append(limitAppend).toString());
    }
    return super.onPrepareStatement(sql);
  }

  private static final String replaceOrderByStatement(String query) {
    final StringBuilder sb = new StringBuilder();
    final String[] predicate = query.split("\\,");

    for (String s : predicate) {
      sb.append(", ").append(IS_NULL_FUNCTION).append("(").append(NULL_IF_FUNCTION).append("(");
      int directionIndex = s.trim().lastIndexOf(SPACE);
      String field = s.trim().substring(0, directionIndex);
      sb.append(field);
      String direction = s.trim().substring(directionIndex);
      sb.append(",'')), ").append(field).append(SPACE).append(direction.trim());
    }
    sb.append(SPACE);
    return sb.toString().substring(1);
  }
}
