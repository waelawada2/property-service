package com.sothebys.propertydb;

import ch.qos.logback.access.tomcat.LogbackValve;
import com.sothebys.propertydb.mapper.MappingConfiguration;
import com.sothebys.propertydb.repository.support.CustomRepositoryImpl;
import net.sf.ehcache.CacheManager;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.retry.annotation.EnableRetry;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableRetry
@EnableCaching
@EnableSwagger2
@EnableAsync
@EnableJpaRepositories(repositoryBaseClass = CustomRepositoryImpl.class)
@SpringBootApplication
public class PropertyServiceApplication {

  @Value("${threadpool.task-executor.core-pool-size:10}")
  private int corePoolSize;
  
  @Value("${threadpool.task-executor.max-pool-size:25}")
  private int maxPoolSize;
  
  @Value("${threadpool.task-executor.queue-capacity:20}")
  private int queueCapacity; 
  
  /**
   * This method invokes property-Service SpringBoot application
   */
  public static void main(String[] args) {
    SpringApplication.run(PropertyServiceApplication.class, args);
  }

  @Bean
  public ModelMapper modelMapper() {
    ModelMapper modelMapper = new ModelMapper();
    modelMapper.addMappings(MappingConfiguration.ARTIST_TO_ARTIST_SEARCH_DTO_PROPERTY_MAP);
    modelMapper.addMappings(MappingConfiguration.ARTIST_TO_ARTIST_DTO_PROPERTY_MAP);
    modelMapper.addMappings(MappingConfiguration.ARTIST_DTO_TO_ARTIST_PROPERTY_MAP);

    modelMapper.addMappings(MappingConfiguration.PROPERTY_TO_PROPERTY_SEARCH_DTO_PROPERTY_MAP);
    modelMapper.addMappings(MappingConfiguration.PROPERTY_TO_PROPERTY_DTO_PROPERTY_MAP);
    modelMapper.addMappings(MappingConfiguration.PROPERTY_ADD_REQUEST_DTO_PROPERTY_TO_PROPERTY_MAP);

    return modelMapper;
  }

  /**
   * This method used to generate property-service api
   *
   * @return docket
   */
  @Bean
  public Docket propertyServiceApi() {
    // formatter:off
    return new Docket(DocumentationType.SWAGGER_2).apiInfo(propertyServiceInfo()).select()
        .apis(RequestHandlerSelectors.basePackage("com.sothebys")).paths(PathSelectors.any())
        .build().pathMapping("/");
    // formatter:on
  }

  /**
   * This method used to display property service information
   *
   * @return apiInfo
   */
  private ApiInfo propertyServiceInfo() {
    return new ApiInfoBuilder().title("Property Service API")
        .description("Property Service manages property and artist information")
        .termsOfServiceUrl("http://www.sothebys.com/en/terms-conditions.html")
        .license("Sothebys License Version 1.0")
        .licenseUrl("http://www.sothebys.com/en/privacy-policy.html").version("1.0").build();
  }

  @Bean
  public EmbeddedServletContainerFactory servletContainer() {
    TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();

    // configure access logs
    LogbackValve logbackValve = new LogbackValve();
    logbackValve.setFilename("logback-access.xml");
    tomcat.addContextValves(logbackValve);

    return tomcat;
  }

  @Bean
  @Autowired
  public EhCacheCacheManager cacheManager(CacheManager cacheManager) {
    return new EhCacheCacheManager(cacheManager);
  }

  @Bean
  @Autowired
  public EhCacheManagerFactoryBean ehCache(
      @Value("${cache.config.path:cache/ehcache.xml}") String cacheConfigPath) {
    EhCacheManagerFactoryBean ehCacheFactoryBean = new EhCacheManagerFactoryBean();
    ehCacheFactoryBean.setConfigLocation(new ClassPathResource(cacheConfigPath));
    return ehCacheFactoryBean;
  }

  @Bean
  public TaskExecutor taskExecutor() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(corePoolSize);
    executor.setMaxPoolSize(maxPoolSize);
    executor.setQueueCapacity(queueCapacity);
    return executor;
  }

}
