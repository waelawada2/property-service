package com.sothebys.propertydb.configuration.views;

import static java.time.format.DateTimeFormatter.ofPattern;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

import com.sothebys.propertydb.constants.PropertyServiceConstants;
import com.sothebys.propertydb.dto.ArchiveNumberDTO;
import com.sothebys.propertydb.dto.ArtistCatalogueRaisoneeDTO;
import com.sothebys.propertydb.dto.ArtistDTO;
import com.sothebys.propertydb.dto.PropertyCatalogueRaisoneeDTO;
import com.sothebys.propertydb.dto.PropertyDTO;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import liquibase.util.csv.opencsv.CSVWriter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * @author Wael Awada
 */
@Component
public class PropertyCsvView extends CsvView {

  private static final String PIPE_DELIMITER = "|";
  private static final String YES_VALUE = "Y";
  private static final String NO_VALUE = "N";

  public static final String[] PROPERTY_HEADER = {"Object_Code", "EOS_Object_ID", "Sort_Code",
      "Artist_ID", "Artist_Code", "Artist_Name", "CR_Volume", "CR_Number", "Title_(Orig)",
      "Title_(EN)", "Signature_Details", "Medium", "Category", "Subcategory", "Year_Circa_check",
      "Year1", "Year2", "Cast_Circa_check", "Cast_Year1", "Cast_Year2", "PH", "Year_Override",
      "Pre-size_Text", "H_in", "W_in", "D_in", "H_cm", "W_cm", "D_cm", "Measurement_Override",
      "Weight_kg", "Parts", "Edition_Status", "Edition_#", "Edition_Type", "Series_Total",
      "AP_Total", "BTP_Total", "CP_Total", "CTP_Total", "DP_Total", "FP/PP_Total", "HCP_Total",
      "MP_Total", "PRP_Total", "RTP_Total", "TP_Total", "WP_Total", "Foundry", "Edition_Override",
      "Edition_Notes", "Stamped_check", "Signed_check", "Certificate_check", "Authentication_check",
      "Archive_#", "Flag_Authenticity_check", "Flag_Restitution_check", "Flag_Condition_check",
      "Flag_SFS_check", "Flag_Medium_check", "Flag_Export_Country", "Tag_Subject", "Tag_Figure",
      "Tag_Gender", "Tag_Position", "Tag_Animal", "Tag_Main_Colour", "Tag_Text", "Tag_Location",
      "Tag_Sitter", "Tag_Other", "Auto_Orientation", "Auto_Image", "Auto_Scale", "Auto_Area_cm",
      "Research_Complete_check", "Researcher_Name", "Researcher_Notes_Date",
      "Researcher_Notes_Text", "Image_File_Name", "Image_Update_Y/N"};

  @Override
  protected void buildCsvDocument(Map<String, Object> model, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    String fileName = LocalDateTime.now().format(ofPattern("yyyyMMddHHmmss"));
    response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".csv");
    @SuppressWarnings("unchecked")
    List<PropertyDTO> properties = (List<PropertyDTO>) model.get("properties");
    try (CSVWriter csvWriter = new CSVWriter(response.getWriter(), CSVWriter.DEFAULT_SEPARATOR,
        CSVWriter.DEFAULT_QUOTE_CHARACTER, CSVWriter.DEFAULT_QUOTE_CHARACTER)) {
      csvWriter.writeNext(PROPERTY_HEADER);
      properties.stream().map(PropertyCsvView::exportPropertyData).forEach(csvWriter::writeNext);
    }
  }

  public static String[] exportPropertyData(PropertyDTO property) {
    int index = 0;
    String[] data = new String[PROPERTY_HEADER.length];
    String flagAuthenticity = "";
    String flagRestitution = "";
    String flagCondition = "";
    String flagSfs = "";
    String flagMedium = "";
    String countryName = "";
    String imageSubjects = "";
    String imageFigure = "";
    String imageGenders = "";
    String imagePositions = "";
    String imageAnimals = "";
    String imageMainColours = "";
    String imageText = "";
    String expertiseLocation = "";
    String expertiseSitters = "";
    String otherTags = "";
    String autoOrientation = "";
    String autoImage = "";
    String autoScale = "";
    String autoVolume = "";
    if (null != property.getFlags()) {
      flagAuthenticity = convertBooleanToString(property.getFlags().getFlagAuthenticity());
      flagRestitution = convertBooleanToString(property.getFlags().getFlagRestitution());
      flagCondition = convertBooleanToString(property.getFlags().getFlagCondition());
      flagSfs = convertBooleanToString(property.getFlags().getFlagSfs());
      flagMedium = convertBooleanToString(property.getFlags().getFlagMedium());
      if (null != property.getFlags().getFlagsCountry()) {
        countryName = Objects.toString(property.getFlags().getFlagsCountry(), "");
      }
    }
    if (property.getTags() != null) {
      imageSubjects = Optional.ofNullable(property.getTags().getImageSubjects())
          .orElseGet(Collections::emptyList).stream().collect(joining(PIPE_DELIMITER));
      imageFigure = Objects.toString(property.getTags().getImageFigure(), "");
      imageGenders = Optional.ofNullable(property.getTags().getImageGenders())
          .orElseGet(Collections::emptyList).stream().collect(joining(PIPE_DELIMITER));
      imagePositions = Optional.ofNullable(property.getTags().getImagePositions())
          .orElseGet(Collections::emptyList).stream().collect(joining(PIPE_DELIMITER));
      imageAnimals = Optional.ofNullable(property.getTags().getImageAnimals())
          .orElseGet(Collections::emptyList).stream().collect(joining(PIPE_DELIMITER));
      imageMainColours = Optional.ofNullable(property.getTags().getImageMainColours())
          .orElseGet(Collections::emptyList).stream().collect(joining(PIPE_DELIMITER));
      imageText = Objects.toString(property.getTags().getImageText(), "");
      expertiseLocation = Objects.toString(property.getTags().getExpertiseLocation(), "");
      expertiseSitters = Optional.ofNullable(property.getTags().getExpertiseSitters())
          .orElseGet(Collections::emptyList).stream()
          .map(sitter -> sitter == null ? "" : sitter.fullName())
          .collect(joining(PIPE_DELIMITER));
      otherTags = Optional.ofNullable(property.getTags().getOtherTags())
          .orElseGet(Collections::emptyList).stream().collect(joining(PIPE_DELIMITER));
      autoOrientation = Objects.toString(property.getTags().getAutoOrientation(), "");
      autoImage = convertBooleanToString(property.getTags().isAutoImage());
      autoScale = Objects.toString(property.getTags().getAutoScale(), "");
      autoVolume = Objects.toString(property.getTags().getAutoVolume(), "");
    }
    data[index++] = Optional.ofNullable(property.getObjectCodes()).orElseGet(Collections::emptyList)
        .stream().map(String::trim).filter(StringUtils::isNotBlank)
        .collect(joining(PIPE_DELIMITER));
    data[index++] =
        property.getIsPropertyDeleted() ? PropertyServiceConstants.DELETED : property.getId();
    data[index++] = property.getSortCode();
    data[index++] = property.getArtists().stream().map(ArtistDTO::getId)
        .collect(joining(PIPE_DELIMITER));
    data[index++] = property.getArtists().stream().map(ArtistDTO::getUlanId)
        .filter(Objects::nonNull).map(Object::toString).collect(joining(PIPE_DELIMITER));
    data[index++] = property.getArtists().stream().map(ArtistDTO::getDisplay)
        .filter(Objects::nonNull).collect(joining(PIPE_DELIMITER));

    data[index++] = getPropertyCrMappedValues(property, PropertyCatalogueRaisoneeDTO::getVolume);
    data[index++] = getPropertyCrMappedValues(property, PropertyCatalogueRaisoneeDTO::getNumber);

    data[index++] = Objects.toString(property.getOriginalTitle(), "");
    data[index++] = Objects.toString(property.getTitle(), "");
    data[index++] = Objects.toString(property.getSignature(), "");
    data[index++] = Objects.toString(property.getMedium(), "");
    data[index++] =
        property.getParentCategory() != null ? property.getParentCategory().getName() : "";
    data[index++] = property.getSubCategory() != null ? property.getParentCategory().getName() : "";
    data[index++] =
        property.getYearCirca() != null ? convertBooleanToString(property.getYearCirca()) : "";
    data[index++] = Objects.toString(property.getYearStart(), "");
    data[index++] = Objects.toString(property.getYearEnd(), "");
    data[index++] =
        property.getCastYearCirca() != null ? convertBooleanToString(property.getCastYearCirca())
            : "";
    data[index++] = Objects.toString(property.getCastYearStart(), "");
    data[index++] = Objects.toString(property.getCastYearEnd(), "");
    data[index++] =
        property.getPosthumous() != null ? convertBooleanToString(property.getPosthumous()) : "";
    data[index++] = property.getYearText();
    data[index++] = (property.getPresizeType() == null) ? "" : property.getPresizeType().getName();
    data[index++] =
        property.getHeightCm() != null ? convertCmToIn(property.getHeightCm().toString()) : "";
    data[index++] =
        property.getWidthCm() != null ? convertCmToIn(property.getWidthCm().toString()) : "";
    data[index++] =
        property.getDepthCm() != null ? convertCmToIn(property.getDepthCm().toString()) : "";
    data[index++] = Objects.toString(property.getHeightCm(), "");
    data[index++] = Objects.toString(property.getWidthCm(), "");
    data[index++] = Objects.toString(property.getDepthCm(), "");
    data[index++] = Objects.toString(property.getSizeText(), "");
    data[index++] = Objects.toString(property.getWeight(), "");
    data[index++] = Objects.toString(property.getParts(), "");
    data[index++] = Objects.toString(property.getEditionSizeTypeName(), "");
    data[index++] = Objects.toString(property.getEditionNumber(), "");
    data[index++] = Objects.toString(property.getEditionName(), "");
    data[index++] = Objects.toString(property.getSeriesTotal(), "");
    data[index++] = Objects.toString(property.getArtistProofTotal(), "");
    data[index++] = Objects.toString(property.getBonATirerTotal(), "");
    data[index++] = Objects.toString(property.getCancellationProofTotal(), "");
    data[index++] = Objects.toString(property.getColorTrialProofTotal(), "");
    data[index++] = Objects.toString(property.getDedicatedProofTotal(), "");
    data[index++] = Objects.toString(property.getPrintersProofTotal(), "");
    data[index++] = Objects.toString(property.getHorsCommerceProofTotal(), "");
    data[index++] = Objects.toString(property.getMuseumProofTotal(), "");
    data[index++] = Objects.toString(property.getProgressiveProofTotal(), "");
    data[index++] = Objects.toString(property.getRightToProduceProofTotal(), "");
    data[index++] = Objects.toString(property.getTrialProofTotal(), "");
    data[index++] = Objects.toString(property.getWorkingProofTotal(), "");
    data[index++] = property.getFoundry();
    data[index++] = Objects.toString(property.getEditionOverride(), "");
    data[index++] = Objects.toString(property.getEditionSizeNotes(), "");
    data[index++] = convertBooleanToString(property.getStamped());
    data[index++] = convertBooleanToString(property.getSigned());
    data[index++] = convertBooleanToString(property.getCertificate());
    data[index++] = convertBooleanToString(property.getAuthentication());
    data[index++] = Optional.ofNullable(property.getArchiveNumbers())
        .orElseGet(Collections::emptyList).stream().map(ArchiveNumberDTO::getArchiveNumber)
        .filter(Objects::nonNull).collect(joining(PIPE_DELIMITER));
    data[index++] = flagAuthenticity;
    data[index++] = flagRestitution;
    data[index++] = flagCondition;
    data[index++] = flagSfs;
    data[index++] = flagMedium;
    data[index++] = countryName;
    data[index++] = imageSubjects;
    data[index++] = imageFigure;
    data[index++] = imageGenders;
    data[index++] = imagePositions;
    data[index++] = imageAnimals;
    data[index++] = imageMainColours;
    data[index++] = imageText;
    data[index++] = expertiseLocation;
    data[index++] = expertiseSitters;
    data[index++] = otherTags;
    data[index++] = autoOrientation;
    data[index++] = autoImage;
    data[index++] = autoScale;
    data[index++] = autoVolume;
    data[index++] = convertBooleanToString(property.getResearchCompleteCheck());
    data[index++] = Objects.toString(property.getResearcherName(), "");
    data[index++] = Objects.toString(property.getResearcherNotesDate(), "");
    data[index++] = Objects.toString(property.getResearcherNotesText(), "");
    data[index++] = property.getImagePath();
    data[index++] = NO_VALUE;
    return data;
  }

  private static String getPropertyCrMappedValues(PropertyDTO property,
      Function<PropertyCatalogueRaisoneeDTO, String> mapper) {
    Map<Long, PropertyCatalogueRaisoneeDTO> artCRIdPropCR = property.getPropertyCatalogueRaisonees()
        .stream().collect(toMap(
            propCR -> propCR.getArtistCatalogueRaisoneeId(), Function.identity(), (a, b) -> a));
    return property.getArtists().stream().map(ArtistDTO::getArtistCatalogueRaisonees)
        .flatMap(List::stream).sorted(comparing(ArtistCatalogueRaisoneeDTO::getSortId))
        .map(ArtistCatalogueRaisoneeDTO::getId).map(artCRIdPropCR::get)
        .map(propCR -> propCR == null ? "" : mapper.apply(propCR)).collect(joining(PIPE_DELIMITER));
  }

  /**
   * This method used to verify given key is true then return 'Y' otherwise return ""
   *
   * @param key boolean field of the property object.
   * @return String <code>Y</code> if the given <code>key</code> is 'true', otherwise
   *     <code>""</code>.
   */
  private static String convertBooleanToString(Boolean key) {
    return (key != null && key) ? YES_VALUE : "";
  }

  /**
   * This method used to centimeters into inches
   *
   * @param centimeters refers to the input centimeters
   * @return inches post conversion
   */
  private static String convertCmToIn(String centimeters) {
    Double inches = Double.parseDouble(centimeters) / 2.54;
    return String.format("%.2f", inches);
  }

}
