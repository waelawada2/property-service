package com.sothebys.propertydb.configuration.views;

import java.util.Locale;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

/**
 * @author Wael Awada
 */
public class CsvViewResolver implements ViewResolver {

  @Override
  public View resolveViewName(String view, Locale locale) throws Exception {
    if (view.contains("properties")) {
      return new PropertyCsvView();
    } else if (view.contains("artists")) {
      return new ArtistCsvView();
    }
    else throw new Exception("Unable to find proper view for this object");

  }
}
