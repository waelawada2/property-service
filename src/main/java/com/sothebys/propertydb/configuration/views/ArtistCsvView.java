package com.sothebys.propertydb.configuration.views;

import static java.time.format.DateTimeFormatter.ofPattern;
import static java.util.Collections.EMPTY_LIST;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.sothebys.propertydb.dto.ArtistCatalogueRaisoneeDTO;
import com.sothebys.propertydb.dto.ArtistDTO;
import com.sothebys.propertydb.dto.CategoryDTO;
import com.sothebys.propertydb.dto.CountryDTO;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import liquibase.util.csv.opencsv.CSVWriter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * @author Wael Awada
 */
@Component
public class ArtistCsvView extends CsvView {

  private static final String PIPE_DELIMITER = "|";
  private static final String UNKNOWN = "Unknown";
  private static final String PAINTING = "Painting";
  private static final String WORK_ON_PAPER = "Work on Paper";
  private static final String MOUNTED = "Mounted";
  private static final String SCULPTURE = "Sculpture";
  private static final String PHOTOGRAPH = "Photograph";
  private static final String PRINT = "Print";
  private static final String INSTALLATION = "Installation";
  private static final String VIDEO = "Video";
  private static final String OTHER = "Other";
  private static final String YES_VALUE = "Y";

  public static final String[] ARTIST_HEADER = {"Artist_Code", "Artist_ID", "First_Name",
      "Last_Name", "Birth_Year", "Death_Year", "Display (free text)", "Nationality (Country)",
      "Gender", "Unknown_check", "Painting_check", "Work_on_Paper_Check", "Mounted_check",
      "Sculpture_check", "Photograph_check", "Print_check", "Installation_check", "Video_check",
      "Other_check", "CR_Author", "CR_Year", "CR_Volumes", "CR_Type", "Notes"};

  @Override
  protected void buildCsvDocument(Map<String, Object> model, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    String fileName = LocalDateTime.now().format(ofPattern("yyyyMMddHHmmss"));

    response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".csv");

    @SuppressWarnings("unchecked")
    List<ArtistDTO> artistsList = (List<ArtistDTO>) model.get("artists");

    try (CSVWriter csvWriter = new CSVWriter(response.getWriter(), CSVWriter.DEFAULT_SEPARATOR,
        CSVWriter.DEFAULT_QUOTE_CHARACTER, CSVWriter.DEFAULT_QUOTE_CHARACTER)) {
      csvWriter.writeNext(ARTIST_HEADER);
      artistsList.stream().map(ArtistCsvView::exportArtistData).forEach(csvWriter::writeNext);
    }
  }

  public static String[] exportArtistData(ArtistDTO artist) {
    int index = 0;
    String[] data = new String[ARTIST_HEADER.length];
    List<String> artistCategoryList = artist.getCategories().stream().map(CategoryDTO::getName)
        .collect(toList());

    data[index++] = Optional.ofNullable(artist.getUlanId()).map(Number::toString).orElse(EMPTY);
    data[index++] = artist.getId();
    data[index++] = artist.getFirstName();
    data[index++] = artist.getLastName();
    data[index++] = Optional.ofNullable(artist.getBirthYear()).map(Number::toString).orElse(EMPTY);
    data[index++] = Optional.ofNullable(artist.getDeathYear()).map(Number::toString).orElse(EMPTY);
    data[index++] = artist.getDisplay();
    data[index++] = artist.getCountries().stream().map(CountryDTO::getCountryName)
        .filter(StringUtils::isNotBlank).collect(joining(PIPE_DELIMITER));
    data[index++] = artist.getGender();
    data[index++] = categoryNameMatchesOrNot(UNKNOWN, artistCategoryList);
    data[index++] = categoryNameMatchesOrNot(PAINTING, artistCategoryList);
    data[index++] = categoryNameMatchesOrNot(WORK_ON_PAPER, artistCategoryList);
    data[index++] = categoryNameMatchesOrNot(MOUNTED, artistCategoryList);
    data[index++] = categoryNameMatchesOrNot(SCULPTURE, artistCategoryList);
    data[index++] = categoryNameMatchesOrNot(PHOTOGRAPH, artistCategoryList);
    data[index++] = categoryNameMatchesOrNot(PRINT, artistCategoryList);
    data[index++] = categoryNameMatchesOrNot(INSTALLATION, artistCategoryList);
    data[index++] = categoryNameMatchesOrNot(VIDEO, artistCategoryList);
    data[index++] = categoryNameMatchesOrNot(OTHER, artistCategoryList);

    data[index++] = artist.getArtistCatalogueRaisonees().stream()
        .map(ArtistCatalogueRaisoneeDTO::getAuthor).filter(StringUtils::isNotBlank)
        .collect(joining(PIPE_DELIMITER));
    data[index++] = artist.getArtistCatalogueRaisonees().stream()
        .map(ArtistCatalogueRaisoneeDTO::getYear).filter(Objects::nonNull).map(Number::toString)
        .collect(joining(PIPE_DELIMITER));

    data[index++] = artist.getArtistCatalogueRaisonees().stream()
        .map(ArtistCatalogueRaisoneeDTO::getVolumes).filter(StringUtils::isNotBlank)
        .collect(joining(PIPE_DELIMITER));
    data[index++] = artist.getArtistCatalogueRaisonees().stream()
        .map(ArtistCatalogueRaisoneeDTO::getType).filter(StringUtils::isNotBlank)
        .collect(joining(PIPE_DELIMITER));
    data[index++] = artist.getNotes();
    return data;
  }

  /**
   * This method used to verify the category data matches in the list or not
   *
   * @param categoryName of the category name
   * @param categoryList of the category list
   * @return String <code>Y</code> if the given <code>categoryName</code> contains in the list 'Y',
   * otherwise <code>""</code>.
   */
  private static String categoryNameMatchesOrNot(String categoryName, List<String> categoryList) {
    return defaultIfNull(categoryList, EMPTY_LIST).contains(categoryName) ? YES_VALUE : EMPTY;
  }

}
