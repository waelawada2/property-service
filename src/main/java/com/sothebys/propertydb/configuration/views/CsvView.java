package com.sothebys.propertydb.configuration.views;

import com.sothebys.propertydb.controller.support.AdditionalMediaType;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.view.AbstractView;

/**
 * @author Wael Awada
 */
public abstract class CsvView extends AbstractView {

  @SuppressWarnings("WeakerAccess")
  protected String url;

  @SuppressWarnings("WeakerAccess")
  public CsvView() {
    setContentType(AdditionalMediaType.TEXT_CSV_VALUE);
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  protected boolean generatesDownloadContent() {
    return true;
  }

  @Override
  protected final void renderMergedOutputModel(Map<String, Object> model,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    response.setContentType(getContentType());
    buildCsvDocument(model, request, response);
  }

  protected abstract void buildCsvDocument(Map<String, Object> model, HttpServletRequest request,
      HttpServletResponse response) throws Exception;

}
