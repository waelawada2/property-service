package com.sothebys.propertydb.configuration;

import com.sothebys.propertydb.util.AmazonS3CSVHandler;
import com.sothebys.propertydb.util.AmazonS3FileHandler;
import com.sothebys.propertydb.util.AmazonS3ImageHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by wael.awada on 4/27/17.
 */
@Configuration
public class AmazonS3Configuration {

  @Value("${aws.region}")
  private String s3Region;

  @Value("${aws.s3.images.access-key-id}")
  private String s3ImagesAccessKey;
  @Value("${aws.s3.images.bucket}")
  private String s3ImagesBucket;
  @Value("${aws.s3.images.folder}")
  private String s3ImagesFolder;
  @Value("${aws.s3.images.access-key-secret}")
  private String s3ImagesSecretKey;

  @Value("${aws.s3.imports.access-key-id}")
  private String s3FileImportsAccessKey;
  @Value("${aws.s3.imports.bucket}")
  private String s3FileImportsBucket;
  @Value("${aws.s3.imports.access-key-secret}")
  private String s3FileImportsSecretKey;
  
  @Value("${aws.s3.exports.access-key-id}")
  private String s3FileExportDataAccessKey;
  @Value("${aws.s3.exports.bucket}")
  private String s3FileExportDataBucket;
  @Value("${aws.s3.exports.access-key-secret}")
  private String s3FileExportDataSecretKey;

  
  @Value("${aws.s3.website-images.access-key-id}")
  private String websiteS3FileExportDataAccessKey;
  @Value("${aws.s3.website-images.bucket}")
  private String websiteS3FileExportDataBucket;
  @Value("${aws.s3.website-images.access-key-secret}")
  private String websiteS3FileExportDataSecretKey;
  
  @Bean(name = "imageS3Client")
  public AmazonS3FileHandler getImageClient() {
    return new AmazonS3ImageHandler(s3ImagesAccessKey, s3ImagesSecretKey, s3ImagesBucket,
        s3ImagesFolder, s3Region);
  }

  @Bean(name = "importS3Client")
  public AmazonS3FileHandler getImportClient() {
    return new AmazonS3CSVHandler(s3FileImportsAccessKey, s3FileImportsSecretKey,
        s3FileImportsBucket, s3Region);
  }
  
  @Bean(name = "exportDataS3Client")
  public AmazonS3FileHandler getExportDataClient() {
    return new AmazonS3CSVHandler(s3FileExportDataAccessKey, s3FileExportDataSecretKey,
        s3FileExportDataBucket, s3Region);
  }
  
  @Bean(name = "websiteImageDataS3Client")
  public AmazonS3FileHandler getWebSiteImage() {
    return new AmazonS3CSVHandler(websiteS3FileExportDataAccessKey, websiteS3FileExportDataSecretKey,
        websiteS3FileExportDataBucket, s3Region);
  }


}
