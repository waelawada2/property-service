package com.sothebys.propertydb.configuration;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AmazonSnsConfiguration {

  @Value("${aws.region}")
  private String s3Region;
  @Value("${aws.sns.access-key-id}")
  private String accessKeyId;
  @Value("${aws.sns.access-key-secret}")
  private String accessKeySecret;

  /**
   * Creates one instance of Amazon Sns Client.
   * 
   * @return {@link AmazonSNSClient}.
   */
  @Bean(name = "snsClient")
  public AmazonSNSClient snsClient() {
    final BasicAWSCredentials basicAwsCredentials =
        new BasicAWSCredentials(accessKeyId, accessKeySecret);

    final AWSCredentialsProvider credentialsProvider =
        new AWSStaticCredentialsProvider(basicAwsCredentials);

    AmazonSNSClient snsClient = (AmazonSNSClient) AmazonSNSClientBuilder.standard()
        .withRegion(s3Region).withCredentials(credentialsProvider).build();
 
    return snsClient;
  }

}
