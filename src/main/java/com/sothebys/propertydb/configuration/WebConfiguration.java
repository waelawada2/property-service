package com.sothebys.propertydb.configuration;

import com.sothebys.propertydb.configuration.views.CsvViewResolver;
import com.sothebys.propertydb.controller.support.AdditionalMediaType;
import com.sothebys.propertydb.interceptor.PrincipalToUserInterceptor;
import com.sothebys.propertydb.service.UserService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;

/**
 * Created by wael.awada on 5/15/17.
 */
@Configuration
public class WebConfiguration extends WebMvcConfigurerAdapter {

  private UserService userService;
  private ServletContext servletContext;

  @Autowired
  public WebConfiguration(UserService userService, ServletContext servletContext) {
    this.userService = userService;
    this.servletContext = servletContext;
  }

  @Bean
  PrincipalToUserInterceptor principalToUserInterceptor() {
    return new PrincipalToUserInterceptor(userService);
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(principalToUserInterceptor());
  }

  @Override
  public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
    configurer.defaultContentType(MediaType.APPLICATION_JSON).ignoreAcceptHeader(true)
              .mediaType("json", MediaType.APPLICATION_JSON)
              .mediaType("csv", AdditionalMediaType.TEXT_CSV)
              .useJaf(false)
              .favorPathExtension(true).favorParameter(false);

  }

  @Bean
  public ViewResolver contentNegotiatingViewResolver(
      ContentNegotiationManager manager) {
    List<ViewResolver> resolvers = new ArrayList<ViewResolver>();

    CsvViewResolver csvViewResolver = new CsvViewResolver();
    resolvers.add(csvViewResolver);

    ContentNegotiatingViewResolver resolver = new ContentNegotiatingViewResolver();
    resolver.setViewResolvers(resolvers);
    resolver.setContentNegotiationManager(manager);
    return resolver;
  }

  @Bean
  public ViewResolver getCsvViewResolver(){
    CsvViewResolver resolver = new CsvViewResolver();
    return resolver;
  }

}
