package com.sothebys.propertydb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by wael.awada on 5/15/17.
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

  private UserRepository userRepository;

  @Autowired
  public UserServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public User createOrFindUser(final User user) {
    User databaseUser = userRepository.findByEmail(user.getEmail());
    if (databaseUser == null) {
      log.info("No user found with e-mail '{}', creating new one.", user.getEmail());
      databaseUser = userRepository.save(user);
    }

    return databaseUser;
  }

  /**
   * {@inheritDoc}
   */
  @Cacheable(value = "users", unless = "#result == null")
  @Override
  public User findByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public User findByKeycloakUserId(String keycloakUserId) {
    return userRepository.findByKeycloakUserId(keycloakUserId);
  }

  /**
   * {@inheritDoc}
   */
  @Cacheable(value = "users", unless = "#result == null")
  @Override
  public User findByUsername(String username) {
    return userRepository.findByUserName(username);
  }
  
  /**
   * {@inheritDoc}
   */
  @Transactional(readOnly = true)
  @Override
  public Page<User> findAllUsers(Pageable pageRequest) {
    return userRepository.findAll(pageRequest);
  }
  
  /**
   * {@inheritDoc}
   */
  @Transactional(readOnly = true)
  @Override
  public Page<User> findBySearchTerm(Specification<User> spec, Pageable pageRequest) {
    return userRepository.findAll(spec, pageRequest);
  }
}