package com.sothebys.propertydb.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by wael.awada on 5/1/17.
 */
public interface FileService {

  String uploadFile(MultipartFile multipart, String type);

}
