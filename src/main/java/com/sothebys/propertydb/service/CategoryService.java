package com.sothebys.propertydb.service;

import java.util.List;

import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.model.Category;

public interface CategoryService {

  /**
   * Create new Category with the data of the provided {@code Category} instance.
   *
   * @param category The ID of the category that should be created if category doesn't exist.
   * @return The updated {@code Category} instance.
   * @throws com.sothebys.propertydb.exception.AlreadyExistsException if duplicate category entries
   *         exist in DB.
   */
  Category createParentCategory(Category category);

  Category createCategoryForParent(Long parentId, Category category);

  /**
   * Delete an existing category identified by the provided {@code categoryId}.
   *
   * @param categoryId The ID of the category to be deleted.
   * @throws com.sothebys.propertydb.exception.NotFoundException if the category does not exist.
   * @throws CannotPerformOperationException if the category cannot be deleted because it is still
   *         referenced by other entities.
   */
  void deleteParentCategory(Long categoryId);

  void deleteByCategoryIdAndParentId(Long categoryId, Long parentCategoryId);

  Category findParentCategory(Long categoryId);

  Category findByCategoryIdAndParentCategoryId(Long categoryId, Long parentCategoryId);

  Category findByCategoryName(String categoryName);

  Category findByCategoryNameAndParentCategoryName(String categoryName, String parentCategoryName);

  List<Category> getAllCategories();

  List<Category> getAllChildCategories(Long parentId);

  List<Category> getAllParentCategories();

  Category updateParentCategoryById(Long categoryId, Category category);

  Category updateByCategoryIdAndParentCategoryId(Long categoryId, Long parentId, Category category);

  List<Category> findCategoryContainingName(String name);
  
  Category mergeCategories(Long mergeToCategoryId, Long mergeFromCategoryId);

  Category getCategory(Long categoryId);

}
