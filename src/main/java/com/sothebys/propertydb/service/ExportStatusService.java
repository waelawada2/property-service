package com.sothebys.propertydb.service;

import com.sothebys.propertydb.search.PollStatus;

/**
 * Service that provides operations for poll exportId and status management.
 *
 * @author Srinivasarao.Batthula
 */
public interface ExportStatusService {

  /**
   * Retrieve exportStatusObj details in an output.
   *
   * @param exportId updated pollExportID.
   */
  void saveExportStatus(String exportId); 
  
  /**
   * Retrieve export details in an output.
   *
   * @param exportId updated pollExportID.
   * @return PollStatus updated pollExportID status.
   */
  PollStatus findExportStatus(String exportId); 
  
  /**
   * Retrieve exportStatusObj details in an output.
   *
   * @param exportId updated pollExportID.
   * @param PollStatus refers to exportStatus entity 
   */
  void updateExportStatus(String exportId, PollStatus status); 

}
