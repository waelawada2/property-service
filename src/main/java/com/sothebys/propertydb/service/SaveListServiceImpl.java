package com.sothebys.propertydb.service;

import com.sothebys.propertydb.exception.AlreadyExistsException;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.ForbiddenException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.exception.PropertyServiceException;
import com.sothebys.propertydb.model.AddPropertiesToSavelistResult;
import com.sothebys.propertydb.model.Mail;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.ResearcherNotes;
import com.sothebys.propertydb.model.SaveList;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.repository.PropertyRepository;
import com.sothebys.propertydb.repository.SaveListRepository;
import com.sothebys.propertydb.repository.UserRepository;
import com.sothebys.propertydb.search.Status;
import com.sothebys.propertydb.util.AlphaNumericIdGenerator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


/**
 * @author SrinivasaRao.Batthula
 */
@Slf4j
@Service
public class SaveListServiceImpl implements SaveListService {

  public static final String SAVE_LIST_EXTERNAL_ID_PREFIX = "SL-";
  public static final String SAVEDLIST_EXPORTER_NAME_PREFIX = "savedlist-";

  private final SaveListRepository saveListRepository;

  private final UserRepository userRepository;

  private final PropertyRepository propertyRepository;

  private ExportService savedListExportService;

  public SaveListServiceImpl(
      SaveListRepository saveListRepository,
      UserRepository userRepository,
      PropertyRepository propertyRepository,
      @Qualifier("savedListExportService") ExportService savedListExportService) {
    this.saveListRepository = saveListRepository;
    this.userRepository = userRepository;
    this.propertyRepository = propertyRepository;
    this.savedListExportService = savedListExportService;
  }

  @Autowired
  private ExportStatusService exportStatusService;

  @Autowired
  private EmailService emailService;

  @Value("${spring.mail.from}")
  private String fromAddress;

  @Value("${spring.mail.content-type}")
  private String contentType;

  @Value("${page.record-count:20}")
  private int pageRecordCount;

  @Override
  @Transactional
  public SaveList createSaveList(SaveList saveList, User user) {
    validateUserByUserName(user.getUserName(), saveList);
    saveList.setUser(user);
    saveList
        .setOpenSaveList(saveList.getOpenSaveList() == null ? false : saveList.getOpenSaveList());
    try {
      SaveList saveListObj = saveListRepository.save(saveList);
      saveListObj.setExternalId(AlphaNumericIdGenerator.getAlphaNumbericId(saveListObj.getId(),
          SAVE_LIST_EXTERNAL_ID_PREFIX));
      return saveListRepository.save(saveListObj);
    } catch (DataIntegrityViolationException ex) {
      throw new AlreadyExistsException(
          String.format("SavedList with Name '%s' already exists", saveList.getName()), ex);
    }
  }

  @Override
  @Transactional
  public SaveList updateByUserAndExternalId(User user, String externalId, SaveList saveList) {
    validateUserByUserName(user.getUserName(), saveList);
    SaveList saveListObj = saveListRepository.findByExternalId(externalId);
    if (saveListObj == null) {
      throw new NotFoundException(String.format(
          "shared save list with userId=%d and externalId=%s is not found, hence update failed",
          user.getId(), externalId));
    }
    if (!user.getUserName().equalsIgnoreCase(saveListObj.getUser().getUserName())) {
      throw new ForbiddenException(String.format(
          "userName '%s' doesn't match with user object name, hence update failed with ID=%s",
          user.getUserName(), externalId));
    }
    saveList.setExternalId(saveListObj.getExternalId());
    saveList.setId(saveListObj.getId());
    saveList
        .setOpenSaveList(saveList.getOpenSaveList() == null ? false : saveList.getOpenSaveList());
    saveList.setProperties(saveListObj.getProperties());
    saveList.setSharedUsers(saveListObj.getSharedUsers());
    return saveListRepository.save(saveList);

  }

  @Override
  @Transactional
  public SaveList addUsersToSaveListByUserNameAndExternalId(String userName, String externalId,
      List<String> newSharedUsersList) {

    SaveList sharedSaveList = null;
    List<User> sharedUserList = new ArrayList<User>();
    SaveList sharedSaveListObj = saveListRepository.findByExternalId(externalId);
    if (sharedSaveListObj == null) {
      throw new NotFoundException(String.format(
          "no shared user found with userId=%s and externalId=%s, hence update failed", userName,
          externalId));
    }
    User userObj = findUserByUserName(userName);
    if (userObj == null) {
      throw new NotFoundException(
          String.format("User doesnt match with user-name with userId=%s , hence update failed",
              userName, externalId));
    } else if (sharedSaveListObj != null && !sharedSaveListObj.getOpenSaveList()
        && sharedSaveListObj.getSharedUsers().contains(userName)) {
      throw new ForbiddenException(String.format(
          "shared save list status closed for userId=%s and externalId=%s, hence update failed",
          userName, externalId));
    }
    List<User> existingSharedUserList = sharedSaveListObj.getSharedUsers();
    if (!newSharedUsersList.isEmpty()) {
      for (String sharedUserName : newSharedUsersList) {
        User sharedUserObj = findUserByUserName(sharedUserName);
        if (sharedUserObj != null) {
          sharedUserList.add(sharedUserObj);
        }
      }
      existingSharedUserList.addAll(sharedUserList);
      sharedSaveListObj.setSharedUsers(existingSharedUserList);
    }
    try {
      sharedSaveList = saveListRepository.save(sharedSaveListObj);
    } catch (Exception ex) {
      throw new CannotPerformOperationException(String.format(
          "shared user list contains duplicate user userId=%s and externalId=%s, hence update failed",
          userName, externalId));
    }

    for (User user : sharedUserList) {
      sendSharedSavedListEmail(userName, sharedSaveListObj, user.getEmail());
    }

    return sharedSaveList;
  }

  public void sendSharedSavedListEmail(String userName, SaveList sharedSaveList,
      String sharedUser) {

    Mail mail = new Mail();

    try {
      String emailBody = "Hi, <br/><br/>";
      String url = ServletUriComponentsBuilder.fromCurrentRequest().toUriString();
      String host = Arrays.asList(url.split("/")).get(2);
      url = "http://" + host + "/eos-ui/object?savelist=" + sharedSaveList.getExternalId();
      emailBody =
          userName + " shared the below list with you:<br/><br/>" + "<a href=" + url + ">" + url
              + "</a>";

      emailBody += "<br/><br/> Thanks, <br/> EOS Team";

      mail.setFrom(fromAddress);
      mail.setTo(sharedUser);
      mail.setSubject(sharedSaveList.getName());
      mail.setContent(emailBody);
      mail.setContentType(contentType);

      emailService.sendMail(mail);
    } catch (Exception ex) {
      log.error(String.format("Failed to send email for the Shared Saved List - %s",
          sharedSaveList.getExternalId()),
          ex);
    }
  }

  @Override
  @Transactional
  public void deleteByUserNameAndExternalId(String userName, String externalId) {
    SaveList saveListObj = saveListRepository.findByExternalId(externalId);
    if (saveListObj == null) {
      throw new NotFoundException(
          String.format("no save list found with with externalId=%s, hence deletion failed",
              userName, externalId));
    }

    if (saveListObj.getUser() != null
        && userName.equalsIgnoreCase(saveListObj.getUser().getUserName())) {
      saveListRepository.delete(saveListObj);
    } else {
      throw new ForbiddenException(String.format(
          "userName '%s' doesn't match with user object name, hence deletion failed with ID=%s",
          userName, externalId));
    }
    log.info("save list owner successfully deleted with ID={}", externalId);
  }

  @Override
  @Transactional
  public void deleteUsersFromSaveListByUserNameAndExternalId(String userName, String externalId,
      List<String> deletedUsersList) {
    try {
      SaveList sharedSaveList = saveListRepository.findSharedUsersByExternalId(externalId);
      if (sharedSaveList == null) {
        throw new NotFoundException(String.format(
            "no shared user found with userName=%s and externalId=%s, hence deletion failed",
            userName, externalId));
      }
      User userObj = findUserByUserName(userName);
      if (userObj == null) {
        throw new NotFoundException(String.format(
            "userName '%s' doesn't found in the eexception, hence deletion failed with ID=%s",
            userName, externalId));
      }
      List<User> existingSharedUserList = sharedSaveList.getSharedUsers();
      if (sharedSaveList != null && null != sharedSaveList.getUser()
          && sharedSaveList.getUser().getUserName().equalsIgnoreCase(userName)) {
        if (!deletedUsersList.isEmpty()) {
          for (String deletedUserName : deletedUsersList) {
            User deletedUserObj = findUserByUserName(deletedUserName);
            existingSharedUserList.remove(deletedUserObj);
          }

        }
      } else if (!existingSharedUserList.isEmpty()) {
        for (User sharedUser : existingSharedUserList) {
          if (sharedUser.getUserName().equalsIgnoreCase(userName)) {
            existingSharedUserList.remove(sharedUser);
            break;
          }
        }
      }
      sharedSaveList.setSharedUsers(existingSharedUserList);
      saveListRepository.save(sharedSaveList);
    } catch (Exception e) {
      throw new PropertyServiceException("shared user deletion failed", e);
    }
    log.info("shared user successfully deleted with ID={}", externalId);
  }


  @Override
  @Transactional(readOnly = true)
  public SaveList findSaveListByUserNameAndExternalId(String userName, String externalId) {
    User user = findUserByUserName(userName);

    SaveList saveList = saveListRepository.findByUserAndExternalId(user, externalId);

    if (saveList == null) {
      saveList = saveListRepository.findByExternalIdAndSharedUsers(externalId, user);
    }

    if (saveList == null) {
      throw new NotFoundException(String.format(
          "userName '%s' doesn't match with user object name, hence No save list found with ID=%s",
          userName, externalId));
    }
    return saveList;
  }

  @Transactional(readOnly = true)
  @Override
  public Page<SaveList> findSaveListsByUser(User user, Pageable pageRequest) {

    Page<SaveList> saveListPageResponse = null;
    if (pageRequest.getPageSize() == pageRecordCount) {
      List<SaveList> saveListsList = new ArrayList<SaveList>();
      Page<SaveList> saveList =
          saveListRepository.findDistinctByUserOrSharedUsers(user, user, pageRequest);
      saveListsList.addAll(saveList.getContent());

      if (null != saveList && saveList.getTotalPages() > 1) {
        for (int page = 1; page < saveList.getTotalPages(); page++) {
          saveList = saveListRepository.findDistinctByUserOrSharedUsers(user, user,
              new PageRequest(page, pageRequest.getPageSize(), pageRequest.getSort()));
          saveListsList.addAll(saveList.getContent());
        }
      }
      saveListPageResponse = new PageImpl<SaveList>(saveListsList);
    } else {
      saveListPageResponse =
          saveListRepository.findDistinctByUserOrSharedUsers(user, user, pageRequest);
    }
    return saveListPageResponse;
  }

  @Transactional(readOnly = true)
  @Override
  public Page<SaveList> findSaveListByNameAndUser(String saveListName, User user,
      Pageable pageRequest) {
    return saveListRepository.findDistinctByNameContainingAndUserOrNameContainingAndSharedUsers(
        saveListName, user, saveListName, user, pageRequest);
  }

  @Transactional(readOnly = true)
  private User findUserByUserName(String userName) {
    User userObj = userRepository.findByUserName(userName);
    if (userObj == null) {
      throw new NotFoundException(String.format("No userId found with userName=%s", userName));
    }
    return userObj;
  }

  @Transactional(readOnly = true)
  private void validateUserByUserName(String reqUserName, SaveList saveList) {
    if (!CollectionUtils.isEmpty(saveList.getSharedUsers())) {
      for (User user : saveList.getSharedUsers()) {
        User sharedUserObj = userRepository.findByUserName(user.getUserName());
        if (sharedUserObj == null) {
          throw new CannotPerformOperationException(String.format(
              "Unable to insert save List. Specified Shared user with Name %s doesn't exist",
              user.getUserName()));
        }
        if (reqUserName.equalsIgnoreCase(user.getUserName())) {
          throw new CannotPerformOperationException(
              String.format("Unable to insert save List. Owner name match with Shared user name %s",
                  reqUserName));
        }
      }
    }
  }

  @Override
  @Transactional
  public SaveList updatePropertiesByUserAndExternalId(User user, String externalId,
      SaveList saveList) {
    validateUserByUserName(user.getUserName(), saveList);
    SaveList saveListObj = saveListRepository.findByExternalId(externalId);
    if (saveListObj == null) {
      throw new NotFoundException(String.format(
          "Updated save list with userId=%d and externalId=%s is not found, hence update failed",
          user.getId(), externalId));
    }
    if (!user.getUserName().equalsIgnoreCase(saveListObj.getUser().getUserName())) {
      throw new ForbiddenException(String.format(
          "userName '%s' doesn't match with user object name, hence update failed with ID=%s",
          user.getUserName(), externalId));
    }
    saveList.setExternalId(saveListObj.getExternalId());
    saveList.setId(saveListObj.getId());
    saveList.setOpenSaveList(saveListObj.getOpenSaveList());
    saveList.setName(saveListObj.getName());
    saveList.setNotes(saveListObj.getNotes());
    saveList.getProperties().addAll(saveListObj.getProperties());
    saveList.setUser(user);

    return saveListRepository.save(saveList);
  }

  @Override
  @Transactional
  public AddPropertiesToSavelistResult addPropertiesToSaveList(Long saveListId, List<String> propertyExternalIds) {
    int totalInserted = 0;

    if (!CollectionUtils.isEmpty(propertyExternalIds)) {
      totalInserted = saveListRepository.addPropertiesToSaveList(saveListId, propertyExternalIds);
    }

    SaveList saveList = saveListRepository.findOne(saveListId);
    saveListRepository.refresh(saveList);

    AddPropertiesToSavelistResult result = new AddPropertiesToSavelistResult();
    result.setSaveList(saveList);
    result.setTotalFound(propertyExternalIds.size());
    result.setTotalInserted(totalInserted);
    return result;
  }


  private int addPropertiesToSaveList(Long saveListId, List<Property> properties, List<String>
      ignoredIds) {

    List<String> propertyIdsToInsert = properties.stream()
        .filter(elem -> {
          // We filter the list of id's to not include the ignored Ids.
          if (ignoredIds != null) {
            if (ignoredIds.contains(elem.getExternalId())) {
              return false;
            }
          }
          return true;
        })
        .map(elem -> String.valueOf(elem.getExternalId()))
        .collect(Collectors.toList());

    int result = 0;
    try {

      result = saveListRepository
          .addPropertiesToSaveList(saveListId, propertyIdsToInsert);

    } catch (DataIntegrityViolationException e) {
      throw new AlreadyExistsException(String
          .format("Properties are already added to saveList %s ",
              saveListId), e);
    }

    return result;
  }


  @Override
  @Transactional
  public AddPropertiesToSavelistResult addPropertiesToSavelistFromSavelistId(String
      fromSaveListExternalId,
      String toSAveListExternalId, User user, List<String> ignoredIds) {

    SaveList saveListFrom = this.findSaveListByUserNameAndExternalId(user.getUserName(),
        fromSaveListExternalId);

    SaveList saveListTo = this.findSaveListByUserNameAndExternalId(user.getUserName(),
        toSAveListExternalId);

    AddPropertiesToSavelistResult result = new AddPropertiesToSavelistResult();

    int totalInserted = addPropertiesToSaveList(saveListTo.getId(), saveListFrom.getProperties
        (), ignoredIds);

    result.setTotalInserted(totalInserted);
    result.setTotalFound(saveListFrom.getProperties().size());

    return result;
  }

  @Override
  @Transactional
  public AddPropertiesToSavelistResult addPropertiesToSaveList(String saveListExternalId,
      Specification<Property> specification, User user, List<String> ignoredIds) {

    SaveList saveList = this.findSaveListByUserNameAndExternalId(user.getUserName(),
        saveListExternalId);

    List<Property> propertyList = propertyRepository.findAll(specification);

    AddPropertiesToSavelistResult result = new AddPropertiesToSavelistResult();

    result.setTotalFound(propertyList.size() - (ignoredIds != null ? ignoredIds.size() : 0));


    int totalInserted = this.addPropertiesToSaveList(saveList.getId(), propertyList, ignoredIds);
    result.setTotalInserted(totalInserted);

    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public void deletePropertyIdsFromSaveList(String saveListExternalId, String propertyExternalId,
      User user) {
    SaveList saveList = saveListRepository.findByExternalId(saveListExternalId);

    if (saveList == null) {
      throw new NotFoundException(String.format(
          "Updated save list with userId=%d and externalId=%s is not found, hence update failed",
          user.getId(), saveListExternalId));
    } else if (!saveList.getOpenSaveList()) {
      throw new CannotPerformOperationException(
          String.format("Unable to delete Property %s as the saveList %s is closed",
              propertyExternalId, saveList.getExternalId()));
    } else if (saveList.getUser().getUserName() != null
        && saveList.getUser().getUserName().toLowerCase().contains("importer")) {
      throw new CannotPerformOperationException(
          "Objects may not be removed from Import Saved Lists.");
    }

    User reqUserObj = userRepository.findByUserName(user.getUserName());
    if (reqUserObj == null) {
      throw new NotFoundException(String.format(
          "Requested User with Name %s doesn't exist, hence update failed for savedlist with externalId=%s",
          user.getUserName(), saveListExternalId));
    }
    validateUserPermission(user, saveList, saveListExternalId);
    Property property = propertyRepository.findByExternalId(propertyExternalId);
    if (property == null) {
      throw new NotFoundException(
          String.format("Property with id %s is not found", propertyExternalId));
    } else if (!isListContainingProperty(saveList.getId(), property.getId())) {
      throw new NotFoundException(
          String.format("Property with id %s is not found in the saveList %s", propertyExternalId,
              saveListExternalId));
    }
    try {
      saveListRepository.deletePropertyFromSavedList(saveList.getId(), property.getId());
    } catch (Exception e) {
      throw new CannotPerformOperationException(
          String.format("Unable to delete the Property %s from the saveList %s ",
              property.getExternalId(), saveList.getExternalId()),
          e);
    }
  }

  /**
   * Validates the if user is valid, requested user is in savelist and
   * Requested user is in savelist.
   */
  void validateUserPermission(User user, SaveList saveList, String saveListExternalId) {
    if (!user.getUserName().equalsIgnoreCase(saveList.getUser().getUserName())
        && saveList.getSharedUsers() != null
        && !saveList.getSharedUsers().contains(user)) {
      throw new ForbiddenException(String.format(
          "Requested user name %s is neither the owner of the list nor the list is shared to the user.  Hence update failed for savedlist with externalId=%s",
          user.getUserName(), saveListExternalId));
    }
    if (!user.getUserName().equalsIgnoreCase(saveList.getUser().getUserName())
        && saveList.getSharedUsers() != null
        && saveList.getSharedUsers().contains(user) && !saveList.getOpenSaveList()) {
      throw new ForbiddenException(
          String.format(
              "shared save list status closed for userId=%s and externalId=%s, hence update failed",
              user.getUserName(), saveListExternalId));
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional(noRollbackFor = {DataAccessException.class, DataIntegrityViolationException.class,
      ConstraintViolationException.class})
  public void updateSavedListPropertyId(Long savedListId, Long oldPropertyId, Long newPropertyId) {
    saveListRepository.updateSavedListProperty(savedListId, oldPropertyId, newPropertyId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deletePropertyFromSavedList(Long savedListId, Long propertyId) {
    saveListRepository.deletePropertyFromSavedList(savedListId, propertyId);
  }

  @Override
  public List<SaveList> findSavedListsContainingProperty(Long propertyId) {
    return saveListRepository.findListsConstainingProperty(propertyId);
  }

  @Override
  public boolean isListContainingProperty(Long savedListId, Long propertyId) {
    return saveListRepository.containsPropertyId(savedListId, propertyId) > 0;
  }

  @Override
  @Transactional
  public String exportSavedListData(Specification<Property> spec) {
    String exportId = String.format("%s%s", SAVEDLIST_EXPORTER_NAME_PREFIX,
        savedListExportService.generateExportId());
    exportStatusService.saveExportStatus(exportId);
    if (savedListExportService.validateExportData(spec, exportId)) {
      savedListExportService.exportData(spec, exportId);
    }
    return exportId;
  }

  @Override
  @Transactional(readOnly = true)
  public Page<SaveList> getAllSaveLists(Pageable pageable) {
    Page<SaveList> allSaveLists = saveListRepository.findSaveListActveProperties(pageable);
    if (null == allSaveLists || allSaveLists.getTotalElements() == 0) {
      throw new NotFoundException("No save list found");
    }
    return allSaveLists;
  }

  @Override
  @Transactional(readOnly = true)
  public boolean getReseachStatus(String externalId) {
    SaveList saveList = saveListRepository.findByExternalId(externalId);
    if (saveList == null) {
      throw new NotFoundException(
          String.format("No savelist found with externalId=%s", externalId));
    }

    List<Property> properties = saveList.getProperties();
    if (CollectionUtils.isEmpty(properties)) {
      return false;
    }

    return properties.stream().filter(Objects::nonNull)
        .allMatch(p -> {
          ResearcherNotes researcherNotes = p.getResearcherNotes();
          if (researcherNotes != null) {
            if (researcherNotes.getResearchCompleteCheck() != null) {
              return researcherNotes.getResearchCompleteCheck();
            }
          }
          return false;
        });
  }

  @Override
  @Transactional
  public boolean updateResearchStatus(String externalId, boolean status) {

    SaveList saveList = saveListRepository.findByExternalId(externalId);
    if (saveList == null) {
      throw new NotFoundException(
          String.format("No savelist found with externalId=%s", externalId));
    }

    if (!saveList.getOpenSaveList()) {
      throw new CannotPerformOperationException(
          String.format("Unable to find the save list as the saveList %s is closed",
              saveList.getExternalId()));
    }
    List<Property> propertiesList = saveList.getProperties();
    if (!CollectionUtils.isEmpty(propertiesList)) {
      propertiesList.stream().filter(Objects::nonNull).forEach(property -> {
        if (property.getResearcherNotes() != null) {
          property.getResearcherNotes().setResearchCompleteCheck(status);
          propertyRepository.save(property);
        } else {
          ResearcherNotes researcherNotes = new ResearcherNotes();
          researcherNotes.setResearchCompleteCheck(status);
          property.setResearcherNotes(researcherNotes);
          propertyRepository.save(property);
        }
      });
    }
    return getReseachStatus(externalId);
  }

  public List<SaveList> findSaveListContainingName(String saveListName) {
    return saveListRepository.findDistinctByNameContaining(saveListName);
  }

  @Override
  @Transactional
  public void deleteAdminSavelistProperties(String saveListExternalId) {
    SaveList saveListObj = saveListRepository.findByExternalId(saveListExternalId);
    if (saveListObj == null) {
      throw new NotFoundException(String.format(
          "save list with externalId=%s is not found, hence admin delete saveList failed",
          saveListExternalId));
    } else if (!saveListObj.getSharedUsers().isEmpty()) {
      throw new CannotPerformOperationException(String.format(
          "found shared users for the given savedList externalId=%s, hence deletion failed",
          saveListExternalId));
    } else if (CollectionUtils.isEmpty(saveListObj.getProperties())) {
      throw new CannotPerformOperationException(String.format(
          "Empty properties for the given savedList externalId=%s, hence deletion failed",
          saveListExternalId));
    }

    List<Property> propertiesList = saveListObj.getProperties();
    List<String> propertyIds =
        propertiesList.stream().map(ps -> ps.getExternalId()).collect(Collectors.toList());
    Integer count = propertyRepository.updatePropertyIds(propertyIds, Status.Deleted.toString());
    if (count < 1) {
      throw new CannotPerformOperationException(String.format(
          "Failed to update status for deleted admin saveList properties with savedList externalId=%s, hence deletion failed",
          saveListObj.getExternalId()));
    }
    savedListExportService.exportByEmail(propertyIds, saveListObj.getName());
    saveListRepository.delete(saveListObj);
  }

  @Override
  @Transactional(readOnly = true)
  public List<String> getAllAdminSaveLists() {
    List<String> allSaveLists = saveListRepository.findAdminSaveListActveProperties();
    if (CollectionUtils.isEmpty(allSaveLists)) {
      throw new NotFoundException("No save list found");
    }
    return allSaveLists;
  }

  @Override
  @Transactional(readOnly = true)
  public SaveList findSaveListContainingByName(String saveListName) {
    SaveList saveListObj = saveListRepository.findDistinctSaveListByNameContaining(saveListName);
    if (saveListObj == null) {
      throw new NotFoundException(String.format(
          "save list with saveListName=%s is not found, hence admin delete saveList failed",
          saveListName));
    }
    return saveListObj;
  }
  
  @Override
  @Transactional(readOnly = true)
  public long findSaveListPropertiesCountById(String externalId) {
    return saveListRepository.findSaveListPropertiesCountById(externalId);
  }
  
  @Override
  @Transactional(readOnly = true)
  public boolean findSaveListResearchStatusById(String externalId) {
    boolean researchStatus = false;
    long count= saveListRepository.findSaveListResearchStatusById(externalId);
    if(count ==0){
      researchStatus =true;
    }
    return researchStatus;
  }
}
