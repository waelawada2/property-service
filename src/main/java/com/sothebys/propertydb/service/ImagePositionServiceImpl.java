package com.sothebys.propertydb.service;

import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ImagePosition;
import com.sothebys.propertydb.repository.ImagePositionRepository;

import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Minhtri Tran on 11/15/17.
 */
@Service
@Slf4j
public class ImagePositionServiceImpl implements ImagePositionService {

	private final ImagePositionRepository imagePositionRepository;

	@Autowired
	public ImagePositionServiceImpl(ImagePositionRepository imagePositionRepository) {
		this.imagePositionRepository = imagePositionRepository;
	}

  @Transactional(readOnly = true)
  @Override
  public ImagePosition findByName(String name) {
    ImagePosition imagePosition = imagePositionRepository.findByName(name);

		if (imagePosition == null) {
			throw new NotFoundException(String.format("No image position found with name=%s", name));
		}

		return imagePosition;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(readOnly = true)
	@Override
	public List<ImagePosition> findAllImagePositions() {
		return imagePositionRepository.findAllByOrderByNameAsc();
	}

	@Override
	@Transactional
	public ImagePosition updateImagePositionById(Long imagePositionId, ImagePosition imagePosition) {
		ImagePosition existingImagePosition = imagePositionRepository.findOne(imagePositionId);
		if (existingImagePosition != null) {
			if (imagePositionId != null && existingImagePosition.getId() == imagePositionId) {
				existingImagePosition.setName(imagePosition.getName());
				log.info("Image Position successfully updsted with ID={}", imagePositionId);
				return imagePositionRepository.save(existingImagePosition);
			}
		}
		throw new CannotPerformOperationException(String.format(
				"Image Position with Id '%s' does not exists,Hence Image Position update failed", imagePositionId));

	}

	@Override
	@Transactional
	public void deleteImagePositionById(Long imagePositionId) {
		ImagePosition imagePositionObj;
		imagePositionObj = imagePositionRepository.findOne(imagePositionId);
		if (imagePositionObj == null) {
			throw new NotFoundException(String.format("No imagePositionId found with ID=%d", imagePositionId));
		}

		imagePositionObj = imagePositionRepository.findByProperty(imagePositionId);
		if (imagePositionObj != null) {
			throw new NotFoundException(String.format(
					"Image Position with ID = %d can't be deleted, "
							+ "Image Position = %s has already assigned to an Property",
					imagePositionId, imagePositionObj.getName()));
		}

		imagePositionRepository.delete(imagePositionId);
		log.info("Image Position successfully deleted with ID={}", imagePositionId);

	}

	@Override
	@Transactional
	public ImagePosition createImagePosition(ImagePosition imagePosition) {
		ImagePosition createdImagePosition;
		try {
			createdImagePosition = imagePositionRepository.save(imagePosition);
		} catch (DataIntegrityViolationException e) {
			throw new CannotPerformOperationException(String.format("ImagePosition name already exists."));
		}
		return createdImagePosition;
	}
	
    @Transactional
    @Override
    public List<ImagePosition> findAllImagePositionByName(String name) {
        return imagePositionRepository.findByNameContaining(name);
    }
    
    @Override
    @Transactional(readOnly = true)
    public ImagePosition getImagePosition(Long imagePositionId) {
      return imagePositionRepository.findOne(imagePositionId);
    }
    
    
    @Override
    @Transactional
    public ImagePosition mergeImagePosition(Long mergeToImagePositionId, Long mergeFromImagePositionId) {
      imagePositionRepository.updatePropertyImagePosition(mergeToImagePositionId, mergeFromImagePositionId);
      deleteImagePositionById(mergeFromImagePositionId);

      log.info("Image Position successfully deleted with ID={}", mergeFromImagePositionId);
      return getImagePosition(mergeToImagePositionId);
    }

}
