package com.sothebys.propertydb.service;

import java.util.List;

import com.sothebys.propertydb.model.EditionType;

/**
 * Service that provides operations for edition type management.
 */
public interface EditionTypeService {

  EditionType createEditionType(EditionType editionType);

  /**
   * Deletes an edition type identified by the provided {@code editionTypeId}
   *
   * @param editionTypeId The ID of the edition type.
   */
  void deleteEditionType(Long editionTypeId);

  List<EditionType> getAllEditionTypes();

  EditionType getEditionTypeByName(String editionType);

  EditionType getEditionTypeById(Long editionTypesId);
  /**
   * Update an EditionType with the data of the provided {@code EditionType} instance. The editionType is
   * identified by the internal ID.
   *
   * @param editionTypeId The ID of the editionType that should be updated.
   * @param editionType An {@code EditionType} instance with data to be updated.
   * @return The updated {@code EditionType} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no editionType can be found with the
   *         provided ID.
   */
  EditionType updateEditionType(Long editionTypeId,EditionType editionType);

}
