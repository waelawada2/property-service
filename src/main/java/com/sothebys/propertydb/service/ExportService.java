package com.sothebys.propertydb.service;

import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.search.PollStatus;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;

/**
 * Service that provides operations for artist/property/savedList management.
 *
 * @author Srinivasarao.Batthula
 */
public interface ExportService {

  /**
   * Retrieve export details in an output.
   *
   * @param spec refers to the search criteria
   * @param exportId updated artist pollExportID.
   */
  void exportData(Specification<Property> spec, String exportId);


  /**
   * Retrieve Artist Poll Service Export Status by {@code exportId}.
   *
   * @param exportId The export ID of the artist
   * @return String updated artist pollExportID status.
   */
  PollStatus findExportStatus(String exportId);
  
  /**
   * This method use to generate the random exportId value
   * 
   * @return String The randomly generated exportId
   */
  public String generateExportId();
  
  /**
   * Validates count of export data and throws exception if the count exceeds a value.
   */
  public boolean validateExportData(Specification<Property> spec, String exportId);
  
  /**
   * send an email with all deleted saved list properties
   *
   * @param propertyId refers to the list of properties associated with a save list
   * @param name The Name of the SavedList that should be deleted.
   * @throws com.sothebys.propertydb.exception.CannotPerformOperationException if any error occurred
   *         while build the csv file deleted save properties
   */
  void exportByEmail(List<String> propertyIds, String name);

}
