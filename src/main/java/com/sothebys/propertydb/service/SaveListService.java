package com.sothebys.propertydb.service;

import com.sothebys.propertydb.model.AddPropertiesToSavelistResult;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.SaveList;
import com.sothebys.propertydb.model.User;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

/**
 * Service that provides operations for SaveList management.
 */
public interface SaveListService {

  /**
   * Create new SaveList with the data of the provided {@code SaveList} instance.
   *
   * @param saveList The ID of the saveList that should be created if saveList doesn't exist.
   * @param user The {@code user} instance.
   * @return The created {@code SaveList} instance.
   */
  SaveList createSaveList(SaveList saveList, User user);

  /**
   * Update an SaveList with the data of the provided {@code SaveList} instance. The saveList is
   * identified by the internal externalId and userId.
   *
   * @param user The User {@code user} instance.
   * @param externalId The ID of the SaveList that should be updated.
   * @param saveList An {@code SaveList} instance with data to be updated.
   * @return The updated {@code SaveList} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no saveList can be found with
   *     the provided externalId and userId.
   */
  SaveList updateByUserAndExternalId(User user, String externalId, SaveList saveList);

  /**
   * Update an SaveList with the data of the provided {@code SaveList} instance. The saveList is
   * identified by the internal externalId and userId.
   *
   * @param userName The Name of the User that should be updated.
   * @param externalId The ID of the SaveList that should be updated.
   * @param saveList An {@code SaveList} instance with data to be updated.
   * @return The updated {@code SaveList} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no saveList can be found with
   *     the provided externalId and userId.
   */
  SaveList addUsersToSaveListByUserNameAndExternalId(String userName, String externalId,
      List<String> sharedUsers);

  /**
   * Deletes a saved list of the user on userId and externalId
   *
   * @param userName The Name of the User that should be deleted.
   * @param externalId The ID of the SaveList that should be deleted.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no save list can be found with
   *     the provided userId and externalId.
   */
  void deleteByUserNameAndExternalId(String userName, String externalId);

  /**
   * Deletes a saved list of the user and externalId
   *
   * @param userName The Name of the User that should be deleted.
   * @param externalId The ID of the SaveList that should be deleted.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no save list can be found with
   *     the provided userId and externalId.
   */
  void deleteUsersFromSaveListByUserNameAndExternalId(String userName, String externalId,
      List<String> deletedUsersList);

  /**
   * Retrieve SaveList details based on userName and externalId
   *
   * @param userName The username.
   * @param externalId The ID of the SaveList
   * @return {@code SaveList} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no save list can be found with
   *     the provided userName.
   */
  SaveList findSaveListByUserNameAndExternalId(String userName, String externalId);

  /**
   * Retrieve all save Lists based on user.
   *
   * @param user The User {@code user} instance.
   * @param pageRequest refers to the size/sort/page number for the result to be returned
   * @return save-list objects related to the provided user
   */
  Page<SaveList> findSaveListsByUser(User user, Pageable pageRequest);

  /**
   * search save lists by name and user
   *
   * @param name the name of the SaveList
   * @param user The User {@code user} instance.
   * @return {@code Page<SaveList>} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no save list can be found with
   *     the provided userName.
   */
  Page<SaveList> findSaveListByNameAndUser(String name, User user, Pageable pageRequest);

  /**
   * Update an SaveList with the data of the provided {@code SaveList} instance. The saveList is
   * identified by the internal externalId and userId.
   *
   * @param user The User {@code user} instance.
   * @param externalId The ID of the SaveList that should be updated.
   * @param saveList An {@code SaveList} instance with data to be updated.
   * @return The updated {@code SaveList} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no saveList can be found with
   *     the provided externalId and userId.
   */
  SaveList updatePropertiesByUserAndExternalId(User user, String externalId, SaveList saveList);

  AddPropertiesToSavelistResult addPropertiesToSaveList(Long saveListId,
      List<String> propertyExternalIds);

  /**
   * Adds a property identified by propertyExternalId to a saved list identified by
   * SavedListExternalId
   *
   * @param fromSaveListExternalId the id of the saved list to get properties from.
   * @param toSAveListExternalId the id of the saved list to save the properties.
   * @param user the {@link User} making the request
   * @param ignoredIds list of property ids to ignore during the saving.
   */
   AddPropertiesToSavelistResult addPropertiesToSavelistFromSavelistId(String fromSaveListExternalId,
      String toSAveListExternalId, User user, List<String> ignoredIds);


  /**
   * Adds a property identified by propertyExternalId to a saved list identified by
   * SavedListExternalId
   *
   * @param saveListExternalId the id of the saved list to add the properties to
   * @param specification the specification to identify the properties being added to the saved
   *     list
   * @param user the {@link User} making the request
   */
  AddPropertiesToSavelistResult addPropertiesToSaveList(String saveListExternalId,
      Specification<Property> specification, User user, List<String> ignoredIds);

  /**
   * deletes the property identified by propertyExternalId from the saved list identified by
   * SavedListExternalId
   *
   * @param saveListExternalId the id of the saved list from which the property is being deleted
   * @param propertyExternalId the property id of the property to be deleted from the saved list
   * @param user the {@link User} making the request
   */
  void deletePropertyIdsFromSaveList(String saveListExternalId, String propertyExternalId,
      User user);

  /**
   * Change the value of an old property id to a new property id in all saved lists.
   *
   * @param oldPropertyId the old property Id to be change
   * @param newPropertyId the new value of the property id
   */
  void updateSavedListPropertyId(Long savedListId, Long oldPropertyId, Long newPropertyId);

  /**
   * Remove  a property defined by propertyId from a saved list.
   *
   * @param savedListId the id of the savedlist and item is being removed from
   * @param propertyId the property id of the property to be removed
   */
  void deletePropertyFromSavedList(Long savedListId, Long propertyId);

  /**
   * returns a list of saved lists containing a given property defined by propertyId
   *
   * @param propertyId the id of the property contained in the saved lists to be returned
   * @return all the saved lists containing a given property
   */
  List<SaveList> findSavedListsContainingProperty(Long propertyId);

  /**
   * Checks if a property defined by propertyId is in a saved list defined by saved list id.
   *
   * @param savedListId The id of the saved list being checked
   * @param propertyId the id of the property being checked
   */
  boolean isListContainingProperty(Long savedListId, Long propertyId);

  /**
   * Retrieve Property details in an output.
   *
   * @param spec the specification for the list of the property containing the saved list id
   * @return exportId updated property pollExportID.
   */
  String exportSavedListData(Specification<Property> spec);

  /**
   * Returns a list of All saved lists
   *
   * @return list of all the saved lists
   */
  Page<SaveList> getAllSaveLists(Pageable pageable);

  /**
   * Retrieves the research status of a saved list identified by {@code externalId}
   *
   * @param externalId of a saved list
   * @return status of the requested saved list
   */
  boolean getReseachStatus(String externalId);

  /**
   * Updates the research status of the saved list.
   *
   * @param externalId of a saved list
   * @param status of the saved list completed/incomplete
   * @return status of the updated saved list status
   */
  boolean updateResearchStatus(String externalId, boolean status);

  /**
   * search save lists that contain the given name
   *
   * @param name the containing name of the SaveList
   * @return {@code Page<SaveList>} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no save list can be found with
   *     the provided name.
   */
  List<SaveList> findSaveListContainingName(String name);

  /**
   * Delete all saved list properties based on the provided saved list id {@code externalId}.
   *
   * @param externalId of a saved list.
   */
  void deleteAdminSavelistProperties(String saveListExternalId);

  /**
   * Returns a list of All Admin saved lists
   *
   * @return list of all the saved lists
   */
  List<String> getAllAdminSaveLists();

  /**
   * search save lists that contain the given name
   *
   * @param name the containing name of the SaveList
   * @return saveList An {@code SaveList} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no save list can be found with
   *     the provided name.
   */
   SaveList findSaveListContainingByName(String name);
   
   long findSaveListPropertiesCountById(String saveListExternalId);
   
   boolean findSaveListResearchStatusById(String saveListExternalId);

}
