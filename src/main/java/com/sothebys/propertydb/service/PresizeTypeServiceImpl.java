package com.sothebys.propertydb.service;

import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.PresizeType;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.repository.PresizeTypeRepository;
import com.sothebys.propertydb.repository.PropertyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import org.springframework.util.CollectionUtils;

/**
 * PresizeTypes-Service:This class is used for to perform all CRUD operations on PresizeTypes
 * repository
 *
 * @author SrinivasaRao.Batthula
 *
 *
 */
@Slf4j
@Service
public class PresizeTypeServiceImpl implements PresizeTypeService {

  @Autowired
  PresizeTypeRepository presizeTypeRepository;

  @Autowired
  PropertyRepository propertyRepository;

  /**
   * create PresizeTypes
   */
  @Override
  @Transactional
  public PresizeType createPresizeType(PresizeType presizeType) {
    return presizeTypeRepository.save(presizeType);
  }

  /**
   * get All PresizeTypes
   */

  @Override
  @Transactional(readOnly = true)
  public List<PresizeType> getAllPresizeTypes() {
    return presizeTypeRepository.findAllByOrderByNameAsc();
  }

  /**
   * get PresizeTypes
   */
  @Override
  @Transactional(readOnly = true)
  public PresizeType getPresizeTypeById(Long presizeTypeId) {
    return presizeTypeRepository.findOne(presizeTypeId);
  }

  /**
   * get PresizeType
   */
  @Override
  @Transactional(readOnly = true)
  public PresizeType getPresizeTypeByName(String presizeType) {
    return presizeTypeRepository.findByName(presizeType);
  }

  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public PresizeType updatePresizeType(PresizeType presizeType) {
    if (getPresizeTypeById(presizeType.getId()) == null) {
      throw new NotFoundException(
          String.format("No presize type found with ID=%d", presizeType.getId()));
    }

    PresizeType updatedPresizeType = presizeTypeRepository.save(presizeType);
    log.info("Presize type with ID={} successfully updated", updatedPresizeType.getId());

    return updatedPresizeType;
  }

  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public void deletePresizeType(Long presizeTypeId) {
    if (getPresizeTypeById(presizeTypeId) == null) {
      throw new NotFoundException(String.format("No presize type found with ID=%d", presizeTypeId));
    }

    List<Property> propertiesUsingPresizeType  = propertyRepository.findByPresizeTypeId(presizeTypeId);
    if (!CollectionUtils.isEmpty(propertiesUsingPresizeType)) {
      throw new CannotPerformOperationException(
          String.format("Presize type with ID=%d is still in use by %d properties", presizeTypeId, propertiesUsingPresizeType.size()));
    }

    presizeTypeRepository.delete(presizeTypeId);
    log.info("Presize type successfully deleted with ID={}", presizeTypeId);
  }
  
  @Override
  @Transactional
  public PresizeType mergePresizeType(Long mergeToPresizeTypeId, Long mergeFromPresizeTypeId) {
    propertyRepository.updatePropertyPresizeTypes(mergeToPresizeTypeId, mergeFromPresizeTypeId);
    presizeTypeRepository.delete(mergeFromPresizeTypeId);
    log.info("PresizeType successfully deleted with ID={}", mergeFromPresizeTypeId);
    return getPresizeTypeById(mergeToPresizeTypeId);
  }

}
