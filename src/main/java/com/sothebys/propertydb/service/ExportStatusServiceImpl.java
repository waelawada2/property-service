package com.sothebys.propertydb.service;


import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ExportStatus;
import com.sothebys.propertydb.repository.ExportStatusRepository;
import com.sothebys.propertydb.search.PollStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author SrinivasaRao.Batthula
 *
 */
@Slf4j
@Service
public class ExportStatusServiceImpl implements ExportStatusService {

  @Autowired
  private ExportStatusRepository exportStatusRepository;

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void saveExportStatus(String exportId) {
    ExportStatus exportStatusObj = new ExportStatus();
    exportStatusObj.setExportId(exportId);
    exportStatusObj.setStatus(PollStatus.InProgress.name());
    exportStatusRepository.save(exportStatusObj);
    log.info("poll export status successfully created with ID={} and status={}",
        exportStatusObj.getExportId(), exportStatusObj.getStatus());
  }

  @Override
  @Transactional(readOnly = true)
  public PollStatus findExportStatus(String exportId) {
    ExportStatus exportStatusObj = exportStatusRepository.findByExportId(exportId);
    if (exportStatusObj == null) {
      throw new NotFoundException(String.format("poll exportId %s doesn't exist", exportId));
    }
    return PollStatus.valueOf(exportStatusObj.getStatus());
  }
  
  @Override
  @Transactional
  public void updateExportStatus(String exportId, PollStatus status) {
    ExportStatus existingExportStatusObj = exportStatusRepository.findByExportId(exportId);
    existingExportStatusObj.setStatus(status.name());
    exportStatusRepository.save(existingExportStatusObj);
    log.info("poll export status successfully updated with ID={} and status={}",
        existingExportStatusObj.getExportId(), existingExportStatusObj.getStatus());
  }
}
