package com.sothebys.propertydb.service;

import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ImageSubject;
import com.sothebys.propertydb.repository.ImageSubjectRepository;

import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Minhtri Tran on 11/15/17.
 */
@Service
@Slf4j
public class ImageSubjectServiceImpl implements ImageSubjectService {

	private ImageSubjectRepository imageSubjectRepository;

	@Autowired
	public ImageSubjectServiceImpl(ImageSubjectRepository imageSubjectRepository) {
		this.imageSubjectRepository = imageSubjectRepository;
	}

  @Transactional(readOnly = true)
  @Override
  public ImageSubject findByName(String name) {
    ImageSubject imageSubject = imageSubjectRepository.findByName(name);

		if (imageSubject == null) {
			throw new NotFoundException(String.format("No image subject found with name=%s", name));
		}

		return imageSubject;
	}

	@Override
	public ImageSubject findByNameCreateIfNotFound(String name) {
		ImageSubject imageSubject = imageSubjectRepository.findByName(name);

		if (imageSubject == null) {
			ImageSubject newImageSubject = new ImageSubject();
			newImageSubject.setName(name);
			imageSubject = imageSubjectRepository.save(newImageSubject);
		}

		return imageSubject;
	}


	/**
	 * {@inheritDoc}
	 */
	@Transactional(readOnly = true)
	@Override
	public List<ImageSubject> findAllImageSubjects() {
		return imageSubjectRepository.findAllByOrderByNameAsc();
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public List<ImageSubject> findAllImageSubjectsByName(String name) {
		return imageSubjectRepository.findByNameContaining(name);
	}

	@Override
	public ImageSubject updateImageSubjectById(Long imageSubjectId, ImageSubject imageSubject) {
		ImageSubject existingImageSubject = imageSubjectRepository.findOne(imageSubjectId);
		if (existingImageSubject != null) {
			if (imageSubjectId != null && existingImageSubject.getId() == imageSubjectId) {
				existingImageSubject.setName(imageSubject.getName());
				log.info("Image Subject successfully updsted with ID={}", imageSubjectId);
				return imageSubjectRepository.save(existingImageSubject);
			}
		}
		throw new CannotPerformOperationException(String.format(
				"Image Subject with Id '%s' does not exists,Hence Image Subject update failed", imageSubjectId));

	}

	@Override
	public void deleteImageSubjectById(Long imageSubjectId) {
		ImageSubject imageSubjectObj;
		imageSubjectObj = imageSubjectRepository.findOne(imageSubjectId);
		if (imageSubjectObj == null) {
			throw new NotFoundException(String.format("No imageSubjectId found with ID=%d", imageSubjectId));
		}

		imageSubjectObj = imageSubjectRepository.findByProperty(imageSubjectId);
		if (imageSubjectObj != null) {
			throw new NotFoundException(String.format(
					"Image Subject with ID = %d can't be deleted, "
							+ "Image Subjectr = %s has already assigned to an Property",
					imageSubjectId, imageSubjectObj.getName()));
		}

		imageSubjectRepository.delete(imageSubjectId);
		log.info("Image Subject successfully deleted with ID={}", imageSubjectId);

	}

	@Override
	public ImageSubject createImageSubject(ImageSubject imageSubject) {
		ImageSubject createdImageSubject;
		try {
			createdImageSubject = imageSubjectRepository.save(imageSubject);
		} catch (DataIntegrityViolationException e) {
			throw new CannotPerformOperationException(String.format("ImageSubject name already exists."));
		}
		return createdImageSubject;
	}

	@Override
    @Transactional(readOnly = true)
    public ImageSubject getImageSubject(Long imageSubjectId) {
      return imageSubjectRepository.findOne(imageSubjectId);
    }
    
    
    @Override
    @Transactional
    public ImageSubject mergeImageSubject(Long mergeToImageSubjectId, Long mergeFromImageSubjectId) {
      imageSubjectRepository.updatePropertyImageSubject(mergeToImageSubjectId, mergeFromImageSubjectId);
      deleteImageSubjectById(mergeFromImageSubjectId);

      log.info("Image Subject successfully deleted with ID={}", mergeFromImageSubjectId);
      return getImageSubject(mergeToImageSubjectId);
    }
}
