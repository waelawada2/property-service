package com.sothebys.propertydb.service;

import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.User;
import java.io.InputStream;
import java.util.List;
import org.apache.commons.lang3.tuple.Pair;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

public interface PropertyService {

  /**   
   *  The method creates a new property with provided Property entity.
   * 
   * @param propertyObject refers to property entity 
   * @param user refers to the logged in user
   * @return returns the newly created Property Object
   */
  Property createProperty(Property propertyObject, User user);

  /**   
   * Delete a property based on the provided {@code externalId}.
   * 
   * @param propertyId of the property to be deleted
   */
  void deleteProperty(Long propertyId);

  /**
   * Delete property status updated with 'Active' identified by the provided {@code externalId}.
   *
   * @param externalId The externalId of the property.
   * @throws CannotPerformOperationException while removing Property image from S3 bucket
   * @throws NotFoundException When the property could not be found.
   */
  void deletePropertyByExternalId(String externalId);

  /**
   * This method is to get all the properties in the system.
   * 
   * @param pageRequest which refers to sort, pagination & total number of parameters
   * @return List of all the properties 
   */
  Page<Property> findAllProperties(Pageable pageRequest);

 /**    
  * This method is find the list of properties based on the specification provided
  *  
  * @param spec refers to the search criteria 
  * @param pageRequest refers to the pa
  * @return returns list of properties matching the search criteria
  */
  Page<Property> findBySearchTerm(Specification<Property> spec, Pageable pageRequest);

  /**
   * Retrieve a property with status as an 'Active' identified by the provided {@code externalId}.
   *
   * @param externalId The external ID of the property.
   * @return A {@link Property} instance if the property was found, otherwise {@code null}.
   * @throws NotFoundException When the property could not be found.
   */
  Property getPropertyByExternalId(String externalId);

  /**
   * Retrieve a property identified by the provided {@code propertyId}.
   *
   * @param propertyId The ID of the property.
   * @return A {@link Property} instance if the property was found, otherwise {@code null}.
   * @throws NotFoundException When the property could not be found.
   */
  Property getPropertyById(Long propertyId);

  /**
   * Delete property image identified by the provided {@code externalId}.
   *
   * @param externalId The external ID of the property.
   * @throws IllegalStateException When the image could not be deleted.
   * @throws NotFoundException When the property or the image could not be found.
   */
  Property removePropertyImage(String externalId);

  /**
   * Update property with the data of the provided {@code Property} instance. The property is
   * identified by the external ID.
   *
   * @param propertyExternalId The external ID of the property.
   * @param property An {@code Property} instance with data to be updated.
   * @param user A {@code User} that refers to the user updating the property
   * @param dropImage that refers if the image has been dropped or not
   * @return The updated {@code Property} instance.
   * @throws CannotPerformOperationException
   */
  Property updatePropertyByExternalId(String propertyExternalId, Property property, User user,
      boolean dropImage);

  /**
   * Update property with the data of the provided {@code Property} instance. The property is
   * identified by the external ID.
   *
   * @param propertyExternalId The external ID of the property.
   * @param property An {@code Property} instance with data to be updated.
   * @param user A {@code User} that refers to the user updating the property
   * @param dropImage that refers if the image has been dropped or not
   * @param isMerge that refers if the operation is merge
   * @return The updated {@code Property} instance.
   * @throws CannotPerformOperationException
   */
  Property updatePropertyByExternalId(String propertyExternalId, Property property, User user,
      boolean dropImage, boolean isMerge);

  /**
   * Returns a an 'Active' property's default image as a stream.
   *
   * @param externalId The external ID of the property.
   * @return The image as a stream.
   * @throws NotFoundException When the property or the image could not be found.
   */
  Pair<InputStream, Long> getDefaultImage(String externalId);

  /**
   * Creates a new default image for the property identified by the provided {@code externalId}.
   *
   * @param externalId The external ID of the property.
   * @param inputStream The image as a stream.
   * @param inputSize The image size in bytes.
   * @param user The user updating the image of the property
   * @return The generated file name.
   * @throws NotFoundException When the property or the image could not be found.
   */
  String createDefaultImage(String externalId, InputStream inputStream, long inputSize, User user);

  /**
   * Creates Properties based on numberOfDuplicates provided
   * 
   * @param externalId of the property to be duplicated
   * @param numberOfDuplicates refers to the number if duplicates to be created
   * @return properties List refers to the list of properties created after duplication
   * @throws CannotPerformOperationException in case numberOfDuplicates doesn't match the criteria
   * @throws NotFoundException is thrown if the property is not found
   */
  List<Property> createDuplicateProperties(String externalId, Integer numberOfDuplicates,
      User user) throws NotFoundException, CannotPerformOperationException;

  /**
   * Return merged property object.
   *
   * @param mergeFromId The external ID of the property.
   * @param mergeToId The external ID of the property.
   * @param property An {@code Property} instance with data to be merged.
   * @param user A {@code User} that refers to the user merging the property
   * @param dropImage is a boolean value that refers if an image has been dropped or not
   * @param securityContext containing the credentials data to be reused for calling events-service.
   * @return merged property object.
   * @throws NotFoundException When the property could not be found.
   */
  Property mergePropertyByExternalId(String mergeFromId, String mergeToId, Property property,
      User user, boolean dropImage, KeycloakSecurityContext securityContext);

  /**
   * Finds details for the list of properties for a provided save list
   * 
   * @param propertyId refers to the list of properties associated with a save list
   * @param pageRequest refers to the page related properties
   * @return list of properties found from the provided list
   */
  Page<Property> findPropertiesByExternalIdIn(List<String> propertyId, Pageable pageRequest);

  /**
   * Retrieve Property details in an output.
   */
  String exportPropertyData();

  /**
   * Delete all properties based on the provided list of externalIds.
   * 
   * @return The success deleted propertyIds count.
   */
  Integer deleteAllProperties(List<String> ids);

  /**
   * Delete a property imagePath based on the provided list of externalIds.
   * 
   * @param ids The externalIds of the property.
   */
  void deletePropertyImagePath(List<String> ids);

}
