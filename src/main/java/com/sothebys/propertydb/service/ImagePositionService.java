package com.sothebys.propertydb.service;

import java.util.List;
import com.sothebys.propertydb.model.ImagePosition;

public interface ImagePositionService {

	ImagePosition findByName(String name);

	/**
	 * Search for all genders.
	 *
	 * @return list of the genders
	 */
	List<ImagePosition> findAllImagePositions();

	ImagePosition updateImagePositionById(Long imagePositionId, ImagePosition imagePosition);

	void deleteImagePositionById(Long imagePositionId);

	ImagePosition createImagePosition(ImagePosition imagePosition);
	
	List<ImagePosition> findAllImagePositionByName(String name);
	
	ImagePosition getImagePosition(Long imagePositionId);
	
	ImagePosition mergeImagePosition(Long mergeToImagePositionId, Long mergeFromImagePositionId);
}
