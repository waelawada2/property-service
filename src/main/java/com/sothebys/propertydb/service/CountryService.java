package com.sothebys.propertydb.service;

import java.util.List;

import com.sothebys.propertydb.model.Country;

/**
 * Service that provides operations for country management.
 */
public interface CountryService {

  /**
   * Create new Country with the data of the provided {@code Country} instance.
   *
   * @param country The ID of the country that should be created if country doesn't exist.
   * @return The updated {@code Country} instance.
   */
  Country createCountry(Country country);

  /**
   * Retrieve all country details
   * 
   * @return A list of {@code Country} instances.
   */
  List<Country> findAllCountries();

  /**
   * Update an country with the data of the provided {@code Country} instance. The country is
   * identified by the internal ID.
   *
   * @param countryId The ID of the country that should be updated.
   * @param country An {@code Country} instance with data to be updated.
   * @return The updated {@code Country} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no country can be found with the
   *         provided ID.
   */
  Country updateCountryById(Long countryId, Country country);

  /**
   * Delete country identified by the provided {@code countryId}.
   *
   * @param countryId The countryId of the country.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no country can be found with the
   *         provided ID.
   * 
   */
  void deleteCountryById(Long countryId);

  /**
   * Retrieve all country details by name search
   * 
   * @return A list of {@code Country} instances.
   */
  List<Country> findAllCountriesByName(String name);

  /**
   * Retrieve country details by given country name
   * 
   * @return The country {@code Country} instance.
   */
  Country findByCountryName(String countryName);
  
  /**
   * Merge countries
   * @param mergeToCountryId
   * @param mergeFromCountryId
   * @return The country {@code Country} instance.
   */
  Country mergeCountries(Long mergeToCountryId, Long mergeFromCountryId);

}
