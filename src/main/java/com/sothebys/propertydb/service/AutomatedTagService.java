package com.sothebys.propertydb.service;

import com.sothebys.propertydb.model.Orientation;
import com.sothebys.propertydb.model.Scale;

/**
 * Service that provides operations for country management.
 */
public interface AutomatedTagService {

	/**
	 * Retrieve all orientation list.
	 * 
	 * @return An array of {@code Orientation} instances.
	 */
	Orientation[] getAllOrientation();

	/**
	 * Retrieve all scale list.
	 * 
	 * @return An array of {@code Scale} instances.
	 */
	Scale[] getAllScales();

}
