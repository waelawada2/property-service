package com.sothebys.propertydb.service;

import java.util.List;

import com.sothebys.propertydb.model.PresizeType;

public interface PresizeTypeService {

  PresizeType createPresizeType(PresizeType presizeType);

  List<PresizeType> getAllPresizeTypes();

  PresizeType getPresizeTypeById(Long presizeTypeId);

  PresizeType getPresizeTypeByName(String presizeType);

  /**
   * Update a presize type identified with the provided {@presizeType} object.
   *
   * @param presizeType Presize type with the new data.
   * @return The presize type object with the updated data.
   */
  PresizeType updatePresizeType(PresizeType presizeType);

  /**
   * Delete a presize type identified by the provided {@code presizeTypeId}
   * 
   * @param presizeTypeId The ID of the presize type.
   */
  void deletePresizeType(Long presizeTypeId);
  
  
  /**
   * Return merged PresizeType object.
   * 
   * @param mergeToPresizeTypeId
   * @param mergeFromPresizeTypeId
   * @return PresizeType object
   */
  PresizeType mergePresizeType(Long mergeToPresizeTypeId, Long mergeFromPresizeTypeId);

}
