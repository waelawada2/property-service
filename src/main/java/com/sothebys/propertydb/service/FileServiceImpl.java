package com.sothebys.propertydb.service;

import static com.sothebys.propertydb.configuration.views.ArtistCsvView.ARTIST_HEADER;
import static com.sothebys.propertydb.configuration.views.PropertyCsvView.PROPERTY_HEADER;
import static org.apache.commons.lang3.StringUtils.equalsAnyIgnoreCase;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.removeStart;

import com.sothebys.propertydb.exception.PropertyServiceException;
import com.sothebys.propertydb.util.AmazonS3FileHandler;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.IntStream;
import javax.activation.UnsupportedDataTypeException;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by wael.awada on 5/1/17.
 */
@Component
public class FileServiceImpl implements FileService {

  private static final String UTF8_BOM = "\uFEFF";

  @Autowired
  @Qualifier("importS3Client")
  private AmazonS3FileHandler amazonS3FileHandler;

  @Override
  public String uploadFile(MultipartFile multipart, String type) {
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(multipart.getInputStream()))) {
      switch (type.toUpperCase()) {
        case "PROPERTY":
          validateHeader(reader.readLine(), PROPERTY_HEADER);
          break;
        case "ARTIST":
          validateHeader(reader.readLine(), ARTIST_HEADER);
          break;
        default:
          throw new UnsupportedDataTypeException(type);
      }
      String fileName = amazonS3FileHandler.getCSVFileName(type, multipart.getOriginalFilename());
      return amazonS3FileHandler.uploadFile(
          multipart.getInputStream(), multipart.getSize(), fileName);
    } catch (IOException e) {
      throw new PropertyServiceException("Couldn't read the data of multipart file", e);
    }
  }

  /**
   * Validates a comma separated value header.
   */
  private static void validateHeader(String csvHeader, String... expectedHeader) {
    csvHeader = removeStart(csvHeader, UTF8_BOM);
    if (isBlank(csvHeader)) {
      throw new PropertyServiceException("Error: File does not contain any headers");
    }
    String[] uploadedHeader = csvHeader.split(",", -1);
    if (expectedHeader.length != uploadedHeader.length) {
      throw new PropertyServiceException(
          "Error: File does not contain the expected number of columns");
    }
    IntStream.range(0, expectedHeader.length)
        .mapToObj(i -> Pair.of(expectedHeader[i], uploadedHeader[i].replace("\"", "")))
        .filter(validate -> !validate.getKey().equalsIgnoreCase(validate.getValue()))
        .findFirst()
        .map(diff -> isBlank(diff.getValue()) ? "Error: File contains empty headers"
            : String.format(equalsAnyIgnoreCase(diff.getValue(), expectedHeader)
                    ? "Error: File contains columns which are not in the expected order, Uploaded : %s, Expected : %s"
                    : "Error: File contains column headers which are not expected, Uploaded : %s, Expected : %s",
                diff.getValue(), diff.getKey()))
        .map(PropertyServiceException::new)
        .ifPresent(e -> {
          throw e;
        });
  }

}
