package com.sothebys.propertydb.service;

import java.util.List;
import com.sothebys.propertydb.model.ImageAnimal;

/**
 * @author 171524
 *
 */
public interface ImageAnimalService {

  ImageAnimal findByName(String name);

  ImageAnimal findByNameCreateIfNotFound(String name);

	/**
	 * Creates an Animal
	 * 
	 * @param animal
	 *            An {@code ImageAnimal} instance with data to be inserted
	 * @return the instance of the created ImageAnimal
	 *
	 */
	ImageAnimal createImageAnimal(ImageAnimal imageAnimal);

	/**
	 * Search for all animals.
	 *
	 * @return list of the animals
	 */
	List<ImageAnimal> findAllImageAnimals();

	/**
	 * Search for animals by the name.
	 *
	 * @param name
	 * 
	 * @return list of the animal. Search based on the name
	 */
	List<ImageAnimal> findAllImageAnimalsByName(String name);

	/**
	 * Deletes animal
	 * 
	 * @param id
	 *
	 */
	void deleteImageAnimal(Long id);

	/**
	 * Update the animal with the data of the provided {@code ImageAnimal} instance.
	 *
	 * @param imageAnimal
	 *            An {@code ImageAnimal} instance with data to be updated.
	 * @return The updated {@code ImageAnimal} instance.
	 * @throws com.sothebys.propertydb.exception.NotFoundException
	 *             if no imageAnimal can be found.
	 * @throws com.sothebys.propertydb.exception.CannotPerformOperationException
	 *             if end-point is unable to acquire lock on the table to update the
	 *             property.
	 */
	ImageAnimal updateImageAnimal(ImageAnimal imageAnimal);
	
	
	/**
	 * @param mergeToImageAnimalId
	 * @param mergeFromImageAnimalId
	 * @return ImageAnimal
	 */
	ImageAnimal mergeImageAnimal(Long mergeToImageAnimalId, Long mergeFromImageAnimalId);

	ImageAnimal getImageAnimal(Long imageAnimalId);
}
