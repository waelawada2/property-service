package com.sothebys.propertydb.service;

import java.util.List;

import com.sothebys.propertydb.model.Edition;
import com.sothebys.propertydb.model.EditionSizeType;

/**
 * Service that provides operations for edition size type management.
 */
public interface EditionSizeTypeService {

  /**
   * Retrieve all edition size type details
   *
   * @return A list of {@code EditionSizeType} instances.
   */
  List<EditionSizeType> findAllEditionSizeTypes();

  /**
   * Retrieve editionSizeType identified by the provided {@code editionSizeTypeName}.
   *
   * @param editionSizeType The name of the editionSizeType.
   * @return An {@code EditionSizeType} instance if editionSizeType name was found, otherwise
   *         {@code null}.
   */
  EditionSizeType findEditionSizeTypeByName(String editionSizeType);

  /**
   * Create new EditionSizeType with the data of the provided {@code EditionSizeType} instance.
   *
   * @param editionSizeType The ID of the editionSizeType that should be created if editionSizeType
   *        doesn't exist.
   * @return The updated {@code EditionSizeType} instance.
   */
  EditionSizeType createEditionSizeType(EditionSizeType editionSizeType);

  /**
   * Deletes an edition size type identified by the provided {@code editionSizeTypeId}
   *
   * @param editionSizeTypeId The ID of the edition size type.
   */
  void deleteEditionSizeType(Long editionSizeTypeId);

  /**
   * Retrieve a editionSizeType identified by the provided {@code editionSizeTypesId}.
   *
   * @param editionSizeTypesId The ID of the editionSizeType.
   * @return A {@link EditionSizeType} instance if the editionSizeType was found, otherwise
   *         {@code null}.
   */
  EditionSizeType findEditionSizeTypeById(Long editionSizeTypesId);

  /**
   * Update an EditionSizeType with the data of the provided {@code EditionSizeType} instance. The
   * editionSizeType is identified by the internal ID.
   *
   * @param editionSizeTypeId The ID of the editionSizeType that should be updated.
   * @param editionSizeType An {@code EditionSizeType} instance with data to be updated.
   * @return The updated {@code EditionSizeType} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no editionSizeType can be found
   *         with the provided ID.
   */
  EditionSizeType updateEditionSizeType(Long editionSizeTypeId, EditionSizeType editionSizeType);

  /**
   * Calculate Edition Size total with the data of the provided {@code EditionSizeType} instance.  
   * @param edition 
   */
  void calculateEditionSize(Edition edition);
  
  /**
   * Return merged EditionSizeType object.
   * 
   * @param mergeToEditionSizeTypeId
   * @param mergeFromEditionSizeTypeId
   * @return EditionSizeType object
   */
  EditionSizeType mergeEditionSizeType(Long mergeToEditionSizeTypeId, Long mergeFromEditionSizeTypeId);

}
