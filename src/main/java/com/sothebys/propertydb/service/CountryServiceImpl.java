package com.sothebys.propertydb.service;

import org.springframework.dao.DataIntegrityViolationException;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.Country;
import com.sothebys.propertydb.repository.CountryRepository;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author SrinivasaRao.Batthula
 *
 */
@Slf4j
@Service
public class CountryServiceImpl implements CountryService {

  @Autowired
  private CountryRepository countryRepository;


  @Override
  @Transactional(readOnly = true)
  public List<Country> findAllCountries() {
    return countryRepository.findAll();
  }

  @Override
  @Transactional
  public Country createCountry(Country country) {
    Country createdCountry;
    try {
      createdCountry = countryRepository.save(country);
    } catch (DataIntegrityViolationException e) {
      throw new CannotPerformOperationException(String.format("Country name already exists."));
    }
    return createdCountry;
  }

  @Transactional
  @Override
  public Country updateCountryById(Long countryId, Country country) {
    Country existingCountry = countryRepository.findOne(countryId);
    if (existingCountry != null) {
      if (countryId != null && existingCountry.getId() == countryId) {
        existingCountry.setCountryName(country.getCountryName());
        log.info("Country successfully updated with ID={}", countryId);
        return countryRepository.save(existingCountry);
      }
    }
    throw new CannotPerformOperationException(String
        .format("Country with Id '%s' does not exists,Hence country update failed", countryId));
  }

  @Transactional
  @Override
  public void deleteCountryById(Long countryId) {
    Country countryObj;
    countryObj = countryRepository.findOne(countryId);
    if (countryObj == null) {
      throw new NotFoundException(String.format("No country found with ID=%d", countryId));
    }

    countryObj = countryRepository.findCountryByCountryId(countryId);
    if (countryObj != null) {
      throw new NotFoundException(String.format("Country with ID = %d can't be deleted, "
              + "Country = %s has already assigned to an Artist", countryId,
          countryObj.getCountryName()));
    }

    countryObj = countryRepository.findCountryByFlagsCountryId(countryId);
    if (countryObj != null) {
      throw new NotFoundException(String.format("Country with ID=%d can't be deleted, "
              + "Country = %s has already assigned to a Property", countryId,
          countryObj.getCountryName()));
    }

    countryRepository.delete(countryId);
    log.info("Presize type successfully deleted with ID={}", countryId);
  }

  @Transactional(readOnly = true)
  @Override
  public List<Country> findAllCountriesByName(String countryName) {
    return countryRepository.findByCountryNameContaining(countryName);
  }

  @Transactional(readOnly = true)
  @Override
  public Country findByCountryName(String countryName) {
    return countryRepository.findByCountryName(countryName);
  }
  
  @Override
  @Transactional
  public Country mergeCountries(Long mergeToCountryId, Long mergeFromCountryId) {

    countryRepository.updatePropertyCountry(mergeToCountryId, mergeFromCountryId);
    countryRepository.updateArtistCountry(mergeToCountryId, mergeFromCountryId);
    countryRepository.deleteArtistCountry(mergeFromCountryId);
    deleteCountryById(mergeFromCountryId);

    log.info("Country successfully deleted with ID={}", mergeFromCountryId);
    return countryRepository.findOne(mergeToCountryId);
  }
}
