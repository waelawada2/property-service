package com.sothebys.propertydb.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.OtherTag;
import com.sothebys.propertydb.repository.OtherTagRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by Minhtri Tran on 11/15/17.
 */
@Slf4j
@Service
public class OtherTagServiceImpl implements OtherTagService {

  private OtherTagRepository otherTagRepository;

  @Autowired
  public OtherTagServiceImpl(OtherTagRepository otherTagRepository) {
    this.otherTagRepository = otherTagRepository;
  }

  @Transactional(readOnly = true)
  @Override
  public OtherTag findByName(String name) {
    OtherTag otherTag = otherTagRepository.findByName(name);

    if (otherTag == null) {
      throw new NotFoundException(String.format("No image position found with name=%s", name));
    }

    return otherTag;
  }

  @Override
  public OtherTag findByNameCreateIfNotFound(String name) {
    OtherTag otherTag = otherTagRepository.findByName(name);

    if (otherTag == null) {
      OtherTag newOtherTag = new OtherTag();
      newOtherTag.setName(name);
      otherTag = otherTagRepository.save(newOtherTag);
    }

    return otherTag;
  }

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public OtherTag createOtherTag(OtherTag otherTag) {
		Objects.requireNonNull(otherTag.getName(), "Tag value must not be null");
		List<OtherTag> searchOtherTag = otherTagRepository.findAllByName(otherTag.getName());
		if (searchOtherTag == null || searchOtherTag.size() == 0) {
			// OtherTag newOtherTag = new OtherTag();
			// newOtherTag.setName(otherTag.getName());
			otherTag = otherTagRepository.save(otherTag);
		} else {
			throw new CannotPerformOperationException(String.format("Tag already exists."));
		}

		return otherTag;

	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void deleteOtherTag(Long id) {
		Objects.requireNonNull(id, "id parameter must not be null");

		OtherTag otherTag = otherTagRepository.findById(id);
		if (otherTag == null) {
			throw new NotFoundException(String.format("Tag with Id %s doesn't exist", id));
		}
/*		int usedOtherTagsCount = otherTagRepository.countOtherTags(id);
		if (usedOtherTagsCount > 0) {
			throw new CannotPerformOperationException("The tag cannot be deleted as it is used by other objects");
		}*/
		try {
			otherTagRepository.delete(otherTag.getId());
		} catch (CannotAcquireLockException e) {
			throw new CannotPerformOperationException("Lock couldn't be acquired on table, please try again later");
		}
		log.info("Other Tag '{}' is sucessfully deleted", otherTag.getName());
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public OtherTag updateOtherTag(OtherTag otherTag) {
		Objects.requireNonNull(otherTag.getId(), "Tag id must not be null");
		Objects.requireNonNull(otherTag.getName(), "Tag value must not be null");
		OtherTag searchOtherTag = otherTagRepository.findById(otherTag.getId());
		if (searchOtherTag == null) {
			throw new NotFoundException(String.format("Tag with Id %s doesn't exist", otherTag.getId()));
		}
/*		int usedOtherTagsCount = otherTagRepository.countOtherTags(otherTag.getId());
		if (usedOtherTagsCount > 0) {
			throw new CannotPerformOperationException("The tag cannot be edited as it is used by other objects");
		}*/
		searchOtherTag.setName(otherTag.getName());
		try {
			searchOtherTag = otherTagRepository.save(searchOtherTag);
			log.info("Other tag '{}' successfully updated", searchOtherTag.getName());
		} catch (CannotAcquireLockException e) {
			throw new CannotPerformOperationException("Lock couldn't be acquired on table, please try again later");
		}
		return searchOtherTag;
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(readOnly = true)
	@Override
	public List<OtherTag> findAllOtherTags() {
		return otherTagRepository.findAllByOrderByNameAsc();
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public List<OtherTag> findAllOtherTagsByName(String name) {
		return otherTagRepository.findByNameContaining(name);
	}
}
