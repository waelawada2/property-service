package com.sothebys.propertydb.service;

import com.sothebys.propertydb.constants.PropertyServiceConstants;
import com.sothebys.propertydb.dto.PropertyDTO;
import com.sothebys.propertydb.mapper.PropertyModelMapper;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.repository.PropertyRepository;
import com.sothebys.propertydb.search.PollStatus;
import com.sothebys.propertydb.search.Status;
import com.sothebys.propertydb.util.AmazonS3FileHandler;
import com.sothebys.propertydb.util.ExportData;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author SrinivasaRao.Batthula
 */

@Slf4j
@Qualifier("propertyExportService")
@Service
public class PropertyExportServiceImpl implements ExportService {

  @Autowired
  private PropertyRepository propertyRepository;

  @Autowired
  @Qualifier("exportDataS3Client")
  private AmazonS3FileHandler amazonS3ExportDataHandler;

  @Autowired
  private ExportData exportData;

  @Autowired
  private ExportStatusService exportStatusService;

  @Autowired
  private PropertyModelMapper propertyModelMapper;

  @Value("${exportData.max-count:20000}")
  private long maxExportDataCount;
  
  @Override
  @Transactional
  @Async
  public void exportData(Specification<Property> spec, String exportId) {
    log.info("Start Processing for the export ID='{}'", exportId);
    PollStatus status = PollStatus.InProgress;

    String exportCSVFileName =
        String.format("%s%s", exportId, PropertyServiceConstants.CSV_EXTENSION);

    List<PropertyDTO> propertyDtos = getProperties();
    try {
      exportData.buildPropertyCsvDocument(propertyDtos, exportCSVFileName);
      amazonS3ExportDataHandler.uploadExportDataFileIntoS3(exportCSVFileName);
      status = PollStatus.Completed;
    } catch (Exception e) {
      log.error("Property csv file='{}' export failed", exportCSVFileName);
      status = PollStatus.Error;
    }
    exportStatusService.updateExportStatus(exportId, status);

  }

  @Transactional(readOnly = true)
  public List<PropertyDTO> getProperties() {
    List<PropertyDTO> propertyList = new ArrayList<PropertyDTO>();
    List<Property> properties =
        propertyRepository.findPropertiesByStatus(Status.Active);
    propertyList =
        propertyModelMapper.mapToDTO(properties);

    return propertyList;
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(readOnly = true)
  @Override
  public PollStatus findExportStatus(String exportId) {
    return exportStatusService.findExportStatus(exportId);
  }

  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public String generateExportId() {
    return UUID.randomUUID().toString();
  }
  
  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public boolean validateExportData(Specification<Property> spec, String exportId) {
	  log.info("Validating the count of Active Property data");
	  long propertyCount = propertyRepository.countPropertiesByStatus(Status.Active);
	  log.info("Active Property count = '{}'", Long.toString(propertyCount));
	  if(propertyCount > maxExportDataCount) {
		  PollStatus status = PollStatus.LimitExceeded;
		  exportStatusService.updateExportStatus(exportId, status);
		  return false;
	  }
	  return true;
  }

  @Override
  public void exportByEmail(List<String> propertyIds, String name) {
    // TODO Auto-generated method stub    
  }
  
 }
