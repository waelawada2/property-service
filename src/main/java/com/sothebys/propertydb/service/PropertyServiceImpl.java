package com.sothebys.propertydb.service;

import static org.springframework.http.HttpMethod.PATCH;

import com.sothebys.propertydb.dto.SnsMessageDto;
import com.sothebys.propertydb.dto.SnsMessageOperation;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.MergeException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.exception.PropertyServiceException;
import com.sothebys.propertydb.model.ArchiveNumber;
import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.Edition;
import com.sothebys.propertydb.model.ExpertiseSitter;
import com.sothebys.propertydb.model.ImageAnimal;
import com.sothebys.propertydb.model.ImageGender;
import com.sothebys.propertydb.model.ImageMainColour;
import com.sothebys.propertydb.model.ImagePosition;
import com.sothebys.propertydb.model.ImageSubject;
import com.sothebys.propertydb.model.OtherTag;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.PropertyCatalogueRaisonee;
import com.sothebys.propertydb.model.SaveList;
import com.sothebys.propertydb.model.Tags;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.repository.PropertyRepository;
import com.sothebys.propertydb.search.Status;
import com.sothebys.propertydb.util.AlphaNumericIdGenerator;
import com.sothebys.propertydb.util.AmazonS3FileHandler;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.exception.ConstraintViolationException;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class PropertyServiceImpl implements PropertyService {

  public static final String PROPERTY_EXTERNAL_ID_PREFIX = "OO-";
  public static final String PROPERTY_EXPORTER_NAME_PREFIX = "property-";

  @Autowired
  @Qualifier("imageS3Client")
  private AmazonS3FileHandler amazonS3ImageHandler;

  @Autowired
  @Qualifier("websiteImageDataS3Client")
  private AmazonS3FileHandler amazonS3WebsiteDataHandler;

  @Autowired
  private PropertyRepository propertyRepository;

  @Autowired
  private SaveListService saveListService;

  @Autowired
  private ExportStatusService exportStatusService;

  @Value("${page.record-count:20}")
  private int pageRecordCount;

  @Value("${event-service.base-url}")
  private String eventServiceUrl;

  private RestTemplate restTemplate;
  private ExportService propertyExportService;
  
  private MessageService messageService;

  public PropertyServiceImpl(RestTemplateBuilder restTemplateBuilder,
      @Qualifier("propertyExportService") ExportService propertyExportService, MessageService messageService) {
    restTemplate = restTemplateBuilder.build();
    this.propertyExportService = propertyExportService;
    this.messageService = messageService;
  }

  @Override
  @Transactional
  public Property createProperty(Property property, User user) {
    property.setCreatedBy(user);
    Property createdPropertyObj = null;
    try {
      Property propertyObj = propertyRepository.saveAndFlush(property);
      propertyObj.setExternalId(AlphaNumericIdGenerator.getAlphaNumbericId(propertyObj.getId(),
          PROPERTY_EXTERNAL_ID_PREFIX));
      createdPropertyObj = propertyRepository.saveAndFlush(propertyObj);
    } catch (CannotAcquireLockException e) {
      throw new CannotPerformOperationException(
          "Lock couldn't be acquired on table. Please try again later", e);
    } catch (DataIntegrityViolationException e) {
      throw new PropertyServiceException("Object Code should be unique", e);
    }

    if (createdPropertyObj != null) {
      SnsMessageDto messageDto = new SnsMessageDto();
      messageDto.setId(createdPropertyObj.getExternalId());
      messageService.sendSnsMessage(messageDto, SnsMessageOperation.CREATE);
    }

    return createdPropertyObj;
  }

  @Override
  @Transactional
  public void deleteProperty(Long propertyId) {
    Property propertyObj = propertyRepository.findOne(propertyId);
    if (propertyObj == null) {
      throw new NotFoundException(String.format("Property with ID %s doesn't exist", propertyId));
    }
    propertyObj.setObjectCodes(new ArrayList<>());
    List<PropertyCatalogueRaisonee> propertyCatalogueRaisonees =
        propertyObj.getPropertyCatalogueRaisonees();
    if (!CollectionUtils.isEmpty(propertyCatalogueRaisonees)) {
      for (PropertyCatalogueRaisonee cr : propertyCatalogueRaisonees) {
        cr.setStatus(Status.Deleted);
      }
    }
    propertyObj.setPropertyCatalogueRaisonees(propertyCatalogueRaisonees);
    propertyObj.setStatus(Status.Deleted);
    propertyRepository.save(propertyObj);
    log.info("Property object status change to Deleted for ID={}", propertyId);
  }

  /**
   * to delete property using externalId
   *
   * @param externalId throws IOException,DataAccessException and Exception
   */
  @Override
  @Transactional
  public void deletePropertyByExternalId(String externalId) {
    Property propertyObject = propertyRepository.findByExternalId(externalId);
    if (propertyObject == null || propertyObject.getStatus() != Status.Active) {
      throw new NotFoundException(String.format("Property with ID %s doesn't exist", externalId));
    }
    String imageKey = propertyObject.getImagePath();
    try {
      if (StringUtils.isNotBlank(imageKey)) {
        amazonS3ImageHandler.deleteFile(imageKey);
        deleteProperty(propertyObject.getId());
      } else {
        deleteProperty(propertyObject.getId());
      }
      
      SnsMessageDto messageDto = new SnsMessageDto();
      messageDto.setId(externalId);
      messageService.sendSnsMessage(messageDto, SnsMessageOperation.DELETE);
      
    } catch (Exception e) {
      throw new PropertyServiceException("Property deletion failed", e);
    }
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Property> findAllProperties(Pageable pageRequest) {
    Page<Property> propertyPageResponse;
    if (pageRequest.getPageSize() == pageRecordCount) {
      Page<Property> properties =
          propertyRepository.findPropertiesByStatus(pageRequest, Status.Active);
      List<Property> propertyList = new ArrayList<>(properties.getContent());
      if (properties.getTotalPages() > 1) {
        for (int page = 1; page < properties.getTotalPages(); page++) {
          properties = propertyRepository.findPropertiesByStatus(
              new PageRequest(page, pageRequest.getPageSize(), pageRequest.getSort()),
              Status.Active);
          propertyList.addAll(properties.getContent());
        }
      }
      propertyPageResponse = new PageImpl<>(propertyList);
    } else {
      propertyPageResponse = propertyRepository.findPropertiesByStatus(pageRequest, Status.Active);
    }
    return propertyPageResponse;
  }

  /**
   * @param spec refers to the search criteria
   * @param pageRequest The information of the requested page.
   * @return the properties meeting the search criteria
   */
  @Override
  @Transactional(readOnly = true)
  public Page<Property> findBySearchTerm(Specification<Property> spec, Pageable pageRequest) {
    return propertyRepository.findAll(spec, pageRequest);
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(readOnly = true)
  @Override
  public Property getPropertyByExternalId(String externalId) {
    final Property property = propertyRepository.findByExternalId(externalId);
    if (property == null || property.getStatus() == Status.Deleted) {
      throw new NotFoundException(String.format("Property with ID %s doesn't exist", externalId));
    } else if (property.getStatus() == Status.Merged) {
      throw new MergeException(
          String.format("Property already merged to ID %s ", property.getMergedToId()),
          property.getMergedToId());
    }
    return property;
  }

  @Override
  @Transactional(readOnly = true)
  public Property getPropertyById(Long propertyId) {
    return propertyRepository.findOne(propertyId);
  }

  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public Property removePropertyImage(String externalId) {
    Property property = getPropertyByExternalId(externalId);
    if (property == null) {
      throw new NotFoundException(String.format("Property with ID %s doesn't exist", externalId));
    }
    if (StringUtils.isEmpty(property.getImagePath())) {
      throw new NotFoundException(String.format("Property with ID %s has no image", externalId));
    }
    amazonS3ImageHandler.deleteFile(property.getImagePath());
    property.setImagePath(null);
    if (property.getTags() == null) {
      property.setTags(new Tags());
    }
    property.getTags().setAutoImage(false);
    return propertyRepository.save(property);
  }
  
  @Retryable(value = {CannotAcquireLockException.class}, maxAttempts = 2)
  @Override
  @Transactional
  public Property updatePropertyByExternalId(String propertyExternalId, Property property,
      User user, boolean dropImage) {
	  return updatePropertyByExternalId(propertyExternalId,property, user, dropImage, false);
  }

  @Override
  @Transactional
  public Property updatePropertyByExternalId(String propertyExternalId, Property property,
      User user, boolean dropImage, boolean isMerge) {
    Property propertyObject = null;
    Property existingProperty = getPropertyByExternalId(propertyExternalId);
    if (dropImage) {
      if (StringUtils.isNotBlank(existingProperty.getImagePath())) {
        amazonS3ImageHandler.deleteFile(existingProperty.getImagePath());
      }
    } else {
      property.setImagePath(existingProperty.getImagePath());
    }
    property.setId(existingProperty.getId());
    if (CollectionUtils.isEmpty(property.getArtists())) {
      property.setArtists(existingProperty.getArtists());
    }

    if (property.getCurrentOwnerId() == null) {
      property.setCurrentOwnerId(existingProperty.getCurrentOwnerId());
    }

    if (property.getTags() == null) {
      property.setTags(existingProperty.getTags());
    }
    // If image path is present in the database set the autoImage value to true else set to false.
    property.getTags().setAutoImage(StringUtils.isNotBlank(property.getImagePath()));
    property.setExternalId(existingProperty.getExternalId());
    property.setUploadReference(existingProperty.getUploadReference());
    property.setStatus(existingProperty.getStatus());
    property.setCreatedBy(existingProperty.getCreatedBy());
    property.setCreatedDate(existingProperty.getCreatedDate());

    if (!CollectionUtils.isEmpty(existingProperty.getObjectCodes()) && !isMerge) {
      property.setObjectCodes(
          existingProperty.getObjectCodes().stream().distinct().collect(Collectors.toList()));
    }

    property.setUpdatedBy(user != null ? user : existingProperty.getUpdatedBy());
    try {
      return propertyRepository.save(property);
    } catch (CannotAcquireLockException e) {
      throw new CannotPerformOperationException(
          "Lock couldn't be acquired on table, please try again later", e);
    }
  }

  @Override
  @Transactional(readOnly = true)
  public Pair<InputStream, Long> getDefaultImage(String externalId) {
    final Property property = getPropertyByExternalId(externalId);
    if (StringUtils.isEmpty(property.getImagePath())) {
      throw new NotFoundException(
          String.format("Property with ID=%s has no default image", externalId));
    }
    return amazonS3ImageHandler.getFileContent(property.getImagePath());
  }

  @Override
  @Transactional
  public String createDefaultImage(final String externalId, final InputStream inputStream,
      final long inputSize, final User user) {
    try {
      final Property property = getPropertyByExternalId(externalId);
      final String imageName = amazonS3ImageHandler.uploadFile(inputStream, inputSize);
      if (StringUtils.isNotBlank(property.getImagePath())) {
        amazonS3ImageHandler.deleteFile(property.getImagePath());
      }
      property.setImagePath(imageName);
      if (property.getTags() == null) {
        property.setTags(new Tags());
      }
      property.getTags().setAutoImage(true);
      updatePropertyByExternalId(property.getExternalId(), property, user, false);
      return imageName;
    } finally {
      IOUtils.closeQuietly(inputStream);
    }
  }

  @Override
  @Transactional(noRollbackFor = {DataAccessException.class, DataIntegrityViolationException.class,
      ConstraintViolationException.class})
  public Property mergePropertyByExternalId(final String mergeFromId, final String mergeToId,
      final Property mergeToProperty, User user, boolean dropImage,
      KeycloakSecurityContext securityContext) {
    final Property mergeFromProperty = getPropertyByExternalId(mergeFromId);
    final Property existingMergeToProperty = getPropertyByExternalId(mergeToId);
    final Long mergeToPropertyId = existingMergeToProperty.getId();

    copyArchiveNumbersAndObjectCodes(mergeToId, mergeToProperty, mergeFromProperty,
        existingMergeToProperty);
    // TODO: Review that copyArchiveNumbersAndObjectCodes is already doing this...
    mergeToProperty.setObjectCodes(
        existingMergeToProperty.getObjectCodes() != null ? existingMergeToProperty.getObjectCodes()
            : new ArrayList<>());
    mergeToProperty.getObjectCodes()
        .addAll(mergeFromProperty.getObjectCodes() != null ? mergeFromProperty.getObjectCodes()
            : new ArrayList<>());
    if (!CollectionUtils.isEmpty(mergeToProperty.getArchiveNumbers())) {
      mergeToProperty.setArchiveNumbers(new ArrayList<>(
          mergeToProperty.getArchiveNumbers().stream().collect(Collectors.toCollection(
              () -> new TreeSet<>(Comparator.comparing(ArchiveNumber::getArchiveNumber))))));
    }
    if (!CollectionUtils.isEmpty(mergeToProperty.getObjectCodes())) {
      mergeToProperty.setObjectCodes(
          mergeToProperty.getObjectCodes().stream().distinct().collect(Collectors.toList()));
    }

    if (mergeFromProperty.getTags() != null) {
      if (mergeFromProperty.getTags().getImageSubjects() != null) {
        mergeToProperty.getTags()
            .setImageSubjects(Stream
                .concat(
                    (Optional.ofNullable(existingMergeToProperty.getTags().getImageSubjects())
                        .orElse(Collections.emptyList()).stream()),
                    mergeFromProperty.getTags().getImageSubjects().stream())
                .distinct().collect(Collectors.toList()));

      }
      if (mergeFromProperty.getTags().getImageGenders() != null) {
        mergeToProperty.getTags()
            .setImageGenders(Stream
                .concat(
                    (Optional.ofNullable(existingMergeToProperty.getTags().getImageGenders())
                        .orElse(Collections.emptyList()).stream()),
                    mergeFromProperty.getTags().getImageGenders().stream())
                .distinct().collect(Collectors.toList()));
      }
      if (mergeFromProperty.getTags().getImagePositions() != null) {
        mergeToProperty.getTags()
            .setImagePositions(Stream
                .concat(
                    (Optional.ofNullable(existingMergeToProperty.getTags().getImagePositions())
                        .orElse(Collections.emptyList()).stream()),
                    mergeFromProperty.getTags().getImagePositions().stream())
                .distinct().collect(Collectors.toList()));
      }
      if (mergeFromProperty.getTags().getImageAnimals() != null) {
        mergeToProperty.getTags()
            .setImageAnimals(Stream
                .concat(
                    (Optional.ofNullable(existingMergeToProperty.getTags().getImageAnimals())
                        .orElse(Collections.emptyList()).stream()),
                    mergeFromProperty.getTags().getImageAnimals().stream())
                .distinct().collect(Collectors.toList()));
      }
      if (mergeFromProperty.getTags().getImageMainColours() != null) {        
        mergeToProperty.getTags()
            .setImageMainColours(Stream
                .concat(
                    (Optional.ofNullable(existingMergeToProperty.getTags().getImageMainColours())
                        .orElse(Collections.emptyList()).stream()),
                    mergeFromProperty.getTags().getImageMainColours().stream())
                .distinct().limit(3).collect(Collectors.toList()));
      }
      if (mergeFromProperty.getTags().getExpertiseSitters() != null) {
        mergeToProperty.getTags()
            .setExpertiseSitters(Stream
                .concat(
                    (Optional.ofNullable(existingMergeToProperty.getTags().getExpertiseSitters())
                        .orElse(Collections.emptyList()).stream()),
                    mergeFromProperty.getTags().getExpertiseSitters().stream())
                .distinct().collect(Collectors.toList()));
      }
      if (mergeFromProperty.getTags().getOtherTags() != null) {
        mergeToProperty.getTags()
            .setOtherTags(Stream
                .concat(
                    (Optional.ofNullable(existingMergeToProperty.getTags().getOtherTags())
                        .orElse(Collections.emptyList()).stream()),
                    mergeFromProperty.getTags().getOtherTags().stream())
                .distinct().collect(Collectors.toList()));
      }
    }
    mergeFromProperty.setMergedToId(mergeToId);
    mergeFromProperty.setObjectCodes(new ArrayList<String>());
    mergeFromProperty.setStatus(Status.Merged);

    List<SaveList> listsContainingOldProperty =
        saveListService.findSavedListsContainingProperty(mergeFromProperty.getId());

    for (SaveList saveList : listsContainingOldProperty) {
      if (!saveListService.isListContainingProperty(saveList.getId(), mergeToPropertyId)) {
        saveListService.updateSavedListPropertyId(saveList.getId(), mergeFromProperty.getId(),
            mergeToPropertyId);
      } else {
        saveListService.deletePropertyFromSavedList(saveList.getId(), mergeFromProperty.getId());
      }
    }

    final Property updatedProperty =
        updatePropertyByExternalId(mergeToId, mergeToProperty, user, dropImage, true);

    propertyRepository.save(mergeFromProperty);
    patchEventsItems(mergeFromId, mergeToId, securityContext.getTokenString());
    log.info("Property successfully merged from ID=" + mergeFromId + " to ID=" + mergeToId);
    
    SnsMessageDto messageDto = new SnsMessageDto();
    messageDto.setId(mergeFromId);
    messageDto.setMergedTo(mergeToId);
    messageService.sendSnsMessage(messageDto, SnsMessageOperation.MERGE);
    
    return updatedProperty;
  }

  private void copyArchiveNumbersAndObjectCodes(String mergeToId, Property mergeToProperty,
      Property mergeFromProperty, Property existingMergeToProperty) {

    if (mergeFromProperty.getArchiveNumbers() != null
        && mergeToProperty.getArchiveNumbers() != null) {
      mergeToProperty.getArchiveNumbers().addAll(mergeFromProperty.getArchiveNumbers());
    }

    if (!CollectionUtils.isEmpty(mergeToProperty.getArchiveNumbers())) {
      mergeToProperty.setArchiveNumbers(new ArrayList<>(
          mergeToProperty.getArchiveNumbers().stream().collect(Collectors.toCollection(
              () -> new TreeSet<>(Comparator.comparing(ArchiveNumber::getArchiveNumber))))));
    }

    List<String> objectCodes = new ArrayList<>();
    objectCodes.addAll(
        !CollectionUtils.isEmpty(existingMergeToProperty.getObjectCodes()) ? existingMergeToProperty
            .getObjectCodes() : new ArrayList<>());
    objectCodes.addAll(
        !CollectionUtils.isEmpty(mergeFromProperty.getObjectCodes()) ? mergeFromProperty
            .getObjectCodes() : new ArrayList<>());

    if (!CollectionUtils.isEmpty(objectCodes)) {
      mergeToProperty.setObjectCodes(objectCodes.stream().distinct().collect(Collectors.toList()));
    }
  }

  private void patchEventsItems(String mergeFromId, String mergeToId, String token) {
    HttpHeaders headers = new HttpHeaders();
    headers.set("Authorization", "Bearer " + token);
    Map<String, String> body = Stream
        .of(Pair.of("sourceObjectId", mergeFromId), Pair.of("targetObjectId", mergeToId))
        .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    ResponseEntity<List> response = restTemplate
        .exchange(eventServiceUrl + "items", PATCH, new HttpEntity(body, headers), List.class);
    if (!response.getStatusCode().is2xxSuccessful()) {
      throw new PropertyServiceException(
          String.format("Patch of %s to %s in events' items failed", mergeFromId, mergeToId));
    }
  }

  @Override
  public List<Property> createDuplicateProperties(String externalId, Integer numberOfDuplicates,
      User user) throws NotFoundException, CannotPerformOperationException {
    if (numberOfDuplicates <= 0 || numberOfDuplicates > 30) {
      throw new CannotPerformOperationException(
          String.format("No property can be created with parameter=%s", numberOfDuplicates));
    }
    final Property existingProperty = getPropertyByExternalId(externalId);
    if (existingProperty == null) {
      throw new NotFoundException(String.format("No property found with ID=%s", externalId));
    }
    List<Property> propertiesList = new ArrayList<Property>();
    for (int i = 0; i < numberOfDuplicates; i++) {
      Property cloneProperty = cloneProperty(existingProperty);
      Property property = createProperty(cloneProperty, user);
      propertiesList.add(property);
    }
    if (StringUtils.isNotBlank(existingProperty.getImagePath())
        && existingProperty.getImagePath().contains(".jpg")) {
      for (Property property : propertiesList) {
        Pair<InputStream, Long> content = getDefaultImage(externalId);
        createDefaultImage(property.getExternalId(), content.getKey(), content.getValue(), user);
      }
    }
    return propertiesList;
  }

  /**
   * Clone Property from provided Property
   *
   * @return cloned property
   */
  private Property cloneProperty(Property existingProperty) {
    List<Artist> artists = existingProperty.getArtists();
    List<Artist> artistsList = new ArrayList<>(artists);
    List<ArchiveNumber> archiveNumbers = existingProperty.getArchiveNumbers();
    List<ArchiveNumber> archiveNumbersList = new ArrayList<>();
    for (ArchiveNumber archiveNumber : archiveNumbers) {
      ArchiveNumber addArchiveNumber = new ArchiveNumber();
      addArchiveNumber.setArchiveNumber(archiveNumber.getArchiveNumber());
      archiveNumbersList.add(addArchiveNumber);
    }

    List<PropertyCatalogueRaisonee> propertyCatalogueRaisonees =
        existingProperty.getPropertyCatalogueRaisonees();
    List<PropertyCatalogueRaisonee> propertyCatalogueRaisoneesList = new ArrayList<>();
    if (!CollectionUtils.isEmpty(propertyCatalogueRaisonees)) {
      for (PropertyCatalogueRaisonee cr : propertyCatalogueRaisonees) {
        PropertyCatalogueRaisonee addPropertyCatalogueRaisonee = new PropertyCatalogueRaisonee();
        addPropertyCatalogueRaisonee.setNumber(cr.getNumber());
        addPropertyCatalogueRaisonee.setVolume(cr.getVolume());
        if (cr.getArtistCatalogueRaisonee() != null) {
          addPropertyCatalogueRaisonee.setArtistCatalogueRaisonee(cr.getArtistCatalogueRaisonee());
        }
        propertyCatalogueRaisoneesList.add(addPropertyCatalogueRaisonee);
      }
    }

    Tags existingTags = existingProperty.getTags();
    Tags tags = new Tags();
    if (existingTags != null) {
      BeanUtils.copyProperties(existingTags, tags); // shallow copy
      tags.setExpertiseSitters(
          deepCopyList(existingTags.getExpertiseSitters(), ExpertiseSitter.class));
      tags.setOtherTags(deepCopyList(existingTags.getOtherTags(), OtherTag.class));
      tags.setImageMainColours(
          deepCopyList(existingTags.getImageMainColours(), ImageMainColour.class));
      tags.setImageAnimals(deepCopyList(existingTags.getImageAnimals(), ImageAnimal.class));
      tags.setImageGenders(deepCopyList(existingTags.getImageGenders(), ImageGender.class));
      tags.setImageSubjects(deepCopyList(existingTags.getImageSubjects(), ImageSubject.class));
      tags.setImagePositions(deepCopyList(existingTags.getImagePositions(), ImagePosition.class));
    }

    Property cloneProperty = new Property();
    cloneProperty.setArtists(artistsList);
    cloneProperty.setArchiveNumbers(archiveNumbersList);
    cloneProperty.setPropertyCatalogueRaisonees(propertyCatalogueRaisoneesList);
    cloneProperty.setCategory(existingProperty.getCategory());
    cloneProperty.setPresizeType(existingProperty.getPresizeType());
    cloneProperty.setDimensions(existingProperty.getDimensions());
    cloneProperty.setAuthenticity(existingProperty.getAuthenticity());
    cloneProperty.setEdition(new Edition());
    Edition editionObj = existingProperty.getEdition();
    if (null != editionObj) {
      cloneProperty.getEdition().setEditionSize(editionObj.getEditionSize());
      cloneProperty.getEdition().setEditionType(editionObj.getEditionType());
      cloneProperty.getEdition().setUniqueEdition(editionObj.getUniqueEdition());
    }
    cloneProperty.setResearcherNotes(existingProperty.getResearcherNotes());
    cloneProperty.setReminderNotes(existingProperty.getReminderNotes());
    cloneProperty.setDateDetails(existingProperty.getDateDetails());
    cloneProperty.setSizeText(existingProperty.getSizeText());
    cloneProperty.setFlags(existingProperty.getFlags());
    cloneProperty.setFoundry(existingProperty.getFoundry());
    cloneProperty.setMedium(existingProperty.getMedium());
    cloneProperty.setParts(existingProperty.getParts());
    cloneProperty.setSignature(existingProperty.getSignature());
    cloneProperty.setSortCode(existingProperty.getSortCode());
    cloneProperty.setTitle(existingProperty.getTitle());
    cloneProperty.setOriginalTitle(existingProperty.getOriginalTitle());
    cloneProperty.setPosthumous(existingProperty.getPosthumous());
    cloneProperty.setUploadReference(existingProperty.getUploadReference());
    cloneProperty.setSortCode(existingProperty.getSortCode());
    cloneProperty.setTags(tags);
    return cloneProperty;
  }

  @Override
  @Transactional(readOnly = true)
  public Page<Property> findPropertiesByExternalIdIn(List<String> propertyId,
      Pageable pageRequest) {
    return propertyRepository.findPropertiesByExternalIdIn(propertyId, pageRequest);

  }

  /**
   * Deep copy a list of object.
   *
   * @param list A list of objects to copy from
   * @param classType The class type of the object
   * @param <T> The object type
   * @return A deep-copy of the input list.
   * @throws PropertyServiceException When an object of the passed in class type cannot be
   * instantiated
   */
  private <T> List<T> deepCopyList(List<T> list, Class<T> classType) {
    List<T> newItems = new ArrayList<>();
    for (T item : list) {
      try {
        T newItem = classType.newInstance();
        BeanUtils.copyProperties(item, newItem);
        newItems.add(newItem);
      } catch (InstantiationException | IllegalAccessException e) {
        throw new PropertyServiceException("Failed to deep copy list", e);
      }
    }
    return newItems;
  }

  @Override
  @Transactional
  public String exportPropertyData() {
    String exportId = String.format("%s%s", PROPERTY_EXPORTER_NAME_PREFIX,
        propertyExportService.generateExportId());
    exportStatusService.saveExportStatus(exportId);
    if (propertyExportService.validateExportData(null, exportId)) {
      propertyExportService.exportData(null, exportId);
    }
    return exportId;
  }

  @Override
  @Transactional
  public Integer deleteAllProperties(List<String> propertyIds) {
    return propertyRepository.updatePropertyIds(propertyIds, Status.Deleted.toString());
  }

  @Override
  @Transactional
  @Async
  public void deletePropertyImagePath(List<String> propertyIds) {
    long threadId = Thread.currentThread().getId();
    log.info("Thread # " + threadId + " is doing this task");
    String propertyImagePath = null;
    int count = propertyIds.size();
    for (String externalId : propertyIds) {
      try {
        propertyImagePath = propertyRepository.findPropertyImagePath(externalId);
        if (StringUtils.isNotBlank(propertyImagePath)) {
          amazonS3ImageHandler.deleteFile(propertyImagePath);
        }
      } catch (Exception ex) {
        log.info(String.format(
            "Failed to delete Image '%s' from the S3 bucket", propertyImagePath), ex);
      }
      count--;
    }
    if (count == 0) {
      Thread.currentThread().interrupt();
    }
  }

}
