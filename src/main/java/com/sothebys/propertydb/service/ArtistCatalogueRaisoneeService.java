package com.sothebys.propertydb.service;

import java.util.List;

import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ArtistCatalogueRaisonee;
import com.sothebys.propertydb.model.User;

/**
 * Created by waelawada on 4/9/17.
 */
public interface ArtistCatalogueRaisoneeService {

  /**
   * find an Artist catalogue raisonee identified by {@code id}
   * 
   * @param id of the CR to be searched
   * @return Artists Catalogue Raisonn with Id {@code id}
   * @throws NotFoundException if an artist with the specified {@code artistExternalId} is not found
   */
  ArtistCatalogueRaisonee findArtistCatalogueRaisoneeById(Long id);

  /**
   * Save a list of artist catalogue raisonees {@code catalogueRaisonees} for an artist identified
   * by {@code artistExternalId}
   * 
   * @param artistExternalId
   * @param catalogueRaisonees
   * @param user User updating catalogue raisonees
   * @return a list of saved artist catalogue raisonees
   * @throws NotFoundException if an artist with the specified {@code artistExternalId} is not found
   */
  List<ArtistCatalogueRaisonee> saveArtistCataloguesRaisoneesForArtistByExternalId(
      String artistExternalId, List<ArtistCatalogueRaisonee> catalogueRaisonees, User user);

  /**
   * delete a catalogue raisonnee identified by {@code catalogueRaisoneeId} for artist identified by
   * {@code artistExternalId}
   * 
   * @param artistExternalId
   * @param catalogueRaisoneeId
   * @param user details to delete the catalogue raisonee
   * @throws NotFoundException if an artist with the specified {@code artistExternalId} is not found
   */
  void deleteCatalogueRaisoneeForArtistByExternalId(String artistExternalId,
      Long catalogueRaisoneeId, User user);

  /**
   * update a catalogue raisonnee identified by {@code catalogueRaisoneeId} for artist identified by
   * {@code artistExternalId}
   * 
   * @param artistExternalId The external ID of the artist
   * @param catalogueRaisonee of the artist to be updated
   * @param catalogueRaisoneeId The catalogueRaisoneeId of the catalogueRaisonee to be updated
   * @param user details to update catalogue raisonees
   * @return updated artist catalogue raisonees
   * @throws NotFoundException if the artist or the artist's catalogue raisonee is not found
   */
  ArtistCatalogueRaisonee updateCatalogueRaisoneeForArtistByExternalId(String artistExternalId,
      ArtistCatalogueRaisonee catalogueRaisonee, Long catalogueRaisoneeId, User user);
}
