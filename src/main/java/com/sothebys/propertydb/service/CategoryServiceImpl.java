package com.sothebys.propertydb.service;

import com.sothebys.propertydb.exception.AlreadyExistsException;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.Category;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.repository.CategoryRepositry;
import com.sothebys.propertydb.repository.PropertyRepository;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

/**
 * Category-Service:This class is used for to perform all CRUD operations on Category repository
 *
 * @author SrinivasaRao.Batthula
 */
@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {

  public static final int FIRST_LIST_INDEX = 0;

  @Autowired
  CategoryRepositry categoryRepository;
  @Autowired
  PropertyRepository propertyRepository;

  /**
   * create Category
   */
  @Override
  @Transactional
  public Category createParentCategory(Category category) {
    Category existingCategory = findByCategoryName(category.getName());
    if (existingCategory != null) {
      if (category.getId() != null && existingCategory.getId() == category.getId()) {
        log.info("Category successfully updated with ID={}", category.getId());
        return categoryRepository.save(category);
      }
      throw new AlreadyExistsException(
          String.format("Parent Category with Name '%s' already exists", category.getName()));
    }
    log.info("Category successfully created with ID={}", category.getId());
    return categoryRepository.save(category);
  }

  /**
   * create Sub Category
   */
  @Override
  @Transactional
  public Category createCategoryForParent(Long parentId, Category category) {
    Category parentCategory = findParentCategory(parentId);
    if (parentCategory != null) {
      Category childCategory =
          findByCategoryNameAndParentCategoryName(category.getName(), parentCategory.getName());
      if (childCategory != null) {
        if (category.getId() != null && category.getId() == childCategory.getId()) {
          category.setParent(parentCategory);
          log.info(String.format("Category with Id %d is successfully updated ", category.getId()));
          return categoryRepository.save(category);
        }
        throw new CannotPerformOperationException(
            String.format("category name with name '%s' already belong to parentId %d",
                category.getName(), parentId));
      } else {
        category.setParent(parentCategory);
      }
    } else {
      throw new NotFoundException(
          String.format("Parent Category with id %d doesn't exist", parentId));
    }
    log.info("Category successfully created with ID={}", category.getId());
    return categoryRepository.save(category);
  }

  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public void deleteParentCategory(Long categoryId) {
    deleteByCategoryIdAndParentId(categoryId, null);
  }

  @Transactional
  @Override
  public void deleteByCategoryIdAndParentId(Long categoryId, Long parentCategoryId) {
    if (findByCategoryIdAndParentCategoryId(categoryId, parentCategoryId) == null) {
      throw new NotFoundException((String.format(
          "Category with Id=%d with parentId=%d is not found", categoryId, parentCategoryId)));
    }
    List<Property> propertiesUsingCategory = propertyRepository.findByCategoryId(categoryId);
    if (!CollectionUtils.isEmpty(propertiesUsingCategory)) {
      throw new CannotPerformOperationException(
          String.format("Category with ID=%d is still in use by %d properties", categoryId,
              propertiesUsingCategory.size()));
    }
    List<Category> artistsUsingCategory = categoryRepository.findByArtist(categoryId);
    if (artistsUsingCategory != null && !artistsUsingCategory.isEmpty()) {
      throw new CannotPerformOperationException(
          String.format("Category with ID=%d is still in use by %d artists", categoryId,
              artistsUsingCategory.size()));
    }
    // If Category being deleted is a parent and has no more children
    if (null == parentCategoryId) {
      if (categoryRepository.findByParentId(categoryId).isEmpty()) {
        categoryRepository.deleteById(categoryId);
        log.info("Category successfully deleted with ID={}", categoryId);
      } else {
        throw new CannotPerformOperationException(
            String.format("Category with ID=%d still in use by sub-categories", categoryId));
      }
    }
    // The category is a child category with no properties attached to it
    categoryRepository.deleteById(categoryId);
  }

  @Override
  public Category findParentCategory(Long categoryId) {
    return findByCategoryIdAndParentCategoryId(categoryId, null);
  }

  @Override
  public Category findByCategoryIdAndParentCategoryId(Long categoryId, Long parentCategoryId) {
    if (parentCategoryId != null) {
      if (categoryRepository.findByParentId(parentCategoryId) == null) {
        throw new NotFoundException(
            String.format("Parent Category with Id %d doesn't exist", parentCategoryId));
      }
    }
    Category category = categoryRepository.findByIdAndParentId(categoryId, parentCategoryId);
    if (category == null) {
      if (parentCategoryId == null) {
        throw new NotFoundException(
            String.format("Category with Id %d doesn't exists", categoryId));
      }
      throw new NotFoundException(String.format(
          "Category with Id %d doesn't exist for parent with Id %d", categoryId, parentCategoryId));
    }
    return category;
  }

  @Transactional(readOnly = true)
  @Override
  public Category findByCategoryName(String categoryName) {
    return findByCategoryNameAndParentCategoryName(categoryName, null);
  }

  @Transactional(readOnly = true)
  @Override
  public Category findByCategoryNameAndParentCategoryName(String categoryName,
      String parentCategoryName) {
    if (parentCategoryName == null) {
      List<Category> categories = categoryRepository.findByNameAndParentIsNull(categoryName);
      if (categories != null && categories.size() == 1) {
        return categories.get(FIRST_LIST_INDEX);
      }
    } else {
      List<Category> categories =
          categoryRepository.findByNameAndParentName(categoryName, parentCategoryName);
      if (categories != null && categories.size() == 1) {
        return categories.get(FIRST_LIST_INDEX);
      }
    }
    return null;
  }

  /**
   * get All Categories
   */

  @Override
  @Transactional(readOnly = true)
  public List<Category> getAllCategories() {
    return categoryRepository.findAll();
  }

  @Override
  @Transactional(readOnly = true)
  public List<Category> getAllChildCategories(Long parentId) {
    return categoryRepository.findByParentId(parentId);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Category> getAllParentCategories() {
    Long parentId = null;
    List<Category> categories = categoryRepository.findByParentId(parentId);
    if(categories != null && !categories.isEmpty()) {
      categories.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }
    return categories;
  }

  @Override
  @Transactional
  public Category updateParentCategoryById(Long categoryId, Category category) {
    return updateByCategoryIdAndParentCategoryId(categoryId, null, category);
  }

  /**
   * update Category
   */
  @Override
  @Transactional
  public Category updateByCategoryIdAndParentCategoryId(Long categoryId, Long parentId,
      Category category) {
    Category currentCategory = findByCategoryIdAndParentCategoryId(categoryId, parentId);
    category.setId(currentCategory.getId());
    if (parentId == null) {
      category = createParentCategory(category);
    } else {
      category = createCategoryForParent(parentId, category);
    }
    log.info("Category with ID={} successfully updated", categoryId);
    return category;
  }
  
  @Override
  @Transactional(readOnly = true)
  public List<Category> findCategoryContainingName(String categoryName) {
    return categoryRepository.findDistinctByNameContaining(categoryName);
  }
  
  @Override
  @Transactional(readOnly = true)
  public Category getCategory(Long categoryId) {
    return categoryRepository.findOne(categoryId);
  }

  @Override
  @Transactional
  public Category mergeCategories(Long mergeToCategoryId, Long mergeFromCategoryId) {

    propertyRepository.updatePropertyCategories(mergeToCategoryId, mergeFromCategoryId);
    categoryRepository.updateArtistCategory(mergeToCategoryId, mergeFromCategoryId);
    categoryRepository.deleteArtistCategory(mergeFromCategoryId);
    categoryRepository.deleteById(mergeFromCategoryId);

    log.info("Category successfully deleted with ID={}", mergeFromCategoryId);
    return getCategory(mergeToCategoryId);
  }
 
}
