package com.sothebys.propertydb.service;

import java.util.List;

import com.sothebys.propertydb.model.OtherTag;

public interface OtherTagService {

  OtherTag findByName(String name);

	/**
	 * Creates new other tag if not presnt
	 * 
	 * @param other
	 *            tag An {@code OtherTag} instance with data to be inserted
	 * @return the instance of the created other tag
	 *
	 */
  OtherTag findByNameCreateIfNotFound(String name);

	/**
	 * Creates new other tag
	 * 
	 * @param otherTag
	 *            An {@code OtherTag} instance with data to be inserted
	 * @return the instance of the created other tag
	 *
	 */
	OtherTag createOtherTag(OtherTag otherTag);

	/**
	 * Deletes a tag
	 * 
	 * @param id
	 *
	 */
	void deleteOtherTag(Long id);

	/**
	 * Update the other tag with the data of the provided {@code OtherTag} instance.
	 *
	 * @param otherTag
	 *            An {@code OtherTag} instance with data to be updated.
	 * @return The updated {@code OtherTag} instance.
	 * @throws com.sothebys.propertydb.exception.NotFoundException
	 *             if no otherTag can be found.
	 * @throws com.sothebys.propertydb.exception.CannotPerformOperationException
	 *             if end-point is unable to acquire lock on the table to update the
	 *             property.
	 */
	OtherTag updateOtherTag(OtherTag otherTag);

	/**
	 * Search for all tags.
	 *
	 * @return list of the tag search
	 */
	List<OtherTag> findAllOtherTags();

	/**
	 * Search for tags by the provided name.
	 *
	 * @param otherTagName
	 * 
	 * @return list of the tag. Search based on the name
	 */
	List<OtherTag> findAllOtherTagsByName(String otherTagName);

}
