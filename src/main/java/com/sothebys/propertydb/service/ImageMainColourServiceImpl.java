package com.sothebys.propertydb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ImageMainColour;
import com.sothebys.propertydb.repository.ImageMainColourRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by Minhtri Tran on 11/15/17.
 */
@Service
@Slf4j
public class ImageMainColourServiceImpl implements ImageMainColourService {

  private final ImageMainColourRepository imageMainColourRepository;

  @Autowired
  public ImageMainColourServiceImpl(ImageMainColourRepository imageMainColourRepository) {
    this.imageMainColourRepository = imageMainColourRepository;
  }

  @Transactional(readOnly = true)
  @Override
  public ImageMainColour findByName(String name) {
    ImageMainColour imageMainColour = imageMainColourRepository.findByName(name);

    if (imageMainColour == null) {
      throw new NotFoundException(String.format("No image mainColour found with name=%s", name));
    }

    return imageMainColour;
  }
  
	/**
	 * {@inheritDoc}
	 */
	@Transactional(readOnly = true)
	@Override
	public List<ImageMainColour> findAllImageMainColours() {
		return imageMainColourRepository.findAllByOrderByNameAsc();
	}

  @Override
  @Transactional(readOnly = true)
  public ImageMainColour getImageMainColourById(Long imageMainColourId) {

    final ImageMainColour imageMainColour = imageMainColourRepository.findOne(imageMainColourId);
    if (imageMainColour == null) {
      throw new NotFoundException(
          String.format("No Image MainColour found with ID=%d", imageMainColourId));
    }
    return imageMainColour;
  }

  @Override
  @Transactional
  public ImageMainColour createImageMainColour(ImageMainColour imageMainColour) {
    ImageMainColour createdImageMainColour;
    try {
      createdImageMainColour = imageMainColourRepository.save(imageMainColour);
    } catch (DataIntegrityViolationException e) {
      throw new CannotPerformOperationException(
          String.format("ImageMainColour name already exists."));
    }
    return createdImageMainColour;
  }

  @Override
  @Transactional
  public void deleteImageMainColourById(Long imageMainColourId) {

    ImageMainColour imageMainColourObj;
    imageMainColourObj = imageMainColourRepository.findOne(imageMainColourId);
    if (imageMainColourObj == null) {
      throw new NotFoundException(
          String.format("No Image MainColour found with ID=%d", imageMainColourId));
    }

    imageMainColourObj = imageMainColourRepository.findByProperty(imageMainColourId);
    if (imageMainColourObj != null) {
      throw new NotFoundException(String.format("Image MainColour with ID = %d can't be deleted, "
              + "Image MainColour = %s has already assigned to an Property", imageMainColourId,
          imageMainColourObj.getName()));
    }

    imageMainColourRepository.delete(imageMainColourId);
    log.info("Image MainColour successfully deleted with ID={}", imageMainColourId);
  }

  @Override
  @Transactional
  public ImageMainColour updateImageMainColourById(Long imageMainColourId,
      ImageMainColour imageMainColour) {

    ImageMainColour existingImageMainColour = imageMainColourRepository.findOne(imageMainColourId);
    if (existingImageMainColour != null) {
      if (imageMainColourId != null && existingImageMainColour.getId() == imageMainColourId) {
        existingImageMainColour.setName(imageMainColour.getName());
        log.info("Image MainColour successfully updsted with ID={}", imageMainColourId);
        return imageMainColourRepository.save(existingImageMainColour);
      }
    }
    throw new CannotPerformOperationException(String
        .format("Image MainColour with Id '%s' does not exists,Hence Image MainColour update failed",
            imageMainColourId));
  }
  
  @Override
  @Transactional
  public ImageMainColour mergeImageMainColour(Long mergeToImageMainColourId,
      Long mergeFromImageMainColourId) {
    
    imageMainColourRepository.updatePropertyImageMainColor(mergeToImageMainColourId,
        mergeFromImageMainColourId);
    imageMainColourRepository.delete(mergeFromImageMainColourId);
    log.info("Image MainColour successfully deleted with ID={}", mergeFromImageMainColourId);
    return imageMainColourRepository.findByProperty(mergeToImageMainColourId);
  }

  
}
