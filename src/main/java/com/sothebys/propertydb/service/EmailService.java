package com.sothebys.propertydb.service;

import com.sothebys.propertydb.model.Mail;

public interface EmailService {
  
  public void sendMail(Mail mail);
}
