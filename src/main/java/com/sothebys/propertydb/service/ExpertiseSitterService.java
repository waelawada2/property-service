package com.sothebys.propertydb.service;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import com.sothebys.propertydb.model.ExpertiseSitter;

public interface ExpertiseSitterService {

  ExpertiseSitter findByName(String name);

  ExpertiseSitter findByNameCreateIfNotFound(String name);
  
  /**
	 * Creates an ExpertiseSitter
	 * 
	 * @param expertiseSitter
	 *            An {@code ExpertiseSitter} instance with data to be inserted
	 * @return the instance of the created ExpertiseSitter
	 *
	 */
	ExpertiseSitter createExpertiseSitter(ExpertiseSitter expertiseSitter);

	/**
	 * Deletes expertiseSitter
	 * 
	 * @param id
	 *
	 */
	void deleteExpertiseSitter(Long id);

	/**
	 * Update the expertiseSitter with the data of the provided {@code ExpertiseSitter} instance.
	 *
	 * @param expertiseSitter
	 *            An {@code ExpertiseSitter} instance with data to be updated.
	 * @return The updated {@code ExpertiseSitter} instance.
	 * @throws com.sothebys.propertydb.exception.NotFoundException
	 *             if no expertiseSitter can be found.
	 * @throws com.sothebys.propertydb.exception.CannotPerformOperationException
	 *             if end-point is unable to acquire lock on the table to update the
	 *             property.
	 */
	ExpertiseSitter updateExpertiseSitter(ExpertiseSitter expertiseSitter);

	ExpertiseSitter findExpertiseSitterById(Long sitterById);

	List<ExpertiseSitter> findBySearchTerm(Specification<ExpertiseSitter> specification);

	List<ExpertiseSitter> findAllExpertiseSitters();
	
	ExpertiseSitter mergeExpertiseSitter(Long mergeToExpertiseSitterId, Long mergeFromExpertiseSitterId);

}
