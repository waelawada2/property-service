package com.sothebys.propertydb.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.sothebys.propertydb.model.User;

/**
 * Created by wael.awada on 5/15/17.
 */
public interface UserService {

  /**
   * Creates a {@link User} object in the database or returns an existing one depending
   * on a lookup by the user's email address.
   *
   * @param user The user to be created.
   * @return The created user or an existing one.
   */
  User createOrFindUser(User user);

  /**
   * Finds a user by email address
   * @param email
   * @return a user {@link User} that has the email address {@code email}
   */
  User findByEmail(String email);

  /**
   * Returns a user whose id is the {@code subjectId}
   * @param keycloakUserId
   * @return a user {@link User} who has the Keycloak user ID {@code subjectId}
   */
  User findByKeycloakUserId(String keycloakUserId);

  /**
   * Returns a user whose username is the {@code username'}
   * @param username
   * @return a user {@link User} who has the username {@code username}
   */
  User findByUsername(String username);
  
  /**
   * This method finds all the users in the database.
   * 
   * @param pageRequest
   * @return a list of users {@link user}
   */
  Page<User> findAllUsers(Pageable pageRequest);
  
  /**
   * This methods searches User table based on the provided specification.
   *
   * @param spec
   * @param pageRequest
   * @return a list of users {@link user}
   */
  Page<User> findBySearchTerm(Specification<User> spec, Pageable pageRequest);
}