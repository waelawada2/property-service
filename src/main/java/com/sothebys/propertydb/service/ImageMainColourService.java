package com.sothebys.propertydb.service;

import java.util.List;

import com.sothebys.propertydb.model.ImageMainColour;

public interface ImageMainColourService {

	ImageMainColour findByName(String name);

	public List<ImageMainColour> findAllImageMainColours();

	/**
	 *  Retrieve an Image MainColour identified by {@code imageMainColourId}.
	 *
	 * @param imageMainColourId
	 * @return
	 */
	ImageMainColour getImageMainColourById(Long imageMainColourId);

	/**
	 * Creates a new Image MainColour.
	 *
	 * @param imageMainColour
	 * @return Created {@code ImageMainColour} instance.
	 */
	ImageMainColour createImageMainColour(ImageMainColour imageMainColour);

	/**
	 * Deletes an ImageMainColour identified by {@code imageMainColourId}.
	 *
	 * @param imageMainColourId of a ImageMainColour.
	 */
	void deleteImageMainColourById(Long imageMainColourId);

	/**
	 * Updates an ImageMainColour.
	 *
	 * @param imageMainColourId
	 * @param imageMainColour
	 * @return Updated {@code ImageMainColour} instance.
	 */
	ImageMainColour updateImageMainColourById(Long imageMainColourId, ImageMainColour imageMainColour);
	
	/**
	   * Return merged ImageMainColour object.
	   * 
	   * @param mergeToImageMainColourId
	   * @param mergeFromImageMainColourId
	   * @return ImageMainColour object
	   */
	ImageMainColour mergeImageMainColour(Long mergeToImageMainColourId, Long mergeFromImageMainColourId);

}
