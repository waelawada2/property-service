package com.sothebys.propertydb.service;

import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.ArtistCatalogueRaisonee;
import com.sothebys.propertydb.model.User;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

/**
 * Service that provides operations for artist management.
 *
 * @author Nagarajutalluri
 * @author Gregor Zurowski
 */
public interface ArtistService {

  /**
   * Creates a new artist
   * 
   * @param artist An {@code Artist} instance with data to be inserted
   * @param user A {@code User} that refers to the user updating the artist
   * @param validArtist if the value true then create artist otherwise display warn message
   * @return the instance of the created artist
   * @throws com.sothebys.propertydb.exception.CannotPerformOperationException if end-point is
   *         unable to acquire lock on the table to update the property provided external ID.It also
   *         throws the same if the artist ulan-id already exists
   */
  Artist createArtist(Artist artist, User user, boolean validArtist);

  /**
   * Delete artist identified by the provided {@code artistId}.
   *
   * @param artistId The ID of the artist.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no artist can be found with the
   *         provided ID.
   */
  void deleteArtist(Long artistId);

  /**
   * delete artist by using externalId
   *
   * @param externalId of the artist to be deleted
   */
  void deleteArtist(String externalId);

  /**
   * Retrieve all artists.
   *
   * @return A list of {@code Artist} instances.
   */
  List<Artist> findAllArtists();

  /**
   * Retrieve artist identified by the provided {@code externalId}.
   *
   * @param externalId of the artist.
   * @return An {@code Artist} instance if artist was found, otherwise {@code null}.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no artist can be found with the
   *         provided ID
   */
  Artist findArtistByExternalId(String externalId);

  /**
   * Retrieve artist identified by the provided {@code ulanId}.
   *
   * @param ulanId of the artist.
   * @return An {@code Artist} instance if artist was found, otherwise {@code null}.
   */
  Artist findArtistByUlanId(Long ulanId);

  /**
   * Retrieve artist identified by the provided {@code artistId}.
   *
   * @param artistId The ID of the artist.
   * @return An {@code Artist} instance if artist was found, otherwise {@code null}.
   */
  Artist findArtistById(Long artistId);

  /**
   * Search for artists by the provided spec.
   *
   * @param spec refers to specification criteria for search
   * @param pageRequest refers to the size/sort/page number for the result to be returned
   * @return list of the artist search based on the spec
   */
  Page<Artist> findBySearchTerm(Specification<Artist> spec, Pageable pageRequest);

  /**
   * Update an artist with the data of the provided {@code Artist} instance. The artist is identifed
   * by the internal ID.
   *
   * @param artistId The ID of the artist that should be updated.
   * @param user The user updating the artist
   * @param artist An {@code Artist} instance with data to be updated.
   * @return The updated {@code Artist} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no artist can be found with the
   *         provided ID.
   */
  Artist updateArtist(Long artistId, Artist artist, User user);

  /**
   * Update an artist with the data of the provided {@code Artist} instance. The artist is
   * identified by the external ID.
   *
   * @param externalId The external ID of the artist.
   * @param artist An {@code Artist} instance with data to be updated.
   * @param user A {@code User} that refers to the user updating the artist
   * @return The updated {@code Artist} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no artist can be found with the
   *         provided external ID.
   * @throws com.sothebys.propertydb.exception.CannotPerformOperationException if end-point is
   *         unable to acquire lock on the table to update the property provided external ID.
   */
  Artist updateArtistByExternalId(String externalId, Artist artist, User user);

  /**
   * Retrieve all artists.
   * 
   * @param pageRequest refers to the size/sort/page number for the result to be returned
   * @return all the artists
   */
  Page<Artist> findAllArtists(Pageable pageRequest);

  /**
   * Set the artist catalogue raisonees for the artist with an internal id {@code artistId} to be
   * the list {@code catalogueRaisonees}
   * 
   * @param artistId external Id of the artist
   * @param catalogueRaisonees refers to the list of the catalogue raisonees
   * @return
   */
  Artist setArtistCatalogueRaisoneeList(Long artistId,
      final List<ArtistCatalogueRaisonee> catalogueRaisonees);
  
  /**
   * Retrieve Artist details in an output.
   *
   * @return exportId updated artist pollExportID.
   */
  String exportArtistData();
  
}
