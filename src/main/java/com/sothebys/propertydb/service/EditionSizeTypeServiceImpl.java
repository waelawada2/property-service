package com.sothebys.propertydb.service;

import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.repository.PropertyRepository;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.Edition;
import com.sothebys.propertydb.model.EditionSize;
import com.sothebys.propertydb.model.EditionSizeType;
import com.sothebys.propertydb.repository.EditionSizeTypeRepository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;


/**
 * @author SrinivasaRao.Batthula
 *
 */
@Slf4j
@Service
public class EditionSizeTypeServiceImpl implements EditionSizeTypeService {

  @Autowired
  EditionSizeTypeRepository editionSizeTypeRepository;

  @Autowired
  private PropertyRepository propertyRepository;

  @Override
  @Transactional(readOnly = true)
  public List<EditionSizeType> findAllEditionSizeTypes() {
    return editionSizeTypeRepository.findAllByOrderByIdAsc();
  }

  @Override
  @Transactional(readOnly = true)
  public EditionSizeType findEditionSizeTypeByName(String editionSizeType) {
    return editionSizeTypeRepository.findByName(editionSizeType);
  }

  @Override
  @Transactional
  public EditionSizeType createEditionSizeType(EditionSizeType editionSizeType) {
    return editionSizeTypeRepository.save(editionSizeType);
  }

  @Override
  @Transactional(readOnly = true)
  public EditionSizeType findEditionSizeTypeById(Long editionSizeTypesId) {
    final EditionSizeType editionSizeType = editionSizeTypeRepository.findOne(editionSizeTypesId);
    if (editionSizeType == null) {
      throw new NotFoundException(
          String.format("No edition size type found with ID=%d", editionSizeTypesId));
    }
    return editionSizeType;
  }

  @Override
  @Transactional
  public void deleteEditionSizeType(Long editionSizeTypeId) {

    List<Property> propertiesUsingEditionSizeType = propertyRepository
        .findByEditionEditionSizeTypeId(editionSizeTypeId);
    if (!CollectionUtils.isEmpty(propertiesUsingEditionSizeType)) {
      throw new CannotPerformOperationException(
          String.format("EditionSizeType with ID=%d is still in use by %d properties",
              editionSizeTypeId,
              propertiesUsingEditionSizeType.size()));
    }

    editionSizeTypeRepository.delete(editionSizeTypeId);
    log.info("Edition size type successfully deleted with ID={}", editionSizeTypeId);
  }

  @Override
  @Transactional
  public EditionSizeType updateEditionSizeType(Long editionSizeTypeId,
      EditionSizeType editionSizeType) {
    Objects.requireNonNull(editionSizeTypeId,
        "editionSizeTypeId is null, Hence editionSizeType update failed");
    EditionSizeType existingEditionSizeType = editionSizeTypeRepository.findOne(editionSizeTypeId);
    if (existingEditionSizeType == null || existingEditionSizeType.getId() != editionSizeTypeId) {
      throw new NotFoundException(String.format(
          "EditionSizeType with Id '%s' does not exists, Hence editionSizeType update failed",
          editionSizeTypeId));
    }
    existingEditionSizeType.setName(editionSizeType.getName());
    log.info("EditionSizeType successfully updated with ID={}", editionSizeTypeId);
    return editionSizeTypeRepository.save(existingEditionSizeType);
  }
  
  
  /**
   * This method used to calculate edition size
   * 
   * @param edition
   * @return 
   */
  public void calculateEditionSize(Edition edition){
    int editionSizeTotal = 0;
     if(null!=edition){
      EditionSize editionSize = edition.getEditionSize();
      if (null != editionSize) {
        editionSizeTotal = calculateEditionSizeTotal(editionSize);
        editionSize.setEditionSizeTotal(editionSizeTotal);
      }
     }
  }
  
  
  /**
   * This method used to calculate edition size total value
   * 
   * @param editionSize
   * @return int
   */
    private int calculateEditionSizeTotal(final EditionSize editionSize) {

    int editionSizeTotal = 0;
 
      editionSizeTotal =
          applyPlusOneRule(editionSize.getArtistProofTotal());
   
      editionSizeTotal = editionSizeTotal
          + applyPlusOneRule(editionSize.getTrialProofTotal());
  
      editionSizeTotal = editionSizeTotal
          + applyPlusOneRule(editionSize.getBonATirerTotal());

      editionSizeTotal = editionSizeTotal
          + applyPlusOneRule(editionSize.getPrintersProofTotal());
    
      editionSizeTotal = editionSizeTotal
          + applyPlusOneRule(editionSize.getHorsCommerceProofTotal());

      editionSizeTotal =
          editionSizeTotal + applyPlusOneRule(editionSize.getSeriesTotal());
      
      editionSizeTotal =
          editionSizeTotal + applyPlusOneRule(editionSize.getHorsCommerceProofTotal());
      
      editionSizeTotal =
          editionSizeTotal + applyPlusOneRule(editionSize.getMuseumProofTotal());
    
    return editionSizeTotal;
  }
  /**
   * This method is used verify given int value as -1 then apply plus one otherwise returns actual
   * int value value
   *
   * @param editionSize
   * @return int
   */
  private int applyPlusOneRule(int editionSize) {
    return (editionSize == -1) ? 1 : editionSize;
  }
  
  @Override
  @Transactional
  public EditionSizeType mergeEditionSizeType(Long mergeToEditionSizeTypeId,
      Long mergeFromEditionSizeTypeId) {
    propertyRepository.updatePropertyEditionSizeTypes(mergeToEditionSizeTypeId,
        mergeFromEditionSizeTypeId);
    editionSizeTypeRepository.delete(mergeFromEditionSizeTypeId);
    log.info("PresizeType successfully deleted with ID={}", mergeFromEditionSizeTypeId);
    return findEditionSizeTypeById(mergeToEditionSizeTypeId);
  }

}
