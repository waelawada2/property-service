package com.sothebys.propertydb.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import com.sothebys.propertydb.model.SaveSearch;

/**
 * Service that provides operations for SaveSearch management.
 */
public interface SaveSearchService {

  /**
   * Search for saveSearches by the provided spec.
   *
   * @param spec
   * @param pageRequest
   * @return
   */
  Page<SaveSearch> findBySearchTerm(Specification<SaveSearch> spec, Pageable pageRequest);

  /**
   * Retrieve all save Searches based on userId.
   * 
   * @param pageRequest
   * @return Page<SaveSearch>
   */
  Page<SaveSearch> findSaveSearchesByUserId(Pageable pageRequest, long userId);

  /**
   * Retrieve SaveSearch details based on userName and externalId
   * 
   * @return {@code SaveSearch} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no save search can be found with
   *         the provided userName.
   */
  SaveSearch findSaveSearchesByExternalId(String userName, String externalId);
  
  /**
   * Retrieve SaveSearch details based on externalId
   * 
   * @return {@code SaveSearch} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no save search can be found with
   *         the provided external Id.
   */
  SaveSearch findSaveSearchesByExternalId(String externalId);


  /**
   * Retrieve User details based on userName
   *
   * @param userName The Name of the User.
   * @return The created {@code SaveSearch} instance.
   * @return The id {@code User} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no user can be found with the
   *         provided userName.
   */
  long findUserId(String userName);

  /**
   * Create new SaveSearch with the data of the provided {@code SaveSearch} instance.
   *
   * @param saveSearch The ID of the saveSearch that should be created if saveSearch doesn't exist.
   * @return The created {@code SaveSearch} instance.
   *
   */
  SaveSearch createSaveSearch(SaveSearch saveSearch, long userId);

  /**
   * Deletes a saved list of the user on userId and externalId
   *
   * @throws com.sothebys.propertydb.exception.NotFoundException if no save search can be found with
   *         the provided userId and externalId.
   */
  void deleteByUserIdAndExternalId(long userId, String externalId);

  /**
   * Update an SaveSearch with the data of the provided {@code SaveSearch} instance. The saveSearch
   * is identified by the internal externalId and userId.
   *
   * @param externalId The ID of the SaveSearch that should be updated.
   * @param userId The ID of the SaveSearch that should be updated.
   * @param saveSearch An {@code SaveSearch} instance with data to be updated.
   * @return The updated {@code SaveSearch} instance.
   * @throws com.sothebys.propertydb.exception.NotFoundException if no saveSearch can be found with
   *         the provided externalId and userId.
   */
  SaveSearch updateByUserIdAndExternalId(long userId, String externalId, SaveSearch saveSearch);

}
