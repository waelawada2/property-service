package com.sothebys.propertydb.service;

import org.springframework.stereotype.Service;

import com.sothebys.propertydb.model.Orientation;
import com.sothebys.propertydb.model.Scale;


/**
 * @author aneeshal
 *
 */
@Service
public class AutomatedTagServiceImpl implements AutomatedTagService {
	
	@Override
	public Orientation[] getAllOrientation() {
		return Orientation.values();
	}

	@Override
	public Scale[] getAllScales() {
		return Scale.values();
	}
	
}
