package com.sothebys.propertydb.service;

import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ImageGender;
import com.sothebys.propertydb.repository.ImageGenderRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Minhtri Tran on 11/15/17.
 */
@Service
public class ImageGenderServiceImpl implements ImageGenderService {

  @Autowired
  private ImageGenderRepository imageGenderRepository;

  @Transactional(readOnly = true)
  @Override
  public ImageGender findByName(String name) {
    ImageGender imageGender = imageGenderRepository.findByName(name);

    if (imageGender == null) {
      throw new NotFoundException(String.format("No image gender found with name=%s", name));
    }

    return imageGender;
  }
	/**
	 * {@inheritDoc}
	 */
	@Transactional(readOnly = true)
	@Override
	public List<ImageGender> findAllImageGenders() {
		return imageGenderRepository.findAllByOrderByNameAsc();
	}
}
