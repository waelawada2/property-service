package com.sothebys.propertydb.service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ExpertiseSitter;
import com.sothebys.propertydb.repository.ExpertiseSitterRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by Minhtri Tran on 11/15/17.
 */
@Slf4j
@Service
public class ExpertiseSitterServiceImpl implements ExpertiseSitterService {

  private ExpertiseSitterRepository expertiseSitterRepository;

  @Autowired
  public ExpertiseSitterServiceImpl(ExpertiseSitterRepository expertiseSitterRepository) {
    this.expertiseSitterRepository = expertiseSitterRepository;
  }

  @Transactional(readOnly = true)
  @Override
  public ExpertiseSitter findByName(String name) {
		Objects.requireNonNull(name, "Name parameter must not be null");
		String[] nameArray = name.split("\\s");
		String firstName = nameArray[0];
		String lastName = (nameArray.length > 1
				? StringUtils.join(Arrays.copyOfRange(nameArray, 1, nameArray.length), " ")
				: null);
		ExpertiseSitter expertiseSitter = expertiseSitterRepository.findByLastNameAndFirstNameIn(lastName, firstName);

    return expertiseSitter;
  }

  @Override
  public ExpertiseSitter findByNameCreateIfNotFound(String name) {
		Objects.requireNonNull(name, "Name parameter must not be null");
		String[] nameArray = name.split("\\s");
		String firstName = nameArray[0];
		String lastName = (nameArray.length > 1 ? StringUtils.join(Arrays.copyOfRange(nameArray, 1, nameArray.length), " ") : null);
		ExpertiseSitter expertiseSitter = expertiseSitterRepository.findByLastNameAndFirstNameIn(lastName, firstName);

    if (expertiseSitter == null) {
      ExpertiseSitter newExpertiseSitter = new ExpertiseSitter();
			newExpertiseSitter.setFirstName(firstName);
			newExpertiseSitter.setLastName(lastName);
      expertiseSitter = expertiseSitterRepository.save(newExpertiseSitter);
    }

    return expertiseSitter;
  }
  
  /**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public ExpertiseSitter createExpertiseSitter(final ExpertiseSitter expertiseSitter) {

		ExpertiseSitter createdSitter = null;
	    try {
	        
	        if (StringUtils.isNotBlank(expertiseSitter.getFirstName())
	            || StringUtils.isNotBlank(expertiseSitter.getLastName())) {
	          List<ExpertiseSitter> sitterList = expertiseSitterRepository
	              .findDistinctExpertiseSitterByLastNameAndFirstName(
	            		  expertiseSitter.getLastName(), expertiseSitter.getFirstName());
	          if (!CollectionUtils.isEmpty(sitterList)) {
	            throw new CannotPerformOperationException(
	                "An Expertise Sitter already exists with the provided values.");
	          }
	        createdSitter = expertiseSitterRepository.save(expertiseSitter);
	          log.info("Expertise Sitter successfully created with ID={}", createdSitter.getId());
	        }else {
	        	throw new CannotPerformOperationException(String.format(
	    	            "Unable to insert Expertise Sitter. First Name & Last Name is empty."));
	        }

	      } catch (DataIntegrityViolationException e) {
	        throw new CannotPerformOperationException(String.format(
	            "Unable to insert Expertise Sitter. Specified Expertise Sitter %s already exist", expertiseSitter.getFirstName() + " " + expertiseSitter.getLastName()));
	      } catch (CannotAcquireLockException e) {
	        throw new CannotPerformOperationException(
	            "Lock couldn't be acquired on table, please try again later");

	}
	      return createdSitter;

	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void deleteExpertiseSitter(Long id) {
		Objects.requireNonNull(id, "id parameter must not be null");

		ExpertiseSitter expertiseSitter = expertiseSitterRepository.findById(id);
		if (expertiseSitter == null) {
			throw new NotFoundException(String.format("ExpertiseSitter object with Id %s doesn't exist", id));
		}
		int usedExpertiseSittersCount = expertiseSitterRepository.countExpertiseSitters(id);
		if (usedExpertiseSittersCount > 0) {
			throw new CannotPerformOperationException(
					"The expertiseSitter object cannot be deleted as it is used by other objects");
		}
		try {
			expertiseSitterRepository.delete(expertiseSitter.getId());
		} catch (CannotAcquireLockException e) {
			throw new CannotPerformOperationException("Lock couldn't be acquired on table, please try again later");
		}
		log.info("Expertise Sitter '{}' is sucessfully deleted", expertiseSitter.getFirstName() + " " + expertiseSitter.getLastName());
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public ExpertiseSitter updateExpertiseSitter(ExpertiseSitter expertiseSitter) {
		
		Objects.requireNonNull(expertiseSitter.getId(), "ExpertiseSitter id must not be null");
		ExpertiseSitter currentSitter = findExpertiseSitterById(expertiseSitter.getId());
	    if (currentSitter == null) {
	      throw new NotFoundException(String.format("No Sitter found with ID=%d", expertiseSitter.getId()));
		}

		if (StringUtils.isNotBlank(expertiseSitter.getFirstName())
				|| StringUtils.isNotBlank(expertiseSitter.getLastName())) {
			List<ExpertiseSitter> sitterList = expertiseSitterRepository
					.findDistinctExpertiseSitterByLastNameAndFirstName(expertiseSitter.getLastName(),
							expertiseSitter.getFirstName());
			if (!CollectionUtils.isEmpty(sitterList)) {
			throw new CannotPerformOperationException(
						"An Expertise Sitter already exists with the provided values.");
			}
		} else {
			throw new CannotPerformOperationException(
					String.format("Unable to update Expertise Sitter. First Name & Last Name is empty."));
		}

	    currentSitter.setLastName(expertiseSitter.getLastName());
	    currentSitter.setFirstName(expertiseSitter.getFirstName());

		try {
	    	currentSitter = expertiseSitterRepository.save(currentSitter);
	      log.info("ExpertiseSitter with ID={} successfully updated", expertiseSitter.getId());
		} catch (CannotAcquireLockException e) {
			throw new CannotPerformOperationException("Lock couldn't be acquired on table, please try again later");
	    }
	    return currentSitter;
		
	}
	
	  /**
	   * {@inheritDoc}
	   */
	  @Transactional(readOnly = true)
	  @Override
	  public ExpertiseSitter findExpertiseSitterById(Long sitterById) {
	    return expertiseSitterRepository.findOne(sitterById);
		}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(readOnly = true)
	@Override
	public List<ExpertiseSitter> findBySearchTerm(Specification<ExpertiseSitter> specification) {
		return expertiseSitterRepository.findAll(specification);
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(readOnly = true)
	@Override
	public List<ExpertiseSitter> findAllExpertiseSitters() {
		List<ExpertiseSitter> sitterList = expertiseSitterRepository
				.findAll();
		return sitterList;
	}
	
	@Override
    @Transactional
    public ExpertiseSitter mergeExpertiseSitter(Long mergeToExpertiseSitterId, Long mergeFromExpertiseSitterId) {
	  expertiseSitterRepository.updatePropertyExpertiseSitter(mergeToExpertiseSitterId, mergeFromExpertiseSitterId);
	  deleteExpertiseSitter(mergeFromExpertiseSitterId);

      log.info("Expertise Sitter successfully deleted with ID={}", mergeFromExpertiseSitterId);
      return findExpertiseSitterById(mergeToExpertiseSitterId);
    }
	
}
