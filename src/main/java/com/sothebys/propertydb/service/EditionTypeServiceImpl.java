package com.sothebys.propertydb.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.EditionType;
import com.sothebys.propertydb.repository.EditionTypeRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author SrinivasaRao.Batthula
 *
 */
@Slf4j
@Service
public class EditionTypeServiceImpl implements EditionTypeService {

  @Autowired
  private EditionTypeRepository editionTypeRepository;

  @Override
  @Transactional
  public EditionType createEditionType(EditionType editionType) {
    Long maxSortCode = editionTypeRepository.getMaxSortCode();
    editionType.setSortCode(maxSortCode);
    return editionTypeRepository.save(editionType);
  }

  @Transactional
  @Override
  public void deleteEditionType(Long editionTypeId) {
    EditionType editionType = getEditionTypeById(editionTypeId);
    if (editionType == null) {
      throw new NotFoundException(String.format("No edition type found with ID=%d", editionTypeId));
    }
    editionTypeRepository.delete(editionTypeId);
    log.info("Edition type successfully deleted with ID={}", editionTypeId);
  }

  @Override
  @Transactional(readOnly = true)
  public List<EditionType> getAllEditionTypes() {
    return editionTypeRepository.findAllByOrderBySortCodeAsc();
  }

  @Override
  @Transactional(readOnly = true)
  public EditionType getEditionTypeByName(String editionType) {
    return editionTypeRepository.findByName(editionType);
  }

  @Override
  @Transactional(readOnly = true)
  public EditionType getEditionTypeById(Long editionTypesId) {
    return editionTypeRepository.findOne(editionTypesId);
  }

  @Override
  @Transactional
  public EditionType updateEditionType(Long editionTypeId, EditionType editionType) {
    Objects.requireNonNull(editionTypeId, "editionTypeId is null, Hence editionType update failed");
    EditionType existingEditionType = editionTypeRepository.findOne(editionTypeId);
    if (existingEditionType == null || existingEditionType.getId() != editionTypeId) {
      throw new NotFoundException(
          String.format("EditionType with Id '%s' does not exists, Hence editionType update failed",
              editionTypeId));
    }
    existingEditionType.setName(editionType.getName());
    existingEditionType.setDescription(editionType.getDescription());
    existingEditionType.setSortCode(editionType.getSortCode());
    log.info("EditionType successfully updated with ID={}", editionTypeId);
    return editionTypeRepository.save(existingEditionType);
  }
}
