package com.sothebys.propertydb.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.ArtistCatalogueRaisonee;
import com.sothebys.propertydb.model.PropertyCatalogueRaisonee;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.repository.ArtistCatalogueRaisoneeRepository;
import com.sothebys.propertydb.search.Status;

/**
 * Created by waelawada on 3/26/17.
 */
@Service
public class ArtistCatalogueRaisoneeServiceImpl implements ArtistCatalogueRaisoneeService {

  @Autowired
  ArtistCatalogueRaisoneeRepository artistCatalogueRaisoneeRepository;

  @Autowired
  ArtistService artistService;

  /**
   * {@inheritDoc}
   */
  public ArtistCatalogueRaisonee findArtistCatalogueRaisoneeById(Long id) {
    return artistCatalogueRaisoneeRepository.findArtistCatalogueRaisoneesById(id);

  }

  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public List<ArtistCatalogueRaisonee> saveArtistCataloguesRaisoneesForArtistByExternalId(
      String artistExternalId, List<ArtistCatalogueRaisonee> catalogueRaisonees, User user) {
    Artist artist = artistService.findArtistByExternalId(artistExternalId);
    if (artist == null) {
      throw new NotFoundException(String.format("No artist found with ID=%s", artistExternalId));
    }

    List<ArtistCatalogueRaisonee> returnedArtistCatalogueRaisonees = new ArrayList<>();
    for (ArtistCatalogueRaisonee artistCatalogueRaisonee : catalogueRaisonees) {
      ArtistCatalogueRaisonee savedArtistCatalogueRaisonee =
          artistCatalogueRaisoneeRepository.save(artistCatalogueRaisonee);
      returnedArtistCatalogueRaisonees.add(savedArtistCatalogueRaisonee);
    }

    artist.getArtistCatalogueRaisonees().addAll(returnedArtistCatalogueRaisonees);
    artistService.updateArtist(artist.getId(), artist, user);

    return returnedArtistCatalogueRaisonees;
  }

  @Transactional
  private void deleteCatalogueRaisoneeForArtistById(Long artistId, Long catalogueRaisoneeId,
      User user) {
    boolean found = false;

    final Artist artist = artistService.findArtistById(artistId);
    if (artist == null) {
      throw new NotFoundException(String.format("No artist found with ID=%d", artistId));
    }

    final List<ArtistCatalogueRaisonee> artistCatalogueRaisoneesList =
        artist.getArtistCatalogueRaisonees();
    for (ArtistCatalogueRaisonee artistCatalogueRaisonee : artistCatalogueRaisoneesList) {
      if (Long.compare(catalogueRaisoneeId, artistCatalogueRaisonee.getId()) == 0) {
        List<PropertyCatalogueRaisonee> propertyCatalogueRaisoneeList =
            artistCatalogueRaisonee.getPropertyCatalogueRaisonees();
        if (!CollectionUtils.isEmpty(propertyCatalogueRaisoneeList)) {
          throw new CannotPerformOperationException(String.format(
              "Catalogue raisonee ID=%d cannot be deleted as it is referred by property catalogue raisonees",
              catalogueRaisoneeId));
        }
      artistCatalogueRaisonee.setStatus(Status.Deleted);
      artistCatalogueRaisoneeRepository.save(artistCatalogueRaisonee);
      found = true;
      break;
    }
    }

    if (found) {
      artistService.updateArtist(artist.getId(), artist, user);
    } else {
      throw new NotFoundException(
          String.format("No catalog raisonee found with ID=%d", catalogueRaisoneeId));
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void deleteCatalogueRaisoneeForArtistByExternalId(String artistExternalId,
      Long catalogueRaisoneeId, User user) {

    final Artist artist = artistService.findArtistByExternalId(artistExternalId);
    if (artist == null) {
      throw new NotFoundException(String.format("No artist found with ID=%s", artistExternalId));
    }
    deleteCatalogueRaisoneeForArtistById(artist.getId(), catalogueRaisoneeId, user);
  }

  @Override
  public ArtistCatalogueRaisonee updateCatalogueRaisoneeForArtistByExternalId(
      String artistExternalId, ArtistCatalogueRaisonee updatedCatalogueRaisoneeObj,
      Long catalogueRaisoneeId, User user) {

    final Artist artist = artistService.findArtistByExternalId(artistExternalId);
    if (artist == null) {
      throw new NotFoundException(String.format("No artist found with ID=%s", artistExternalId));
    }
    ArtistCatalogueRaisonee existingCatalogueRaisoneeObj =
        artistCatalogueRaisoneeRepository.findArtistCatalogueRaisoneesById(catalogueRaisoneeId);
    if (existingCatalogueRaisoneeObj == null) {
      throw new NotFoundException(
          String.format("No artist catalogue raisonee found with ID=%s", catalogueRaisoneeId));
    }
    updatedCatalogueRaisoneeObj.setId(existingCatalogueRaisoneeObj.getId());
    updatedCatalogueRaisoneeObj.setSortId(existingCatalogueRaisoneeObj.getSortId());
    updatedCatalogueRaisoneeObj.setStatus(Status.Active);
    ArtistCatalogueRaisonee updatedCatalogueRaisonee =
        artistCatalogueRaisoneeRepository.save(updatedCatalogueRaisoneeObj);
    artistService.updateArtist(artist.getId(), artist, user);
    return updatedCatalogueRaisonee;
  }

}
