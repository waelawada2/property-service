package com.sothebys.propertydb.service;

import com.sothebys.propertydb.constants.PropertyServiceConstants;
import com.sothebys.propertydb.dto.PropertyDTO;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.mapper.PropertyModelMapper;
import com.sothebys.propertydb.model.Mail;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.repository.PropertyRepository;
import com.sothebys.propertydb.search.PollStatus;
import com.sothebys.propertydb.util.AmazonS3FileHandler;
import com.sothebys.propertydb.util.ExportData;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author SrinivasaRao.Batthula
 */

@Slf4j
@Qualifier("savedListExportService")
@Service
public class SavedListExportServiceImpl implements ExportService {

  @Autowired
  private PropertyRepository propertyRepository;

  @Autowired
  @Qualifier("exportDataS3Client")
  private AmazonS3FileHandler amazonS3ExportDataHandler;

  @Autowired
  private ExportData exportData;

  @Autowired
  private ExportStatusService exportStatusService;

  @Autowired
  private PropertyModelMapper propertyModelMapper;

  @Value("${exportData.max-count:20000}")
  private long maxExportDataCount;
  
  @Value("${spring.mail.from}")
  private String fromAddress;

  @Value("${spring.mail.content-type}")
  private String contentType;

  @Value("${aws.s3.exports.aws-server-name}")
  private String awsServerName;

  @Value("${aws.s3.exports.bucket}")
  private String bucketName;

  @Value("${spring.mail.to}")
  private String toAddress;

  @Value("${spring.mail.subject}")
  private String subject;

  @Autowired
  private ExportData buildExportData;

  @Autowired
  private EmailService emailService;  
  
  @Override
  @Transactional
  @Async
  public void exportData(Specification<Property> spec,
      String exportId) {
    log.info("Start Processing for the export ID='{}'", exportId);
    PollStatus status = PollStatus.InProgress;

    String exportCSVFileName =
        String.format("%s%s", exportId, PropertyServiceConstants.CSV_EXTENSION);
    List<PropertyDTO> properties = getPropertiesBySavedList(spec);
    try {
      log.debug("Building CSV Document");
      exportData.buildPropertyCsvDocument(properties, exportCSVFileName);
      log.debug("Building CSV Document Complete");
      amazonS3ExportDataHandler.uploadExportDataFileIntoS3(exportCSVFileName);
      status = PollStatus.Completed;
    } catch (Exception e) {
      log.error("SavedList csv file='{}' export failed", exportCSVFileName);
      status = PollStatus.Error;
    }
    log.debug("Export %s is complete", exportId);
    exportStatusService.updateExportStatus(exportId, status);

  }

  @Transactional(readOnly = true)
  public List<PropertyDTO> getPropertiesBySavedList(Specification<Property> spec) {
    List<PropertyDTO> propertyList = new ArrayList<PropertyDTO>();
    List<Property> properties = propertyRepository.findAll(spec);
    log.debug("Got a total of %d properties ", properties.size());
    log.debug("Mapping to DTOs");
    propertyList =
        propertyModelMapper.mapToDTO(properties);
    log.debug("Mapping to DTOs complete");
    return propertyList;
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(readOnly = true)
  @Override
  public PollStatus findExportStatus(String exportId) {
    return exportStatusService.findExportStatus(exportId);
  }

  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public String generateExportId() {
    return UUID.randomUUID().toString();
  }
  
  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public boolean validateExportData(Specification<Property> spec, String exportId) {
	  log.info("Validating the count of Saved List data");
	  long savedListCount = propertyRepository.count(spec);
	  log.info("Saved List count = '{}'", Long.toString(savedListCount));
	  if(savedListCount > maxExportDataCount) {
		  PollStatus status = PollStatus.LimitExceeded;
		  exportStatusService.updateExportStatus(exportId, status);
		  return false;
	  }
	  return true;
  }
  
  @Override
  @Transactional
  @Async
  public void exportByEmail(List<String> propertyIds, String name) {
    String date = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
    String exportCSVFileName =
        new StringBuilder(subject).append(PropertyServiceConstants.UNDERSCORE).append(name)
            .append(PropertyServiceConstants.UNDERSCORE)
            .append(new SimpleDateFormat("dd-MMM-yyyy").format(new Date()))
            .append(PropertyServiceConstants.CSV_EXTENSION).toString();
    try {

      List<Property> propertiesList = propertyRepository.findPropertiesByExternalIdIn(propertyIds);
      List<PropertyDTO> propertyDTOList = propertyModelMapper.mapToDTO(propertiesList);
      propertyDTOList.stream().forEach(propertyDTO -> {
        propertyDTO.setIsPropertyDeleted(true);
      });

      buildExportData.buildPropertyCsvDocument(propertyDTOList, exportCSVFileName);
      amazonS3ExportDataHandler.uploadExportDataFileIntoS3(exportCSVFileName);

      Mail mail = new Mail();
      StringBuilder emailBody =
          new StringBuilder("You have successfully deleted all objects within Saved List, ");
      emailBody.append(name).append("<br/> Click <a href=")
          .append(constructErrorFileUploadUrlForEmail(exportCSVFileName))
          .append(">here</a> to see your deleted objects.");
      emailBody.append("<br/><br/> Thanks, <br/> EOS Team");
      mail.setFrom(fromAddress);
      mail.setTo(toAddress);
      mail.setSubject(subject + PropertyServiceConstants.UNDERSCORE + name
          + PropertyServiceConstants.UNDERSCORE + date);
      mail.setContent(emailBody.toString());
      mail.setContentType(contentType);
      log.info(String.format("Deleted Savelist property an email body='%s'", emailBody.toString()));
      emailService.sendMail(mail);
    } catch (Exception ex) {
      log.error(String.format("Failed to send an email for the Deleted Saved List - %s", name));
    }
  }

  private String constructErrorFileUploadUrlForEmail(final String fileName) {
    String fileUrl = null;
    try {
      fileUrl = awsServerName + PropertyServiceConstants.BACK_SLASH + bucketName
          + PropertyServiceConstants.BACK_SLASH
          + URLEncoder.encode(fileName, PropertyServiceConstants.UTF_8);
    } catch (UnsupportedEncodingException e) {
      throw new CannotPerformOperationException(
          String.format("Failed to construct the error file upload url for email report"), e);
    }
    return fileUrl;
  }
 
}
