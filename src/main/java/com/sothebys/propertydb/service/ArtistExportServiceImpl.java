package com.sothebys.propertydb.service;

import com.sothebys.propertydb.constants.PropertyServiceConstants;
import com.sothebys.propertydb.dto.ArtistDTO;
import com.sothebys.propertydb.mapper.ArtistModelMapper;
import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.repository.ArtistRepository;
import com.sothebys.propertydb.search.PollStatus;
import com.sothebys.propertydb.search.Status;
import com.sothebys.propertydb.util.AmazonS3FileHandler;
import com.sothebys.propertydb.util.ExportData;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author SrinivasaRao.Batthula
 */

@Slf4j
@Qualifier("artistExportService")
@Service
public class ArtistExportServiceImpl implements ExportService {
  
  @Autowired
  private ArtistRepository artistRepository;
  
  @Autowired
  @Qualifier("exportDataS3Client")
  private AmazonS3FileHandler amazonS3ExportDataHandler;
  
  @Autowired
  private ExportData buildExportData;
  
  @Autowired
  private ExportStatusService exportStatusService;
  
  @Autowired
  private ArtistModelMapper artistModelMapper;
  
  @Value("${exportData.max-count:20000}")
  private long maxExportDataCount;
  
  /**
   * {@inheritDoc}
   */
  
  @Transactional
  @Override
  @Async
  public void exportData(Specification<Property> spec, String exportId) {
    log.info("Start Processing for the export ID='{}'", exportId);
    PollStatus status = PollStatus.InProgress;
    String exportCSVFileName =
        String.format("%s%s", exportId, PropertyServiceConstants.CSV_EXTENSION);
    List<ArtistDTO> artistDtos = getArtists();
    try {
      buildExportData.buildArtistCsvDocument(artistDtos, exportCSVFileName);
      amazonS3ExportDataHandler.uploadExportDataFileIntoS3(exportCSVFileName);
      status = PollStatus.Completed;
    } catch (Exception e) {
      log.error("Artist csv file='{}' export failed", exportCSVFileName);
      status = PollStatus.Error;
    }
    exportStatusService.updateExportStatus(exportId, status);
  }

  @Transactional(readOnly = true)
  public List<ArtistDTO> getArtists() {
    List<ArtistDTO> artistDtos = new ArrayList<ArtistDTO>();
    List<Artist> artists =
        artistRepository.findDistinctArtistsByStatus(Status.Active);
    artistDtos =
        artistModelMapper.mapToArtistDTOs(artists);
    return artistDtos;
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(readOnly = true)
  @Override
  public PollStatus findExportStatus(String exportId) {
    return exportStatusService.findExportStatus(exportId);
  }
  
  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public String generateExportId() {
    return UUID.randomUUID().toString();
  }
  
  /**
   * {@inheritDoc}
   *
   * TODO: Can be removed or can be added later for validations as needed for Artist export
   */
  @Transactional
  @Override
  public boolean validateExportData(Specification<Property> spec, String exportId) {
	  log.info("Validating the count of Active Artist data");
	  long artistCount = artistRepository.countDistinctArtistsByStatus(Status.Active);
	  log.info("Active Artist count = '{}'", Long.toString(artistCount));
	  if(artistCount > maxExportDataCount) {
		  PollStatus status = PollStatus.LimitExceeded;
		  exportStatusService.updateExportStatus(exportId, status);
		  return false;
	  }
	  return true;
  }
  
  @Override
  public void exportByEmail(List<String> propertyIds, String name) {
    // TODO Auto-generated method stub    
  }

}
