package com.sothebys.propertydb.service;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.SnsMessageDto;
import com.sothebys.propertydb.dto.SnsMessageOperation;
import com.sothebys.propertydb.exception.MessageServiceException;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service("MessageService")
public class MessageServiceImpl implements MessageService {
  private AmazonSNSClient snsClient;
  
  @Value("${aws.sns.topics.create-topic-arn}")
  private String createTopicArn;
  
  @Value("${aws.sns.topics.update-topic-arn}")
  private String updateTopicArn;
  
  @Value("${aws.sns.topics.delete-topic-arn}")
  private String deleteTopicArn;
  
  private ObjectMapper objectMapper;

  @Autowired
  public MessageServiceImpl(@Qualifier("snsClient") AmazonSNSClient snsClient, @Lazy ObjectMapper objectMapper) {
    this.snsClient = snsClient;
    this.objectMapper = objectMapper;
  }
  
  @Override
  @Async("snsMessageTaskExecutor")
  public void sendSnsMessage(SnsMessageDto messageDto, SnsMessageOperation messageOperation) {
    String snsMessage;
    try {
      snsMessage = objectMapper.writeValueAsString(messageDto);
      PublishRequest publishRequest = new PublishRequest();
      publishRequest.setMessage(snsMessage);
      publishRequest.setSubject(messageOperation.name());
      switch (messageOperation.name()) {
        case "CREATE":
          publishRequest.setTopicArn(createTopicArn);
          break;
        case "MERGE":
          publishRequest.setTopicArn(updateTopicArn);
          break;
        case "DELETE":
          publishRequest.setTopicArn(deleteTopicArn);
          break;
        default:
          throw new MessageServiceException("SnsMessageOperation not supported");
      }
      
      PublishResult publishResult = snsClient.publish(publishRequest);
      if(publishResult == null || StringUtils.isBlank(publishResult.getMessageId())) {
        throw new MessageServiceException("Sns Publish Result is null or the message Id is empty"); 
      }
      
      log.info("SNS Message Id - " + publishResult.getMessageId());
      log.info("SNS Message Operation - " + messageOperation.name());
      log.info("SNS Message - " + snsMessage);
    } catch (Exception e) {
      log.error("Error sending the message: "+messageDto.toString());
      log.error("Operation : "+messageOperation.name());
      log.error("Exception Message: " + e.getMessage());
      throw new MessageServiceException("Sns Message Operation failed!");
    }
  }
}
