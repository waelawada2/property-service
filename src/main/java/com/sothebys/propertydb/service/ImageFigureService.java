package com.sothebys.propertydb.service;

import java.util.List;

import com.sothebys.propertydb.model.ImageFigure;

public interface ImageFigureService {

  ImageFigure findByName(String name);
  
  
	/**
	 * Search for all figures.
	 *
	 * @return list of the figures
	 */
	List<ImageFigure> findAllImageFigures();
}
