package com.sothebys.propertydb.service;

import com.sothebys.propertydb.dto.SnsMessageDto;
import com.sothebys.propertydb.dto.SnsMessageOperation;

public interface MessageService {

  void sendSnsMessage(SnsMessageDto message, SnsMessageOperation messageOperation);

}
