package com.sothebys.propertydb.service;

import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ImageAnimal;
import com.sothebys.propertydb.model.ImageFigure;
import com.sothebys.propertydb.repository.ImageFigureRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Minhtri Tran on 11/15/17.
 */
@Service
public class ImageFigureServiceImpl implements ImageFigureService {

  private ImageFigureRepository imageFigureRepository;

  @Autowired
  public ImageFigureServiceImpl(ImageFigureRepository imageFigureRepository) {
    this.imageFigureRepository = imageFigureRepository;
  }

  @Transactional(readOnly = true)
  @Override
  public ImageFigure findByName(String name) {
    ImageFigure imageFigure = imageFigureRepository.findByName(name);

    if (imageFigure == null) {
      throw new NotFoundException(String.format("No image figure found with name=%s", name));
    }

    return imageFigure;
  }
  

	/**
	 * {@inheritDoc}
	 */
	@Transactional(readOnly = true)
	@Override
	public List<ImageFigure> findAllImageFigures() {
		return imageFigureRepository.findAllByOrderByNameAsc();
	}
}
