// Copyright (c) 2018 Sotheby's, Inc.
package com.sothebys.propertydb.service.external.client;

import com.sothebys.propertydb.search.ClientSearchInputDTO;
import java.util.List;
import org.keycloak.KeycloakSecurityContext;

/**
 * @author jsilva.
 */
public interface ClientService {
   List<String> clientIdsSearch(ClientSearchInputDTO clientDto,  KeycloakSecurityContext securityContext);
}
