// Copyright (c) 2018 Sotheby's, Inc.
package com.sothebys.propertydb.service.external.client;

import com.sothebys.propertydb.dto.response.ClientInfoDTO;
import com.sothebys.propertydb.exception.InputOutOfRangeException;
import com.sothebys.propertydb.search.ClientSearchInputDTO;
import com.sothebys.propertydb.search.ClientUtil;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author jsilva.
 */
@Slf4j
@Service
public class ClientServiceImpl implements ClientService {

  public static final String CLIENTS_PROJECTION_ID_ONLY = "clients?projection=idOnly&size=1000";
  private final RestTemplate restTemplate;

  @Value("${client-service.base-url}")
  private String CLIENT_SERVICE_URL;


  public ClientServiceImpl(RestTemplateBuilder restTemplateBuilder) {
    this.restTemplate = restTemplateBuilder.build();
  }

  /**
   * Calls client service GET with given input fields
   *
   * @param clientDto search input DTO
   */
  @Override
  public List<String> clientIdsSearch(ClientSearchInputDTO clientDto,
      KeycloakSecurityContext securityContext) {
    List<String> clientIds = new ArrayList<>();
    final MultiValueMap<String, String> clientSearchQueryParams = ClientUtil
        .validateClientSearchInputDTO(clientDto);
    if (!clientSearchQueryParams.isEmpty()) {
      final URI clientSearchEndpoint = UriComponentsBuilder
          .fromHttpUrl(CLIENT_SERVICE_URL + CLIENTS_PROJECTION_ID_ONLY)
          .queryParams(clientSearchQueryParams).build().toUri();
      clientIds = clientIdsSearchExternalService(clientSearchEndpoint,
          securityContext.getTokenString());
    }
    return clientIds;
  }

  private List<String> clientIdsSearchExternalService(URI clientServiceQueryParam,
      String accessToken) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set("Authorization", "Bearer " + accessToken);
    HttpEntity<?> requestEntity = new HttpEntity(headers);

    ParameterizedTypeReference<PagedResources<Resource<ClientInfoDTO>>> ptr =
        new ParameterizedTypeReference<PagedResources<Resource<ClientInfoDTO>>>() {
        };

    ResponseEntity<PagedResources<Resource<ClientInfoDTO>>> serviceGet = restTemplate.
        exchange(clientServiceQueryParam, HttpMethod.GET, requestEntity, ptr);

    if (serviceGet.getBody().getMetadata().getTotalElements() > 1000) {
      throw new InputOutOfRangeException("Too many clients related to the search");
    }
    return serviceGet.getBody().getContent().stream().map(Resource::getContent)
        .map(ClientInfoDTO::getEntityId).collect(Collectors.toList());
  }

}
