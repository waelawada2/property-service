package com.sothebys.propertydb.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ImageAnimal;
import com.sothebys.propertydb.repository.ImageAnimalRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by Minhtri Tran on 11/15/17.
 */
@Slf4j
@Service
public class ImageAnimalServiceImpl implements ImageAnimalService {

  private ImageAnimalRepository imageAnimalRepository;

  @Autowired
  public ImageAnimalServiceImpl(ImageAnimalRepository imageAnimalRepository) {
    this.imageAnimalRepository = imageAnimalRepository;
  }

  @Transactional(readOnly = true)
  @Override
  public ImageAnimal findByName(String name) {
    ImageAnimal imageAnimal = imageAnimalRepository.findByName(name);

    if (imageAnimal == null) {
      throw new NotFoundException(String.format("No image animal found with name=%s", name));
    }

    return imageAnimal;
  }

  @Override
  public ImageAnimal findByNameCreateIfNotFound(String name) {
    ImageAnimal imageAnimal = imageAnimalRepository.findByName(name);

    if (imageAnimal == null) {
      ImageAnimal newImageAnimal = new ImageAnimal();
      newImageAnimal.setName(name);
      imageAnimal = imageAnimalRepository.save(newImageAnimal);
    }

    return imageAnimal;
  }

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public ImageAnimal createImageAnimal(ImageAnimal imageAnimal) {
		Objects.requireNonNull(imageAnimal.getName(), "Animal name must not be null");
		String animalName = imageAnimal.getName().toUpperCase();
		List<ImageAnimal> searchImageAnimal = imageAnimalRepository.findAllByName(animalName);
		if (searchImageAnimal == null || searchImageAnimal.size() == 0) {
			imageAnimal.setName(animalName);
			imageAnimal = imageAnimalRepository.save(imageAnimal);
		} else {
			throw new CannotPerformOperationException(String.format("Animal name already exists."));
		}

		return imageAnimal;

	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional(readOnly = true)
	@Override
	public List<ImageAnimal> findAllImageAnimals() {
		return imageAnimalRepository.findAllByOrderByNameAsc();
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public List<ImageAnimal> findAllImageAnimalsByName(String name) {
		return imageAnimalRepository.findByNameContaining(name);
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void deleteImageAnimal(Long id) {
		Objects.requireNonNull(id, "id parameter must not be null");

		ImageAnimal imageAnimal = imageAnimalRepository.findById(id);
		if (imageAnimal == null) {
			throw new NotFoundException(String.format("Animal object with Id %s doesn't exist", id));
		}
		int usedImageAnimalsCount = imageAnimalRepository.countImageAnimals(id);
		if (usedImageAnimalsCount > 0) {
			throw new CannotPerformOperationException(
					"The animal object cannot be deleted as it is used by other objects");
		}
		try {
			imageAnimalRepository.delete(imageAnimal.getId());
		} catch (CannotAcquireLockException e) {
			throw new CannotPerformOperationException("Lock couldn't be acquired on table, please try again later");
		}
		log.info("'{}' is sucessfully deleted", imageAnimal.getName());
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public ImageAnimal updateImageAnimal(ImageAnimal imageAnimal) {
		Objects.requireNonNull(imageAnimal.getId(), "Animal id must not be null");
		Objects.requireNonNull(imageAnimal.getName(), "Animal name must not be null");
		ImageAnimal searchImageAnimal = imageAnimalRepository.findById(imageAnimal.getId());
		if (searchImageAnimal == null) {
			throw new NotFoundException(String.format("Animal object with Id %s doesn't exist", imageAnimal.getId()));
		}
		List<ImageAnimal> imageAnimalList = imageAnimalRepository.findAllByName(imageAnimal.getName());
		if (imageAnimalList != null && imageAnimalList.size() > 0) {
			throw new CannotPerformOperationException(String.format("Animal name already exists."));
		}
		searchImageAnimal.setName(imageAnimal.getName().toUpperCase());
		try {
			searchImageAnimal = imageAnimalRepository.save(searchImageAnimal);
			log.info("'{}' successfully updated", searchImageAnimal.getName());
		} catch (CannotAcquireLockException e) {
			throw new CannotPerformOperationException("Lock couldn't be acquired on table, please try again later");
		}
		return searchImageAnimal;
	}

	
  @Override
  @Transactional(readOnly = true)
  public ImageAnimal getImageAnimal(Long imageAnimalId) {
    return imageAnimalRepository.findOne(imageAnimalId);
  }
	
  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public ImageAnimal mergeImageAnimal(Long mergeToImageAnimalId, Long mergeFromImageAnimalId) {
    imageAnimalRepository.updatePropertyImageAnimals(mergeToImageAnimalId, mergeFromImageAnimalId);
    deleteImageAnimal(mergeFromImageAnimalId);

    log.info("Image Animal successfully deleted with ID={}", mergeFromImageAnimalId);
    return getImageAnimal(mergeToImageAnimalId);
  }
	
}
