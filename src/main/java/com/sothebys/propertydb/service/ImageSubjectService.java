package com.sothebys.propertydb.service;

import java.util.List;
import com.sothebys.propertydb.model.ImageSubject;

public interface ImageSubjectService {

	ImageSubject findByName(String name);
	
	
	ImageSubject findByNameCreateIfNotFound(String name);


	/**
	 * Search for all subjects.
	 *
	 * @return list of the subjects
	 */
	List<ImageSubject> findAllImageSubjects();

	/**
	 * Search for subjects by the name.
	 *
	 * @param name
	 *
	 * @return list of the subject. Search based on the name
	 */
	List<ImageSubject> findAllImageSubjectsByName(String name);

	ImageSubject updateImageSubjectById(Long imageSubjectId, ImageSubject imageSubject);

	void deleteImageSubjectById(Long imageSubjectId);

	ImageSubject createImageSubject(ImageSubject imageSubject);
	
	ImageSubject mergeImageSubject(Long mergeToImageSubjectId, Long mergeFromImageSubjectId);

	ImageSubject getImageSubject(Long imageSubjectId);
	
}
