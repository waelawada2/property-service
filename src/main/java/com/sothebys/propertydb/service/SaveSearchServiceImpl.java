package com.sothebys.propertydb.service;

import com.sothebys.propertydb.exception.AlreadyExistsException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.exception.PropertyServiceException;
import com.sothebys.propertydb.model.SaveSearch;
import com.sothebys.propertydb.model.SaveSearchCriteria;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.repository.SaveSearchRepository;
import com.sothebys.propertydb.repository.UserRepository;
import com.sothebys.propertydb.util.AlphaNumericIdGenerator;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author SrinivasaRao.Batthula
 */
@Slf4j
@Service
public class SaveSearchServiceImpl implements SaveSearchService {

  public static final String SAVE_SAERCH_EXTERNAL_ID_PREFIX = "SS-";

  @Autowired
  private SaveSearchRepository saveSearchRepository;

  @Autowired
  private UserRepository userRepository;

  @Transactional(readOnly = true)
  @Override
  public Page<SaveSearch> findBySearchTerm(Specification<SaveSearch> spec, Pageable pageRequest) {
    return saveSearchRepository.findAll(spec, pageRequest);
  }

  @Transactional(readOnly = true)
  @Override
  public Page<SaveSearch> findSaveSearchesByUserId(Pageable pageRequest, long userId) {
    return saveSearchRepository.findSaveSearchesByUserId(pageRequest, userId);
  }

  @Override
  @Transactional(readOnly = true)
  public SaveSearch findSaveSearchesByExternalId(String userName, String externalId) {
    return saveSearchRepository.findByUserIdAndExternalId(findUserId(userName), externalId);
  }
  
  @Override
  @Transactional(readOnly = true)
  public SaveSearch findSaveSearchesByExternalId(String externalId) {
    return saveSearchRepository.findByExternalId(externalId);
  }


  @Override
  @Transactional(readOnly = true)
  public long findUserId(String userName) {
    User user = userRepository.findByUserName(userName);
    if (user == null) {
      throw new NotFoundException(String.format("No userId found with userName=%s", userName));
    }
    return user.getId();
  }

  @Override
  @Transactional
  public SaveSearch createSaveSearch(SaveSearch saveSearch, long userId) {
    saveSearch.setUserId(userId);
    try {
      SaveSearch saveSearchObj = saveSearchRepository.save(saveSearch);
      List<SaveSearchCriteria> list = saveSearchObj.getSearchCriterias();
      for (SaveSearchCriteria ssc : list) {
        ssc.setSaveSearches(saveSearchObj);
      }
      saveSearchObj.setExternalId(AlphaNumericIdGenerator.getAlphaNumbericId(saveSearchObj.getId(),
          SAVE_SAERCH_EXTERNAL_ID_PREFIX));
      return saveSearchRepository.save(saveSearchObj);
    } catch (DataIntegrityViolationException ex) {
      throw new AlreadyExistsException(
          String.format("SaveSearch with Name '%s' already exists", saveSearch.getName()), ex);
    }
  }

  @Override
  @Transactional
  public void deleteByUserIdAndExternalId(long userId, String externalId) {
    SaveSearch saveSearchObj = saveSearchRepository.findByUserIdAndExternalId(userId, externalId);
    if (saveSearchObj == null) {
      throw new NotFoundException(String.format(
          "Saved Search with userId=%d with externalId=%s is not found", userId, externalId));
    }
    try {
      saveSearchRepository.deleteById(saveSearchObj.getId());
    } catch (Exception e) {
      throw new PropertyServiceException("Saved Search list deletion failed", e);
    }
    log.info("Saved search List successfully deleted with ID={}", externalId);
  }

  @Override
  @Transactional
  public SaveSearch updateByUserIdAndExternalId(long userId, String externalId,
      SaveSearch saveSearch) {
    SaveSearch saveSearchObj = saveSearchRepository.findByUserIdAndExternalId(userId, externalId);
    if (saveSearchObj == null) {
      throw new NotFoundException(String.format(
          "Saved Search with userId=%d with externalId=%s is not found, hence save search update failed",
          userId, externalId));
    }
    if (saveSearch.getSearchCriterias() != null && saveSearchObj.getSearchCriterias() != null) {
      SaveSearchCriteria updatedSaveSearchCriteria = saveSearch.getSearchCriterias().get(0);
      SaveSearchCriteria existingSaveSearchCriteria = saveSearchObj.getSearchCriterias().get(0);
      updatedSaveSearchCriteria.setId(existingSaveSearchCriteria.getId());
      updatedSaveSearchCriteria.setSaveSearches(saveSearch);
    }
    saveSearch.setExternalId(saveSearchObj.getExternalId());
    saveSearch.setId(saveSearchObj.getId());
    saveSearch.setUserId(saveSearchObj.getUserId());
    saveSearch.setSearchType(saveSearchObj.getSearchType());
    return saveSearchRepository.save(saveSearch);
  }
}
