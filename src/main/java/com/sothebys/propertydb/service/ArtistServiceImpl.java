package com.sothebys.propertydb.service;

import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.ArtistCatalogueRaisonee;
import com.sothebys.propertydb.model.Category;
import com.sothebys.propertydb.model.Country;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.repository.ArtistRepository;
import com.sothebys.propertydb.repository.CountryRepository;
import com.sothebys.propertydb.repository.PropertyRepository;
import com.sothebys.propertydb.search.Status;
import com.sothebys.propertydb.util.AlphaNumericIdGenerator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

/**
 * @author Nagarajutalluri
 */
@Slf4j
@Service
public class ArtistServiceImpl implements ArtistService {

  public static final String ARTIST_EXTERNAL_ID_PREFIX = "AA-";
  
  public static final String ARTIST_EXPORTER_NAME_PREFIX = "artist-";
  
  public static final String ASTERISK = "*";
  
  @Autowired
  private ArtistRepository artistRepository;
  @Autowired
  private CategoryService categoryService;

  @Autowired
  private CountryRepository countryRepository;

  @Autowired
  private PropertyRepository propertyRepository;
  
  @Autowired
  private ExportStatusService exportStatusService;
  
  private ExportService artistExportService;

  public ArtistServiceImpl(
      @Lazy @Qualifier("artistExportService") ExportService artistExportService) {
    this.artistExportService = artistExportService;
  }
  

  @Value("${page.record-count:20}")
  private int pageRecordCount;
  
  private void assignCategories(Artist artist, List<Category> categories) {
    List<Category> updatedCategories = new ArrayList<>();
    for (Category category : categories) {
      Category dbCategory =
          categoryService.findByCategoryNameAndParentCategoryName(category.getName(), null);
      if (dbCategory == null) {
        throw new NotFoundException(
            "Category can not be assigned to artist, category can not be found");
      }
      updatedCategories.add(dbCategory);
    }
    artist.setCategories(updatedCategories);
  }

  private void assignCountries(final Artist artist, List<Country> countries) {
    List<Country> updatedCountries = new ArrayList<>();
    for (Country country : countries) {
      Country countryObject = countryRepository.findByCountryName(country.getCountryName());
      if (countryObject == null) {
        Country newCountry = new Country();
        newCountry.setCountryName(country.getCountryName());
        countryObject = countryRepository.save(newCountry);
      }
      updatedCountries.add(countryObject);
    }
    artist.setCountries(updatedCountries);
  }

  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public Artist createArtist(final Artist artist, final User user, boolean invalidArtist) {
    Artist createdArtist = null;

    assignCountries(artist, artist.getCountries());
    assignCategories(artist, artist.getCategories());

    artist.setCreatedBy(user);
    try {
      List<Artist> duplicateArtistList = findDuplicateArtists(artist);
      if (!invalidArtist && !CollectionUtils.isEmpty(duplicateArtistList)) {
        throw new CannotPerformOperationException(
            "An artist already exists with the provided values. Are you sure you would like to create a new artist");
      }
      createdArtist = artistRepository.save(artist);
      createdArtist.setExternalId(AlphaNumericIdGenerator.getAlphaNumbericId(createdArtist.getId(),
          ARTIST_EXTERNAL_ID_PREFIX));
      createdArtist = artistRepository.save(createdArtist);
    } catch (DataIntegrityViolationException e) {
      throw new CannotPerformOperationException(String.format(
          "Unable to insert Artist. Specified Artist Code %s already exist", artist.getUlanId()));
    } catch (CannotAcquireLockException e) {
      throw new CannotPerformOperationException(
          "Lock couldn't be acquired on table, please try again later");

    }
    log.info("Artist successfully created with ID={}", createdArtist.getId());
    return createdArtist;

  }

  private List<Artist> findDuplicateArtists(final Artist artist) {
    List<Artist> artistList = null;
    if (StringUtils.isNotBlank(artist.getFirstName())
        && StringUtils.isNotBlank(artist.getLastName())) {
      
      if (null != artist.getBirthYear() && null != artist.getDeathYear()
          && StringUtils.isNotBlank(artist.getGender())
          && !CollectionUtils.isEmpty(artist.getCountries())) {
        artistList = artistRepository
            .findDistinctArtistByStatusAndLastNameAndFirstNameAndBirthYearAndDeathYearAndGenderAndCountriesIn(
                Status.Active, artist.getLastName(), artist.getFirstName(), artist.getBirthYear(),
                artist.getDeathYear(), artist.getGender(), artist.getCountries());

      } else if (null != artist.getBirthYear() && !CollectionUtils.isEmpty(artist.getCountries())
          && StringUtils.isNotBlank(artist.getGender())) {
        artistList = artistRepository
            .findDistinctArtistByStatusAndLastNameAndFirstNameAndBirthYearAndGenderAndCountriesIn(
                Status.Active, artist.getLastName(), artist.getFirstName(), artist.getBirthYear(),
                artist.getGender(), artist.getCountries());
      } else if (!CollectionUtils.isEmpty(artist.getCountries())
          && StringUtils.isNotBlank(artist.getGender())) {
        artistList = artistRepository
            .findDistinctArtistByStatusAndLastNameAndFirstNameAndGenderAndCountriesIn(Status.Active,
                artist.getLastName(), artist.getFirstName(), artist.getGender(),
                artist.getCountries());

      } else if (null != artist.getBirthYear() && StringUtils.isNotBlank(artist.getGender())) {
        artistList =
            artistRepository.findDistinctArtistByStatusAndLastNameAndFirstNameAndBirthYearAndGender(
                Status.Active, artist.getLastName(), artist.getFirstName(), artist.getBirthYear(),
                artist.getGender());
      }
    }
    return artistList;
  }

  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public void deleteArtist(Long artistId) {
    Artist artist = findArtistById(artistId);
    if (artist == null || artist.getStatus() != Status.Active) {
      throw new NotFoundException(String.format("No artist found with ID=%d", artistId));
    }
    artist.setStatus(Status.Deleted);
    artistRepository.save(artist);
    log.info("Artist status successfully change to Deleted for ID={}", artistId);
  }

  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public void deleteArtist(String externalId) {
    Objects.requireNonNull(externalId, "externalID parameter must not be null");

    Artist artist = findArtistByExternalId(externalId);
    if (artist == null || artist.getStatus() != Status.Active) {
      throw new NotFoundException(String.format("No artist found with ID=%s", externalId));
    }
    final Long countActiveProperties = propertyRepository
        .countByArtistsInAndStatus(Collections.singletonList(artist), Status.Active);
    if (countActiveProperties > 0) {
      throw new CannotPerformOperationException(
          "Objects are still assigned to this artist, please reassign objects before deleting this artist.");
    }
    artist.setStatus(Status.Deleted);
    artistRepository.save(artist);
    log.info("Artist status successfully changed to Deleted for ID={}", artist.getId());
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(readOnly = true)
  @Override
  public List<Artist> findAllArtists() {
    return artistRepository.findAll();
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(readOnly = true)
  @Override
  public Artist findArtistByExternalId(String externalId) {
    Artist artist = null;
    if(ASTERISK.equals(externalId)) {
    	List<Artist> artistList = artistRepository.findByExternalIdIsNotNull();
    	if(artistList != null && !artistList.isEmpty()) {
    		artist = artistList.get(0);
    	}
    } else {
    	artist = artistRepository.findByExternalId(externalId);
    }
    if (artist == null || artist.getStatus() == Status.Deleted) {
      throw new NotFoundException(String.format("Artist with ID %s doesn't exist", externalId));
    }
    return artist;
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(readOnly = true)
  @Override
  public Artist findArtistByUlanId(Long ulanId) {
    return artistRepository.findByUlanId(ulanId);
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(readOnly = true)
  @Override
  public Artist findArtistById(Long artistId) {
    return artistRepository.findOne(artistId);
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(readOnly = true)
  @Override
  public Page<Artist> findBySearchTerm(Specification<Artist> spec, Pageable pageRequest) {
    return artistRepository.findAll(spec, pageRequest);
  }

  /**
   * {@inheritDoc}
   */
  @Transactional(readOnly = true)
  @Override
  public Page<Artist> findAllArtists(Pageable pageRequest) {
    Page<Artist> artistPageResponse = null;

    if (pageRequest.getPageSize() == pageRecordCount) {
      List<Artist> artistList = new ArrayList<Artist>();
      Page<Artist> artists =
          artistRepository.findDistinctArtistsByStatus(pageRequest, Status.Active);
      artistList.addAll(artists.getContent());

      if (artists.getTotalPages() > 1) {
        for (int page = 1; page < artists.getTotalPages(); page++) {
          artists = artistRepository.findDistinctArtistsByStatus(
              new PageRequest(page, pageRequest.getPageSize(), pageRequest.getSort()),
              Status.Active);
          artistList.addAll(artists.getContent());
        }
      }
      artistPageResponse = new PageImpl<Artist>(artistList);
    } else {
      artistPageResponse = artistRepository.findDistinctArtistsByStatus(pageRequest, Status.Active);
    }
    return artistPageResponse;
  }

  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public Artist updateArtist(Long artistId, final Artist artist, final User user) {
    Artist currentArtist = findArtistById(artistId);
    if (currentArtist == null) {
      throw new NotFoundException(String.format("No artist found with ID=%d", artistId));
    }

    if (!StringUtils.isEmpty(artist.getLastName())) {
      currentArtist.setLastName(artist.getLastName());
    }
    currentArtist.setFirstName(artist.getFirstName());
    currentArtist.setBirthYear(artist.getBirthYear());
    currentArtist.setDeathYear(artist.getDeathYear());
    currentArtist.setDisplay(artist.getDisplay());
    currentArtist.setGender(artist.getGender());
    currentArtist.setNotes(artist.getNotes());
    currentArtist.setUpdatedBy(user);
    assignCategories(currentArtist, artist.getCategories());
    assignCountries(currentArtist, artist.getCountries());
    try {
      currentArtist = artistRepository.save(currentArtist);
      log.info("Artist with ID={} successfully updated", artistId);
    } catch (CannotAcquireLockException e) {
      throw new CannotPerformOperationException(
          "Lock couldn't be acquired on table, please try again later");
    }
    return currentArtist;
  }

  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public Artist updateArtistByExternalId(String externalId, final Artist artist, User user) {
    Artist searchArtist = findArtistByExternalId(externalId);
    if (searchArtist == null) {
      throw new NotFoundException(String.format("No artist found with external ID=%s", externalId));
    }

    return updateArtist(searchArtist.getId(), artist, user);
  }

  public Artist setArtistCatalogueRaisoneeList(Long artistId,
      final List<ArtistCatalogueRaisonee> catalogueRaisonees) {
    Artist artist = findArtistById(artistId);
    if (artist == null) {
      throw new NotFoundException(String.format("No artist found with ID=%d", artistId));
    }
    artist.setArtistCatalogueRaisonees(catalogueRaisonees);
    return artistRepository.save(artist);
  }
  
  /**
   * {@inheritDoc}
   */
  @Transactional
  @Override
  public String exportArtistData() {
    String exportId =
        String.format("%s%s", ARTIST_EXPORTER_NAME_PREFIX, artistExportService.generateExportId());
    exportStatusService.saveExportStatus(exportId);
    artistExportService.exportData(null, exportId);
    return exportId;
  }

}
