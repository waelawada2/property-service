package com.sothebys.propertydb.service;

import java.util.List;

import com.sothebys.propertydb.model.ImageGender;

public interface ImageGenderService {

  ImageGender findByName(String name);
  

	/**
	 * Search for all genders.
	 *
	 * @return list of the genders
	 */
	List<ImageGender> findAllImageGenders();
}
