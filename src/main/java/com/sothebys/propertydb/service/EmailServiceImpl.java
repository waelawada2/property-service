package com.sothebys.propertydb.service;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import com.sothebys.propertydb.model.Mail;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EmailServiceImpl implements EmailService {

  @Autowired
  private JavaMailSender emailSender;

  public void sendMail(Mail mail) {
    try {
      MimeMessage mimeMessage = emailSender.createMimeMessage();

      mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(mail.getTo()));
      mimeMessage.setFrom(new InternetAddress(mail.getFrom()));
      mimeMessage.setContent(mail.getContent(), mail.getContentType());
      mimeMessage.setSubject(mail.getSubject());

      emailSender.send(mimeMessage);
    } catch (Exception ex) {
      log.error("Failed to send email for the Saved List", ex);
    }
  }

}
