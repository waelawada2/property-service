package com.sothebys.propertydb.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Validator annotation for marking types that need at least
 * one artist identifier ({@code artistId} or {@code ulandId}.
 */
@Documented
@Constraint(validatedBy = HasArtistIdentifierValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface HasArtistIdentifier {

  String message() default "At least one Artist ID or ULAN ID must be provided";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}
