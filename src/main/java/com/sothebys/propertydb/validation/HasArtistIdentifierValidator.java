package com.sothebys.propertydb.validation;

import com.sothebys.propertydb.dto.request.PropertyAddRequestDTO;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator that checks that {@link PropertyAddRequestDTO} object have
 * at least one artist identifier. An artist identifier can
 * be either an {@code artistId} or a {@code ulanId}.
 *
 * @author Gregor Zurowski
 */
public class HasArtistIdentifierValidator implements
    ConstraintValidator<HasArtistIdentifier, PropertyAddRequestDTO> {

  @Override
  public void initialize(HasArtistIdentifier constraintAnnotation) {
  }

  /**
   * Checks that the given {@link PropertyAddRequestDTO} instance has
   * at least one artist identifier.
   */
  @Override
  public boolean isValid(PropertyAddRequestDTO value, ConstraintValidatorContext context) {
    return (value.getArtistIds() != null && value.getArtistIds().size() > 0) ||
        (value.getUlanIds() != null && value.getUlanIds().size() > 0);
  }

}
