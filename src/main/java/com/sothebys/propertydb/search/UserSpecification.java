/**
 * 
 */
package com.sothebys.propertydb.search;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.sothebys.propertydb.model.User;

/**
 * This class provides user specification object & criteria.
 * 
 * @author Manju.Rana
 *
 */
public class UserSpecification implements Specification<User>{

  private SearchCriteria criteria;

  public UserSpecification(final SearchCriteria criteria) {
    super();
    this.criteria = criteria;
  }

  public SearchCriteria getCriteria() {
    return criteria;
  }

  public Predicate toPredicate(final Root<User> root, final CriteriaQuery<?> query,
      final CriteriaBuilder builder) {

    switch (criteria.getOperation()) {
      case EQUALITY:
        return builder.equal(root.get(criteria.getKey()), criteria.getValue());
      case NEGATION:
        return builder.notEqual(root.get(criteria.getKey()), criteria.getValue());
      case GREATER_THAN:
        return builder.greaterThan(root.<String>get(criteria.getKey()),
            criteria.getValue().toString());
      case LESS_THAN:
        return builder.lessThan(root.<String>get(criteria.getKey()),
            criteria.getValue().toString());
      case LIKE:
        return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
      case STARTS_WITH:
        return builder.like(root.<String>get(criteria.getKey()), criteria.getValue() + "%");
      case ENDS_WITH:
        return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue());
      case CONTAINS:
        return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
      default:
        return null;
    }
  }
}