package com.sothebys.propertydb.search;

/**
 * Enum contains the poll ExportId status.
 */
public enum PollStatus {
  
  Completed, InProgress, Error, LimitExceeded;

}
