package com.sothebys.propertydb.search;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

/**
 * @author SrinivasaRao Batthula
 *
 */

@Data
public class SaveListInputDTO {
  @NotNull
  @Size(max = 200)
  private String name;

}
