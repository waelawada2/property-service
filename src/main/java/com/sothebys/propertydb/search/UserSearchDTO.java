package com.sothebys.propertydb.search;

import lombok.Data;

/**
 * This class holds user name to be returned when lod = MIN.
 * 
 * @author Manju.Rana
 *
 */
@Data
public class UserSearchDTO {

  private String userName;
}
