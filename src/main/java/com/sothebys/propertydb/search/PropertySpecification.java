package com.sothebys.propertydb.search;

import com.sothebys.propertydb.dto.RangeDTO;
import com.sothebys.propertydb.exception.BadRequestException;
import com.sothebys.propertydb.model.ArchiveNumber;
import com.sothebys.propertydb.model.ArtistCatalogueRaisonee;
import com.sothebys.propertydb.model.Authenticity;
import com.sothebys.propertydb.model.Category;
import com.sothebys.propertydb.model.Country;
import com.sothebys.propertydb.model.DateDetails;
import com.sothebys.propertydb.model.Dimensions;
import com.sothebys.propertydb.model.Edition;
import com.sothebys.propertydb.model.EditionSize;
import com.sothebys.propertydb.model.EditionType;
import com.sothebys.propertydb.model.ExpertiseSitter;
import com.sothebys.propertydb.model.Flags;
import com.sothebys.propertydb.model.ImageAnimal;
import com.sothebys.propertydb.model.ImageFigure;
import com.sothebys.propertydb.model.ImageGender;
import com.sothebys.propertydb.model.ImageMainColour;
import com.sothebys.propertydb.model.ImagePosition;
import com.sothebys.propertydb.model.ImageSubject;
import com.sothebys.propertydb.model.Orientation;
import com.sothebys.propertydb.model.OtherTag;
import com.sothebys.propertydb.model.PresizeType;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.PropertyCatalogueRaisonee;
import com.sothebys.propertydb.model.ReminderNotes;
import com.sothebys.propertydb.model.ResearcherNotes;
import com.sothebys.propertydb.model.SaveList;
import com.sothebys.propertydb.model.Scale;
import com.sothebys.propertydb.model.Tags;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.data.jpa.domain.Specification;

public class PropertySpecification implements Specification<Property> {

  private static final String PROPERTY_EDITION_SIZE_TOTAL = "editionSizeTotal";
  private static final String ASTERISK = "*";

  private SearchCriteria criteria;

  public PropertySpecification(final SearchCriteria criteria) {
    super();
    this.criteria = criteria;
  }

  public SearchCriteria getCriteria() {
    return criteria;
  }

  @Override
  public Predicate toPredicate(final Root<Property> root, final CriteriaQuery<?> query,
      final CriteriaBuilder builder) {

    Join<Property, DateDetails> propertyDateDetails = null;
    Join<Property, Edition> propertyEdition = null;
    Join<Property, EditionType> propertyEditionType = null;
    Join<Property, EditionSize> propertyEditionSize = null;
    Join<Property, Dimensions> propertyDimensions = null;
    Join<Property, Authenticity> propertyAuthenticity = null;
    Join<Property, Flags> propertyFlags = null;
    Join<Property, Category> propertyCategory = null;
    Join<Property, List<PropertyCatalogueRaisonee>> propertyCatalogueRaisonee = null;
    Join<Property, ResearcherNotes> propertyResearcherNotes = null;
    Join<Property, ArchiveNumber> archiveNumbersJoin = null;
    Join<Property, List<SaveList>> savedLists = null;
    Join<Property, Tags> propertyTags = null;

    query.distinct(true);

    if ("artistIds".equalsIgnoreCase(criteria.getKey())) {
      List<String> artistIdsList = (List<String>) criteria.getValue();
      List<Predicate> artistIdPredicateList = new ArrayList<Predicate>();
      if (artistIdsList != null && artistIdsList.size() == 1
          && ASTERISK.equals(artistIdsList.get(0))) {
        return builder.isNotNull(root.join("artists", JoinType.LEFT).get("externalId"));
      } else {
        for (int artistId = 0; artistId < artistIdsList.size(); artistId++) {
          artistIdPredicateList.add(builder.equal(
              root.join("artists", JoinType.LEFT).get("externalId"), artistIdsList.get(artistId)));
        }
        return builder
            .and(artistIdPredicateList.toArray(new Predicate[artistIdPredicateList.size()]));
      }
    }

    if ("saveListId".equalsIgnoreCase(criteria.getKey())) {
      savedLists = root.join("saveList", JoinType.LEFT);
      query.distinct(true);
      return savedLists.get("id").in(getCriteria().getValue());
    }

    if ("archiveNumbers".equalsIgnoreCase(criteria.getKey())) {
      archiveNumbersJoin = root.join("archiveNumbers", JoinType.LEFT);
      query.distinct(true);
      Expression<String> expression = archiveNumbersJoin.get("archiveNumber");
      List<String> archiveNumberList = (List<String>) criteria.getValue();
      if (archiveNumberList != null && archiveNumberList.size() == 1
          && ASTERISK.equals(archiveNumberList.get(0))) {
        return expression.isNotNull();
      } else {
        return expression.in(criteria.getValue());
      }
    }

    if ("artistCatalogueRaisoneeIds".equalsIgnoreCase(criteria.getKey())) {
      Join<Property, List<PropertyCatalogueRaisonee>> artistPropertyCatalogueRaisonee =
          root.joinList("propertyCatalogueRaisonees", JoinType.LEFT);
      Join<PropertyCatalogueRaisonee, ArtistCatalogueRaisonee> artistCatalogRaisonee =
          artistPropertyCatalogueRaisonee.join("artistCatalogueRaisonee", JoinType.LEFT);
      Expression<String> expression = artistCatalogRaisonee.get("id");
      return expression.in((List<String>) criteria.getValue());
    }

    if ("number".equalsIgnoreCase(criteria.getKey())) {
      List<String> numbersList = (List<String>) criteria.getValue();
      List<Predicate> numberPredicateList = new ArrayList<Predicate>();
      if (numbersList != null && numbersList.size() == 1 && ASTERISK.equals(numbersList.get(0))) {
        return builder
            .isNotNull(root.join("propertyCatalogueRaisonees", JoinType.LEFT).get("number"));
      } else {
        for (int numberId = 0; numberId < numbersList.size(); numberId++) {
          propertyCatalogueRaisonee = root.join("propertyCatalogueRaisonees", JoinType.LEFT);
          List<String> numberWithcridList =
              Arrays.asList(numbersList.get(numberId).toString().split(":"));
          numberPredicateList.add(
              builder.equal(propertyCatalogueRaisonee.get("number"), numberWithcridList.get(0)));

          if (numberWithcridList.size() > 1 && numberWithcridList.get(1).startsWith("crId")) {
            Join<PropertyCatalogueRaisonee, ArtistCatalogueRaisonee> artistCatalogRaisonee =
                propertyCatalogueRaisonee.join("artistCatalogueRaisonee", JoinType.LEFT);
            List<String> crIdList = Arrays.asList(numberWithcridList.get(1).toString().split("="));
            numberPredicateList
                .add(builder.equal(artistCatalogRaisonee.get("id"), crIdList.get(1)));
          }
        }
        return builder.and(numberPredicateList.toArray(new Predicate[numberPredicateList.size()]));
      }
    }

    if ("volume".equalsIgnoreCase(criteria.getKey())) {
      propertyCatalogueRaisonee = root.join("propertyCatalogueRaisonees", JoinType.LEFT);
      query.distinct(true);
      Expression<String> expression = propertyCatalogueRaisonee.get("volume");
      if (ASTERISK.equals(criteria.getValue())) {
        return expression.isNotNull();
      } else {
        return expression.in(criteria.getValue());
      }
    }

    if ("editionName".equalsIgnoreCase(criteria.getKey())) {
      propertyEdition = root.join("edition", JoinType.LEFT);
      propertyEditionType = propertyEdition.join("editionType", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyEditionType.get("name"));
      } else {
        return builder.equal(propertyEditionType.get("name"), criteria.getValue());
      }
    }

    if ("uniqueEdition".equalsIgnoreCase(criteria.getKey())) {
      propertyEdition = root.join("edition", JoinType.LEFT);
      return builder.equal(propertyEdition.get("uniqueEdition"), criteria.getValue());
    }

    if ("presizeName".equalsIgnoreCase(criteria.getKey())) {
      Join<Property, PresizeType> propertyPresizeType = root.join("presizeType", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyPresizeType.get("name"));
      } else {
        return builder.equal(propertyPresizeType.get("name"), criteria.getValue());
      }
    }

    if ("category".equalsIgnoreCase(criteria.getKey())) {
      propertyCategory = root.join("category", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyCategory.get("name"));
      } else {
        return builder.equal(propertyCategory.get("name"), criteria.getValue());
      }
    }

    if ("weight".equalsIgnoreCase(criteria.getKey())) {
      propertyDimensions = root.join("dimensions", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyDimensions.get("weight"));
      } else {
        return builder.equal(propertyDimensions.get("weight"),
            PropertyUtil.convertStringToBigDecimal(criteria.getValue().toString()));
      }
    }

    if ("height".equals(criteria.getKey()) || "width".equals(criteria.getKey())
        || "depth".equals(criteria.getKey())) {
      propertyDimensions = root.join("dimensions", JoinType.LEFT);
      RangeDTO dimensionRange = (RangeDTO) criteria.getValue();
      return getRangeOperation(builder, propertyDimensions, dimensionRange);
    }

    if ("authentication".equalsIgnoreCase(criteria.getKey())) {
      propertyAuthenticity = root.join("authenticity", JoinType.LEFT);
      return builder.equal(propertyAuthenticity.get("authentication"), criteria.getValue());
    }

    if ("signed".equalsIgnoreCase(criteria.getKey())) {
      propertyAuthenticity = root.join("authenticity", JoinType.LEFT);
      return builder.equal(propertyAuthenticity.get("signed"), criteria.getValue());
    }

    if ("certificate".equalsIgnoreCase(criteria.getKey())) {
      propertyAuthenticity = root.join("authenticity", JoinType.LEFT);
      return builder.equal(propertyAuthenticity.get("certificate"), criteria.getValue());
    }

    if ("stamped".equalsIgnoreCase(criteria.getKey())) {
      propertyAuthenticity = root.join("authenticity", JoinType.LEFT);
      return builder.equal(propertyAuthenticity.get("stamped"), criteria.getValue());
    }

    if ("reminderEmployee".equalsIgnoreCase(criteria.getKey())) {
      Join<Property, ReminderNotes> propertyReminderNotes =
          root.join("reminderNotes", JoinType.LEFT);
      return builder.like(propertyReminderNotes.get("reminderEmployee"),
          "%" + criteria.getValue() + "%");
    }

    if ("researcherName".equalsIgnoreCase(criteria.getKey())) {
      propertyResearcherNotes = root.join("researcherNotes", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyResearcherNotes.get("researcherName"));
      } else {
        return builder.like(propertyResearcherNotes.get("researcherName"),
            "%" + criteria.getValue() + "%");
      }
    }

    if ("yearCirca".equalsIgnoreCase(criteria.getKey())) {
      propertyDateDetails = root.join("dateDetails", JoinType.LEFT);
      return builder.equal(propertyDateDetails.get("yearCirca"), criteria.getValue());
    }

    if ("castYearCirca".equalsIgnoreCase(criteria.getKey())) {
      propertyDateDetails = root.join("dateDetails", JoinType.LEFT);
      return builder.equal(propertyDateDetails.get("castYearCirca"), criteria.getValue());
    }

    if ("castYearStart".equalsIgnoreCase(criteria.getKey())) {
      propertyDateDetails = root.join("dateDetails", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyDateDetails.get("castYearStart"));
      } else {
        return builder.equal(propertyDateDetails.get("castYearStart"),
            PropertyUtil.convertStringToInteger(criteria.getValue().toString()));
      }
    }

    if ("castYearEnd".equalsIgnoreCase(criteria.getKey())) {
      propertyDateDetails = root.join("dateDetails", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyDateDetails.get("castYearEnd"));
      } else {
        return builder.equal(propertyDateDetails.get("castYearEnd"),
            PropertyUtil.convertStringToInteger(criteria.getValue().toString()));
      }
    }
    if ("yearStart".equalsIgnoreCase(criteria.getKey())) {
      RangeDTO yearRange = (RangeDTO) criteria.getValue();
      propertyDateDetails = root.join("dateDetails", JoinType.LEFT);
      return getRangeOperation(builder, propertyDateDetails, yearRange);
    }
    if ("yearEnd".equalsIgnoreCase(criteria.getKey())) {
      propertyDateDetails = root.join("dateDetails", JoinType.LEFT);

      if (criteria.getValue() instanceof HashMap) {
        return getPredicateBetweenRange(propertyDateDetails, builder);
      } else if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyDateDetails.get("yearEnd"));
      }

      return builder.equal(propertyDateDetails.get("yearEnd"), criteria.getValue());
    }
    if ("yearText".equalsIgnoreCase(criteria.getKey())) {
      propertyDateDetails = root.join("dateDetails", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyDateDetails.get("yearText"));
      } else {
        return builder.like(propertyDateDetails.get("yearText"), "%" + criteria.getValue() + "%");
      }
    }
    if ("editionNumber".equalsIgnoreCase(criteria.getKey())) {
      propertyEdition = root.join("edition", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyEdition.get("editionNumber"));
      } else {
        return builder.equal(propertyEdition.get("editionNumber"), criteria.getValue());
      }
    }
    if (PROPERTY_EDITION_SIZE_TOTAL.equalsIgnoreCase(criteria.getKey())) {
      RangeDTO editionSizeRange = (RangeDTO) criteria.getValue();
      propertyEdition = root.join("edition", JoinType.LEFT);
      propertyEditionSize = propertyEdition.join("editionSize", JoinType.LEFT);
      return getRangeOperation(builder, propertyEditionSize, editionSizeRange);
    }

    if ("flagAuthenticity".equalsIgnoreCase(criteria.getKey())) {
      propertyFlags = root.join("flags", JoinType.LEFT);
      return builder.equal(propertyFlags.get("flagAuthenticity"), criteria.getValue());
    }
    if ("flagCondition".equalsIgnoreCase(criteria.getKey())) {
      propertyFlags = root.join("flags", JoinType.LEFT);
      return builder.equal(propertyFlags.get("flagCondition"), criteria.getValue());
    }
    if ("flagMedium".equalsIgnoreCase(criteria.getKey())) {
      propertyFlags = root.join("flags", JoinType.LEFT);
      return builder.equal(propertyFlags.get("flagMedium"), criteria.getValue());
    }
    if ("flagRestitution".equalsIgnoreCase(criteria.getKey())) {
      propertyFlags = root.join("flags", JoinType.LEFT);
      return builder.equal(propertyFlags.get("flagRestitution"), criteria.getValue());
    }
    if ("flagSfs".equalsIgnoreCase(criteria.getKey())) {
      propertyFlags = root.join("flags", JoinType.LEFT);
      return builder.equal(propertyFlags.get("flagSfs"), criteria.getValue());
    }
    if ("flagsCountry".equalsIgnoreCase(criteria.getKey())) {
      propertyFlags = root.join("flags", JoinType.LEFT);
      Join<Property, Country> propertyFlagCountry =
          propertyFlags.join("flagsCountry", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyFlagCountry.get("countryName"));
      } else {
        return builder.equal(propertyFlagCountry.get("countryName"), criteria.getValue());
      }
    }
    if ("researcherNoteDate".equalsIgnoreCase(criteria.getKey())) {
      propertyResearcherNotes = root.join("researcherNotes", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyResearcherNotes.get("date"));
      } else {
        return builder.equal(propertyResearcherNotes.get("date"),
            PropertyUtil.dateFormatMMddYYYY(criteria.getValue().toString()));
      }
    }
    if ("researchCompleteCheck".equalsIgnoreCase(criteria.getKey())) {
      propertyResearcherNotes = root.join("researcherNotes", JoinType.LEFT);
      return builder.equal(propertyResearcherNotes.get("researchCompleteCheck"),
          criteria.getValue());
    }
    if ("catalogueRaisoneeId".equalsIgnoreCase(criteria.getKey())) {
      propertyCatalogueRaisonee = root.join("propertyCatalogueRaisonees", JoinType.LEFT);
      query.distinct(true);
      Expression<String> expression = propertyCatalogueRaisonee.get("id");
      return expression.in(criteria.getValue());
    }

    if ("parts".equalsIgnoreCase(criteria.getKey())) {
      Predicate result = null;
      RangeDTO partsRange = (RangeDTO) criteria.getValue();
      return getRangedOperationForRoot(root, builder, partsRange);
    }

    if ("currentOwnerIds".equalsIgnoreCase(criteria.getKey())) {
      return root.get("currentOwnerId").in(criteria.getValue());
    }

    if ("artistCatalogueRaisoneeAuthor".equalsIgnoreCase(criteria.getKey())) {
      Join<Property, List<PropertyCatalogueRaisonee>> artistPropertyCatalogueRaisonee =
          root.joinList("propertyCatalogueRaisonees", JoinType.LEFT);
      Join<PropertyCatalogueRaisonee, ArtistCatalogueRaisonee> artistCatalogRaisonee =
          artistPropertyCatalogueRaisonee.join("artistCatalogueRaisonee", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(artistCatalogRaisonee.get("author"));
      } else {
        return builder.like(artistCatalogRaisonee.get("author"), "%" + criteria.getValue() + "%");
      }
    }

    // For tags
    if ("expertiseLocation".equalsIgnoreCase(criteria.getKey())) {
      propertyTags = root.join("tags", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyTags.get("expertiseLocation"));
      } else {
        return builder.like(propertyTags.get("expertiseLocation"), "%" + criteria.getValue() + "%");
      }
    }
    if ("expertiseSitters".equalsIgnoreCase(criteria.getKey())) {
      query.distinct(true);
      propertyTags = root.join("tags", JoinType.LEFT);
      Join<Tags, List<ExpertiseSitter>> propertyExpertiseSitter = propertyTags
          .joinList("expertiseSitters", JoinType.LEFT);
//			Expression<String> expression = builder.concat(propertyExpertiseSitter.get("firstName"), " ");
//			Expression<String> expressionLastName = builder.coalesce(propertyExpertiseSitter.get("lastName"), "");
//			expression = builder.concat(expression, expressionLastName);
      Expression<String> expression = propertyExpertiseSitter.get("id");
      return expression.in((List<String>) criteria.getValue());

    }
    if ("imageSubjects".equalsIgnoreCase(criteria.getKey())) {
      query.distinct(true);
      propertyTags = root.join("tags", JoinType.LEFT);
      Join<Tags, List<ImageSubject>> propertyImageSubjects =
          propertyTags.joinList("imageSubjects", JoinType.LEFT);
      Expression<String> expression = propertyImageSubjects.get("name");
      return expression.in((List<String>) criteria.getValue());
    }
    if ("imageFigure".equalsIgnoreCase(criteria.getKey())) {
      propertyTags = root.join("tags", JoinType.LEFT);
      Join<Tags, ImageFigure> propertyImageFigure = propertyTags.join("imageFigure", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyImageFigure.get("name"));
      } else {
        return builder.like(propertyImageFigure.get("name"), "%" + criteria.getValue() + "%");
      }
    }
    if ("imageGenders".equalsIgnoreCase(criteria.getKey())) {
      query.distinct(true);
      propertyTags = root.join("tags", JoinType.LEFT);
      Join<Tags, List<ImageGender>> propertyImageGender =
          propertyTags.joinList("imageGenders", JoinType.LEFT);
      Expression<String> expression = propertyImageGender.get("name");
      return expression.in((List<String>) criteria.getValue());
    }
    if ("imagePositions".equalsIgnoreCase(criteria.getKey())) {
      query.distinct(true);
      propertyTags = root.join("tags", JoinType.LEFT);
      Join<Tags, List<ImagePosition>> propertyImagePosition =
          propertyTags.joinList("imagePositions", JoinType.LEFT);
      Expression<String> expression = propertyImagePosition.get("name");
      return expression.in((List<String>) criteria.getValue());
    }
    if ("imageAnimals".equalsIgnoreCase(criteria.getKey())) {
      query.distinct(true);
      propertyTags = root.join("tags", JoinType.LEFT);
      Join<Tags, List<ImageAnimal>> propertyImageAnimal =
          propertyTags.joinList("imageAnimals", JoinType.LEFT);
      Expression<String> expression = propertyImageAnimal.get("name");
      return expression.in((List<String>) criteria.getValue());
    }
    if ("imageMainColours".equalsIgnoreCase(criteria.getKey())) {
      query.distinct(true);
      propertyTags = root.join("tags", JoinType.LEFT);
      Join<Tags, List<ImageMainColour>> propertyImageMainColour =
          propertyTags.joinList("imageMainColours", JoinType.LEFT);
      Expression<String> expression = propertyImageMainColour.get("name");
      return expression.in((List<String>) criteria.getValue());
    }
    if ("imageText".equalsIgnoreCase(criteria.getKey())) {
      propertyTags = root.join("tags", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyTags.get("imageText"));
      } else {
        return builder.like(propertyTags.get("imageText"), "%" + criteria.getValue() + "%");
      }
    }
    if ("otherTags".equalsIgnoreCase(criteria.getKey())) {
      query.distinct(true);
      propertyTags = root.join("tags", JoinType.LEFT);
      Join<Tags, List<OtherTag>> propertyOtherTags =
          propertyTags.joinList("otherTags", JoinType.LEFT);
      Expression<String> expression = propertyOtherTags.get("name");
      return expression.in((List<String>) criteria.getValue());
    }
    if ("autoOrientation".equalsIgnoreCase(criteria.getKey())) {
      boolean isOrientationValid =
          EnumUtils.isValidEnum(Orientation.class, criteria.getValue().toString());
      query.distinct(true);
      propertyTags = root.join("tags", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyTags.get("autoOrientation"));
      } else if (isOrientationValid) {
        return builder.equal(propertyTags.get("autoOrientation"),
            Orientation.valueOf(criteria.getValue().toString()));
      }
    }

    if ("autoScale".equalsIgnoreCase(criteria.getKey())) {
      RangeDTO autoScaleRange = (RangeDTO) criteria.getValue();
      List<Scale> list =
          Arrays.asList(Scale.XXS, Scale.XS, Scale.S, Scale.M, Scale.L, Scale.XL, Scale.XXL);
      Scale lowerBound =
          autoScaleRange.getMinValue() != null && !autoScaleRange.getMinValue().isEmpty()
              ? Scale.valueOf(autoScaleRange.getMinValue())
              : null;
      Scale higherBound =
          autoScaleRange.getMaxValue() != null && !autoScaleRange.getMaxValue().isEmpty()
              ? Scale.valueOf(autoScaleRange.getMaxValue())
              : null;
      List<Scale> filteredList = list.stream()
          .filter(lowerBound != null ? p -> p.compareTo(lowerBound) >= 0 : Objects::nonNull)
          .filter(higherBound != null ? p -> p.compareTo(higherBound) <= 0 : Objects::nonNull)
          .collect(Collectors.toList());
      propertyTags = root.join("tags", JoinType.LEFT);
      return query.where(propertyTags.get(criteria.getKey()).in(filteredList)).getRestriction();
    }

    if ("autoImage".equalsIgnoreCase(criteria.getKey())) {
      propertyTags = root.join("tags", JoinType.LEFT);
      return builder.equal(propertyTags.get("autoImage"), criteria.getValue());
    }
    if ("autoVolumeCm".equalsIgnoreCase(criteria.getKey())) {
      propertyTags = root.join("tags", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyTags.get("autoVolume"));
      } else {
        return builder.equal(propertyTags.get("autoVolume"),
            PropertyUtil.convertStringToBigDecimal(criteria.getValue().toString()));
      }
    }
    if ("autoVolumeIn".equalsIgnoreCase(criteria.getKey())) {
      propertyTags = root.join("tags", JoinType.LEFT);
      if (ASTERISK.equals(criteria.getValue())) {
        return builder.isNotNull(propertyTags.get("autoVolume"));
      } else {
        propertyDimensions = root.join("dimensions", JoinType.LEFT);

        BigDecimal areaVolumeIn =
            PropertyUtil.convertStringToBigDecimal(criteria.getValue().toString());
        BigDecimal multiplicationFactor = new BigDecimal(2.54);
        BigDecimal volumeCm =
            (areaVolumeIn.multiply(multiplicationFactor).multiply(multiplicationFactor)
                .multiply(multiplicationFactor)).setScale(0, RoundingMode.HALF_UP);
        BigDecimal areaCm =
            (areaVolumeIn.multiply(multiplicationFactor).multiply(multiplicationFactor)).setScale(0,
                RoundingMode.HALF_UP);

        Expression<BigDecimal> roundValue =
            builder.function("ROUND", BigDecimal.class, propertyTags.get("autoVolume"));

        Predicate predVolumeVal = builder.equal(roundValue, volumeCm);
        Predicate predAreaVal = builder.equal(roundValue, areaCm);

        Predicate predHeightNotNull = propertyDimensions.get("height").isNotNull();
        Predicate predWidthNotNull = propertyDimensions.get("width").isNotNull();
        Predicate predDepthNotNull = propertyDimensions.get("depth").isNotNull();
        Predicate predHeightNull = propertyDimensions.get("height").isNull();
        Predicate predWidthNull = propertyDimensions.get("width").isNull();
        Predicate predDepthNull = propertyDimensions.get("depth").isNull();

        Predicate predVol =
            builder.and(predVolumeVal, predWidthNotNull, predHeightNotNull, predDepthNotNull);

        Predicate pred1 = builder.and(predHeightNotNull, predWidthNotNull, predDepthNull);
        Predicate pred2 = builder.and(predHeightNotNull, predWidthNull, predDepthNotNull);
        Predicate pred3 = builder.and(predHeightNull, predWidthNotNull, predDepthNotNull);
        Predicate predAreaDim = builder.or(pred1, pred2, pred3);

        Predicate predArea = builder.and(predAreaVal, predAreaDim);

        return builder.or(predVol, predArea);
      }
    }

    switch (criteria.getOperation()) {
      case EQUALITY:
        if (ASTERISK.equals(criteria.getValue())) {
          return builder.isNotNull(root.<String>get(criteria.getKey()));
        } else {
          return builder.equal(root.get(criteria.getKey()), criteria.getValue());
        }
      case NEGATION:
        return builder.notEqual(root.get(criteria.getKey()), criteria.getValue());
      case GREATER_THAN:
        return builder.greaterThan(root.<String>get(criteria.getKey()),
            criteria.getValue().toString());
      case LESS_THAN:
        return builder.lessThan(root.<String>get(criteria.getKey()),
            criteria.getValue().toString());
      case LIKE:
        if (ASTERISK.equals(criteria.getValue())) {
          return builder.isNotNull(root.<String>get(criteria.getKey()));
        } else {
          return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
        }
      case STARTS_WITH:
        return builder.like(root.<String>get(criteria.getKey()), criteria.getValue() + "%");
      case ENDS_WITH:
        return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue());
      case CONTAINS:
        return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
      default:
        return null;
    }
  }

  private Predicate getRangedOperationForRoot(final Root<Property> root,
      final CriteriaBuilder builder, RangeDTO partsRange) {
    Predicate result = null;
    BigDecimal minValue = PropertyUtil.convertStringToBigDecimal(partsRange.getMinValue());
    BigDecimal maxValue = PropertyUtil.convertStringToBigDecimal(partsRange.getMaxValue());
    if (minValue == null && maxValue == null && partsRange.getMinValue() != null
        && partsRange.getMaxValue() != null)
      throw new BadRequestException("Not valid input " + partsRange.getMinValue());
    if (validateRangeForBetweenOperation(partsRange)) {
      result = builder.between(root.get(criteria.getKey()), minValue, maxValue);
    } else if (validateRangeForGreaterThatOrEqualOperation(partsRange)) {
      result = builder.greaterThanOrEqualTo(root.get(criteria.getKey()), minValue);
    } else if (validateRangeForLessThanOrEqualOperation(partsRange)) {
      result = builder.lessThanOrEqualTo(root.get(criteria.getKey()), maxValue);
    } else if (validateRangeForIsNotNullField(partsRange)) {
      result = builder.isNotNull(root.get(criteria.getKey()));
    }
    return result;
  }

  private Predicate getRangeOperation(final CriteriaBuilder builder, Join<Property, ?> property,
      RangeDTO range) {
    Predicate result = null;
    BigDecimal minValue = PropertyUtil.convertStringToBigDecimal(range.getMinValue());
    BigDecimal maxValue = PropertyUtil.convertStringToBigDecimal(range.getMaxValue());
    if (minValue == null && maxValue == null && range.getMinValue() != null
        && range.getMaxValue() != null)
      throw new BadRequestException("Not valid input " + range.getMinValue());
    if (validateRangeForBetweenOperation(range)) {
      result = builder.between(property.get(criteria.getKey()), minValue, maxValue);
    } else if (validateRangeForGreaterThatOrEqualOperation(range)) {
      result = builder.greaterThanOrEqualTo(property.get(criteria.getKey()), minValue);
    } else if (validateRangeForLessThanOrEqualOperation(range)) {
      result = builder.lessThanOrEqualTo(property.get(criteria.getKey()), maxValue);
    } else if (validateRangeForIsNotNullField(range)) {
      result = builder.isNotNull(property.get(criteria.getKey()));
    }
    return result;
  }

  public boolean validateRangeForIsNotNullField(RangeDTO heightRange) {
    return heightRange.getMinValue() == null && heightRange.getMaxValue() == null;
  }

  public boolean validateRangeForLessThanOrEqualOperation(RangeDTO heightRange) {
    return heightRange.getMinValue() == null && heightRange.getMaxValue() != null;
  }

  public boolean validateRangeForGreaterThatOrEqualOperation(RangeDTO heightRange) {
    return heightRange.getMinValue() != null && heightRange.getMaxValue() == null;
  }

  public boolean validateRangeForBetweenOperation(RangeDTO heightRange) {
    return heightRange.getMinValue() != null && heightRange.getMaxValue() != null;
  }

  private Predicate getPredicateBetweenRange(Join<Property, DateDetails> propertyDateDetailsJoin,
      CriteriaBuilder builder) {
    HashMap<String, Integer> range = (HashMap<String, Integer>) criteria.getValue();
    Iterator<Integer> iterator = range.values().iterator();
    return builder.between(propertyDateDetailsJoin.get(criteria.getKey()), iterator.next(),
        iterator.next());
  }

}
