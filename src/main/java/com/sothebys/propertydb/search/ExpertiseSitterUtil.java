package com.sothebys.propertydb.search;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

public class ExpertiseSitterUtil {

	public static boolean isBlank(final Integer parameter) {
		return parameter == null || parameter.intValue() <= 0;
	}

	public static boolean isBlank(final Long parameter) {
		return parameter == null || parameter.longValue() <= 0;
	}

	public static boolean isEmpty(final Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	public static boolean isEmpty(final String parameter) {
		return parameter == null || parameter.trim().isEmpty();
	}

	/**
	 * to validate ExpertiseSitterSearchInputDTO
	 * 
	 * @param sitterDto
	 * @return boolean
	 */
	public static boolean validateSitterDTO(ExpertiseSitterSearchInputDTO sitterDTO) {
		boolean dosearch = true;
		if (StringUtils.isEmpty(sitterDTO.getFirstName()) && StringUtils.isEmpty(sitterDTO.getLastName())
				&& StringUtils.isEmpty(sitterDTO.getName())) {
			dosearch = false;
		}
		return dosearch;
	}
}
