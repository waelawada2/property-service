package com.sothebys.propertydb.search;

import java.util.List;
import lombok.Data;

/**
 * Created by aneesha.l
 */
@Data
public class TagsInputDTO {

  private String imageText;

  private String imageFigure;

  private String expertiseLocation;

  private List<String> imageSubjects;

  private List<String> imageGenders;

  private List<String> imagePositions;

  private List<String> imageAnimals;

  private List<String> imageMainColours;

  private List<Long> expertiseSitters;

  private List<String> otherTags;

  private String autoOrientation;
  
  private String autoScale;
  
  private Boolean autoImage;

  private String autoVolume;

  private String autoVolumeUnit;

}
