package com.sothebys.propertydb.search;

import com.sothebys.propertydb.dto.RangeDTO;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;


@Slf4j
public class PropertyUtil {

  private static final String END_YEAR = "endYear";
  private static final String DATE_FORMAT = "yyyy-MM-dd";
  private static final String DATE_FORMAT_MMddYYYY = "MM-dd-yyyy";
  private static final String YEAR_RANGE_REGEX = "\\d{4}\\.\\.\\.\\d{4}";
  private static final Pattern YEAR_RANGE_PATTERN = Pattern.compile(YEAR_RANGE_REGEX);
  private static final String START_YEAR = "startYear";

  public static boolean isBlank(final Float parameter) {
    return parameter == null || parameter.floatValue() <= 0;
  }

  public static boolean isBlank(final BigDecimal parameter) {
    return parameter == null || parameter.doubleValue() <= 0;
  }

  public static boolean isBlank(final Integer parameter) {
    return parameter == null || parameter.intValue() <= 0;
  }

  public static boolean isBlank(final Long parameter) {
    return parameter == null || parameter.longValue() <= 0;
  }

  public static boolean isEmpty(final Collection<?> collection) {
    return collection == null || collection.isEmpty();
  }

  public static boolean isEmpty(final Date parameter) {
    return parameter == null;
  }

  /**
   * to validate PropertySearchInputDTO
   *
   * @return boolean
   */
  public static boolean validatePropertyDTO(PropertySearchInputDTO propertyDTO) {
    boolean dosearch = true;
    if (isEmpty(propertyDTO.getArchiveNumbers()) && isEmpty(propertyDTO.getArtistIds())
        && (propertyDTO.getAuthentication() == null) && (propertyDTO.getCastYearCirca() == null)
        && StringUtils.isEmpty(propertyDTO.getCastYearStart())
        && StringUtils.isEmpty(propertyDTO.getCastYearEnd())
        && StringUtils.isEmpty(propertyDTO.getCategory()) && (propertyDTO.getCertificate() == null)
        && (propertyDTO.getYearCirca() == null) && StringUtils.isEmpty(propertyDTO.getDepthCm())
        && StringUtils.isEmpty(propertyDTO.getEditionName())
        && StringUtils.isEmpty(propertyDTO.getEditionNumber())
        && StringUtils.isEmpty(propertyDTO.getExternalId())
        && StringUtils.isEmpty(propertyDTO.getFoundry()) && StringUtils
        .isEmpty(propertyDTO.getHeightCm())
        && isBlank(propertyDTO.getId()) && StringUtils.isEmpty(propertyDTO.getMedium())
        && StringUtils.isEmpty(propertyDTO.getOriginalTitle()) && StringUtils
        .isEmpty(propertyDTO.getParts())
        && (propertyDTO.getPosthumous() == null)
        && StringUtils.isEmpty(propertyDTO.getPresizeName())
        && isEmpty(propertyDTO.getNumbers())
        && StringUtils.isEmpty(propertyDTO.getVolume())
        && StringUtils.isEmpty(propertyDTO.getReminderEmployee())
        && StringUtils.isEmpty(propertyDTO.getReminderNotesText())
        && StringUtils.isEmpty(propertyDTO.getResearcherName())
        && StringUtils.isEmpty(propertyDTO.getResearchCompleteCheck()) && (
        propertyDTO.getResearchCompleteCheck() == null)
        && StringUtils.isEmpty(propertyDTO.getResearcherNotesDate())
        && StringUtils.isEmpty(propertyDTO.getSignature()) && (propertyDTO.getSigned() == null)
        && StringUtils.isEmpty(propertyDTO.getSortCode()) && (propertyDTO.getStamped() == null)
        && StringUtils.isEmpty(propertyDTO.getSubCategory())
        && StringUtils.isEmpty(propertyDTO.getTitle()) && (propertyDTO.getUniqueEdition() == null)
        && StringUtils.isEmpty(propertyDTO.getWeight()) && StringUtils
        .isEmpty(propertyDTO.getWidthCm())
        && StringUtils.isEmpty(propertyDTO.getYearStart()) && StringUtils
        .isEmpty(propertyDTO.getYearEnd())
        && StringUtils.isEmpty(propertyDTO.getYearText())
        && StringUtils.isEmpty(propertyDTO.getUploadReference()) && (propertyDTO.getFlags() == null)
        && propertyDTO.getUnknownEditionSizeType() == null
        && StringUtils.isEmpty(propertyDTO.getEditionSizeTotal())
        && propertyDTO.getSizeText() == null
        && isBlank(propertyDTO.getCatalogueRaisoneeId())
        && isEmpty(propertyDTO.getCurrentOwnerIds())
        && isEmpty(propertyDTO.getArtistCatalogueRaisoneeIds())
        && StringUtils.isEmpty(propertyDTO.getArtistCatalogueRaisoneeAuthor())
        && ((propertyDTO.getTags() == null)
        || (StringUtils.isEmpty(propertyDTO.getTags().getExpertiseLocation())
        && propertyDTO.getTags().getExpertiseSitters() == null
        && propertyDTO.getTags().getImageAnimals() == null
        && StringUtils.isEmpty(propertyDTO.getTags().getImageFigure())
        && propertyDTO.getTags().getImageGenders() == null
        && propertyDTO.getTags().getImageMainColours() == null
        && propertyDTO.getTags().getImagePositions() == null
        && propertyDTO.getTags().getImageSubjects() == null && StringUtils
        .isEmpty(propertyDTO.getTags().getImageText())
        && propertyDTO.getTags().getOtherTags() == null
        && propertyDTO.getTags().getAutoImage() == null
        && propertyDTO.getTags().getAutoOrientation() == null
        && propertyDTO.getTags().getAutoScale() == null
        && propertyDTO.getTags().getAutoVolume() == null))) {

      dosearch = false;
    }
    return dosearch;
  }

  private static Date convertDateYearFormat(final Date date) {
    Date convertedDate = null;
    DateTimeFormatter formatter =
        DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
    LocalDate localDate = LocalDate.parse(date.toString(), formatter);
    if (null != localDate) {
      convertedDate = java.sql.Date.valueOf(localDate);
    }
    return convertedDate;
  }

  /**
   * This method used to convert date format without timezone format
   *
   * @return date
   */
  public static Date dateFormat(final Date dateString) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
    String formatDate = null;
    Date date = null;
    try {
      formatDate = dateFormat.format(dateString);
      if (!StringUtils.isEmpty(formatDate)) {
        date = dateFormat.parse(formatDate);
      }
    } catch (ParseException e) {
      log.warn("Could not parse date '{}'", dateString);
    }
    return date;
  }

  /**
   * This method used to convert String to a date to the format MM-dd-YYYY
   *
   * @return date
   */
  public static Date dateFormatMMddYYYY(final String dateString) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_MMddYYYY);
    Date date = null;
    try {
      if (!StringUtils.isEmpty(dateString)) {
        date = convertDateYearFormat(dateFormat.parse(dateString));
      }
    } catch (ParseException e) {
      log.warn("Could not parse date string '{}'", dateString);
    }
    return date;
  }

  /**
   * This method used to convert String to an Integer
   *
   * @return date
   */
  public static Integer convertStringToInteger(final String intString) {
    Integer num = null;
    try {
      if (!StringUtils.isEmpty(intString)) {
        num = new Integer(Integer.parseInt(intString.trim()));
      }
    } catch (NumberFormatException e) {
      log.warn("Could not parse interger '{}'", intString);
    }
    return num;
  }

  /**
   * This method used to convert String to a Big Decimal
   *
   * @return date
   */
  public static BigDecimal convertStringToBigDecimal(final String bigDecimalString) {
    BigDecimal bigDecimal = null;
    try {
      if (!StringUtils.isEmpty(bigDecimalString)) {
        bigDecimal = new BigDecimal(bigDecimalString.trim());
      }
    } catch (NumberFormatException e) {
      log.warn("Not a valid BigDecimal '{}'", bigDecimal);
    }
    return bigDecimal;
  }

  /**
   * This method is used to get the values for range searches
   *
   * @param rangedValue
   * @param splitterString
   * @return
   */
  public static RangeDTO getRangeValues(String rangedValue, String splitterString) {
    RangeDTO range = null;
    if (rangedValue.contains(splitterString) && !rangedValue.equals(splitterString)) {
      String[] Dimensions = rangedValue.split(Pattern.quote(splitterString));
      range = new RangeDTO(
          !Dimensions[0].isEmpty() && !Dimensions[0].equals("*") ? Dimensions[0] : null,
          Dimensions.length > 1 && !Dimensions[1].equals("*") ? Dimensions[1] : null);
    } else {
      String rangeVariable = !rangedValue.equals("*") ? rangedValue : null;
      range = new RangeDTO(rangeVariable , rangeVariable);
    }
    return range;
  }
}
