package com.sothebys.propertydb.search;

import java.util.Objects;

public class SaveSearchUtil {

  public static boolean isEmpty(final String parameter) {
    return parameter == null || parameter.trim().isEmpty();
  }

  public static boolean isBlank(final long parameter) {
    return Objects.isNull(parameter) || parameter <= 0;
  }

  /**
   * to validate SaveSearchInputDTO
   * 
   * @param saveSearchInputDto
   * @return boolean
   */
  public static boolean validateSaveSearchDTO(SaveSearchInputDTO saveSearchInputDto) {
    boolean dosearch = true;
    if (isBlank(saveSearchInputDto.getUserId()) && isEmpty(saveSearchInputDto.getName())) {
      dosearch = false;
    }
    return dosearch;
  }
}
