/**
 * 
 */
package com.sothebys.propertydb.search;

import lombok.Data;

/**
 * This DTO contains the search-able fields for User
 * 
 * @author Manju.Rana
 */
@Data
public class UserSearchInputDTO {
  
  private String firstName;
  
  private String lastName;
  
  private String userName;

  private String email;
}