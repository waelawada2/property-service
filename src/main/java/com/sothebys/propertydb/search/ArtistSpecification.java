package com.sothebys.propertydb.search;

import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.Category;
import com.sothebys.propertydb.model.Country;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class ArtistSpecification implements Specification<Artist> {

  private SearchCriteria criteria;

  public ArtistSpecification(final SearchCriteria criteria) {
    super();
    this.criteria = criteria;
  }

  public SearchCriteria getCriteria() {
    return criteria;
  }

  @Override
  public Predicate toPredicate(final Root<Artist> root, final CriteriaQuery<?> query,
      final CriteriaBuilder builder) {

    if ("countryNames".equalsIgnoreCase(criteria.getKey())) {
      Join<Artist, List<Country>> artistCountry = root.join("countries", JoinType.LEFT);
      query.distinct(true);
      Expression<String> expression = artistCountry.get("countryName");
      return expression.in(criteria.getValue());
    }

    if ("categoryNames".equalsIgnoreCase(criteria.getKey())) {
      List<String> categoryValueList = (List<String>) criteria.getValue();
      Subquery<Artist> subQuery = query.subquery(Artist.class);
      Root<Artist> subQRoot = subQuery.from(Artist.class);
      subQuery.select(subQRoot.get("id"));
      Join<Artist, List<Category>> artistCategory = subQRoot.join("categories", JoinType.LEFT);
      subQuery.groupBy(subQRoot.get("id"));
      Expression<Long> countExp = builder.count(artistCategory);
      subQuery.having(builder.equal(countExp, categoryValueList.size()));
      Expression<String> expression = artistCategory.get("name");
      Predicate catNamePredicate = expression.in(criteria.getValue());
      subQuery.where(builder.or(catNamePredicate));
      query.where(root.get("id").in(subQuery));
      return root.get("id").in(subQuery);
    }

    if ("birthYear".equalsIgnoreCase(criteria.getKey())) {
      String birthYear = (String) criteria.getValue();
      if (birthYear.contains("-")) {
        Integer birthFromYear = null;
        Integer birthToYear = null;
        String[] deathYearArray = birthYear.split("-");
        if (!StringUtils.isEmpty(deathYearArray[0])) {
          birthFromYear = Integer.parseInt(deathYearArray[0]);
        }
        if (!StringUtils.isEmpty(deathYearArray[1])) {
          birthToYear = Integer.parseInt(deathYearArray[1]);
        }
        if (birthFromYear != null && birthToYear != null) {
          return builder.between(root.get(criteria.getKey()), birthFromYear, birthToYear);
        }
      } else {
        return builder.equal(root.get(criteria.getKey()), criteria.getValue());
      }

    }

    if ("deathYear".equalsIgnoreCase(criteria.getKey())) {
      String deathYear = (String) criteria.getValue();
      if (deathYear.contains("-")) {
        Integer deathFromYear = null;
        Integer deathToYear = null;
        String[] deathYearArray = deathYear.split("-");
        if (!StringUtils.isEmpty(deathYearArray[0])) {
          deathFromYear = Integer.parseInt(deathYearArray[0]);
        }
        if (!StringUtils.isEmpty(deathYearArray[0])) {
          deathToYear = Integer.parseInt(deathYearArray[0]);
        }
        if (deathFromYear != null && deathToYear != null) {
          return builder.between(root.get(criteria.getKey()), deathFromYear, deathToYear);
        }
      } else {
        return builder.equal(root.get(criteria.getKey()), criteria.getValue());
      }
    }
    if ("name".equalsIgnoreCase(criteria.getKey())) {
      Predicate lastNamePredicate = builder.like(root.get("lastName"), criteria.getValue() + "%");
      Predicate firstNamePredicate = builder.like(root.get("firstName"), criteria.getValue() + "%");
      return builder.or(firstNamePredicate, lastNamePredicate);
    }

    if ("firstName".equalsIgnoreCase(criteria.getKey())) {
      Predicate lastNamePredicate =
          builder.like(root.get("lastName"), "%" + criteria.getValue() + "%");
      Predicate firstNamePredicate =
          builder.like(root.get("firstName"), "%" + criteria.getValue() + "%");
      return builder.or(firstNamePredicate, lastNamePredicate);
    }

    if ("lastName".equalsIgnoreCase(criteria.getKey())) {
      Predicate lastNamePredicate =
          builder.like(root.get("lastName"), "%" + criteria.getValue() + "%");
      Predicate firstNamePredicate =
          builder.like(root.get("firstName"), "%" + criteria.getValue() + "%");
      return builder.or(firstNamePredicate, lastNamePredicate);
    }

    switch (criteria.getOperation()) {
      case EQUALITY:
        return builder.equal(root.get(criteria.getKey()), criteria.getValue());
      case NEGATION:
        return builder.notEqual(root.get(criteria.getKey()), criteria.getValue());
      case GREATER_THAN:
        return builder.greaterThan(root.<String>get(criteria.getKey()),
            criteria.getValue().toString());
      case LESS_THAN:
        return builder.lessThan(root.<String>get(criteria.getKey()),
            criteria.getValue().toString());
      case LIKE:
        return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
      case STARTS_WITH:
        return builder.like(root.<String>get(criteria.getKey()), criteria.getValue() + "%");
      case ENDS_WITH:
        return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue());
      case CONTAINS:
        return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
      default:
        return null;
    }
  }

}
