package com.sothebys.propertydb.search;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.model.SaveSearch;

@Component
public class SaveSearchSpecificationBuilder {

  private final List<SearchCriteria> params;

  public SaveSearchSpecificationBuilder() {
    params = new ArrayList<SearchCriteria>();
  }

  public Specification<SaveSearch> build() {
    if (params.size() == 0) {
      return null;
    }

    List<Specification<SaveSearch>> specs = new ArrayList<Specification<SaveSearch>>();
    for (SearchCriteria param : params) {
      specs.add(new SaveSearchSpecification(param));
    }

    Specification<SaveSearch> result = specs.get(0);
    for (int i = 1; i < specs.size(); i++) {
      result = Specifications.where(result).and(specs.get(i));
    }
    return result;
  }

  public SaveSearchSpecificationBuilder with(String key, String operation, Object value) {
    PropertyOperation op = PropertyOperation.getSimpleOperation(operation.charAt(0));
    params.add(new SearchCriteria(key, op, value));
    return this;
  }
}
