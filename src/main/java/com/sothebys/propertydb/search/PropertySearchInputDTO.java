package com.sothebys.propertydb.search;

import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

/**
 * dto to search properties
 *
 * @author SrinivasaRao Batthula
 */
@Data
public class PropertySearchInputDTO {

  private List<String> archiveNumbers;

  @NotNull
  @Size(min = 1)
  private List<String> artistIds;

  private Boolean authentication;

  private Boolean castYearCirca;

  private String castYearStart;

  private String castYearEnd;

  @Size(max = 250)
  private String category;

  private Boolean certificate;

  private Boolean yearCirca;

  private String depthCm;

  private String editionName; // TODO: Can be removed?

  private String editionNumber;

  @Size(max = 250)
  private String foundry;

  private String heightCm;

  @Size(max = 1000)
  private String medium;

  @Size(max = 500)
  private String originalTitle;

  private String parts;

  private Boolean posthumous;

  @Size(max = 250)
  private String presizeName;

  private List<String> numbers;

  private String volume;

  @Size(max = 200)
  private String reminderEmployee;

  @Size(max = 200)
  private String reminderNotesText;

  @Size(max = 200)
  private String researcherName;

  private String researcherNotesDate;

  @Size(max = 1000, message = "Researcher notes text can not be greater than {max} characters")
  private String researcherNotesText;
  
  private Boolean researchCompleteCheck;

  private Integer seriesTotal;

  @Size(max = 1000, message = "Signature can not be greater than 1000 characters")
  private String signature;

  private Boolean signed;

  @Size(max = 200)
  private String sortCode;

  private Boolean stamped;

  @Size(max = 250)
  private String subCategory;

  @Size(max = 500)
  private String title;

  private Boolean uniqueEdition;

  private String uploadReference;

  private String weight;

  private String widthCm;

  private String yearStart;

  private String yearEnd;

  @Size(max = 200)
  private String yearText;

  private Boolean unknownEditionSizeType;

  private Long id;

  private String externalId;

  @Size(max = 500)
  private String sizeText;

  private String editionSizeTotal;

  @Size(max = 200)
  private FlagsInputDTO flags;

  private Long catalogueRaisoneeId;

  private List<String> artistCatalogueRaisoneeIds;

  private List<String> currentOwnerIds;
  
  private String artistCatalogueRaisoneeAuthor;

  @Size(max = 500)
  private TagsInputDTO tags;
}
