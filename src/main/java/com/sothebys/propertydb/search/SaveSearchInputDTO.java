package com.sothebys.propertydb.search;

import lombok.Data;

/**
 * @author SrinivasaRao Batthula
 *
 */

@Data
public class SaveSearchInputDTO {

  private String name;

  private long userId;

}
