package com.sothebys.propertydb.search;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.model.ExpertiseSitter;

@Component
public class ExpertiseSitterSpecificationBuilder {

  private final List<SearchCriteria> params;

  public ExpertiseSitterSpecificationBuilder() {
    params = new ArrayList<SearchCriteria>();
  }

  public Specification<ExpertiseSitter> build() {
    if (params.size() == 0) {
      return null;
    }

    List<Specification<ExpertiseSitter>> specs = new ArrayList<Specification<ExpertiseSitter>>();
    for (SearchCriteria param : params) {
      specs.add(new ExpertiseSitterSpecification(param));
    }

    Specification<ExpertiseSitter> result = specs.get(0);
    for (int i = 1; i < specs.size(); i++) {
      result = Specifications.where(result).and(specs.get(i));
      // result = Specifications.where(result).or(specs.get(i));
    }
    return result;
  }

  public ExpertiseSitterSpecificationBuilder with(String key, String operation, Object value) {
    PropertyOperation op = PropertyOperation.getSimpleOperation(operation.charAt(0));
    params.add(new SearchCriteria(key, op, value));
    return this;
  }
}
