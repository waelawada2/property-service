package com.sothebys.propertydb.search;

import org.springframework.stereotype.Component;

@Component
public class SaveSearchBuilder {

  public SaveSearchSpecificationBuilder createSpecificationBuilder(
      SaveSearchInputDTO saveSearchInputDTO) {
    SaveSearchSpecificationBuilder saveSearchSpecificationBuilder =
        new SaveSearchSpecificationBuilder();

    if (null != saveSearchInputDTO.getName()) {
      saveSearchSpecificationBuilder.with("name", PropertyOperation.Operator.LIKEJ.getOp(),
          saveSearchInputDTO.getName());
    }

    if (!SaveSearchUtil.isBlank(saveSearchInputDTO.getUserId())) {
      saveSearchSpecificationBuilder.with("userId", PropertyOperation.Operator.EQJ.getOp(),
          saveSearchInputDTO.getUserId());
    }

    return saveSearchSpecificationBuilder;
  }

}
