/**
 * 
 */
package com.sothebys.propertydb.search;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * This class helps to create the userSpecification Builder object for the fields available in UserSearchInputDTO.
 * 
 * @author Manju.Rana
 */
@Component
public class UserSearchBuilder {

  public UserSpecificationBuilder createSpecificationBuilder(
      UserSearchInputDTO userSearchInputDto) {
    UserSpecificationBuilder userSpecificationBuilder = new UserSpecificationBuilder();

    if (!StringUtils.isEmpty(userSearchInputDto.getFirstName())) {
      userSpecificationBuilder.with("firstName", PropertyOperation.Operator.STARTS_WITH.getOp(),
          userSearchInputDto.getFirstName());
    }

    if (!StringUtils.isEmpty(userSearchInputDto.getLastName())) {
      userSpecificationBuilder.with("lastName", PropertyOperation.Operator.STARTS_WITH.getOp(),
          userSearchInputDto.getLastName());
    }

    if (!StringUtils.isEmpty(userSearchInputDto.getUserName())) {
      userSpecificationBuilder.with("userName", PropertyOperation.Operator.STARTS_WITH.getOp(),
          userSearchInputDto.getUserName());
    }

    if (!StringUtils.isEmpty(userSearchInputDto.getEmail())) {
      userSpecificationBuilder.with("email", PropertyOperation.Operator.STARTS_WITH.getOp(),
          userSearchInputDto.getEmail());
    }
    return userSpecificationBuilder;
  }
}
