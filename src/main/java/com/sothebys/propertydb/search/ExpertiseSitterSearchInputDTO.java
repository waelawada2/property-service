package com.sothebys.propertydb.search;

import lombok.Data;

/**
 * @author nagaraju
 *
 */

@Data
public class ExpertiseSitterSearchInputDTO {

  private String firstName;

  private String lastName;
  
  private String name;

}
