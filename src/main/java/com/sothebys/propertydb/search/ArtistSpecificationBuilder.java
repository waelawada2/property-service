package com.sothebys.propertydb.search;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.model.Artist;

@Component
public class ArtistSpecificationBuilder {

  private final List<SearchCriteria> params;

  public ArtistSpecificationBuilder() {
    params = new ArrayList<SearchCriteria>();
  }

  public Specification<Artist> build() {
    if (params.size() == 0) {
      return null;
    }

    List<Specification<Artist>> specs = new ArrayList<Specification<Artist>>();
    for (SearchCriteria param : params) {
      specs.add(new ArtistSpecification(param));
    }

    Specification<Artist> result = specs.get(0);
    for (int i = 1; i < specs.size(); i++) {
      result = Specifications.where(result).and(specs.get(i));
      // result = Specifications.where(result).or(specs.get(i));
    }
    return result;
  }

  public ArtistSpecificationBuilder with(String key, String operation, Object value) {
    PropertyOperation op = PropertyOperation.getSimpleOperation(operation.charAt(0));
    params.add(new SearchCriteria(key, op, value));
    return this;
  }
}
