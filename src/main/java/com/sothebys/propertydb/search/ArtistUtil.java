package com.sothebys.propertydb.search;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

public class ArtistUtil {

  public static boolean isBlank(final Integer parameter) {
    return parameter == null || parameter.intValue() <= 0;
  }

  public static boolean isBlank(final Long parameter) {
    return parameter == null || parameter.longValue() <= 0;
  }

  public static boolean isEmpty(final Collection<?> collection) {
    return collection == null || collection.isEmpty();
  }

  public static boolean isEmpty(final String parameter) {
    return parameter == null || parameter.trim().isEmpty();
  }

  /**
   * to validate ArtistSearchInputDTO
   * 
   * @param artistDto
   * @return boolean
   */
  public static boolean validateArtistDTO(ArtistSearchInputDTO artistDto) {
    boolean dosearch = true;
    if (isBlank(artistDto.getId()) && null == artistDto.getApproved()
        && StringUtils.isEmpty(artistDto.getBirthYear()) && StringUtils.isEmpty(artistDto.getDeathYear())
        && StringUtils.isEmpty(artistDto.getFirstName()) && StringUtils.isEmpty(artistDto.getLastName())
        && isEmpty(artistDto.getCategoryNames()) && StringUtils.isEmpty(artistDto.getExternalId())
        && StringUtils.isEmpty(artistDto.getGender()) && isEmpty(artistDto.getCountryNames())
        && StringUtils.isEmpty(artistDto.getDisplay()) && StringUtils.isEmpty(artistDto.getName())) {
      dosearch = false;
    }
    return dosearch;
  }
}
