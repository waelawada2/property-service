/**
 * 
 */
package com.sothebys.propertydb.search;

import org.apache.commons.lang3.StringUtils;

/**
 * This class provides utility methods for User DTO.
 * 
 * @author Manju.Rana
 *
 */
public class UserUtil {

  /**
   * This method validates the UserSearchInputDTO.
   * 
   * @param userSearchInputDTO
   * @return
   */
  public static boolean validateUserSearchInputDTO(UserSearchInputDTO userSearchInputDTO) {
    boolean performSearch = true;
    if (StringUtils.isEmpty(userSearchInputDTO.getFirstName())
        && StringUtils.isEmpty(userSearchInputDTO.getLastName())
        && StringUtils.isEmpty(userSearchInputDTO.getUserName())
        && StringUtils.isEmpty(userSearchInputDTO.getEmail())) {
      performSearch = false;
    }
    return performSearch;
  }
}
