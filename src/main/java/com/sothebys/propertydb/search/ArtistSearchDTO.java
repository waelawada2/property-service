package com.sothebys.propertydb.search;

import lombok.Data;

/**
 * holds Artist ID info
 *
 * @author nagaraju
 *
 */
@Data
public class ArtistSearchDTO {
  private String id;
}
