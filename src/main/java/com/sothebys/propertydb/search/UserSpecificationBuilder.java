/**
 * 
 */
package com.sothebys.propertydb.search;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

import com.sothebys.propertydb.model.User;

/**
 * This class provides methods to build User Specification
 * 
 * @author Manju.Rana
 */
@Component
public class UserSpecificationBuilder {
  
  private final List<SearchCriteria> params;

  public UserSpecificationBuilder() {
    params = new ArrayList<SearchCriteria>();
  }

  public Specification<User> build() {
    if (params.size() == 0) {
      return null;
    }

    List<Specification<User>> specs = new ArrayList<Specification<User>>();
    for (SearchCriteria param : params) {
      specs.add(new UserSpecification(param));
    }
    
    Specification<User> result = specs.get(0);
    for (int i = 1; i < specs.size(); i++) {
      result = Specifications.where(result).and(specs.get(i));
    }
    return result;
  }

  public UserSpecificationBuilder with(String key, String operation, Object value) {
    PropertyOperation op = PropertyOperation.getSimpleOperation(operation.charAt(0));
    params.add(new SearchCriteria(key, op, value));
    return this;
  }

}
