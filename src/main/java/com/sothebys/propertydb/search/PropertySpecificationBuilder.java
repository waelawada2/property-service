package com.sothebys.propertydb.search;

import com.sothebys.propertydb.model.Property;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

@Component
public class PropertySpecificationBuilder {

  private final List<SearchCriteria> params;
  List<Specification<Property>> specs;

  public PropertySpecificationBuilder() {
    params = new ArrayList<SearchCriteria>();
    specs = new ArrayList<Specification<Property>>();
  }
    
  public Specification<Property> build() {
    if (params.size() == 0) {
      return null;
    }
     
    for (SearchCriteria param : params) {
      specs.add(new PropertySpecification(param));
    }

    Specification<Property> result = specs.get(0);
    for (int i = 1; i < specs.size(); i++) {
      result = Specifications.where(result).and(specs.get(i));
      // result = Specifications.where(result).or(specs.get(i));
    }
    return result;
  }

  public PropertySpecificationBuilder with(String key, String operation, Object value) {
    PropertyOperation op = PropertyOperation.getSimpleOperation(operation.charAt(0));
    params.add(new SearchCriteria(key, op, value));    
    return this;
  }  
  
  
}
  
  