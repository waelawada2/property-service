// Copyright (c) 2018 Sotheby's, Inc.
package com.sothebys.propertydb.search;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author jsilva.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientSearchInputDTO {

  private String clientCountry;

  private String clientCity;

  private String clientState;

  private String clientRegion;

  private String clientMarketingSegmentName;

  private String clientName;

  private Integer clientLevel;

  private String clientIndividualFirstName;

  private String clientIndividualMiddleName;

  private String clientIndividualLastName;

  private String clientIndividualGenderName;

  @JsonFormat(pattern = "MM-dd-yyyy")
  @DateTimeFormat(pattern = "MM-dd-yyyy")
  private Date clientIndividualBirthDate;

  private String clientIndividualAge;

  private Boolean clientIndividualDeceased;

  private String clientMainClientName;

  private String clientTypeName;

  private String clientStatusName;

  private String clientKeyClientClientManagerName;

  private String clientKeyClientManagerExternalId;


}
