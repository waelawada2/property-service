package com.sothebys.propertydb.search;

import org.springframework.stereotype.Component;

@Component
public class ArtistSearchBuilder {

  public ArtistSpecificationBuilder createSpecificationBuilder(ArtistSearchInputDTO artistDto) {
    ArtistSpecificationBuilder artistSpecificationBuilder = new ArtistSpecificationBuilder();
    if (!ArtistUtil.isBlank(artistDto.getId())) {
      artistSpecificationBuilder.with("id", PropertyOperation.Operator.EQJ.getOp(),
          artistDto.getId());
    }
    if (!ArtistUtil.isEmpty(artistDto.getExternalId())) {
      artistSpecificationBuilder.with("externalId", PropertyOperation.Operator.EQJ.getOp(),
          artistDto.getExternalId());
    }

    if (!ArtistUtil.isEmpty(artistDto.getFirstName())) {
      artistSpecificationBuilder.with("firstName", PropertyOperation.Operator.LIKEJ.getOp(),
          artistDto.getFirstName());
    }
    if (!ArtistUtil.isEmpty(artistDto.getLastName())) {
      artistSpecificationBuilder.with("lastName", PropertyOperation.Operator.LIKEJ.getOp(),
          artistDto.getLastName());
    }

    if (!ArtistUtil.isEmpty(artistDto.getBirthYear())) {
      artistSpecificationBuilder.with("birthYear", PropertyOperation.Operator.EQJ.getOp(),
          artistDto.getBirthYear());
    }
    if (!ArtistUtil.isEmpty(artistDto.getDeathYear())) {
      artistSpecificationBuilder.with("deathYear", PropertyOperation.Operator.EQJ.getOp(),
          artistDto.getDeathYear());
    }
    if (!ArtistUtil.isEmpty(artistDto.getDisplay())) {
      artistSpecificationBuilder.with("display", PropertyOperation.Operator.STARTS_WITH.getOp(),
          artistDto.getDisplay());
    }
    if (!ArtistUtil.isEmpty(artistDto.getGender())) {
      artistSpecificationBuilder.with("gender", PropertyOperation.Operator.EQJ.getOp(),
          artistDto.getGender());
    }

    if (null != artistDto.getApproved()) {
      artistSpecificationBuilder.with("approved", PropertyOperation.Operator.EQJ.getOp(),
          artistDto.getApproved());
    }
    if (null != artistDto.getName()) {
      artistSpecificationBuilder.with("name", PropertyOperation.Operator.EQJ.getOp(),
          artistDto.getName());
    }

    if (null != artistDto.getCountryNames() && !ArtistUtil.isEmpty(artistDto.getCountryNames())) {
      artistSpecificationBuilder.with("countryNames", PropertyOperation.Operator.EQJ.getOp(),
          artistDto.getCountryNames());
    }

    if (!ArtistUtil.isEmpty(artistDto.getCategoryNames())) {
      artistSpecificationBuilder.with("categoryNames", PropertyOperation.Operator.EQJ.getOp(),
          artistDto.getCategoryNames());
    }

    artistSpecificationBuilder.with("status", PropertyOperation.Operator.EQJ.getOp(),
        Status.Active);

    return artistSpecificationBuilder;
  }

}
