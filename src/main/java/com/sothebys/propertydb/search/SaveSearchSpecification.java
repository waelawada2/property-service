package com.sothebys.propertydb.search;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.sothebys.propertydb.model.SaveSearch;

public class SaveSearchSpecification implements Specification<SaveSearch> {

  private SearchCriteria criteria;

  public SaveSearchSpecification(final SearchCriteria criteria) {
    super();
    this.criteria = criteria;
  }

  public SearchCriteria getCriteria() {
    return criteria;
  }

  @Override
  public Predicate toPredicate(final Root<SaveSearch> root, final CriteriaQuery<?> query,
      final CriteriaBuilder builder) {
    query.distinct(true);

    switch (criteria.getOperation()) {
      case EQUALITY:
        return builder.equal(root.get(criteria.getKey()), criteria.getValue());
      case LIKE:
        return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
      default:
        return null;
    }
  }

}
