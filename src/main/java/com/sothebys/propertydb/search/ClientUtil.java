// Copyright (c) 2018 Sotheby's, Inc.
package com.sothebys.propertydb.search;

import java.text.SimpleDateFormat;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

/**
 * @author jsilva.
 */
public class ClientUtil {

  public static MultiValueMap<String, String> validateClientSearchInputDTO(ClientSearchInputDTO clientDTO) {

    final MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

    if (!StringUtils.isEmpty(clientDTO.getClientName())) {
      map.add("name", clientDTO.getClientName());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientCity())) {
      map.add("city", clientDTO.getClientCity());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientCountry())) {
      map.add("country", clientDTO.getClientCountry());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientRegion())) {
      map.add("region", clientDTO.getClientRegion());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientState())) {
      map.add("state", clientDTO.getClientState());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientTypeName())) {
      map.add("clientType.name", clientDTO.getClientTypeName());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientStatusName())) {
      map.add("clientStatus.name", clientDTO.getClientStatusName());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientIndividualFirstName())) {
      map.add("individual.firstName", clientDTO.getClientIndividualFirstName());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientIndividualMiddleName())) {
      map.add("individual.middleName", clientDTO.getClientIndividualMiddleName());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientIndividualLastName())) {
      map.add("individual.lastName", clientDTO.getClientIndividualLastName());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientIndividualGenderName())) {
      map.add("individual.gender.name", clientDTO.getClientIndividualGenderName());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientIndividualAge())) {
      map.add("agedate", clientDTO.getClientIndividualAge());
    }
    if (clientDTO.getClientIndividualDeceased() != null) {
      map.add("individual.deceased", clientDTO.getClientIndividualDeceased().toString());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientMainClientName())) {
      map.add("mainClient.name", clientDTO.getClientMainClientName());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientMarketingSegmentName())) {
      map.add("marketingSegment.name", clientDTO.getClientMarketingSegmentName());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientKeyClientClientManagerName())) {
      map.add("keyClientManager.name", clientDTO.getClientKeyClientClientManagerName());
    }
    if (!StringUtils.isEmpty(clientDTO.getClientKeyClientManagerExternalId())) {
      map.add("keyClientManager.externalId",
          clientDTO.getClientKeyClientManagerExternalId());
    }
    if (clientDTO.getClientLevel() != null) {
      map.add("level", clientDTO.getClientLevel().toString());
    }
    if (clientDTO.getClientIndividualBirthDate() != null) {
      map.add("individual.birthDate",
          new SimpleDateFormat("MM-dd-YYYY").format(clientDTO.getClientIndividualBirthDate()));
    }
    return map;
  }
}
