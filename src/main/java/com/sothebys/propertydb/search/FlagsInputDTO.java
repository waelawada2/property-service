package com.sothebys.propertydb.search;

import lombok.Data;

/**
 * This class used to hold Flags input information
 *
 * @author VenkataPrasad Tammineni
 *
 */

@Data
public class FlagsInputDTO {

  private Boolean flagAuthenticity;

  private Boolean flagCondition;

  private Boolean flagMedium;

  private Boolean flagRestitution;

  private String flagsCountry;

  private Boolean flagSfs;

}
