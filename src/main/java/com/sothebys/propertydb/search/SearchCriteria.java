package com.sothebys.propertydb.search;

import lombok.Data;

@Data
public class SearchCriteria {

  private String key;
  private PropertyOperation operation;
  private Object value;

  public SearchCriteria() {

  }

  public SearchCriteria(final String key, final PropertyOperation operation, final Object value) {
    super();
    this.key = key;
    this.operation = operation;
    this.value = value;
  }

}
