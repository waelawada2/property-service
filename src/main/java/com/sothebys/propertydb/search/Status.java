package com.sothebys.propertydb.search;

public enum Status {
  Active, Deleted, Merged;

}
