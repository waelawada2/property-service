package com.sothebys.propertydb.search;

import org.springframework.stereotype.Component;

@Component
public class ExpertiseSitterSearchBuilder {

	public ExpertiseSitterSpecificationBuilder createSpecificationBuilder(
			ExpertiseSitterSearchInputDTO expertiseSitterDTO) {
		ExpertiseSitterSpecificationBuilder expertiseSitterSpecificationBuilder = new ExpertiseSitterSpecificationBuilder();

		if (!ExpertiseSitterUtil.isEmpty(expertiseSitterDTO.getFirstName())) {
			expertiseSitterSpecificationBuilder.with("firstName", PropertyOperation.Operator.STARTS_WITH.getOp(),
					expertiseSitterDTO.getFirstName());
		}
		if (!ExpertiseSitterUtil.isEmpty(expertiseSitterDTO.getLastName())) {
			expertiseSitterSpecificationBuilder.with("lastName", PropertyOperation.Operator.STARTS_WITH.getOp(),
					expertiseSitterDTO.getLastName());
		}

		if (null != expertiseSitterDTO.getName()) {
			expertiseSitterSpecificationBuilder.with("name", PropertyOperation.Operator.EQJ.getOp(),
					expertiseSitterDTO.getName());
		}
		return expertiseSitterSpecificationBuilder;
	}

}
