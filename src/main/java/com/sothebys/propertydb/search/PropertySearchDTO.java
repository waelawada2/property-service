package com.sothebys.propertydb.search;

import lombok.Data;

/**
 * holds Object ID info
 *
 * @author nagaraju
 *
 */
@Data
public class PropertySearchDTO {
  private String id;

}
