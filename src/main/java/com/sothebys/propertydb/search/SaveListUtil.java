package com.sothebys.propertydb.search;

import java.util.Objects;

import org.springframework.stereotype.Component;

import liquibase.util.StringUtils;

@Component
public class SaveListUtil {

  public static boolean isBlank(final long parameter) {
    return Objects.isNull(parameter) || parameter <= 0;
  }

  /**
   * to validate SaveListInputDTO
   * 
   * @param saveListInputDTO
   * @return boolean
   */
  public boolean validate(SaveListInputDTO saveListInputDTO) {
    boolean dosearch = true;
    if (StringUtils.isEmpty(saveListInputDTO.getName())) {
      dosearch = false;
    }
    return dosearch;
  }
}
