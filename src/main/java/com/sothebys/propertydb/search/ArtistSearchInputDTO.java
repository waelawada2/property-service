package com.sothebys.propertydb.search;

import java.util.List;

import lombok.Data;

/**
 * @author nagaraju
 *
 */

@Data
public class ArtistSearchInputDTO {

  private Boolean approved;

  private String birthYear;

  private List<String> categoryNames;

  private String deathYear;

  private String display;

  private String externalId;

  private String firstName;

  private String gender;

  private Long id;

  private String lastName;

  private List<String> countryNames;
  private String name;

}
