package com.sothebys.propertydb.search;

public enum PropertyOperation {

  EQUALITY, NEGATION, GREATER_THAN, LESS_THAN, LIKE, STARTS_WITH, ENDS_WITH, CONTAINS, BETWEEN;

  public enum Operator {
    LIKE("LIKE "), IN("IN "), BETWEEN("BETWEEN "), NE("!= "), GT("> "), LT("< "), GE(">= "), LE(
        "=> "), EQJ(":"), NEJ("!"), LIKEJ("~"), STARTS_WITH("%");

    /**
     * Enum contains the like operator patterns.
     */
    public enum LikePattern {
      PERCENT_BEFORE, PERCENT_AFTER, PERCENT_AROUND;
    }

    /**
     * Enum contains the main operators OR & AND.
     */
    public enum OperType {
      AND(" AND "), OR(" OR ");

      private String op;

      private OperType(String op) {
        this.op = op;
      }

      public String getOp() {
        return op;
      }
    }

    public static String adjustLikeValue(String value, LikePattern pattern) {
      switch (pattern) {
        case PERCENT_AFTER:
          return value + "%";
        case PERCENT_BEFORE:
          return "%" + value;
        case PERCENT_AROUND:
          return "%" + value + "%";
      }
      return "";
    }

    private String op;

    private Operator(String op) {
      this.op = op;
    }

    public String getOp() {
      return op;
    }
  }

  public static PropertyOperation getSimpleOperation(final char input) {
    switch (input) {
      case ':':
        return EQUALITY;
      case '!':
        return NEGATION;
      case '>':
        return GREATER_THAN;
      case '<':
        return LESS_THAN;
      case '~':
        return LIKE;
      case '%':
        return STARTS_WITH;
      default:
        return null;
    }
  }
}
