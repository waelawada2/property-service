
package com.sothebys.propertydb.search;

import com.sothebys.propertydb.dto.RangeDTO;
import com.sothebys.propertydb.search.PropertyOperation.Operator;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;


@Component
public class PropertySearchBuilder {

  private static final String THREE_DOTS = "...";
  private static final String SPLITTER_STRING = "-";

  public PropertySpecificationBuilder createSpecificationBuilder(
      PropertySearchInputDTO propertyDto) {

    PropertySpecificationBuilder propertySpecificationBuilder = new PropertySpecificationBuilder();
    if (!CollectionUtils.isEmpty(propertyDto.getArchiveNumbers())) {
      propertySpecificationBuilder.with("archiveNumbers", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getArchiveNumbers());
    }

    if (!PropertyUtil.isBlank(propertyDto.getId())) {
      propertySpecificationBuilder.with("id", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getId());
    }
    if (!StringUtils.isEmpty(propertyDto.getExternalId())) {
      propertySpecificationBuilder.with("externalId", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getExternalId());
    }
    if (!StringUtils.isEmpty(propertyDto.getCastYearStart())) {
      propertySpecificationBuilder.with("castYearStart", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getCastYearStart());
    }
    if (!StringUtils.isEmpty(propertyDto.getCastYearEnd())) {
      propertySpecificationBuilder.with("castYearEnd", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getCastYearEnd());
    }

    if ((!StringUtils.isEmpty(propertyDto.getYearStart()) || !StringUtils
        .isEmpty(propertyDto.getYearEnd()))) {
      String splitterString = propertyDto.getYearStart().contains(THREE_DOTS) ? THREE_DOTS : SPLITTER_STRING;
      RangeDTO yearRange = PropertyUtil.getRangeValues(propertyDto.getYearStart(), splitterString);
       propertySpecificationBuilder.with("yearStart", PropertyOperation.Operator.EQJ.getOp(),
          yearRange);
    }
    if (!StringUtils.isEmpty(propertyDto.getYearEnd())) {
      propertySpecificationBuilder.with("yearEnd", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getYearEnd());
    }

    if (null != propertyDto.getAuthentication()) {
      propertySpecificationBuilder.with("authentication", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getAuthentication());
    }
    if (!StringUtils.isEmpty(propertyDto.getOriginalTitle())) {
      propertySpecificationBuilder.with("originalTitle", PropertyOperation.Operator.LIKEJ.getOp(),
          propertyDto.getOriginalTitle());
    }
    if (!StringUtils.isEmpty(propertyDto.getSignature())) {
      propertySpecificationBuilder.with("signature", PropertyOperation.Operator.LIKEJ.getOp(),
          propertyDto.getSignature());
    }
    if (!StringUtils.isEmpty(propertyDto.getTitle())) {
      propertySpecificationBuilder.with("title", PropertyOperation.Operator.LIKEJ.getOp(),
          propertyDto.getTitle());
    }
    if (!StringUtils.isEmpty(propertyDto.getWeight())) {
      propertySpecificationBuilder.with("weight", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getWeight());
    }

    if (!StringUtils.isEmpty(propertyDto.getHeightCm())) {
      RangeDTO heightRange = PropertyUtil.getRangeValues(propertyDto.getHeightCm(), SPLITTER_STRING);
      propertySpecificationBuilder
          .with("height", PropertyOperation.Operator.EQJ.getOp(), heightRange);
    }

    if (!StringUtils.isEmpty(propertyDto.getWidthCm())) {
      RangeDTO widthRange = PropertyUtil.getRangeValues(propertyDto.getWidthCm(), SPLITTER_STRING);
      propertySpecificationBuilder
          .with("width", PropertyOperation.Operator.EQJ.getOp(), widthRange);
    }
    if (!StringUtils.isEmpty(propertyDto.getDepthCm())) {
      RangeDTO depthRange = PropertyUtil.getRangeValues(propertyDto.getDepthCm(), SPLITTER_STRING);
      propertySpecificationBuilder
          .with("depth", PropertyOperation.Operator.EQJ.getOp(), depthRange);
    }
    if (null != propertyDto.getYearCirca()) {
      propertySpecificationBuilder.with("yearCirca", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getYearCirca());
    }
    if (null != propertyDto.getCastYearCirca()) {
      propertySpecificationBuilder.with("castYearCirca", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getCastYearCirca());
    }
    if (!StringUtils.isEmpty(propertyDto.getResearcherName())) {
      propertySpecificationBuilder.with("researcherName", PropertyOperation.Operator.LIKEJ.getOp(),
          propertyDto.getResearcherName());
    }
    if (!StringUtils.isEmpty(propertyDto.getReminderEmployee())) {
      propertySpecificationBuilder.with("reminderEmployee",
          PropertyOperation.Operator.LIKEJ.getOp(), propertyDto.getReminderEmployee());
    }
    if (!StringUtils.isEmpty(propertyDto.getMedium())) {
      propertySpecificationBuilder.with("medium", PropertyOperation.Operator.LIKEJ.getOp(),
          propertyDto.getMedium());
    }
    if (null != propertyDto.getCertificate()) {
      propertySpecificationBuilder.with("certificate", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getCertificate());
    }
    if (!StringUtils.isEmpty(propertyDto.getParts())) {
      RangeDTO partsRanges = PropertyUtil.getRangeValues(propertyDto.getParts(), SPLITTER_STRING);
      propertySpecificationBuilder
          .with("parts", PropertyOperation.Operator.EQJ.getOp(), partsRanges);
    }
    if (!StringUtils.isEmpty(propertyDto.getFoundry())) {
      propertySpecificationBuilder.with("foundry", PropertyOperation.Operator.LIKEJ.getOp(),
          propertyDto.getFoundry());
    }
    if (null != propertyDto.getSigned()) {
      propertySpecificationBuilder.with("signed", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getSigned());
    }
    if (null != propertyDto.getStamped()) {
      propertySpecificationBuilder.with("stamped", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getStamped());
    }
    if (!StringUtils.isEmpty(propertyDto.getSortCode())) {
      propertySpecificationBuilder.with("sortCode", PropertyOperation.Operator.LIKEJ.getOp(),
          propertyDto.getSortCode());
    }
    if (!StringUtils.isEmpty(propertyDto.getYearText())) {
      propertySpecificationBuilder.with("yearText", PropertyOperation.Operator.LIKEJ.getOp(),
          propertyDto.getYearText());
    }
    if (!StringUtils.isEmpty(propertyDto.getCategory())) {
      propertySpecificationBuilder.with("category", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getCategory());
    }

    if (!StringUtils.isEmpty(propertyDto.getPresizeName())) {
      propertySpecificationBuilder.with("presizeName", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getPresizeName());
    }
    if (!StringUtils.isEmpty(propertyDto.getEditionName())) {
      propertySpecificationBuilder.with("editionName", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getEditionName());
    }
    if (!StringUtils.isEmpty(propertyDto.getEditionNumber())) {
      propertySpecificationBuilder.with("editionNumber", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getEditionNumber());
    }

    if (null != propertyDto.getUniqueEdition()) {
      propertySpecificationBuilder.with("uniqueEdition", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getUniqueEdition());
    }

    if (!StringUtils.isEmpty(propertyDto.getNumbers())) {
      propertySpecificationBuilder.with("number",
          PropertyOperation.Operator.EQJ.getOp(), propertyDto.getNumbers());
    }

    if (!StringUtils.isEmpty(propertyDto.getVolume())) {
      propertySpecificationBuilder.with("volume",
          PropertyOperation.Operator.LIKEJ.getOp(), propertyDto.getVolume());
    }

    if (!CollectionUtils.isEmpty(propertyDto.getArtistIds())) {
      propertySpecificationBuilder.with("artistIds", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getArtistIds());
    }

    if (null != propertyDto.getSizeText()) {
      propertySpecificationBuilder.with("sizeText", PropertyOperation.Operator.LIKEJ.getOp(),
          propertyDto.getSizeText());
    }

    if (null != propertyDto.getPosthumous()) {
      propertySpecificationBuilder.with("posthumous", PropertyOperation.Operator.EQJ.getOp(),
          propertyDto.getPosthumous());
    }

    if (!StringUtils.isEmpty(propertyDto.getEditionSizeTotal())) {
      RangeDTO editionSizeRange = PropertyUtil.getRangeValues(propertyDto.getEditionSizeTotal(), SPLITTER_STRING);
      propertySpecificationBuilder
          .with("editionSizeTotal", PropertyOperation.Operator.EQJ.getOp(), editionSizeRange);
    }

    if (null != propertyDto.getFlags()) {
      if (null != propertyDto.getFlags().getFlagAuthenticity()) {
        propertySpecificationBuilder.with("flagAuthenticity",
            PropertyOperation.Operator.EQJ.getOp(), propertyDto.getFlags().getFlagAuthenticity());
      }
      if (null != propertyDto.getFlags().getFlagCondition()) {
        propertySpecificationBuilder.with("flagCondition", PropertyOperation.Operator.EQJ.getOp(),
            propertyDto.getFlags().getFlagCondition());
      }
      if (null != propertyDto.getFlags().getFlagMedium()) {
        propertySpecificationBuilder.with("flagMedium", PropertyOperation.Operator.EQJ.getOp(),
            propertyDto.getFlags().getFlagMedium());
      }
      if (null != propertyDto.getFlags().getFlagRestitution()) {
        propertySpecificationBuilder.with("flagRestitution", PropertyOperation.Operator.EQJ.getOp(),
            propertyDto.getFlags().getFlagRestitution());
      }
      if (null != propertyDto.getFlags().getFlagSfs()) {
        propertySpecificationBuilder.with("flagSfs", PropertyOperation.Operator.EQJ.getOp(),
            propertyDto.getFlags().getFlagSfs());
      }
      if (null != propertyDto.getFlags().getFlagsCountry()) {
        propertySpecificationBuilder.with("flagsCountry", PropertyOperation.Operator.EQJ.getOp(),
            propertyDto.getFlags().getFlagsCountry());
      }
    }
    if (!StringUtils.isEmpty(propertyDto.getResearcherNotesDate())) {
      propertySpecificationBuilder.with("researcherNoteDate",
          PropertyOperation.Operator.EQJ.getOp(), propertyDto.getResearcherNotesDate());
    }
    if (null != propertyDto.getResearchCompleteCheck()) {
      propertySpecificationBuilder.with("researchCompleteCheck",
          PropertyOperation.Operator.EQJ.getOp(), propertyDto.getResearchCompleteCheck());
    }
    if (!PropertyUtil.isBlank(propertyDto.getCatalogueRaisoneeId())) {
      propertySpecificationBuilder.with("catalogueRaisoneeId",
          PropertyOperation.Operator.LIKEJ.getOp(), propertyDto.getCatalogueRaisoneeId());
    }

    if (!CollectionUtils.isEmpty(propertyDto.getArtistCatalogueRaisoneeIds())) {

      propertySpecificationBuilder.with("artistCatalogueRaisoneeIds",
          Operator.EQJ.getOp(), propertyDto.getArtistCatalogueRaisoneeIds());
    }

    if (!StringUtils.isEmpty(propertyDto.getCurrentOwnerIds())) {
      propertySpecificationBuilder.with("currentOwnerIds",
          Operator.EQJ.getOp(), propertyDto.getCurrentOwnerIds());
    }

    if (!StringUtils.isEmpty(propertyDto.getArtistCatalogueRaisoneeAuthor())) {
      propertySpecificationBuilder.with("artistCatalogueRaisoneeAuthor", Operator.LIKEJ.getOp(),
          propertyDto.getArtistCatalogueRaisoneeAuthor());
    }
    if (null != propertyDto.getTags()) {
      if (!StringUtils.isEmpty(propertyDto.getTags().getExpertiseLocation())) {
        propertySpecificationBuilder
            .with("expertiseLocation", PropertyOperation.Operator.LIKEJ.getOp(),
                propertyDto.getTags().getExpertiseLocation());
      }
      if (!CollectionUtils.isEmpty(propertyDto.getTags().getExpertiseSitters())) {
        propertySpecificationBuilder
            .with("expertiseSitters", PropertyOperation.Operator.EQJ.getOp(),
                propertyDto.getTags().getExpertiseSitters());
      }
      if (!CollectionUtils.isEmpty(propertyDto.getTags().getImageSubjects())) {
        propertySpecificationBuilder.with("imageSubjects", PropertyOperation.Operator.EQJ.getOp(),
            propertyDto.getTags().getImageSubjects());
      }
      if (!StringUtils.isEmpty(propertyDto.getTags().getImageFigure())) {
        propertySpecificationBuilder.with("imageFigure", PropertyOperation.Operator.LIKEJ.getOp(),
            propertyDto.getTags().getImageFigure());
      }
      if (!CollectionUtils.isEmpty(propertyDto.getTags().getImageGenders())) {
        propertySpecificationBuilder.with("imageGenders", PropertyOperation.Operator.EQJ.getOp(),
            propertyDto.getTags().getImageGenders());
      }
      if (!CollectionUtils.isEmpty(propertyDto.getTags().getImagePositions())) {
        propertySpecificationBuilder.with("imagePositions", PropertyOperation.Operator.EQJ.getOp(),
            propertyDto.getTags().getImagePositions());
      }
      if (!CollectionUtils.isEmpty(propertyDto.getTags().getImageAnimals())) {
        propertySpecificationBuilder.with("imageAnimals", PropertyOperation.Operator.EQJ.getOp(),
            propertyDto.getTags().getImageAnimals());
      }
      if (!CollectionUtils.isEmpty(propertyDto.getTags().getImageMainColours())) {
        propertySpecificationBuilder
            .with("imageMainColours", PropertyOperation.Operator.EQJ.getOp(),
                propertyDto.getTags().getImageMainColours());
      }
      if (!StringUtils.isEmpty(propertyDto.getTags().getImageText())) {
        propertySpecificationBuilder.with("imageText", PropertyOperation.Operator.LIKEJ.getOp(),
            propertyDto.getTags().getImageText());
      }
      if (!CollectionUtils.isEmpty(propertyDto.getTags().getOtherTags())) {
        propertySpecificationBuilder.with("otherTags", PropertyOperation.Operator.EQJ.getOp(),
            propertyDto.getTags().getOtherTags());
      }
      if (!StringUtils.isEmpty(propertyDto.getTags().getAutoOrientation())) {
        propertySpecificationBuilder.with("autoOrientation", PropertyOperation.Operator.EQJ.getOp(),
            propertyDto.getTags().getAutoOrientation());
      }
      if (!StringUtils.isEmpty(propertyDto.getTags().getAutoScale())) {
        RangeDTO autoScaleRange = PropertyUtil.getRangeValues(propertyDto.getTags().getAutoScale(), SPLITTER_STRING);
        propertySpecificationBuilder
            .with("autoScale", PropertyOperation.Operator.EQJ.getOp(), autoScaleRange);

      }
      if (null != propertyDto.getTags().getAutoImage()) {
        propertySpecificationBuilder.with("autoImage", PropertyOperation.Operator.EQJ.getOp(),
            propertyDto.getTags().getAutoImage());
      }
      if (!StringUtils.isEmpty(propertyDto.getTags().getAutoVolume())) {
        String unit =
            !StringUtils.isEmpty(propertyDto.getTags().getAutoVolumeUnit()) ? propertyDto.getTags()
                .getAutoVolumeUnit() : "cm";
        if (unit.equals("cm")) {
          propertySpecificationBuilder.with("autoVolumeCm", PropertyOperation.Operator.EQJ.getOp(),
              propertyDto.getTags().getAutoVolume());
        } else {
          propertySpecificationBuilder.with("autoVolumeIn", PropertyOperation.Operator.EQJ.getOp(),
              propertyDto.getTags().getAutoVolume());
        }
      }
    }

    propertySpecificationBuilder.with("status", PropertyOperation.Operator.EQJ.getOp(),
        Status.Active);

    return propertySpecificationBuilder;
  }

  public PropertySpecificationBuilder createSpecificationBuilder(Long saveListId) {
    PropertySpecificationBuilder propertySpecificationBuilder = new PropertySpecificationBuilder();
    propertySpecificationBuilder.with("saveListId", Operator.EQJ.getOp(), saveListId);
    propertySpecificationBuilder
        .with("status", PropertyOperation.Operator.EQJ.getOp(), Status.Active);
    return propertySpecificationBuilder;
  }

}