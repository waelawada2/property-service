LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;

INSERT INTO `category` (`id`, `description`, `name`, `parent_id`)
VALUES
  (1,'Other','Other',NULL),
  (2,'Painting','Painting',NULL),
  (3,'Photograph','Photograph',NULL),
  (4,'Print','Print',NULL),
  (5,'Sculpture','Sculpture',NULL),
  (6,'Work on Paper','Work on Paper',NULL);

/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;



LOCK TABLES `edition_type` WRITE;
/*!40000 ALTER TABLE `edition_type` DISABLE KEYS */;

INSERT INTO `edition_type` (`id`, `sort_code`,`description`, `name`)
VALUES
  (1,1,'AP','AP'),
  (2,2,'Bon a Tirer','Bon a Tirer'),
  (3,3,'EE','EE'),
  (4,4,'HC','HC'),
  (5,5,'HE','HE'),
  (6,6,'PH','PH'),
  (7,7,'PP','PP'),
  (8,8,'Series','Series'),
  (9,9,'TP','TP');

/*!40000 ALTER TABLE `edition_type` ENABLE KEYS */;
UNLOCK TABLES;


INSERT INTO `presize_type` (`id`, `description`, `name`)
VALUES
  (1, 'Overall', 'Overall'),
  (2, 'Each', 'Each'),
  (3, 'Measurements Variable', 'Measurements Variable'),
  (4, 'Diameter', 'Diameter'),
  (5, 'Image', 'Image'),
  (6, 'Not By Artist', 'Not By Artist'),
  (7, 'Framed', 'Framed'),
  (8, 'Including Base', 'Including Base'),
  (9, 'Unframed', 'Unframed'),
  (10, 'Largest element', 'Largest element');

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;

INSERT INTO `country` (`id`, `name`)
VALUES
	(259,'Benin'),
	(260,'Angola'),
	(261,'Cambodia'),
	(262,'Kazakhstan'),
	(263,'Paraguay'),
	(264,'Portugal'),
	(265,'New Caledonia'),
	(266,'Syria'),
	(267,'Bahamas'),
	(268,'Greece'),
	(269,'Latvia'),
	(270,'Mongolia'),
	(271,'Morocco'),
	(272,'Mali'),
	(273,'Panama'),
	(274,'Guadeloupe'),
	(275,'Guatemala'),
	(276,'Guyana'),
	(277,'Iraq'),
	(278,'Chile'),
	(279,'Laos'),
	(280,'Argentina'),
	(281,'Isle of Man'),
	(282,'Ukraine'),
	(283,'Tanzania'),
	(284,'Ghana'),
	(285,'Congo'),
	(286,'India'),
	(287,'Canada'),
	(288,'Turkey'),
	(289,'Belgium'),
	(290,'Taiwan'),
	(291,'Finland'),
	(292,'South Africa'),
	(293,'Trinidad and Tobago'),
	(294,'U.S.A.'),
	(295,'Georgia'),
	(296,'Jamaica'),
	(297,'Peru'),
	(298,'Germany'),
	(299,'Puerto Rico'),
	(300,'Fiji'),
	(301,'Madagascar'),
	(302,'Thailand'),
	(303,'Libya'),
	(304,'Costa Rica'),
	(305,'Sweden'),
	(306,'Vietnam'),
	(307,'Poland'),
	(308,'Nigeria'),
	(309,'Bulgaria'),
	(310,'Jordan'),
	(311,'Tunisia'),
	(312,'Croatia'),
	(313,'Sri Lanka'),
	(314,'Uruguay'),
	(315,'United Kingdom'),
	(316,'Kenya'),
	(317,'Switzerland'),
	(318,'Spain'),
	(319,'Lebanon'),
	(320,'Venezuela'),
	(321,'Cuba'),
	(322,'Liberia'),
	(323,'Azerbaijan'),
	(324,'Czech Republic'),
	(325,'Israel'),
	(326,'Australia'),
	(327,'Estonia'),
	(328,'Myanmar'),
	(329,'Cameroon'),
	(330,'Cyprus'),
	(331,'Malaysia'),
	(332,'Iceland'),
	(333,'Armenia'),
	(334,'Gabon'),
	(335,'Austria'),
	(336,'Mozambique'),
	(337,'El Salvador'),
	(338,'Monaco'),
	(339,'Luxembourg'),
	(340,'Brazil'),
	(341,'Algeria'),
	(342,'Slovenia'),
	(343,'Tahiti'),
	(344,'Colombia'),
	(345,'Ecuador'),
	(346,'Hungary'),
	(347,'Japan'),
	(348,'Moldova'),
	(349,'Mauritius'),
	(350,'Albania'),
	(351,'New Zealand'),
	(352,'NA'),
	(353,'Senegal'),
	(354,'Italy'),
	(355,'Honduras'),
	(356,'Antarctica'),
	(357,'Macedonia'),
	(358,'Ethiopia'),
	(359,'Haiti'),
	(360,'Afghanistan'),
	(361,'Singapore'),
	(362,'Egypt'),
	(363,'Sierra Leone'),
	(364,'Bolivia'),
	(365,'Russia'),
	(366,'Malta'),
	(367,'Netherlands'),
	(368,'Pakistan'),
	(369,'China'),
	(370,'Ireland'),
	(371,'Slovakia'),
	(372,'France'),
	(373,'Lithuania'),
	(374,'Kyrgyzstan'),
	(375,'Reunion'),
	(376,'Romania'),
	(377,'Philippines'),
	(378,'Uzbekistan'),
	(379,'Bangladesh'),
	(380,'Nicaragua'),
	(381,'Norway'),
	(382,'Denmark'),
	(383,'Dominican Republic'),
	(384,'Mexico'),
	(385,'Zimbabwe'),
	(386,'Uganda'),
	(387,'Indonesia'),
	(388,'American Samoa'),
	(389,'Andorra'),
	(390,'Anguilla'),
	(391,'Antigua and Barbuda'),
	(392,'Aruba'),
	(393,'Bahrain'),
	(394,'Barbados'),
	(395,'Belarus'),
	(397,'Belize'),
	(398,'Bermuda'),
	(399,'Bhutan'),
	(400,'Bosnia and Herzegovina'),
	(401,'Botswana'),
	(402,'Bouvet Island'),
	(403,'British Indian Ocean Territory'),
	(404,'Brunei Darussalam'),
	(405,'Burkina Faso'),
	(406,'Burundi'),
	(407,'Cape Verde'),
	(408,'Cayman Islands'),
	(409,'Central African Republic'),
	(410,'Chad'),
	(411,'Christmas Island'),
	(412,'Cocos (Keeling) Islands'),
	(413,'Comoros'),
	(414,'Cook Islands'),
	(415,'Croatia (Hrvatska)'),
	(416,'Djibouti'),
	(417,'East Timor'),
	(418,'Equatorial Guinea'),
	(419,'Eritrea'),
	(420,'Falkland Islands (Malvinas)'),
	(421,'Faroe Islands'),
	(422,'France, Metropolitan'),
	(423,'French Guiana'),
	(424,'French Polynesia'),
	(425,'French Southern Territories'),
	(426,'Gambia'),
	(427,'Gibraltar'),
	(428,'Guernsey'),
	(429,'Greenland'),
	(430,'Grenada'),
	(431,'Guam'),
	(432,'Guinea'),
	(433,'Guinea-Bissau'),
	(434,'Heard and Mc Donald Islands'),
	(435,'Hong Kong'),
	(436,'Iran (Islamic Republic of)'),
	(437,'Ivory Coast'),
	(438,'Jersey'),
	(439,'Kiribati'),
	(440,'Korea, Democratic People''s Republic of'),
	(441,'Korea, Republic of'),
	(442,'Kosovo'),
	(443,'Kuwait'),
	(444,'Lao People''s Democratic Republic'),
	(445,'Lesotho'),
	(446,'Libyan Arab Jamahiriya'),
	(447,'Liechtenstein'),
	(448,'Macau'),
	(449,'Malawi'),
	(450,'Maldives'),
	(451,'Marshall Islands'),
	(452,'Martinique'),
	(453,'Mauritania'),
	(454,'Mayotte'),
	(455,'Micronesia, Federated States of'),
	(456,'Moldova, Republic of'),
	(457,'Montenegro'),
	(458,'Montserrat'),
	(459,'Namibia'),
	(460,'Nauru'),
	(461,'Nepal'),
	(462,'Netherlands Antilles'),
	(463,'Niger'),
	(464,'Niue'),
	(465,'Norfolk Island'),
	(466,'Northern Mariana Islands'),
	(467,'Oman'),
	(468,'Palau'),
	(469,'Palestine'),
	(470,'Papua New Guinea'),
	(471,'Pitcairn'),
	(472,'Qatar'),
	(473,'Russian Federation'),
	(474,'Rwanda'),
	(475,'Saint Kitts and Nevis'),
	(476,'Saint Lucia'),
	(477,'Saint Vincent and the Grenadines'),
	(478,'Samoa'),
	(479,'San Marino'),
	(480,'Sao Tome and Principe'),
	(481,'Saudi Arabia'),
	(482,'Serbia'),
	(483,'Seychelles'),
	(484,'Solomon Islands'),
	(485,'Somalia'),
	(486,'South Georgia South Sandwich Islands'),
	(487,'St. Helena'),
	(488,'St. Pierre and Miquelon'),
	(489,'Sudan'),
	(490,'Suriname'),
	(491,'Svalbard and Jan Mayen Islands'),
	(492,'Swaziland'),
	(493,'Syrian Arab Republic'),
	(494,'Tajikistan'),
	(495,'Tanzania, United Republic of'),
	(496,'Togo'),
	(497,'Tokelau'),
	(498,'Tonga'),
	(499,'Turkmenistan'),
	(500,'Turks and Caicos Islands'),
	(501,'Tuvalu'),
	(502,'United Arab Emirates'),
	(503,'United States'),
	(504,'United States minor outlying islands'),
	(505,'Vanuatu'),
	(506,'Vatican City State'),
	(508,'Virgin Islands (British)'),
	(509,'Virgin Islands (U.S.)'),
	(510,'Wallis and Futuna Islands'),
	(511,'Western Sahara'),
	(512,'Yemen'),
	(513,'Zaire'),
	(514,'Zambia');
	
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `edition_size_type` WRITE;
/*!40000 ALTER TABLE `edition_size_type` DISABLE KEYS */;

INSERT INTO `edition_size_type` (`id`, `name`)
VALUES
  (1,'Unspecified'),
  (2,'Known'),
  (3,'Unknown'),
  (4,'Unique'),
  (5,'Open');

/*!40000 ALTER TABLE `edition_size_type` ENABLE KEYS */;
UNLOCK TABLES;
