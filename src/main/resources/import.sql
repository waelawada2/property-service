INSERT INTO `country` (`id`, `name`) VALUES (1, 'India'),(2, 'Japan'),(3, 'U.S.A.'),(4, 'United Kingdom');

INSERT INTO `artist_catalogue_raisonee` (`id`, `author`, `cr_type`, `volumes`, `year`) VALUES (1, 'Helin', 'Painting', '5', 1995), (2, 'Wael', 'Sculpture', '2', 2000), (3, 'Michael', 'Prints', '3', 1997);

INSERT INTO `property_catalogue_raisonee` (`id`, `number`, `volume`,`artist_catalogue_raisonee_id`) VALUES (1, '25', 'VOL-1',1);

INSERT INTO `artist` (`id`, `approved`, `birth_year`, `death_year`, `display`, `external_id`, `first_name`, `gender`, `last_name`, `ulan_id`,`status`) VALUES (1, 1, 1895, 1995, 'Pablo Picasso', '00000002', 'Pablo', 'Male', 'Picasso', 1, 'Active');

INSERT INTO `artist_artist_catalogue_raisonee` (`artist_id`, `catalogue_raisonee_id`) VALUES (1, 1), (1, 2),(1,3);

INSERT INTO `edition_type` (`id`,`sort_code`, `name`, `description`) VALUES (1, 1, 'Series', 'Series'),(2, 2, 'AP', 'AP'),(3, 3, 'TP', 'TP'),(4, 4, 'BAT', 'BAT'),(5, 5,'PP','PP'),(6, 6,'HC','HC');
INSERT INTO `edition_size_type` (`id`, `name`) VALUES (1, 'Unspecified'),(2, 'Known'),(3, 'Unknown'),(4, 'Unique'),(5, 'Open');
INSERT INTO `artist_country` (`artist_id`, `country_id`) VALUES (1, 1);

INSERT INTO `category` (`id`, `description`, `name`, `parent_id`) VALUES (1, 'Other', 'Other', NULL),(2, 'Painting', 'Painting', NULL),(3, 'Photograph', 'Photograph', NULL),(4, 'Print', 'Print', NULL),(5, 'Sculpture', 'Sculpture', NULL),(6, 'Work on Paper', 'Work on Paper', NULL),(7, 'Paper Painting', 'Paper Painting', 2);

INSERT INTO `artist_category` (`artist_id`, `category_id`) VALUES (1, 1);

INSERT INTO `presize_type` (`id`, `name`, `description`) VALUES (1, 'Overall', 'Overall'),(2, 'Each', 'Each'),(3, 'Measurements Variable', 'Measurements Variable'),(4, 'Diameter', 'Diameter'),(5, 'Image', 'Image'),(6, 'Not By Artist', 'Not By Artist'),(7, 'Framed', 'Framed'),(8, 'Including Base', 'Including Base'),(9, 'Unframed', 'Unframed'),(10, 'Largest element', 'Largest element');

INSERT INTO `archive_number` (`id`, `archive_number`) VALUES (1, 'Picasso');
INSERT INTO `archive_number` (`id`, `archive_number`) VALUES (2, 'archive number');

INSERT INTO `property` (`id`, `external_id`, `title`, `original_title`, `signature`, `medium`, `category_id`, `presize_type_id`, `parts`, `height`, `width`, `depth`, `year_start`, `year_end`, `cast_year_start`, `cast_year_end`, `year_text`, `edition_number`, `edition_type_id`, `edition_size_type_id`,`weight`, `foundry`, `certificate`, `signed`, `stamped`, `authentication`,  `sort_code`, `image_path`,`flag_authenticity`,`flag_condition`,`flag_medium`,`flag_restitution`,`flag_sfs`,`flags_country_id`,`status`,`merged_to_id`, `auto_image`) VALUES (1, '00000002', 'Property object1', 'sothebys', 'sothebys', 'Medium', 1, 1, 1, 10, 10, 10, 2015, 2017, 2015, 2017,'Year text', 10, 1, 1, 10,'Foundry', 1, 1, 1, 1, '1', '',0,0,0,0,0,1,'Active','00000002', 0);

INSERT INTO `property_property_catalogue_raisonee` (`property_id`, `catalogue_raisonee_id`) VALUES (1, 1);

INSERT INTO `property_artist` (`property_id`, `artist_id`) VALUES (1, 1);

INSERT INTO `property_archive_number` (`property_id`, `archive_number_id`) VALUES (1, 1),(1,2);

INSERT INTO `object_code` (`property_id`, `object_code`) VALUES (1, 'object code 1'), (1, 'object code 2');

INSERT INTO `user` (`id`, `keycloak_user_id`, `user_name`, `first_name`, `last_name`, `email`) VALUES (1, 1, 'sree', 'ram', 'batthula', 'test@test.com'), (2, 2, 'sree2', 'ram2', 'batthula2', 'test2@test2.com');

INSERT INTO `save_search` (`id`, `external_id`, `name`, `notes`, `date`,`search_type`, `user_id`) VALUES (1, '00000001', 'ram', 'notes', '2017-06-15 19:08:11.325000','properties', 1),(2, '00000002', 'venkat', 'notestwo', '2017-06-15 19:08:11.325000','artists', 2);

INSERT INTO `save_search_criteria` (`id`, `param_name`, `param_value`, `save_search_id`) VALUES (1, 'medium', 'string', 1),(2, 'last_name', 'Picasso', 2);

INSERT INTO `save_list` (`id`, `external_id`, `name`, `notes`, `date`, `user_id`) VALUES (1, '00000001', 'ram', 'notes', '2017-06-15 19:08:11.325000', 1),  (2, '00000002', 'venkat', 'notestwo', '2017-06-15 19:08:11.325000', 1);

INSERT INTO `save_list_property` (`save_list_id`, `property_id`) VALUES (1, 1);

INSERT INTO `save_list_user` (`save_list_id`, `shared_user_id`) VALUES (1, 1);

INSERT INTO `image_figure` (`id`, `name`) VALUES (1, '0'), (2, '1'), (3, '2'), (4, '3'), (5, '4'), (6, '5'), (7, '6'), (8, '7'), (9, '8'), (10, '9'), (11, '10'), (12, '11'), (13, '12'), (14, '13'), (15, '14'), (16, '15'), (17, '16'), (18, '17'), (19, '18'), (20, '19'), (21, '20+');

INSERT INTO `image_subject` (`id`, `name`) VALUES (1, 'Abstract'), (2, 'Interior'), (3, 'Still Life'), (4, 'Portrait'), (5, 'Nude'), (6, 'Architecture'), (7, 'Waterscape'), (8, 'Landscape'), (9, 'Cityscape'), (10, 'Mountainscape');

INSERT INTO `image_gender` (`id`, `name`) VALUES (1, 'Male'), (2, 'Female');

INSERT INTO `image_position` (`id`, `name`) VALUES (1, 'Standing'), (2, 'Seating'), (3, 'Reclining');

INSERT INTO `image_main_colour` (`id`, `name`) VALUES (1, 'White'), (2, 'Black'), (3, 'Grey'), (4, 'Brown'), (5, 'Red'), (6, 'Pink'), (7, 'Orange'), (8, 'Yellow'), (9, 'Green'), (10, 'Blue'), (11, 'Purple'), (12, 'Beige'), (13, 'Transparent'), (14, 'Silver'), (15, 'Gold'), (16, 'Bronze'), (17, 'Raw');
