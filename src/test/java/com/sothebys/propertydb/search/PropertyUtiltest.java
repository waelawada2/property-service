package com.sothebys.propertydb.search;

import com.sothebys.propertydb.dto.RangeDTO;
import org.junit.Assert;
import org.junit.Test;

public class PropertyUtiltest {

  @Test
  public void getRangeValuesTest() {
    String rangedValue = "2002...2005"; 
    String splitterString = "...";
    
    RangeDTO dto = PropertyUtil.getRangeValues(rangedValue, splitterString);
    
    Assert.assertNotNull(dto);
    Assert.assertEquals("2002", dto.getMinValue());
    Assert.assertEquals("2005", dto.getMaxValue());
    
  }
  
  @Test
  public void getRangeValuesTestOneYear() {
    String rangedValue = "2002..."; 
    String splitterString = "...";
    
    RangeDTO dto = PropertyUtil.getRangeValues(rangedValue, splitterString);
    
    Assert.assertNotNull(dto);
    Assert.assertEquals("2002", dto.getMinValue());    
    
  }
  
  @Test
  public void getRangeValuesTestOneYearEnd() {
    String rangedValue = "...2008"; 
    String splitterString = "...";
    
    RangeDTO dto = PropertyUtil.getRangeValues(rangedValue, splitterString);
    
    Assert.assertNotNull(dto);
    Assert.assertEquals("2008", dto.getMaxValue());    
    
  }
  
  @Test
  public void getRangeValuesTestScale() {
    String rangedValue = "S-XXL"; 
    String splitterString = "-";
    
    RangeDTO dto = PropertyUtil.getRangeValues(rangedValue, splitterString);
    
    Assert.assertNotNull(dto);
    Assert.assertEquals("S", dto.getMinValue());
    Assert.assertEquals("XXL", dto.getMaxValue());
    
  }
  
  @Test
  public void getRangeValuesTestDimensionsOneValue() {
    String rangedValue = "70-"; 
    String splitterString = "-";
    
    RangeDTO dto = PropertyUtil.getRangeValues(rangedValue, splitterString);
    
    Assert.assertNotNull(dto);
    Assert.assertEquals("70", dto.getMinValue());
    
  }
  
  @Test
  public void getRangeValuesTestDimensions() {
    String rangedValue = "80.25-105.25"; 
    String splitterString = "-";
    
    RangeDTO dto = PropertyUtil.getRangeValues(rangedValue, splitterString);
    
    Assert.assertNotNull(dto);
    Assert.assertEquals("80.25", dto.getMinValue());
    Assert.assertEquals("105.25", dto.getMaxValue());
    
  }
}
