package com.sothebys.propertydb.search;

import com.sothebys.propertydb.dto.RangeDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;


public class PropertySpecificationTest {

  @InjectMocks
  PropertySpecification propertySpecification;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void validateRangeForIsNotNullFieldTest() {
    //Arrange
    RangeDTO heightRange = getRange("2000", null);
    //Act
    boolean test = propertySpecification.validateRangeForIsNotNullField(heightRange);
    //Asssert
    Assert.assertFalse(test);
  }

  @Test
  public void validateRangeForIsNotNullFieldTestTrue() {
    //Arrange
    RangeDTO otherRange = getRange(null, null);
    //Act
    boolean test2 = propertySpecification.validateRangeForIsNotNullField(otherRange);
    //Asssert
    Assert.assertTrue(test2);
  }

  @Test
  public void validateRangeForLessThanOrEqualOperationTest() {
    //Arrange
    RangeDTO otherRange = getRange(null, "2005");
    //Act
    boolean test = propertySpecification.validateRangeForLessThanOrEqualOperation(otherRange);
    //Asssert
    Assert.assertTrue(test);
  }

  @Test
  public void validateRangeForGreaterThatOrEqualOperationTest() {
    //Arrange
    RangeDTO otherRange = getRange(null, "2005");
    //Act
    boolean test = propertySpecification.validateRangeForGreaterThatOrEqualOperation(otherRange);
    //Asssert
    Assert.assertFalse(test);
  }

  @Test
  public void validateRangeForBetweenOperationTest() {
    //Arrange
    RangeDTO otherRange = getRange(null, "2005");
    //Act
    boolean test = propertySpecification.validateRangeForBetweenOperation(otherRange);
    //Asssert
    Assert.assertFalse(test);
  }

  public RangeDTO getRange(String min, String max) {
    RangeDTO dto = new RangeDTO();
    dto.setMinValue(min);
    dto.setMaxValue(max);
    return dto;
  }
}
