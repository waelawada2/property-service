package com.sothebys.propertydb.search;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;


public class PropertySearchBuilderTest {

  @InjectMocks
  PropertySearchBuilder propertySearchBuilder;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void getRangedValuesTest() {
    // Arrange
    PropertySearchInputDTO inputDTO = new PropertySearchInputDTO();
    TagsInputDTO input = new TagsInputDTO();
    input.setAutoScale("S-L");
    inputDTO.setTags(input);
    // Act
    PropertySpecificationBuilder builder =
        propertySearchBuilder.createSpecificationBuilder(inputDTO);
    // Assert
    Assert.assertNotNull(builder);
  }

  @Test
  public void createSpecificationBuilderTest() {
    // Arrange
    PropertySearchInputDTO inputDTO = new PropertySearchInputDTO();
    inputDTO.setYearStart("2000");
    // Act
    PropertySpecificationBuilder builder =
        propertySearchBuilder.createSpecificationBuilder(inputDTO);
    // Assert
    Assert.assertNotNull(builder);
  }
  
  @Test
  public void createSpecificationBuilderWithYears() {
    // Arrange
    PropertySearchInputDTO inputDTO = new PropertySearchInputDTO();
    inputDTO.setYearStart("2000...2005");
    // Act
    PropertySpecificationBuilder builder =
        propertySearchBuilder.createSpecificationBuilder(inputDTO);
    // Assert
    Assert.assertNotNull(builder);
  }

}
