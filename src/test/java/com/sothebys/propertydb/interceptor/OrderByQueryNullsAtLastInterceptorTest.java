package com.sothebys.propertydb.interceptor;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author jsilva.
 */
public class OrderByQueryNullsAtLastInterceptorTest {

  private static final String QUERY_MATCH =
      "select distinct p.id, acr.id, acr.author, pcr.volume, pcr.number from property p"
          + " left outer join property_property_catalogue_raisonee ppcr on (ppcr.property_id = p.id)"
          + " left outer join property_catalogue_raisonee pcr on (pcr.id = ppcr.catalogue_raisonee_id)"
          + " left outer join artist_catalogue_raisonee acr on (acr.id = pcr.artist_catalogue_raisonee_id and acr.status = 'Active')"
          + " where p.id in (select property_id from property_artist where artist_id = 821159) and p.status = 'Active' and (acr.id in (?) or acr.id is NULL )"
          + " order by pcr.volume asc, pcr.number asc limit ?";

  private static final String QUERY_NO_MATCH =
      "select distinct p.id, acr.id, ppcr.volume, ppcr.number, acr.author from property p"
          + " left outer join property_property_catalogue_raisonee ppcr on (ppcr.property_id = p.id)"
          + " left outer join property_catalogue_raisonee pcr on (pcr.id = ppcr.catalogue_raisonee_id)"
          + " left outer join artist_catalogue_raisonee acr on (acr.id = pcr.artist_catalogue_raisonee_id and acr.status = 'Active')"
          + " where p.id in (select property_id from property_artist where artist_id = 821159) and p.status = 'Active' and (acr.id in (?) or acr.id is NULL )"
          + " group by ppcr.any1 asc, ppcr.any2 asc, ppcr.any3 asc limit ?";

  private static final String QUERY_MATCH_WITH_SORTS =
      "select distinct p.id, acr.id, acr.author, pcr.volume, pcr.number from property p"
          + " left outer join property_property_catalogue_raisonee ppcr on (ppcr.property_id = p.id)"
          + " left outer join property_catalogue_raisonee pcr on (pcr.id = ppcr.catalogue_raisonee_id)"
          + " left outer join artist_catalogue_raisonee acr on (acr.id = pcr.artist_catalogue_raisonee_id and acr.status = 'Active')"
          + " where p.id in (select property_id from property_artist where artist_id = 821159) and p.status = 'Active' and (acr.id in (?) or acr.id is NULL )"
          + " order by  ISNULL(NULLIF(pcr.volume,'')), pcr.volume asc, ISNULL(NULLIF(pcr.number,'')), pcr.number asc limit ?";

  private static final String QUERY_MATCH_WITH_MANY_ORDER_BY_FILTERS =
      "select distinct p.id, acr.id, acr.author, pcr.volume, pcr.number from property p"
          + " left outer join property_property_catalogue_raisonee ppcr on (ppcr.property_id = p.id)"
          + " left outer join property_catalogue_raisonee pcr on (pcr.id = ppcr.catalogue_raisonee_id)"
          + " left outer join artist_catalogue_raisonee acr on (acr.id = pcr.artist_catalogue_raisonee_id and acr.status = 'Active')"
          + " where p.id in (select property_id from property_artist where artist_id = 821159) and p.status = 'Active' and (acr.id in (?) or acr.id is NULL )"
          + " order by acr.author desc, acr.id asc, pcr.volume asc, pcr.number asc limit ?";

  private static final String QUERY_MATCH_WITH_MANY_ORDER_BY_FILTERS_SORTS =
      "select distinct p.id, acr.id, acr.author, pcr.volume, pcr.number from property p"
          + " left outer join property_property_catalogue_raisonee ppcr on (ppcr.property_id = p.id)"
          + " left outer join property_catalogue_raisonee pcr on (pcr.id = ppcr.catalogue_raisonee_id)"
          + " left outer join artist_catalogue_raisonee acr on (acr.id = pcr.artist_catalogue_raisonee_id and acr.status = 'Active')"
          + " where p.id in (select property_id from property_artist where artist_id = 821159) and p.status = 'Active' and (acr.id in (?) or acr.id is NULL )"
          + " order by  ISNULL(NULLIF(acr.author,'')), acr.author desc, ISNULL(NULLIF(acr.id,'')), acr.id asc, ISNULL(NULLIF(pcr.volume,'')), pcr.volume asc, ISNULL(NULLIF(pcr.number,'')), pcr.number asc limit ?";


  private OrderByQueryNullsAtLastInterceptor targetClass
      = new OrderByQueryNullsAtLastInterceptor();

  @Test
  public void test_query_match_default_filters() {
    assertEquals(targetClass.onPrepareStatement(QUERY_MATCH), QUERY_MATCH_WITH_SORTS);
  }

  @Test
  public void test_query_match_many_filters() {
    assertEquals(targetClass.onPrepareStatement(QUERY_MATCH_WITH_MANY_ORDER_BY_FILTERS),
        QUERY_MATCH_WITH_MANY_ORDER_BY_FILTERS_SORTS);
  }

  @Test
  public void test_query_no_match_filter() {
    assertEquals(targetClass.onPrepareStatement(QUERY_NO_MATCH), QUERY_NO_MATCH);
  }

}