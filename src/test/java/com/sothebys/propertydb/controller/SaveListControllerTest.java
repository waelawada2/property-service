package com.sothebys.propertydb.controller;

import static com.sothebys.propertydb.DummyModelHelper.createContentNegotiatingViewResolver;
import static com.sothebys.propertydb.DummyModelHelper.createPropertyDTO;
import static com.sothebys.propertydb.DummyModelHelper.createPropertyModel;
import static com.sothebys.propertydb.DummyModelHelper.createSaveListDTO;
import static com.sothebys.propertydb.DummyModelHelper.createSaveListModel;
import static com.sothebys.propertydb.DummyModelHelper.createSaveListRequestDTO;
import static com.sothebys.propertydb.DummyModelHelper.createUserModel;
import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.controller.support.AdditionalMediaType;
import com.sothebys.propertydb.controller.support.GlobalExceptionHandler;
import com.sothebys.propertydb.dto.PropertyDTO;
import com.sothebys.propertydb.dto.SaveListDTO;
import com.sothebys.propertydb.dto.request.SaveListRequestDTO;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.ForbiddenException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.mapper.PropertyModelMapper;
import com.sothebys.propertydb.mapper.SaveListModelMapper;
import com.sothebys.propertydb.model.AddPropertiesToSavelistResult;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.SaveList;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.search.PollStatus;
import com.sothebys.propertydb.search.PropertyOperation;
import com.sothebys.propertydb.search.PropertySearchBuilder;
import com.sothebys.propertydb.search.PropertySpecification;
import com.sothebys.propertydb.search.PropertySpecificationBuilder;
import com.sothebys.propertydb.search.SearchCriteria;
import com.sothebys.propertydb.service.ExportService;
import com.sothebys.propertydb.service.PropertyService;
import com.sothebys.propertydb.service.SaveListService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class SaveListControllerTest {

  @InjectMocks
  private SaveListController saveListController;

  @Mock
  private SaveListModelMapper saveListModelMapper;

  @Mock
  private SaveListService saveListService;

  @Mock
  private PropertySearchBuilder propertySearchBuilder;

  @Mock
  private PropertyService propertyService;

  @Mock
  private PropertyModelMapper propertyModelMapper;

  @Mock
  private PropertySpecificationBuilder propertySpecificationBuilder;

  @Mock
  private ExportService exportService;

  private MockMvc mockMvc;
  private User user;
  private SaveList saveList;
  private SaveList saveListWithoutProperties;
  private SaveList saveListDifferentUser;
  private SaveListDTO saveListDTO;
  private SaveListRequestDTO saveListRequestDTO;
  private SaveListRequestDTO saveListRequestDTOWithoutProperties;
  private PageRequest pageRequest;
  private Page<Property> propertyPage;
  private Page<PropertyDTO> propertyDTOPage;
  private Specification<Property> propertySpecification;
  private List<String> propertyExternalIds;
  private AddPropertiesToSavelistResult addPropertiesToSavelistResult;

  @Before
  public void setUp() {
    setupMockMvc();
    user = createUserModel();
    saveList = createSaveListModel();

    saveListWithoutProperties = createSaveListModel();
    saveListWithoutProperties.setProperties(Collections.emptyList());

    saveListDifferentUser = createSaveListModel();
    saveListDifferentUser.setName("testUser");

    saveListDTO = createSaveListDTO();
    saveListRequestDTO = createSaveListRequestDTO();

    saveListRequestDTOWithoutProperties = createSaveListRequestDTO();
    saveListRequestDTOWithoutProperties.setPropertyIds(Collections.emptyList());

    pageRequest = new PageRequest(0, 20);
    propertyPage = new PageImpl<>(Collections.singletonList(createPropertyModel()));
    propertyDTOPage = new PageImpl<>(Collections.singletonList(createPropertyDTO()));

    SearchCriteria criteria = new SearchCriteria("saveListId",
        PropertyOperation.getSimpleOperation(':'), "1");
    propertySpecification = new PropertySpecification(criteria);

    propertyExternalIds = Collections.singletonList("OO-123456");

    addPropertiesToSavelistResult = new AddPropertiesToSavelistResult();
    addPropertiesToSavelistResult.setSaveList(saveList);
    addPropertiesToSavelistResult.setTotalInserted(1);
    addPropertiesToSavelistResult.setTotalFound(1);
  }

  private void setupMockMvc() {
    mockMvc = MockMvcBuilders.standaloneSetup(saveListController)
        .setMessageConverters(new MappingJackson2HttpMessageConverter())
        .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
        .setControllerAdvice(new GlobalExceptionHandler())
        .setViewResolvers(createContentNegotiatingViewResolver()).build();
  }

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Test case for createSaveList
   */
  @Test
  public void testCreateSaveListSuccess() throws Exception {
    when(saveListModelMapper.mapToEntity(saveListRequestDTOWithoutProperties))
        .thenReturn(saveListWithoutProperties);
    when(saveListService.createSaveList(saveListWithoutProperties, user))
        .thenReturn(saveListWithoutProperties);
    when(saveListService.addPropertiesToSaveList(1L, Collections.singletonList("00000002")))
        .thenReturn(addPropertiesToSavelistResult);
    when(saveListModelMapper.mapToDTO(saveList)).thenReturn(saveListDTO);

    mockMvc.perform(post("/users/{user-name}/lists", saveList.getName())
        .contentType(MediaType.APPLICATION_JSON).requestAttr("requestUser", user)
        .content(asJsonString(saveListRequestDTO)))
        .andExpect(status().isCreated())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(header().string("Location",
            both(containsString("ram")).and(containsString("SL-00000002"))))
        .andExpect(jsonPath("$.id", is("SL-00000002")))
        .andExpect(jsonPath("$.propertyIds[0]", is("00000002")));

    verify(saveListService, times(1)).createSaveList(saveListWithoutProperties, user);
    verify(saveListService, times(1))
        .addPropertiesToSaveList(1L, Collections.singletonList("00000002"));
    verifyNoMoreInteractions(saveListService);
  }

  /**
   * Test case for createSaveList name doesn't match with user object name scenario
   */
  @Test
  public void testCreateSaveListFailure() throws Exception {
    when(saveListModelMapper.mapToEntity(saveListRequestDTOWithoutProperties))
        .thenReturn(saveListWithoutProperties);
    when(saveListService.createSaveList(saveListWithoutProperties, user))
        .thenThrow(new CannotPerformOperationException());

    mockMvc.perform(post("/users/{user-name}/lists", saveListDifferentUser.getName())
        .contentType(MediaType.APPLICATION_JSON).requestAttr("requestUser", user)
        .content(asJsonString(saveListRequestDTO)))
        .andExpect(status().isConflict())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
  }

  /**
   * Test case for updateSaveList
   */
  @Test
  public void testUpdateSaveListByExternalIdSuccess() throws Exception {
    when(saveListModelMapper.mapToEntity(saveListRequestDTO)).thenReturn(saveList);
    when(saveListService.updateByUserAndExternalId(user, saveList.getExternalId(),
        saveList)).thenReturn(saveList);
    when(saveListModelMapper.mapToDTO(saveList)).thenReturn(saveListDTO);

    mockMvc.perform(
        put("/users/{user-name}/lists/{external-id}", saveList.getName(), saveList.getExternalId())
            .requestAttr("requestUser", user)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(saveListRequestDTO)))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.id", is("SL-00000002")))
        .andExpect(jsonPath("$.propertyIds[0]", is("00000002")));

    verify(saveListService, times(1)).updateByUserAndExternalId(user,
        saveList.getExternalId(), saveList);
    verifyNoMoreInteractions(saveListService);
  }

  /**
   * Test case for updateSaveList, name doesn't match with user object name scenario
   */
  @Test
  public void testUpdateSaveListFailure() throws Exception {
    when(saveListModelMapper.mapToEntity(saveListRequestDTO)).thenReturn(saveList);
    when(saveListService.updateByUserAndExternalId(user, saveList.getExternalId(),
        saveList)).thenThrow(new ForbiddenException());

    mockMvc.perform(
        put("/users/{user-name}/lists/{external-id}", saveList.getName(), saveList.getExternalId())
            .requestAttr("requestUser", user).contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(saveListRequestDTO)))
        .andExpect(status().isForbidden())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
  }

  /**
   * Test case for delete SaveList success scenario
   */
  @Test
  public void testDeleteSaveListByExternalIdSuccess() throws Exception {
    doNothing().when(saveListService).deleteByUserNameAndExternalId("ram", "SL-00000002");

    mockMvc
        .perform(delete("/users/{user-name}/lists/{external-id}", saveList.getName(),
            saveList.getExternalId()).requestAttr("requestUser", user))
        .andExpect(status().isNoContent());

    verify(saveListService, times(1)).deleteByUserNameAndExternalId(saveList.getName(),
        saveList.getExternalId());
    verifyNoMoreInteractions(saveListService);
  }

  /**
   * Test case for findByUserIdandExternalId
   */
  @Test
  public void testFindByUserIdAndExternalIdSuccess() throws Exception {
    when(saveListService.findSaveListByUserNameAndExternalId(saveList.getName(),
        saveList.getExternalId())).thenReturn(saveList);
    when(saveListModelMapper.mapToSaveListDTO(saveList)).thenReturn(saveListDTO);

    mockMvc.perform(get("/users/{user-name}/lists/{external-id}", saveList.getName(),
        saveList.getExternalId()).requestAttr("requestUser", user)
        .content(asJsonString(saveList)))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.id", is("SL-00000002")))
        .andExpect(jsonPath("$.propertyIds[0]", is("00000002")));

    verify(saveListService, times(1)).findSaveListByUserNameAndExternalId(saveList.getName(),
        saveList.getExternalId());
    verifyNoMoreInteractions(saveListService);
  }

  /**
   * Test case for downloadPropertiesFromSaveListUserByExternalId
   */
  @Test
  public void testDownloadPropertiesFromSaveListUserByExternalIdSuccess() throws Exception {
    when(saveListService
        .findSaveListByUserNameAndExternalId(saveList.getName(), saveList.getExternalId()))
        .thenReturn(saveList);
    when(propertySearchBuilder.createSpecificationBuilder(saveList.getId()))
        .thenReturn(propertySpecificationBuilder);
    when(propertySpecificationBuilder.build()).thenReturn(propertySpecification);
    when(propertyService.findBySearchTerm(propertySpecification, pageRequest))
        .thenReturn(propertyPage);
    when(propertyModelMapper.mapEntityPageIntoDTOPage(pageRequest, propertyPage))
        .thenReturn(propertyDTOPage);

    mockMvc
        .perform(get("/users/{user-name}/lists/{external-id}/properties.csv", saveList.getName(),
            saveList.getExternalId()).requestAttr("requestUser", user)
            .accept(AdditionalMediaType.TEXT_CSV_VALUE))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(AdditionalMediaType.TEXT_CSV_VALUE))
        .andExpect(content().string(containsString("OO-00000002")));

    verify(saveListService, times(1)).findSaveListByUserNameAndExternalId(saveList.getName(),
        saveList.getExternalId());
    verifyNoMoreInteractions(saveListService);
  }

  /**
   * Test case for downloadPropertiesFromSaveListUserByExternalId notFound scenario
   */
  @Test
  @Ignore //Ignore for now because of a bug with how exceptions are handled for .csv requests
  public void testDownloadPropertiesFromSaveListUserByExternalIdNotFound() throws Exception {
    when(saveListService
        .findSaveListByUserNameAndExternalId(saveList.getName(), saveList.getExternalId()))
        .thenThrow(new NotFoundException());

    mockMvc.perform(get("/users/{user-name}/lists/{external-id}/properties.csv", saveList.getName(),
        saveList.getExternalId()).requestAttr("requestUser", user)
        .accept(AdditionalMediaType.TEXT_CSV_VALUE))
        .andDo(print())
        .andExpect(status().isNotFound());

    verify(saveListService, times(1)).findSaveListByUserNameAndExternalId(saveList.getName(),
        saveList.getExternalId());
    verifyNoMoreInteractions(saveListService);
  }


  /**
   * Test case for exportIdStatus
   */
  @Test
  public void test_exportIdStatus_success() throws Exception {
    String exportId = "64300571-0e04-4785-8afc-8da2b508a358";
    when(exportService.findExportStatus(exportId)).thenReturn(PollStatus.Completed);
    mockMvc
        .perform(get("/users/{user-name}/lists/export/{id}/status", saveList.getName(), exportId)
            .requestAttr("requestUser", createUserModel()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    verify(exportService, times(1)).findExportStatus(exportId);
    verifyNoMoreInteractions(exportService);
  }

  /**
   * Test case for exportIdStatus failure scenario
   */
  @Test(expected = NotFoundException.class)
  public void test_exportIdStatus_NotFound() throws Exception {
    String exportId = "64300571-0e04-4785-8afc-8da2b508a358";
    PollStatus status = PollStatus.Error;
    when(saveListController.exportIdStatus(exportId, saveList.getName(), createUserModel()))
        .thenThrow(new NotFoundException());
    when(exportService.findExportStatus(exportId)).thenReturn(status);
    mockMvc
        .perform(get("/users/{user-name}/lists/export/{id}/status", saveList.getName(), exportId)
            .requestAttr("requestUser", createUserModel()))
        .andDo(print()).andExpect(status().isNotFound())
        .andExpect(jsonPath("$.error").value("No export id found with ID=" + exportId));
  }

  /**
   * Test case for export properties
   */
  @Test
  public void test_exportSavelist_success() throws Exception {
    String exportId = "64300571-0e04-4785-8afc-8da2b508a358";
    when(saveListService.exportSavedListData(propertySpecification))
        .thenReturn(exportId);
    when(saveListService.findSaveListByUserNameAndExternalId(saveList.getName(),
        saveList.getExternalId())).thenReturn(saveList);
    when(propertySearchBuilder.createSpecificationBuilder(saveList.getId()))
        .thenReturn(propertySpecificationBuilder);
    when(propertySpecificationBuilder.build()).thenReturn(propertySpecification);
    when(propertyModelMapper.mapEntityPageIntoDTOPage(pageRequest, propertyPage))
        .thenReturn(propertyDTOPage);
    mockMvc
        .perform(post("/users/{user-name}/lists/{external-id}/export", saveList.getName(),
            saveList.getExternalId()).contentType(MediaType.APPLICATION_JSON)
            .requestAttr("requestUser", user))
        .andExpect(status().isAccepted())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    verify(saveListService, times(1)).findSaveListByUserNameAndExternalId(saveList.getName(),
        saveList.getExternalId());
    verify(saveListService, times(1)).exportSavedListData(propertySpecification);
    verifyNoMoreInteractions(saveListService);
  }

  /**
   * Test case for delete SaveList success scenario
   */
  @Test
  public void testdeletePropertiesFromSavelistByAdminSuccess() throws Exception {
    List<PropertyDTO> propertyDTOList = new ArrayList<PropertyDTO>();
    PropertyDTO propertyDTOObj = createPropertyDTO();
    propertyDTOList.add(propertyDTOObj);
    when(propertyModelMapper.mapToDTO(saveList.getProperties())).thenReturn(propertyDTOList);
    mockMvc.perform(delete("/users/lists/{external-id}/delete", saveList.getExternalId())
        .requestAttr("requestUser", user)).andExpect(status().isNoContent());

    verify(saveListService, times(1)).deleteAdminSavelistProperties(saveList.getExternalId());
  }

  @Test
  public void testAddSaveListPropertyIdsByExternalId() throws Exception {
    when(saveListService
        .findSaveListByUserNameAndExternalId(user.getUserName(), saveList.getExternalId()))
        .thenReturn(saveList);
    when(saveListService.addPropertiesToSaveList(saveList.getId(), propertyExternalIds))
        .thenReturn(addPropertiesToSavelistResult);

    mockMvc.perform(post("/users/{user-name}/lists/{external-id}/properties", saveList.getName(),
        saveList.getExternalId())
        .requestAttr("requestUser", user)
        .contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(propertyExternalIds)))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$.totalFound", is(1)))
        .andExpect(jsonPath("$.totalInserted", is(1)));

    verify(saveListService).findSaveListByUserNameAndExternalId(user.getUserName(), saveList.getExternalId());
    verify(saveListService).addPropertiesToSaveList(saveList.getId(), propertyExternalIds);
  }

}