package com.sothebys.propertydb.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.sothebys.propertydb.dto.*;
import com.sothebys.propertydb.mapper.MappingConfiguration;
import com.sothebys.propertydb.model.*;
import org.junit.Before;
import org.junit.Test;
import org.modelmapper.ModelMapper;

/**
 *
 * @author nagaraju
 *
 */
public class ArtistModelMapperTest {

  private ModelMapper modelMapper = new ModelMapper();

  @Before
  public void setup() {
    modelMapper.addMappings(MappingConfiguration.ARTIST_TO_ARTIST_SEARCH_DTO_PROPERTY_MAP);
    modelMapper.addMappings(MappingConfiguration.ARTIST_TO_ARTIST_DTO_PROPERTY_MAP);
    modelMapper.addMappings(MappingConfiguration.ARTIST_DTO_TO_ARTIST_PROPERTY_MAP);
  }

  /**
   * @return Artist
   */
  public Artist getArtist() {
    Artist artist = new Artist();
    artist.setApproved(true);
    artist.setBirthYear(1984);
    artist.setDeathYear(2001);
    artist.setDisplay("sothebys");
    artist.setExternalId("0322");
    artist.setFirstName("fname");
    artist.setGender("M");
    artist.setId(1L);
    artist.setLastName("lname");

    List<Country> countriesList = new ArrayList<Country>();
    Country country = new Country();
    country.setId(1L);
    country.setCountryName("American");
    countriesList.add(country);
    artist.setCountries(countriesList);


    List<ArtistCatalogueRaisonee> artistCatalogueRaisoneeList =
        new ArrayList<ArtistCatalogueRaisonee>();

    ArtistCatalogueRaisonee artistCatalogueRaisonee = new ArtistCatalogueRaisonee();
    artistCatalogueRaisonee.setAuthor("Prince");
    artistCatalogueRaisonee.setId(1L);
    artistCatalogueRaisonee.setType("novel");
    artistCatalogueRaisonee.setVolumes("20");
    artistCatalogueRaisonee.setYear(2017);
    artistCatalogueRaisoneeList.add(artistCatalogueRaisonee);
    artist.setArtistCatalogueRaisonees(artistCatalogueRaisoneeList);

    List<Category> categoryList = new ArrayList<>();
    Category category = new Category();
    category.setId(1L);
    category.setName("test category");
    category.setDescription("A category for testing");
    categoryList.add(category);

    artist.setCategories(categoryList);

    return artist;
  }

  /**
   * @return ArtistDTO
   */
  public ArtistDTO getArtistDto() {
    ArtistDTO artistDto = new ArtistDTO();
    artistDto.setApproved(true);
    artistDto.setBirthYear(1984);
    artistDto.setDeathYear(2001);
    artistDto.setDisplay("sothebys");
    artistDto.setId("0322");
    artistDto.setFirstName("fname");
    artistDto.setGender("M");
    artistDto.setLastName("lname");

    CountryDTO countrydto = new CountryDTO();
    countrydto.setId(1L);
    countrydto.setCountryName("American");

    List<ArtistCatalogueRaisoneeDTO> artistCatalogueRaisoneedtolist =
        new ArrayList<ArtistCatalogueRaisoneeDTO>();

    ArtistCatalogueRaisoneeDTO artistCatalogueRaisoneedto = new ArtistCatalogueRaisoneeDTO();
    artistCatalogueRaisoneedto.setAuthor("Prince");
    artistCatalogueRaisoneedto.setId(1L);
    artistCatalogueRaisoneedto.setType("novel");
    artistCatalogueRaisoneedto.setVolumes("20");
    artistCatalogueRaisoneedto.setYear(2017);
    artistCatalogueRaisoneedtolist.add(artistCatalogueRaisoneedto);
    artistDto.setArtistCatalogueRaisonees(artistCatalogueRaisoneedtolist);

    List<CategoryDTO> categoryDTOList = new ArrayList<>();
    CategoryDTO categoryDTO = new CategoryDTO();
    categoryDTO.setName("test category");
    categoryDTOList.add(categoryDTO);

    artistDto.setCategories(categoryDTOList);

    return artistDto;
  }

  /**
   * @return Artist List
   */
  public List<Artist> getArtists() {
    List<Artist> artists = new ArrayList<Artist>();
    Artist artist = new Artist();
    artist.setApproved(true);
    artist.setBirthYear(1984);
    artist.setDeathYear(2001);
    artist.setDisplay("sothebys");
    artist.setExternalId("0322");
    artist.setFirstName("fname");
    artist.setGender("M");
    artist.setId(1L);
    artist.setLastName("lname");

    List<Country> countriesList = new ArrayList<Country>();
    Country country = new Country();
    country.setId(1L);
    country.setCountryName("American");
    countriesList.add(country);
    artist.setCountries(countriesList);

    List<ArtistCatalogueRaisonee> artistCatalogueRaisoneeList =
        new ArrayList<ArtistCatalogueRaisonee>();

    ArtistCatalogueRaisonee artistCatalogueRaisonee = new ArtistCatalogueRaisonee();
    artistCatalogueRaisonee.setAuthor("Prince");
    artistCatalogueRaisonee.setId(1L);
    artistCatalogueRaisonee.setType("novel");
    artistCatalogueRaisonee.setVolumes("20");
    artistCatalogueRaisonee.setYear(2017);
    artistCatalogueRaisoneeList.add(artistCatalogueRaisonee);
    artist.setArtistCatalogueRaisonees(artistCatalogueRaisoneeList);

    List<Category> categoryList = new ArrayList<>();
    Category category = new Category();
    category.setId(1L);
    category.setName("test category");
    category.setDescription("A category for testing");
    categoryList.add(category);

    artist.setCategories(categoryList);

    artists.add(artist);

    Artist artist1 = new Artist();
    artist1.setApproved(true);
    artist1.setBirthYear(1984);
    artist1.setDeathYear(2001);
    artist1.setDisplay("sothebys");
    artist1.setExternalId("0322");
    artist1.setFirstName("fname");
    artist1.setGender("M");
    artist1.setId(2L);
    artist1.setLastName("lname");

    List<Country> countriesList1 = new ArrayList<Country>();
    Country country1 = new Country();
    country1.setId(1L);
    country1.setCountryName("American");
    countriesList1.add(country1);
    artist.setCountries(countriesList1);


    List<ArtistCatalogueRaisonee> artistCatalogueRaisoneelist1 =
        new ArrayList<ArtistCatalogueRaisonee>();

    ArtistCatalogueRaisonee artistCatalogueRaisonee1 = new ArtistCatalogueRaisonee();
    artistCatalogueRaisonee1.setAuthor("Prince");
    artistCatalogueRaisonee1.setId(2L);
    artistCatalogueRaisonee1.setType("Poetry");
    artistCatalogueRaisonee1.setVolumes("20");
    artistCatalogueRaisonee1.setYear(2017);
    artistCatalogueRaisoneelist1.add(artistCatalogueRaisonee);
    artist1.setArtistCatalogueRaisonees(artistCatalogueRaisoneeList);

    List<Category> categoryList2 = new ArrayList<>();
    Category category2 = new Category();
    category.setId(2L);
    category.setName("test category2");
    category.setDescription("A category for testing2");
    categoryList.add(category);

    artist.setCategories(categoryList);

    artists.add(artist1);
    return artists;
  }

  /**
   * Test case for Entity to DTO
   */
  @Test
  public void test_mapToDTO() {
    Artist artist = getArtist();

    ArtistDTO artistDto = modelMapper.map(artist, ArtistDTO.class);
    assertEquals(artist.getExternalId(), artistDto.getId());
    assertEquals(artist.getFirstName(), artistDto.getFirstName());
    assertEquals(artist.isApproved(), artistDto.getApproved());
  }

  /**
   * Test case for EntityList to DTOList
   */
  @Test
  public void test_mapToDTOList() {
    List<Artist> artistList = new ArrayList<Artist>();
    artistList = getArtists();

    for (Artist artistObj : artistList) {
      ArtistDTO artistDtoobj = modelMapper.map(artistObj, ArtistDTO.class);
      assertEquals(artistObj.getExternalId(), artistDtoobj.getId());
      assertEquals(artistObj.getFirstName(), artistDtoobj.getFirstName());
      assertEquals(artistObj.isApproved(), artistDtoobj.getApproved());
    }
  }

  /**
   * Test case for DTO to Entity
   */
  @Test
  public void test_mapToEntity() {
    ArtistDTO artistDto = getArtistDto();

    Artist artist = modelMapper.map(artistDto, Artist.class);
    assertEquals(artistDto.getId(), artist.getExternalId());
    assertEquals(artistDto.getFirstName(), artist.getFirstName());
    assertEquals(artistDto.getApproved(), artist.isApproved());
  }

}
