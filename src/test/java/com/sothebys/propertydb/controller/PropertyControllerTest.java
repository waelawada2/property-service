package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.controller.support.GlobalExceptionHandler;
import com.sothebys.propertydb.dto.PropertyDTO;
import com.sothebys.propertydb.dto.PropertyIdsRequestDto;
import com.sothebys.propertydb.dto.request.PropertyAddRequestCatalogueRaisoneeDTO;
import com.sothebys.propertydb.dto.request.PropertyAddRequestDTO;
import com.sothebys.propertydb.dto.request.PropertyOwnerIdRequestDTO;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.mapper.PropertyModelMapper;
import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.ArtistCatalogueRaisonee;
import com.sothebys.propertydb.model.Authenticity;
import com.sothebys.propertydb.model.Category;
import com.sothebys.propertydb.model.EditionType;
import com.sothebys.propertydb.model.PresizeType;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.PropertyCatalogueRaisonee;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.search.PollStatus;
import com.sothebys.propertydb.search.PropertyOperation;
import com.sothebys.propertydb.search.PropertySearchBuilder;
import com.sothebys.propertydb.search.PropertySearchDTO;
import com.sothebys.propertydb.search.PropertySearchInputDTO;
import com.sothebys.propertydb.search.PropertySpecification;
import com.sothebys.propertydb.search.PropertySpecificationBuilder;
import com.sothebys.propertydb.search.PropertyUtil;
import com.sothebys.propertydb.search.SearchCriteria;
import com.sothebys.propertydb.service.ArtistService;
import com.sothebys.propertydb.service.CategoryService;
import com.sothebys.propertydb.service.EditionTypeService;
import com.sothebys.propertydb.service.ExportService;
import com.sothebys.propertydb.service.PresizeTypeService;
import com.sothebys.propertydb.service.PropertyService;
import com.sothebys.propertydb.service.external.client.ClientService;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


public class PropertyControllerTest {

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Mock
  ArtistService artistService;

  @Mock
  CategoryService categoryService;

  @Mock
  EditionTypeService editionTypeService;

  private MockMvc mockMvc;

  @InjectMocks
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Mock
  PresizeTypeService presizeTypeService;

  @InjectMocks
  private PropertyController propertyController;

  @Mock
  PropertyModelMapper propertyModelMapper;

  @Mock
  private PropertySearchBuilder propertySearchBuilder;

  @Mock
  PropertyService propertyService;
  
  @Mock
  ClientService ClientService;
  
  @Mock
  private PropertySpecificationBuilder propertySpecificationBuilder;

  @Mock
  PropertyUtil propertyUtil;
  
  @Mock
  ExportService exportService;

  /**
   * for create PageRequest
   *
   * @return
   */
  public Pageable createPageRequest() {
    return new PageRequest(0, 50, Sort.Direction.ASC, "id");
  }

  /**
   * @return category
   */
  public Artist getArtistObject() {
    Artist artist = new Artist();
    artist.setId(1L);
    artist.setBirthYear(1956);
    artist.setDeathYear(1989);
    artist.setApproved(true);
    artist.setGender("Male");
    artist.setFirstName("Test");
    artist.setLastName("Lname");
    return artist;
  }

  public User getUser() {
    User user = new User();
    user.setKeycloakUserId("123456-7890123-456789");
    user.setFirstName("Test");
    user.setLastName("User");
    user.setUserName("test.user");
    user.setEmail("test.user@sothebys.com");
    user.setId(1L);
    return user;
  }

  /**
   * @return
   */
  public Category getCategory() {
    Category category = new Category();
    category.setId(1L);
    category.setName("Test1");
    category.setDescription("Test");
    return category;
  }

  /**
   * @return
   */
  public EditionType getEditionType() {
    EditionType editionType = new EditionType();
    editionType.setId(1L);
    editionType.setName("Test1");
    editionType.setDescription("Test");
    return editionType;
  }

  /**
   * @return
   */
  public PresizeType getPresizeType() {
    PresizeType presizeType = new PresizeType();
    presizeType.setId(1L);
    presizeType.setName("Test1");
    presizeType.setDescription("Test");
    return presizeType;
  }

  /**
   * @return categoryDTO
   */
  public PropertyAddRequestDTO getPropertyAddRequestDto() {
    List<String> artistList = new ArrayList<>();
    ArtistCatalogueRaisonee artistCatalogueRaisonee = new ArtistCatalogueRaisonee();

    artistCatalogueRaisonee.setId(1L);
    PropertyAddRequestCatalogueRaisoneeDTO propertyCatalogueRaisonee =
        new PropertyAddRequestCatalogueRaisoneeDTO();
    propertyCatalogueRaisonee.setId(1L);
    propertyCatalogueRaisonee.setNumber("Test");
    propertyCatalogueRaisonee.setVolume("Test");
    propertyCatalogueRaisonee.setArtistCatalogueRaisoneeId(1L);
    List<PropertyAddRequestCatalogueRaisoneeDTO> propertCatalogueRaisoneeList = new ArrayList<>();
    propertCatalogueRaisoneeList.add(propertyCatalogueRaisonee);
    artistList.add("1");
    PropertyAddRequestDTO propertyDTO = new PropertyAddRequestDTO();
    propertyDTO.setArtistIds(artistList);
    propertyDTO.setAuthentication(true);
    propertyDTO.setCategory("category");
    propertyDTO.setEditionName("edition name");
    propertyDTO.setPresizeName("presize name");
    propertyDTO.setPropertyCatalogueRaisonees(propertCatalogueRaisoneeList);
    return propertyDTO;
  }

  public PropertyCatalogueRaisonee getPropertyCatalogueRaisonees() {
    PropertyCatalogueRaisonee propertyCatalogueRaisonees = new PropertyCatalogueRaisonee();
    propertyCatalogueRaisonees.setId(1L);
    propertyCatalogueRaisonees.setNumber("Number");
    propertyCatalogueRaisonees.setVolume("Volume");
    return propertyCatalogueRaisonees;
  }

  /**
   * @return property
   */
  public Property getPropertyObject() {
    Property property = new Property();
    property.setId(1L);
    Artist artist = new Artist();
    artist.setLastName("Picasso");
    artist.setId(1L);
    property.setArtists(Arrays.asList(artist));
    Authenticity authenticity = new Authenticity();
    authenticity.setAuthentication(true);
    property.setAuthenticity(authenticity);
    property.setImagePath("2017321055254");
    property.setExternalId("00000002");
    return property;
  }

  /**
   * @return categoryDTO
   */
  public PropertyDTO getPropertyObjectDto() {
    PropertyDTO propertyDTO = new PropertyDTO();
    propertyDTO.setAuthentication(true);
    propertyDTO.setImagePath("2017321055254");
    return propertyDTO;
  }

  /**
   * @return PropertySearchDTO's
   */
  public List<PropertySearchDTO> getPropertySearchDto() {
    List<PropertySearchDTO> propertysearchDtos = new ArrayList<PropertySearchDTO>();
    PropertySearchDTO propertysearchdto = new PropertySearchDTO();
    propertysearchdto.setId("1");
    propertysearchDtos.add(propertysearchdto);
    PropertySearchDTO propertysearchdto1 = new PropertySearchDTO();
    propertysearchdto1.setId("2");
    propertysearchDtos.add(propertysearchdto1);

    return propertysearchDtos;
  }

  public PropertySearchInputDTO getPropertySearchInputDTO() {
    PropertySearchInputDTO propertyDto = new PropertySearchInputDTO();
    propertyDto.setAuthentication(true);
    List<String> artistid = new ArrayList<String>();
    artistid.add("1");
    propertyDto.setArtistIds(artistid);
    return propertyDto;
  }

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders.standaloneSetup(propertyController)
        .setControllerAdvice(GlobalExceptionHandler.class)
        .setMessageConverters(new MappingJackson2HttpMessageConverter())
        .setCustomArgumentResolvers(pageableArgumentResolver).build();
  }

  /**
   * Test case for createParentCategory
   *
   * @throws Exception
   */
  @Test
  public void test_createProjectObject_success() throws Exception {

    Property property = getPropertyObject();
    Artist artistDTO = getArtistObject();
    PropertyAddRequestDTO propertyAddRequestDTO = getPropertyAddRequestDto();
    Category category = getCategory();
    PresizeType presizeType = getPresizeType();
    EditionType editionType = getEditionType();
    byte[] decodeimage = null;

    when(propertyModelMapper.mapToEntity(propertyAddRequestDTO)).thenReturn(property);
    when(propertyModelMapper.mapToDTO(property)).thenReturn(getPropertyObjectDto());
    when(propertyService.createProperty(property, getUser())).thenReturn(property);
    when(categoryService
        .findByCategoryNameAndParentCategoryName(propertyAddRequestDTO.getCategory(), null))
            .thenReturn(category);
    when(presizeTypeService.getPresizeTypeByName(propertyAddRequestDTO.getPresizeName()))
        .thenReturn(presizeType);
    when(editionTypeService.getEditionTypeByName(propertyAddRequestDTO.getEditionName()))
        .thenReturn(editionType);
    when(artistService.findArtistById(1L)).thenReturn(artistDTO);
    mockMvc
        .perform(post("/properties").contentType(MediaType.APPLICATION_JSON)
            .requestAttr("requestUser", getUser()).content(asJsonString(propertyAddRequestDTO)))
        .andExpect(status().isCreated());
    verify(propertyService, times(1)).createProperty(property, getUser());
    verifyNoMoreInteractions(propertyService);
  }

  /**
   * Test case for getAllProperties failure scenario
   *
   * @throws Exception
   */
  /*
   * @Test(expected = PropertyServiceException.class) public void
   * test_findAllPropertyObject_NotFound() throws Exception { Property property =
   * getPropertyObject(); when(propertyController.findAllPropertyObjects()) .thenThrow(new
   * PropertyServiceException("Property Object id not found...", "loggerMessage"));
   * mockMvc.perform(get("/propeties",
   * property.getId())).andDo(print()).andExpect(status().isInternalServerError())
   * .andExpect(jsonPath("$.error").value("Property id not found..."));
   *
   * }
   */

  /**
   * Test case for deleteArtistById
   *
   * @throws Exception
   */
  @Test
  public void test_deletePropertyId_success() throws Exception {
    Property property = getPropertyObject();
    when(propertyService.getPropertyByExternalId(property.getExternalId())).thenReturn(property);
    mockMvc.perform(delete("/properties/{externalId}", property.getExternalId()))
        .andExpect(status().isNoContent());
    verify(propertyService, times(1)).deletePropertyByExternalId(property.getExternalId());
    verifyNoMoreInteractions(propertyService);
  }

  /**
   * Test case for findAllCategories
   *
   * @throws Exception
   */
  /*
   * @Test public void test_findAllPropertyObjects_success() throws Exception { List<Property>
   * propertyList = new ArrayList<Property>(); Property property1 = getPropertyObject(); Property
   * property2 = getPropertyObject(); PropertyDTO propertyDto = getPropertyObjectDto();
   * propertyList.add(property1); propertyList.add(property2);
   *
   * when(propertyService.getProperties()).thenReturn(propertyList); when(modelMapper.map(property1,
   * PropertyDTO.class)).thenReturn(propertyDto);
   *
   * mockMvc.perform(get("/properties")).andExpect(status().isOk())
   * .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
   * .andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].id", is(1)))
   * .andExpect(jsonPath("$[0].authentication", is(true))).andExpect(jsonPath("$[1].id", is(1)))
   * .andExpect(jsonPath("$[1].authentication", is(true)));
   *
   * verify(propertyService, times(1)).getProperties(); verifyNoMoreInteractions(propertyService); }
   */
  /**
   * Test case for findArtistById notFound scenario
   *
   * @throws Exception
   */
  @Ignore
  @Test(expected = NotFoundException.class)
  public void test_getPropertyById_NotFound() throws Exception {
    Property property = getPropertyObject();
    when(propertyService.getPropertyByExternalId(property.getExternalId())).thenReturn(property);
    when(propertyController.getProperty(property.getExternalId()))
        .thenThrow(new NotFoundException());
    when(propertyService.getPropertyByExternalId(property.getExternalId())).thenReturn(null);
    mockMvc.perform(get("/properties/{externalId}", property.getExternalId())).andDo(print())
        // .andExpect(status().isInternalServerError())
        .andExpect(jsonPath("$.error").value("Property id not found..."));
  }

  /**
   * Test case for findArtistById
   *
   * @throws Exception
   */
  @Test
  public void test_getPropertyById_success() throws Exception {
    Property property = getPropertyObject();
    PropertyDTO propertyDto = getPropertyObjectDto();
    when(propertyService.getPropertyByExternalId(propertyDto.getId())).thenReturn(property);
    when(propertyModelMapper.mapToDTO(property)).thenReturn(propertyDto);
    when(propertyService.getPropertyByExternalId(property.getExternalId())).thenReturn(property);
    mockMvc.perform(get("/properties/{externalId}", property.getExternalId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.imagePath", is(property.getImagePath()))).andDo(print());
    verify(propertyService, times(1)).getPropertyByExternalId(property.getExternalId());
    verifyNoMoreInteractions(propertyService);

  }

  /**
   * Test case for getPropertyImageDetailsByID_PropertyNotfound at DataBase
   *
   * @throws Exception
   */
  public void test_getPropertyImageDetailsByID_PropertyNotfound() throws Exception {
    String imageId = "2017321055254";
    Property property = getPropertyObject();

    when(propertyService.createDefaultImage(eq(property.getExternalId()), any(), anyLong(), getUser()))
        .thenThrow(new NotFoundException("Property not found"));
    mockMvc
        .perform(get("/properties/{externalId}/image.jpg", imageId).requestAttr("requestUser",
            getUser()))
        .andDo(print()).andExpect(status().isNotFound())
        .andExpect(jsonPath("$.error").value("Property not found"));

  }

  /**
   * Test case for getPropertyImageDetailsByID
   *
   * @throws Exception
   */
  @Ignore("Needs to be fixed, because the success case expects an incorrect HTTP status code 406.")
  @Test
  public void test_getPropertyImageDetailsByID_success() throws Exception {
    Property property = getPropertyObject();
    PropertyDTO propertyDto = getPropertyObjectDto();
    String imageData =
        "/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAJABYDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9XvB+mtqDLKyosinoxKswzwf/ANfpX0F8OQy+DrUN94GTvn/lo1eJ+Ev9fa/Q/wBK9y8D/wDIsW/1f/0M0Aa1FFFAH//Z";
    byte[] decodeimage = Base64.getDecoder().decode(imageData);
    when(propertyService.getPropertyByExternalId(property.getExternalId())).thenReturn(property);
    when(propertyService.getDefaultImage(eq(property.getExternalId())))
        .thenReturn(Pair.of(new ByteArrayInputStream(decodeimage), (long) decodeimage.length));
    mockMvc
        .perform(get("/properties/{externalId}/images/default", property.getExternalId())
            .contentType(MediaType.IMAGE_JPEG_VALUE).content(decodeimage))
        .andExpect(status().is(406));
    verify(propertyService, times(1)).getDefaultImage(property.getExternalId());
    verifyNoMoreInteractions(propertyService);
  }

  /**
   * Test case for search Property
   *
   * @throws Exception
   */

  @Test
  @Ignore
  public void test_searchProperty_success() throws Exception {
    Property property = getPropertyObject();
    List<Property> propertylist = new ArrayList<Property>();
    propertylist.add(property);
    Page<Property> properties = new PageImpl<Property>(propertylist);

    PropertySearchInputDTO propertyDto = getPropertySearchInputDTO();

    Pageable pageRequest = createPageRequest();
    SearchCriteria criteria =
        new SearchCriteria("id", PropertyOperation.getSimpleOperation(':'), "1");
    Page<PropertySearchDTO> artistdtos =
        new PageImpl<PropertySearchDTO>(getPropertySearchDto(), pageRequest, 0L);
    Specification<Property> spec = new PropertySpecification(criteria);
    when(PropertyUtil.validatePropertyDTO(propertyDto)).thenReturn(false);
    when(propertyService.findAllProperties(pageRequest)).thenReturn(properties);
    when(propertySearchBuilder.createSpecificationBuilder(propertyDto))
        .thenReturn(propertySpecificationBuilder);
    when(propertySpecificationBuilder.build()).thenReturn(spec);
    when(propertyService.findBySearchTerm(spec, pageRequest)).thenReturn(properties);

    mockMvc.perform(get("/properties").contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(propertyDto))).andExpect(status().isOk());
    verify(propertyService, times(1)).findBySearchTerm(spec, pageRequest);
    verify(propertyService, times(1)).findAllProperties(pageRequest);
    verifyNoMoreInteractions(propertyService);
  }

  /**
   * Test case for update Property
   *
   * @throws Exception
   */
  @Test
  public void test_updatePropertyObject_success() throws Exception {
    Property property = getPropertyObject();
    PropertyDTO propertyDTO = getPropertyObjectDto();
    PropertyAddRequestDTO propertyAddRequestDTO = getPropertyAddRequestDto();
    when(propertyModelMapper.mapToEntity(propertyAddRequestDTO)).thenReturn(property);
    when(propertyService.getPropertyByExternalId(property.getExternalId())).thenReturn(property);
    when(propertyService.updatePropertyByExternalId(property.getExternalId(), property, getUser(),
        false)).thenReturn(property);
    when(propertyModelMapper.mapToDTO(property)).thenReturn(propertyDTO);
    mockMvc.perform(put("/properties/{externalId}", property.getExternalId())
        .requestAttr("requestUser", getUser()).contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(propertyAddRequestDTO))).andExpect(status().isOk());
    verify(propertyService, times(1)).updatePropertyByExternalId(property.getExternalId(), property,
        getUser(), false);
    verifyNoMoreInteractions(propertyService);
  }


  @Test
  public void test_findAllPropertyObjectsWithClientFields_success() throws Exception {
    when(propertySearchBuilder.createSpecificationBuilder(any(PropertySearchInputDTO.class)))
        .thenCallRealMethod();
    when(ClientService.clientIdsSearch(anyObject(), anyObject()))
        .thenReturn(Arrays.asList("EOS-1111", "EOS-2222", "EOS-3333"));
    PropertyDTO propertyDTO = new PropertyDTO();
    propertyDTO.setId("ABC-123456");
    when(propertyModelMapper.mapEntityPageIntoDTOPage(anyObject(), anyObject()))
        .thenReturn(new PageImpl<>(Collections.singletonList(propertyDTO)));
    mockMvc.perform(get("/properties").principal(new TestingAuthenticationToken(null, null)))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    verify(propertyService, times(1)).findBySearchTerm(anyObject(),anyObject());
    verifyNoMoreInteractions(propertyService);
  }

  /**
   * Test case for patch Property current owner id
   *
   * @throws Exception
   */
  @Test
  public void test_patchPropertyObject_success() throws Exception {
    PropertyOwnerIdRequestDTO propertyOwnerIdRequestDTO = getPropertyOwnerIdRequestDTO();
    when(propertyService.getPropertyByExternalId(anyString())).thenReturn(getPropertyObject());
    when(propertyService
        .updatePropertyByExternalId(anyString(), any(Property.class), any(User.class),
            anyBoolean()))
        .thenReturn(any(Property.class));
    mockMvc.perform(patch("/properties/")
        .requestAttr("requestUser", getUser()).contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(propertyOwnerIdRequestDTO))).andExpect(status().isNoContent());
    verify(propertyService, times(5)).updatePropertyByExternalId(
        anyString(), any(Property.class), any(User.class), anyBoolean());
  }

  private PropertyOwnerIdRequestDTO getPropertyOwnerIdRequestDTO() {
    PropertyOwnerIdRequestDTO poid= new PropertyOwnerIdRequestDTO();
    poid.setCurrentOwnerId("CC-87654321");
    poid.setExternalIds(Arrays.asList("00000001","00000002","00000003","00000004","00000005"));
    return poid;
  }

  /**
   * Test case for mergeProperty
   *
   * @throws Exception
   */
  @Test
  public void test_mergePropertyObject_success() throws Exception {
    Property mergeFromProperty = getPropertyObject();
    Property mergeToProperty = getPropertyObject();
    PropertyDTO propertyDTO = getPropertyObjectDto();
    PropertyAddRequestDTO propertyAddRequestDTO = getPropertyAddRequestDto();
    when(propertyModelMapper.mapToEntity(propertyAddRequestDTO)).thenReturn(mergeToProperty);
    when(propertyService.getPropertyByExternalId(mergeFromProperty.getExternalId()))
        .thenReturn(mergeFromProperty);
    when(propertyService.mergePropertyByExternalId(mergeFromProperty.getExternalId(),
        mergeToProperty.getExternalId(), mergeToProperty, getUser(), false, null))
        .thenReturn(mergeToProperty);
    when(propertyModelMapper.mapToDTO(mergeToProperty)).thenReturn(propertyDTO);
    mockMvc.perform(put("/properties/{mergeToId}?action=merge&mergeFromId={newPropertyId}",
        mergeFromProperty.getExternalId(), mergeToProperty.getExternalId())
        .principal(new TestingAuthenticationToken(null, null))
        .requestAttr("requestUser", getUser()).contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(propertyAddRequestDTO))
    ).andExpect(status().isOk());
    verify(propertyService, times(1)).mergePropertyByExternalId(mergeFromProperty.getExternalId(),
        mergeToProperty.getExternalId(), mergeToProperty, getUser(), false, null);
    verifyNoMoreInteractions(propertyService);
  }

  /**
   * Test that an attempt to create a property without an artist identifier
   * results in a bad request response (HTTP 400).
   *   
   */
  @Test
  public void testCreateProjectObjectWithoutArtistIdentifier() throws Exception {

    PropertyAddRequestDTO dto = getPropertyAddRequestDto();
    dto.setArtistIds(Collections.emptyList());
    dto.setUlanIds(Collections.emptyList());

    mockMvc.perform(post("/properties").contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(dto))).andExpect(status().isBadRequest());
  }

  /**
  * @return List<Property>
  */
  public List<Property> getPropertyObjects() {
    List<Property> properties = new ArrayList<Property>();
    Property property = new Property();
    property.setId(1L);
    Artist artist = new Artist();
    artist.setLastName("Picasso");
    artist.setId(1L);
    property.setArtists(Arrays.asList(artist));
    Authenticity authenticity = new Authenticity();
    authenticity.setAuthentication(true);
    property.setAuthenticity(authenticity);
    property.setImagePath("2017321055254");
    property.setExternalId("00000002");
    properties.add(property);
    return properties;
  }

  /**
   * @return PropertyDTO
   */
  public List<PropertyDTO> getPropertyObjectDtos() {
    List<PropertyDTO> propertyDTOs = new ArrayList<PropertyDTO>();
    PropertyDTO propertyDTO = new PropertyDTO();
    propertyDTO.setAuthentication(true);
    propertyDTO.setImagePath("2017321055254");
    propertyDTOs.add(propertyDTO);
    return propertyDTOs;
  }

  /**
   * Test case for create Duplicate property/Object
   * 
   * @throws Exception
   */
  @Test
  public void test_createDuplicateObject_success() throws Exception {
    Property property = getPropertyObject();
    List<Property> properties = getPropertyObjects();
    Artist artistDTO = getArtistObject();
    PropertyAddRequestDTO propertyAddRequestDTO = getPropertyAddRequestDto();
    Category category = getCategory();
    PresizeType presizeType = getPresizeType();
    EditionType editionType = getEditionType();
    String externalId = "00000002";
    Integer numberOfDuplicates = 1;
    when(propertyModelMapper.mapToEntity(propertyAddRequestDTO)).thenReturn(property);
    when(propertyModelMapper.mapToDTO(properties)).thenReturn(getPropertyObjectDtos());
    when(propertyService.createDuplicateProperties(externalId, numberOfDuplicates, getUser()))
        .thenReturn(properties);
    when(categoryService
        .findByCategoryNameAndParentCategoryName(propertyAddRequestDTO.getCategory(), null))
            .thenReturn(category);
    when(presizeTypeService.getPresizeTypeByName(propertyAddRequestDTO.getPresizeName()))
        .thenReturn(presizeType);
    when(editionTypeService.getEditionTypeByName(propertyAddRequestDTO.getEditionName()))
        .thenReturn(editionType);
    when(artistService.findArtistById(1L)).thenReturn(artistDTO);
    mockMvc
        .perform(post("/properties/{externalId}?action=duplicate&numberOfDuplicates=1", externalId)
            .requestAttr("requestUser", getUser()).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());
    verify(propertyService, times(1)).createDuplicateProperties(externalId, numberOfDuplicates,
        getUser());
    verifyNoMoreInteractions(propertyService);
  }
  
  /**
   * Test case for export properties
   *
   * @throws Exception
   */
  @Test
  public void test_exportProperties_success() throws Exception {
    String exportId = "64300571-0e04-4785-8afc-8da2b508a358";
    Property property = getPropertyObject();
    PropertyAddRequestDTO propertyAddRequestDTO = getPropertyAddRequestDto();
    when(propertyModelMapper.mapToEntity(propertyAddRequestDTO)).thenReturn(property);
    when(propertyModelMapper.mapToDTO(property)).thenReturn(getPropertyObjectDto());
    when(propertyService.exportPropertyData()).thenReturn(exportId);
    mockMvc.perform(post("/properties/export").requestAttr("requestUser", getUser())
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted());
    verify(propertyService, times(1)).exportPropertyData();
    verifyNoMoreInteractions(propertyService);
  }
  
  /**
   * Test case for exportIdStatus 
   *
   * @throws Exception
   */
  @Test
  public void test_exportIdStatus_success() throws Exception {
    String exportId = "64300571-0e04-4785-8afc-8da2b508a358";
    when(exportService.findExportStatus(exportId)).thenReturn(PollStatus.Completed);
    mockMvc.perform(get("/properties/export/{id}/status", exportId)).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    verify(exportService, times(1)).findExportStatus(exportId);
    verifyNoMoreInteractions(exportService);
  }
  
  
  /**
   * Test case for exportIdStatus failure scenario
   *
   * @throws Exception
   */
  @Test(expected = NotFoundException.class)
  public void test_exportIdStatus_NotFound() throws Exception {
    String exportId = "64300571-0e04-4785-8afc-8da2b508a358";
    when(propertyController.exportIdStatus(exportId)).thenThrow(new NotFoundException());
    when(exportService.findExportStatus(exportId)).thenReturn(PollStatus.Error);
    mockMvc.perform(get("/properties/export/{id}/status", exportId)).andDo(print())
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$.error").value("No export id found with ID=" + exportId));
  }
  
  /**
   * Test case for list of Properties
   *
   * @throws Exception
   */
  @Test
  public void test_deleteAllProperties_success() throws Exception {
    
    PropertyIdsRequestDto propertyIdsRequestDTto=new PropertyIdsRequestDto();
    List<String> propertyIdList=new ArrayList<String>();
    propertyIdList.add("OO-00000005");
    propertyIdList.add("OO-00000004");
    propertyIdList.add("OO-00000006");
    propertyIdsRequestDTto.setExternalIds(propertyIdList);
    Integer count =new Integer(3);

    when(propertyService.deleteAllProperties(propertyIdsRequestDTto.getExternalIds())).thenReturn(count);
    mockMvc.perform(post("/properties/delete/propertyIds").contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(propertyIdsRequestDTto))).andExpect(status().isCreated());
    verify(propertyService, times(1)).deleteAllProperties(propertyIdsRequestDTto.getExternalIds());
  }
   

}

