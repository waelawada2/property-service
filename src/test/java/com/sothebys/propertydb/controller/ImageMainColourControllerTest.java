/**
 * 
 */
package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.ImageMainColourDTO;
import com.sothebys.propertydb.mapper.ImageMainColourModelMapper;
import com.sothebys.propertydb.model.ImageMainColour;
import com.sothebys.propertydb.service.ImageMainColourService;

/**
 * @author aneesha.l
 *
 */
public class ImageMainColourControllerTest {

	@InjectMocks
	private ImageMainColourController imageMainColourController;

	@Mock
	private ImageMainColourModelMapper imageMainColourModelMapper;

	@Mock
	private ImageMainColourService imageMainColourService;

	@InjectMocks
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	private MockMvc mockMvc;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		mockMvc = MockMvcBuilders.standaloneSetup(imageMainColourController)
				.setMessageConverters(new MappingJackson2HttpMessageConverter())
				.setCustomArgumentResolvers(pageableArgumentResolver).build();

	}

	/**
	 * @return ImageMainColour
	 */
	public ImageMainColour getImageMainColour() {
		ImageMainColour imageMainColour = new ImageMainColour();
		imageMainColour.setId(1L);
		imageMainColour.setName("Red");

		return imageMainColour;
	}

	/**
	 * @return ImageMainColour list
	 */
	public List<ImageMainColour> getImageMainColourList() {
		List<ImageMainColour> imageMainColourList = new ArrayList<ImageMainColour>();

		ImageMainColour imageMainColour = new ImageMainColour();
		imageMainColour.setId(new Long(1));
		imageMainColour.setName("Red");
		imageMainColourList.add(imageMainColour);

		imageMainColour = new ImageMainColour();
		imageMainColour.setId(new Long(2));
		imageMainColour.setName("Blue");
		imageMainColourList.add(imageMainColour);

		imageMainColour = new ImageMainColour();
		imageMainColour.setId(new Long(3));
		imageMainColour.setName("Black");
		imageMainColourList.add(imageMainColour);

		imageMainColour = new ImageMainColour();
		imageMainColour.setId(new Long(4));
		imageMainColour.setName("White");
		imageMainColourList.add(imageMainColour);

		return imageMainColourList;
	}

	/**
	 * @return ImageMainColourDTO
	 */
	public ImageMainColourDTO getImageMainColourDTO() {
		ImageMainColourDTO imageMainColourDTO = new ImageMainColourDTO();
		imageMainColourDTO.setId(1L);
		imageMainColourDTO.setName("Red");

		return imageMainColourDTO;
	}

	/**
	 * @return ImageMainColourDTO list
	 */
	public List<ImageMainColourDTO> getImageMainColourDTOList() {
		List<ImageMainColourDTO> imageMainColourDTOList = new ArrayList<ImageMainColourDTO>();

		ImageMainColourDTO imageMainColourDTO = new ImageMainColourDTO();
		imageMainColourDTO.setId(new Long(1));
		imageMainColourDTO.setName("Red");
		imageMainColourDTOList.add(imageMainColourDTO);

		imageMainColourDTO = new ImageMainColourDTO();
		imageMainColourDTO.setId(new Long(2));
		imageMainColourDTO.setName("Blue");
		imageMainColourDTOList.add(imageMainColourDTO);

		imageMainColourDTO = new ImageMainColourDTO();
		imageMainColourDTO.setId(new Long(3));
		imageMainColourDTO.setName("Black");
		imageMainColourDTOList.add(imageMainColourDTO);

		imageMainColourDTO = new ImageMainColourDTO();
		imageMainColourDTO.setId(new Long(4));
		imageMainColourDTO.setName("White");
		imageMainColourDTOList.add(imageMainColourDTO);

		return imageMainColourDTOList;
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Test case for createImageMainColour
	 *
	 * @throws Exception
	 */

	@Test
	public void test_createImageMainColour_success() throws Exception {
		ImageMainColour imageMainColour = getImageMainColour();
		ImageMainColourDTO imageMainColourDTO = getImageMainColourDTO();

		when(imageMainColourModelMapper.map(imageMainColourDTO, ImageMainColour.class)).thenReturn(imageMainColour);
		when(imageMainColourService.createImageMainColour(imageMainColour)).thenReturn(imageMainColour);
		when(imageMainColourModelMapper.map(imageMainColour, ImageMainColourDTO.class)).thenReturn(imageMainColourDTO);

		mockMvc.perform(post("/imageMainColours").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(imageMainColourDTO))).andExpect(status().isCreated())
				.andExpect(header().string("Location", containsString("/imageMainColours/")));
		verify(imageMainColourService, times(1)).createImageMainColour(imageMainColour);
		verifyNoMoreInteractions(imageMainColourService);
	}

	/**
	 * Test case for deleteImageMainColourById
	 *
	 * @throws Exception
	 */

	@Test
	public void test_deleteImageMainColourById_success() throws Exception {
		ImageMainColour imageMainColour = getImageMainColour();
		doNothing().when(imageMainColourService).deleteImageMainColourById(imageMainColour.getId());
		mockMvc.perform(delete("/imageMainColours/{id}", imageMainColour.getId())).andExpect(status().isNoContent());
		verify(imageMainColourService, times(1)).deleteImageMainColourById(imageMainColour.getId());
	}

	/**
	 * Test case for updateImageMainColour success
	 *
	 * @throws Exception
	 */
	@Test
	public void test_updateImageMainColour_success() throws Exception {
		ImageMainColour imageMainColour = getImageMainColour();
		ImageMainColourDTO imageMainColourDTO = getImageMainColourDTO();

		when(imageMainColourModelMapper.map(imageMainColourDTO, ImageMainColour.class)).thenReturn(imageMainColour);
		when(imageMainColourService.updateImageMainColourById(imageMainColourDTO.getId(), imageMainColour))
				.thenReturn(imageMainColour);

		when(imageMainColourModelMapper.map(imageMainColour, ImageMainColourDTO.class)).thenReturn(imageMainColourDTO);

		mockMvc.perform(put("/imageMainColours/" + imageMainColourDTO.getId())
				.requestAttr("imageMainColourDTO", imageMainColourDTO).contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(imageMainColourDTO))).andExpect(status().isOk());
		verify(imageMainColourService, times(1)).updateImageMainColourById(imageMainColour.getId(), imageMainColour);
	}

	/**
	 * Test case for getting all MainColours
	 *
	 */
	@Test
	public void test_findAllImageMainColours() throws Exception {
		List<ImageMainColourDTO> imageMainColourDTOs = getImageMainColourDTOList();
		List<ImageMainColour> imageMainColours = getImageMainColourList();

		when(imageMainColourService.findAllImageMainColours()).thenReturn(imageMainColours);
		when(imageMainColourModelMapper.mapToImageMainColourDTOs(imageMainColours)).thenReturn(imageMainColourDTOs);

		mockMvc.perform(get("/imageMainColours").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(4)));
		;

		verify(imageMainColourService, times(1)).findAllImageMainColours();
		verifyNoMoreInteractions(imageMainColourService);

	}

	/**
	 * Test case for getting all MainColours
	 *
	 */
	@Test
	public void test_getImageMianColourById_success() throws Exception {
		ImageMainColour imageMainColour = getImageMainColour();
		ImageMainColourDTO imageMainColourDTO = getImageMainColourDTO();

		when(imageMainColourService.getImageMainColourById(imageMainColour.getId())).thenReturn(imageMainColour);
		when(imageMainColourModelMapper.map(imageMainColour, ImageMainColourDTO.class)).thenReturn(imageMainColourDTO);

		mockMvc.perform(get("/imageMainColours/imageMainColour/" + imageMainColourDTO.getId())
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is(imageMainColourDTO.getName()))).andDo(print());

		verify(imageMainColourService, times(1)).getImageMainColourById(imageMainColour.getId());
		verifyNoMoreInteractions(imageMainColourService);

	}

	/**
	 * Test case for getting all MainColours
	 *
	 */
	@Test(expected = Exception.class)
	public void test_getImageMianColourById_failure() throws Exception {
		ImageMainColour imageMainColour = getImageMainColour();
		ImageMainColourDTO imageMainColourDTO = getImageMainColourDTO();

		when(imageMainColourService.getImageMainColourById(imageMainColour.getId())).thenReturn(null);
		when(imageMainColourModelMapper.map(imageMainColour, ImageMainColourDTO.class)).thenReturn(imageMainColourDTO);

		mockMvc.perform(get("/imageMainColours/imageMainColour/" + imageMainColourDTO.getId())
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is(imageMainColourDTO.getName()))).andDo(print());

		verify(imageMainColourService, times(1)).getImageMainColourById(imageMainColour.getId());
		verifyNoMoreInteractions(imageMainColourService);

	}

}
