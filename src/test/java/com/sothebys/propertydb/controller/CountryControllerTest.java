package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.CountryDTO;
import com.sothebys.propertydb.mapper.CountryModelMapper;
import com.sothebys.propertydb.model.Country;
import com.sothebys.propertydb.service.CountryService;

public class CountryControllerTest {

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @InjectMocks
  private CountryController countryController;

  @Mock
  CountryModelMapper countryModelMapper;

  @Mock
  private CountryService countryService;

  private MockMvc mockMvc;

  /**
   * @return country
   */
  private Country getCountry() {
    Country country = new Country();
    country.setId(1L);
    country.setCountryName("U.S.A.");
    return country;
  }

  /**
   * @return countryDTO
   */
  private CountryDTO getCountryDTO() {
    CountryDTO countryDTO = new CountryDTO();
    countryDTO.setId(1L);
    countryDTO.setCountryName("U.S.A.");
    return countryDTO;
  }

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders.standaloneSetup(countryController)
        .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
  }

  /**
   * Test case for findAllCountries
   */
  @Test
  public void test_findAllCountries_success() throws Exception {
    List<Country> countries = Arrays.asList(new Country[] {getCountry(), getCountry()});
    List<CountryDTO> countriesDTOs =
        Arrays.asList(new CountryDTO[] {getCountryDTO(), getCountryDTO()});

    when(countryService.findAllCountries()).thenReturn(countries);
    when(countryModelMapper.mapToDTO(countries)).thenReturn(countriesDTOs);

    // @formatter:off
    mockMvc.perform(get("/countries")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].countryName", is("U.S.A."))).andExpect(jsonPath("$[1].id", is(1)))
        .andExpect(jsonPath("$[1].countryName", is("U.S.A.")));
    // @formatter:on

    verify(countryService, times(1)).findAllCountries();
    verifyNoMoreInteractions(countryService);
  }

  /**
   * Test case for createCountry
   *
   * @throws Exception
   */
  @Test
  public void test_createCountry_success() throws Exception {
    Country country = getCountry();
    CountryDTO countryDTO = getCountryDTO();

    when(countryModelMapper.map(countryDTO, Country.class)).thenReturn(country);
    when(countryService.createCountry(country)).thenReturn(country);
    when(countryModelMapper.map(country, CountryDTO.class)).thenReturn(countryDTO);
    mockMvc
        .perform(post("/countries").contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(country)))
        .andExpect(status().isCreated())
        .andExpect(header().string("Location", containsString("/countries")));;
    verify(countryService, times(1)).createCountry(country);
    verifyNoMoreInteractions(countryService);
  }

  /**
   * Test case for updateCountry
   *
   * @throws Exception
   */
  @Test
  public void test_updateCountry_success() throws Exception {
    Country country = getCountry();
    CountryDTO countryDTO = getCountryDTO();

    when(countryModelMapper.map(countryDTO, Country.class)).thenReturn(country);
    when(countryService.updateCountryById(country.getId(), country)).thenReturn(country);
    when(countryModelMapper.map(country, CountryDTO.class)).thenReturn(countryDTO);

    mockMvc
        .perform(put("/countries/{country-id}", country.getId())
            .contentType(MediaType.APPLICATION_JSON).content(asJsonString(country)))
        .andExpect(status().isOk());
    verify(countryService, times(1)).updateCountryById(country.getId(), country);
    verifyNoMoreInteractions(countryService);
  }

  /**
   * Test case for deleteCountry success scenario
   */
  @Test
  public void test_deleteCountry_success() throws Exception {
    Country country = getCountry();
    mockMvc.perform(delete("/countries/{country-id}", country.getId(), country.getId()))
        .andExpect(status().isNoContent());
    verify(countryService, times(1)).deleteCountryById(country.getId());
    verifyNoMoreInteractions(countryService);
  }

}
