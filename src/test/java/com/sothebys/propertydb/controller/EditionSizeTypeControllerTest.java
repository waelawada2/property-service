package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.EditionSizeTypeDTO;
import com.sothebys.propertydb.mapper.EditionSizeTypeModelMapper;
import com.sothebys.propertydb.model.EditionSizeType;
import com.sothebys.propertydb.service.EditionSizeTypeService;

public class EditionSizeTypeControllerTest {

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @InjectMocks
  private EditionSizeTypeController editionSizeTypeController;

  @Mock
  EditionSizeTypeModelMapper editionSizeTypeModelMapper;

  @Mock
  private EditionSizeTypeService editionSizeTypeService;

  private MockMvc mockMvc;

  /**
   * @return editionSizeType
   */
  private EditionSizeType getEditionSizeType() {
    EditionSizeType editionSizeType = new EditionSizeType();
    editionSizeType.setId(1L);
    editionSizeType.setName("editionSizeType");
    return editionSizeType;
  }

  /**
   * @return editionSizeTypeDTO
   */
  private EditionSizeTypeDTO getEditionSizeTypeDTO() {
    EditionSizeTypeDTO editionSizeTypeDTO = new EditionSizeTypeDTO();
    editionSizeTypeDTO.setId(1L);
    editionSizeTypeDTO.setName("editionSizeType");
    return editionSizeTypeDTO;
  }

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders.standaloneSetup(editionSizeTypeController)
        .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
  }

  /**
   * Test case for findAllEditionSizeTypes
   */
  @Test
  public void test_findAllEditionSizeTypes_success() throws Exception {
    List<EditionSizeType> editionSizeTypes =
        Arrays.asList(new EditionSizeType[] {getEditionSizeType(), getEditionSizeType()});
    List<EditionSizeTypeDTO> editionSizeTypeDTOs =
        Arrays.asList(new EditionSizeTypeDTO[] {getEditionSizeTypeDTO(), getEditionSizeTypeDTO()});

    when(editionSizeTypeService.findAllEditionSizeTypes()).thenReturn(editionSizeTypes);
    when(editionSizeTypeModelMapper.mapToDTO(editionSizeTypes)).thenReturn(editionSizeTypeDTOs);

    // @formatter:off
    mockMvc.perform(get("/edition-size-types")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].name", is("editionSizeType")))
        .andExpect(jsonPath("$[1].id", is(1)))
        .andExpect(jsonPath("$[1].name", is("editionSizeType")));
    // @formatter:on

    verify(editionSizeTypeService, times(1)).findAllEditionSizeTypes();
    verifyNoMoreInteractions(editionSizeTypeService);
  }

  /**
   * Test case for createEditionSizeType
   *
   * @throws Exception
   */
  @Test
  public void test_createEditionSizeType_success() throws Exception {
    EditionSizeType editionSizeType = getEditionSizeType();
    EditionSizeTypeDTO editionSizeTypeDto = getEditionSizeTypeDTO();

    when(editionSizeTypeModelMapper.map(editionSizeTypeDto, EditionSizeType.class))
        .thenReturn(editionSizeType);
    when(editionSizeTypeService.createEditionSizeType(editionSizeType)).thenReturn(editionSizeType);
    when(editionSizeTypeModelMapper.map(editionSizeType, EditionSizeTypeDTO.class))
        .thenReturn(editionSizeTypeDto);
    mockMvc
        .perform(post("/edition-size-types").contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(editionSizeType)))
        .andExpect(status().isCreated())
        .andExpect(header().string("Location", containsString("/edition-size-types")));;
    verify(editionSizeTypeService, times(1)).createEditionSizeType(editionSizeType);
    verifyNoMoreInteractions(editionSizeTypeService);
  }

  /**
   * Test case for geEditionSizeTypeById
   *
   * @throws Exception
   */
  @Test
  public void test_geEditionSizeTypeById_success() throws Exception {
    EditionSizeType editionSizeType = getEditionSizeType();
    EditionSizeTypeDTO editionSizeTypeDto = getEditionSizeTypeDTO();

    when(editionSizeTypeService.findEditionSizeTypeById(editionSizeType.getId()))
        .thenReturn(editionSizeType);
    when(editionSizeTypeModelMapper.map(editionSizeType, EditionSizeTypeDTO.class))
        .thenReturn(editionSizeTypeDto);
    mockMvc.perform(get("/edition-size-types/{editionSizeTypeId}", 1)).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.name", is(editionSizeType.getName()))).andDo(print());
    verify(editionSizeTypeService, times(1)).findEditionSizeTypeById(editionSizeType.getId());
    verifyNoMoreInteractions(editionSizeTypeService);
  }

  /**
   * Test case for updateEditionSizeType
   *
   * @throws Exception
   */
  @Test
  public void test_updateEditionSizeType_success() throws Exception {
    EditionSizeType editionSizeType = getEditionSizeType();
    EditionSizeTypeDTO editionSizeTypeDto = getEditionSizeTypeDTO();

    when(editionSizeTypeModelMapper.map(editionSizeTypeDto, EditionSizeType.class))
        .thenReturn(editionSizeType);
    when(editionSizeTypeService.updateEditionSizeType(editionSizeType.getId(), editionSizeType))
        .thenReturn(editionSizeType);
    when(editionSizeTypeModelMapper.map(editionSizeType, EditionSizeTypeDTO.class))
        .thenReturn(editionSizeTypeDto);

    mockMvc
        .perform(put("/edition-size-types/{editionSizeTypeId}", editionSizeType.getId())
            .contentType(MediaType.APPLICATION_JSON).content(asJsonString(editionSizeType)))
        .andExpect(status().isOk());
    verify(editionSizeTypeService, times(1)).updateEditionSizeType(editionSizeType.getId(),
        editionSizeType);
    verifyNoMoreInteractions(editionSizeTypeService);
  }

  /**
   * Test case for deleteEditionSizeType
   *
   * @throws Exception
   */
  @Test
  public void test_deleteEditionSizeType_success() throws Exception {
    mockMvc.perform(delete("/edition-size-types/{editionSizeTypeId}", 1L))
        .andExpect(status().isNoContent());
    verify(editionSizeTypeService, times(1)).deleteEditionSizeType(1L);
    verifyNoMoreInteractions(editionSizeTypeService);
  }

}
