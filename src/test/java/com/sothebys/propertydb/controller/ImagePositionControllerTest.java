/**
 * 
 */
package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.ImagePositionDTO;
import com.sothebys.propertydb.mapper.ImagePositionModelMapper;
import com.sothebys.propertydb.model.ImagePosition;
import com.sothebys.propertydb.service.ImagePositionService;

/**
 * @author aneesha.l
 *
 */
public class ImagePositionControllerTest {

	@InjectMocks
	private ImagePositionController imagePositionController;

	@Mock
	private ImagePositionModelMapper imagePositionModelMapper;

	@Mock
	private ImagePositionService imagePositionService;

	@InjectMocks
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	private MockMvc mockMvc;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		mockMvc = MockMvcBuilders.standaloneSetup(imagePositionController)
				.setMessageConverters(new MappingJackson2HttpMessageConverter())
				.setCustomArgumentResolvers(pageableArgumentResolver).build();

	}

	/**
	 * @return ImagePosition
	 */
	public ImagePosition getImagePosition() {
		ImagePosition imagePosition = new ImagePosition();
		imagePosition.setId(1L);
		imagePosition.setName("Red");

		return imagePosition;
	}

	/**
	 * @return ImagePosition list
	 */
	public List<ImagePosition> getImagePositionList() {
		List<ImagePosition> imagePositionList = new ArrayList<ImagePosition>();

		ImagePosition imagePosition = new ImagePosition();
		imagePosition.setId(new Long(1));
		imagePosition.setName("Red");
		imagePositionList.add(imagePosition);

		imagePosition = new ImagePosition();
		imagePosition.setId(new Long(2));
		imagePosition.setName("Blue");
		imagePositionList.add(imagePosition);

		imagePosition = new ImagePosition();
		imagePosition.setId(new Long(3));
		imagePosition.setName("Black");
		imagePositionList.add(imagePosition);

		imagePosition = new ImagePosition();
		imagePosition.setId(new Long(4));
		imagePosition.setName("White");
		imagePositionList.add(imagePosition);

		return imagePositionList;
	}

	/**
	 * @return ImagePositionDTO
	 */
	public ImagePositionDTO getImagePositionDTO() {
		ImagePositionDTO imagePositionDTO = new ImagePositionDTO();
		imagePositionDTO.setId(1L);
		imagePositionDTO.setName("Red");

		return imagePositionDTO;
	}

	/**
	 * @return ImagePositionDTO list
	 */
	public List<ImagePositionDTO> getImagePositionDTOList() {
		List<ImagePositionDTO> imagePositionDTOList = new ArrayList<ImagePositionDTO>();

		ImagePositionDTO imagePositionDTO = new ImagePositionDTO();
		imagePositionDTO.setId(new Long(1));
		imagePositionDTO.setName("Red");
		imagePositionDTOList.add(imagePositionDTO);

		imagePositionDTO = new ImagePositionDTO();
		imagePositionDTO.setId(new Long(2));
		imagePositionDTO.setName("Blue");
		imagePositionDTOList.add(imagePositionDTO);

		imagePositionDTO = new ImagePositionDTO();
		imagePositionDTO.setId(new Long(3));
		imagePositionDTO.setName("Black");
		imagePositionDTOList.add(imagePositionDTO);

		imagePositionDTO = new ImagePositionDTO();
		imagePositionDTO.setId(new Long(4));
		imagePositionDTO.setName("White");
		imagePositionDTOList.add(imagePositionDTO);

		return imagePositionDTOList;
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Test case for createImagePosition
	 *
	 * @throws Exception
	 */

	@Test
	public void test_createImagePosition_success() throws Exception {
		ImagePosition imagePosition = getImagePosition();
		ImagePositionDTO imagePositionDTO = getImagePositionDTO();

		when(imagePositionModelMapper.map(imagePositionDTO, ImagePosition.class)).thenReturn(imagePosition);
		when(imagePositionService.createImagePosition(imagePosition)).thenReturn(imagePosition);
		when(imagePositionModelMapper.map(imagePosition, ImagePositionDTO.class)).thenReturn(imagePositionDTO);

		mockMvc.perform(
				post("/imagePositions").contentType(MediaType.APPLICATION_JSON).content(asJsonString(imagePositionDTO)))
				.andExpect(status().isCreated())
				.andExpect(header().string("Location", containsString("/imagePositions/")));
		verify(imagePositionService, times(1)).createImagePosition(imagePosition);
		verifyNoMoreInteractions(imagePositionService);
	}

	/**
	 * Test case for deleteImagePositionById
	 *
	 * @throws Exception
	 */

	@Test
	public void test_deleteImagePositionById_success() throws Exception {
		ImagePosition imagePosition = getImagePosition();
		doNothing().when(imagePositionService).deleteImagePositionById(imagePosition.getId());
		mockMvc.perform(delete("/imagePositions/{id}", imagePosition.getId())).andExpect(status().isNoContent());
		verify(imagePositionService, times(1)).deleteImagePositionById(imagePosition.getId());
	}

	/**
	 * Test case for updateImagePosition success
	 *
	 * @throws Exception
	 */
	@Test
	public void test_updateImagePosition_success() throws Exception {
		ImagePosition imagePosition = getImagePosition();
		ImagePositionDTO imagePositionDTO = getImagePositionDTO();

		when(imagePositionModelMapper.map(imagePositionDTO, ImagePosition.class)).thenReturn(imagePosition);
		when(imagePositionService.updateImagePositionById(imagePositionDTO.getId(), imagePosition))
				.thenReturn(imagePosition);

		when(imagePositionModelMapper.map(imagePosition, ImagePositionDTO.class)).thenReturn(imagePositionDTO);

		mockMvc.perform(
				put("/imagePositions/" + imagePositionDTO.getId()).requestAttr("imagePositionDTO", imagePositionDTO)
						.contentType(MediaType.APPLICATION_JSON).content(asJsonString(imagePositionDTO)))
				.andExpect(status().isOk());
		verify(imagePositionService, times(1)).updateImagePositionById(imagePosition.getId(), imagePosition);
	}

	/**
	 * Test case for getting all Positions
	 *
	 */
	@Test
	public void test_findAllImagePositions() throws Exception {
		List<ImagePositionDTO> imagePositionDTOs = getImagePositionDTOList();
		List<ImagePosition> imagePositions = getImagePositionList();

		when(imagePositionService.findAllImagePositions()).thenReturn(imagePositions);
		when(imagePositionModelMapper.mapToImagePositionDTOs(imagePositions)).thenReturn(imagePositionDTOs);

		mockMvc.perform(get("/imagePositions").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(4)));
		;

		verify(imagePositionService, times(1)).findAllImagePositions();
		verifyNoMoreInteractions(imagePositionService);

	}

}
