/**
 * 
 */
package com.sothebys.propertydb.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.ImageFigureDTO;
import com.sothebys.propertydb.mapper.ImageFigureModelMapper;
import com.sothebys.propertydb.model.ImageFigure;
import com.sothebys.propertydb.service.ImageFigureService;

/**
 * @author aneesha.l
 *
 */
public class ImageFigureControllerTest {

	@InjectMocks
	private ImageFigureController imageFigureController;

	@Mock
	private ImageFigureModelMapper imageFigureModelMapper;

	@Mock
	private ImageFigureService imageFigureService;

	@InjectMocks
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	private MockMvc mockMvc;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		mockMvc = MockMvcBuilders.standaloneSetup(imageFigureController)
				.setMessageConverters(new MappingJackson2HttpMessageConverter())
				.setCustomArgumentResolvers(pageableArgumentResolver).build();

	}

	/**
	 * @return ImageFigure
	 */
	public ImageFigure getImageFigure() {
		ImageFigure imageFigure = new ImageFigure();
		imageFigure.setId(new Long(1));
		imageFigure.setName("Figure one");

		return imageFigure;
	}

	/**
	 * @return ImageFigure list
	 */
	public List<ImageFigure> getImageFigureList() {
		List<ImageFigure> imageFigureList = new ArrayList<ImageFigure>();

		ImageFigure imageFigure = new ImageFigure();
		imageFigure.setId(new Long(1));
		imageFigure.setName("Figure one");
		imageFigureList.add(imageFigure);

		imageFigure = new ImageFigure();
		imageFigure.setId(new Long(2));
		imageFigure.setName("Figure two");
		imageFigureList.add(imageFigure);

		return imageFigureList;
	}

	/**
	 * @return ImageFigureDTO
	 */
	public ImageFigureDTO getImageFigureDTO() {
		ImageFigureDTO imageFigureDTO = new ImageFigureDTO();
		imageFigureDTO.setId(new Long(1));
		imageFigureDTO.setName("Figure one");

		return imageFigureDTO;
	}

	/**
	 * @return ImageFigureDTO list
	 */
	public List<ImageFigureDTO> getImageFigureDTOList() {
		List<ImageFigureDTO> imageFigureDTOList = new ArrayList<ImageFigureDTO>();

		ImageFigureDTO imageFigureDTO = new ImageFigureDTO();
		imageFigureDTO.setId(new Long(1));
		imageFigureDTO.setName("Figure one");
		imageFigureDTOList.add(imageFigureDTO);

		imageFigureDTO = new ImageFigureDTO();
		imageFigureDTO.setId(new Long(2));
		imageFigureDTO.setName("Figure two");
		imageFigureDTOList.add(imageFigureDTO);

		return imageFigureDTOList;
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Test case for getting all Figures
	 *
	 */
	@Test
	public void test_findAllImageFigures() throws Exception {
		List<ImageFigureDTO> imageFigureDTOs = getImageFigureDTOList();
		List<ImageFigure> imageFigures = getImageFigureList();

		when(imageFigureService.findAllImageFigures()).thenReturn(imageFigures);
		when(imageFigureModelMapper.mapToImageFigureDTOs(imageFigures)).thenReturn(imageFigureDTOs);

		mockMvc.perform(get("/imageFigures").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)));
		;

		verify(imageFigureService, times(1)).findAllImageFigures();
		verifyNoMoreInteractions(imageFigureService);

	}

}
