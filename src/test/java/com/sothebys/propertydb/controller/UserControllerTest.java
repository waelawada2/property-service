/**
 * 
 */
package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.isNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.UserDTO;
import com.sothebys.propertydb.mapper.UserModelMapper;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.search.PropertyOperation;
import com.sothebys.propertydb.search.SearchCriteria;
import com.sothebys.propertydb.search.UserSearchBuilder;
import com.sothebys.propertydb.search.UserSearchInputDTO;
import com.sothebys.propertydb.search.UserSpecification;
import com.sothebys.propertydb.search.UserSpecificationBuilder;
import com.sothebys.propertydb.search.UserUtil;
import com.sothebys.propertydb.service.UserService;

import net.sf.ehcache.search.expression.IsNull;

/**
 * @author Manju.Rana
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(UserUtil.class)
public class UserControllerTest {

  @InjectMocks
  private UserController userController;

  @Mock
  UserService userService;

  @Mock
  UserSpecificationBuilder userSpecificationBuilder;

  @Mock
  UserSpecification userSpecification;

  @Mock
  UserSearchBuilder userSearchBuilder;

  @Mock
  UserModelMapper userModelMapper;

  private MockMvc mockMvc;



  @InjectMocks
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;



  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders.standaloneSetup(userController)
        .setMessageConverters(new MappingJackson2HttpMessageConverter())
        .setCustomArgumentResolvers(pageableArgumentResolver).build();
  }

  private User getUser() {
    User user = new User();
    user.setUserName("Manju.Rana");
    user.setFirstName("Manju");
    user.setLastName("Rana");
    user.setEmail("Manju.Rana.Consultant@sothebys.com");
    return user;
  }

  private UserDTO getUserDTO() {
    UserDTO userDto = new UserDTO();
    userDto.setUserName("Manju.Rana");
    userDto.setFirstName("Manju");
    userDto.setLastName("Rana");
    userDto.setEmail("Manju.Rana.Consultant@sothebys.com");
    return userDto;
  }

  private UserSearchInputDTO getUserSearchInputDTO_NotFound() {
    UserSearchInputDTO userSearchInputDTO = new UserSearchInputDTO();
    userSearchInputDTO.setFirstName("abc");
    return userSearchInputDTO;
  }

  private UserSearchInputDTO getUserSearchInputDTO() {
    UserSearchInputDTO userSearchInputDTO = new UserSearchInputDTO();
    userSearchInputDTO.setFirstName("Man");
    return userSearchInputDTO;
  }

  @Test
  public void test_findAllUsers_success() throws Exception {
    List<User> usersList = Arrays.asList(new User[] {getUser(), getUser()});
    Page<User> users = new PageImpl<User>(usersList);

    List<UserDTO> usersDTOsList = Arrays.asList(new UserDTO[] {getUserDTO(), getUserDTO()});
    Page<UserDTO> userDTOs = new PageImpl<UserDTO>(usersDTOsList);

    PageRequest pageRequest = new PageRequest(0, 20);

    when(userService.findAllUsers(pageRequest)).thenReturn(users);
    when(userModelMapper.mapEntityPageIntoDTOPage(pageRequest, users)).thenReturn(userDTOs);;

    mockMvc
        .perform(get("/users").contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(getUserSearchInputDTO())))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));

    verify(userService, times(1)).findAllUsers(pageRequest);
    verifyNoMoreInteractions(userService);
  }

  @Test
  public void test_findUserBySearchTerm_success() throws Exception {
    List<User> usersList = Arrays.asList(new User[] {getUser(), getUser()});
    Page<User> users = new PageImpl<User>(usersList);

    List<UserDTO> usersDTOsList = Arrays.asList(new UserDTO[] {getUserDTO(), getUserDTO()});
    Page<UserDTO> userDTOs = new PageImpl<UserDTO>(usersDTOsList);
    PageRequest pageRequest = new PageRequest(0, 20);

    PowerMockito.mockStatic(UserUtil.class);

    PowerMockito.when(UserUtil.validateUserSearchInputDTO(getUserSearchInputDTO()))
        .thenReturn(true);

    SearchCriteria criteria = new SearchCriteria("firstName", PropertyOperation.STARTS_WITH, "Man");
    Specification<User> spec = new UserSpecification(criteria);

    when(userSearchBuilder.createSpecificationBuilder(getUserSearchInputDTO()))
        .thenReturn(userSpecificationBuilder);
    when(userSpecificationBuilder.build()).thenReturn(spec);
    when(userService.findBySearchTerm(spec, pageRequest)).thenReturn(users);
    when(userModelMapper.mapEntityPageIntoDTOPage(pageRequest, users)).thenReturn(userDTOs);

    mockMvc
        .perform(get("/users?firstName=Man").contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(getUserSearchInputDTO())))
        .andExpect(status().isOk()).andDo(print())
        .andExpect(jsonPath("$.content.[0].firstName", is("Manju")));
    verify(userService, times(1)).findBySearchTerm(spec, pageRequest);
    verifyNoMoreInteractions(userService);

  }

  @Test
  public void test_findUserBySearchTerm_notFound() throws Exception {
    List<User> usersList = Arrays.asList(new User[] {getUser(), getUser()});
    Page<User> users = new PageImpl<User>(usersList);

    List<UserDTO> usersDTOsList = Collections.emptyList();
    Page<UserDTO> userDTOs = new PageImpl<UserDTO>(usersDTOsList);
    PageRequest pageRequest = new PageRequest(0, 20);

    PowerMockito.mockStatic(UserUtil.class);

    PowerMockito.when(UserUtil.validateUserSearchInputDTO(getUserSearchInputDTO_NotFound()))
        .thenReturn(true);

    SearchCriteria criteria = new SearchCriteria("firstName", PropertyOperation.STARTS_WITH, "abc");
    Specification<User> spec = new UserSpecification(criteria);

    when(userSearchBuilder.createSpecificationBuilder(getUserSearchInputDTO_NotFound()))
        .thenReturn(userSpecificationBuilder);
    when(userSpecificationBuilder.build()).thenReturn(spec);
    when(userService.findBySearchTerm(spec, pageRequest)).thenReturn(users);
    when(userModelMapper.mapEntityPageIntoDTOPage(pageRequest, users)).thenReturn(userDTOs);

    mockMvc
        .perform(get("/users?firstName=abc").contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(getUserSearchInputDTO_NotFound())))
        .andExpect(status().isOk()).andDo(print()).andExpect(jsonPath("$.totalElements", is(0)));

    verify(userService, times(1)).findBySearchTerm(spec, pageRequest);
    verifyNoMoreInteractions(userService);

  }


  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

}
