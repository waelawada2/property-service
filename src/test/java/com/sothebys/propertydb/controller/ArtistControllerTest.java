package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.ArtistCatalogueRaisoneeDTO;
import com.sothebys.propertydb.dto.ArtistDTO;
import com.sothebys.propertydb.dto.CategoryDTO;
import com.sothebys.propertydb.dto.CountryDTO;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.mapper.ArtistModelMapper;
import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.ArtistCatalogueRaisonee;
import com.sothebys.propertydb.model.Category;
import com.sothebys.propertydb.model.Country;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.repository.CountryRepository;
import com.sothebys.propertydb.search.ArtistSearchBuilder;
import com.sothebys.propertydb.search.ArtistSearchDTO;
import com.sothebys.propertydb.search.ArtistSearchInputDTO;
import com.sothebys.propertydb.search.ArtistSpecification;
import com.sothebys.propertydb.search.ArtistSpecificationBuilder;
import com.sothebys.propertydb.search.PollStatus;
import com.sothebys.propertydb.search.PropertyOperation;
import com.sothebys.propertydb.search.SearchCriteria;
import com.sothebys.propertydb.service.ArtistCatalogueRaisoneeService;
import com.sothebys.propertydb.service.ArtistService;
import com.sothebys.propertydb.service.CategoryService;
import com.sothebys.propertydb.service.ExportService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

// @EnableSpringDataWebSupport
public class ArtistControllerTest {

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @InjectMocks
  private ArtistController artistController;

  @Mock
  private ArtistModelMapper artistModelMapper;

  @Mock
  private ArtistSearchBuilder artistSearchBuilder;

  @Mock
  private ArtistService artistService;

  @Mock
  private ArtistSpecificationBuilder artistSpecificationBuilder;

  @Mock
  private CategoryService categoryService;

  private MockMvc mockMvc;

  @Mock
  private CountryRepository countryRepository;

  @Mock
  private ArtistCatalogueRaisoneeService artistCatalogueRaisoneeService;

  @InjectMocks
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;
  
  @Mock
  ExportService exportService;

  /**
   * @return Artist
   */
  public Artist getArtist() {
    Artist artist = new Artist();
    artist.setApproved(true);
    artist.setBirthYear(1984);
    artist.setDeathYear(2001);
    artist.setDisplay("sothebys");
    artist.setExternalId("00000002");
    artist.setFirstName("fname");
    artist.setGender("M");
    artist.setId(1L);
    artist.setLastName("lname");

    List<Country> countriesList = new ArrayList<Country>();
    Country country = new Country();
    country.setId(1L);
    country.setCountryName("American");
    countriesList.add(country);
    artist.setCountries(countriesList);

    List<ArtistCatalogueRaisonee> artistCatalogueRaisoneeList =
        new ArrayList<ArtistCatalogueRaisonee>();

    ArtistCatalogueRaisonee artistCatalogueRaisonee = new ArtistCatalogueRaisonee();
    artistCatalogueRaisonee.setAuthor("Prince");
    artistCatalogueRaisonee.setId(1L);
    artistCatalogueRaisonee.setType("novel");
    artistCatalogueRaisonee.setVolumes("20");
    artistCatalogueRaisonee.setYear(2017);
    artistCatalogueRaisoneeList.add(artistCatalogueRaisonee);
    artist.setArtistCatalogueRaisonees(artistCatalogueRaisoneeList);

    List<Category> categoryList = new ArrayList<>();
    Category category = new Category();
    category.setId(1L);
    category.setName("test category");
    category.setDescription("A category for testing");
    categoryList.add(category);

    artist.setCategories(categoryList);

    return artist;
  }

  /**
   * @return ArtistDTO
   */
  public ArtistDTO getArtistDto() {
    ArtistDTO artistDto = new ArtistDTO();
    artistDto.setApproved(true);
    artistDto.setBirthYear(1984);
    artistDto.setDeathYear(2001);
    artistDto.setDisplay("sothebys");
    artistDto.setId("0322");
    artistDto.setFirstName("fname");
    artistDto.setGender("M");
    artistDto.setLastName("lname");

    CountryDTO countrydto = new CountryDTO();
    countrydto.setId(1L);
    countrydto.setCountryName("American");

    List<ArtistCatalogueRaisoneeDTO> artistCatalogueRaisoneedtolist =
        new ArrayList<ArtistCatalogueRaisoneeDTO>();

    ArtistCatalogueRaisoneeDTO artistCatalogueRaisoneedto = new ArtistCatalogueRaisoneeDTO();
    artistCatalogueRaisoneedto.setAuthor("Prince");
    artistCatalogueRaisoneedto.setId(1L);
    artistCatalogueRaisoneedto.setType("novel");
    artistCatalogueRaisoneedto.setVolumes("20");
    artistCatalogueRaisoneedto.setYear(2017);
    artistCatalogueRaisoneedtolist.add(artistCatalogueRaisoneedto);
    artistDto.setArtistCatalogueRaisonees(artistCatalogueRaisoneedtolist);

    List<CategoryDTO> categoryDTOList = new ArrayList<>();
    CategoryDTO categoryDTO = new CategoryDTO();
    categoryDTO.setName("test category");
    categoryDTOList.add(categoryDTO);

    artistDto.setCategories(categoryDTOList);

    return artistDto;
  }

  public User getUser() {
    User user = new User();
    user.setKeycloakUserId("123456-7890123-456789");
    user.setFirstName("Test");
    user.setLastName("User");
    user.setUserName("test.user");
    user.setEmail("test.user@sothebys.com");
    user.setId(1L);
    return user;
  }

  /**
   * @return ArtistDTO
   */
  public List<ArtistSearchDTO> getArtistSearchDto() {
    List<ArtistSearchDTO> artistsearchDtos = new ArrayList<ArtistSearchDTO>();
    ArtistSearchDTO artistsearchdto = new ArtistSearchDTO();
    artistsearchdto.setId("1");
    artistsearchDtos.add(artistsearchdto);
    ArtistSearchDTO artistsearchdto1 = new ArtistSearchDTO();
    artistsearchdto1.setId("2");
    artistsearchDtos.add(artistsearchdto1);

    return artistsearchDtos;
  }

  /**
   * @return ArtistDTO
   */
  public ArtistSearchInputDTO getArtistSearchInputDto() {
    ArtistSearchInputDTO artistDto = new ArtistSearchInputDTO();
    artistDto.setApproved(true);
    artistDto.setBirthYear("1984");
    artistDto.setDeathYear("2001");
    artistDto.setDisplay("sothebys");
    artistDto.setExternalId("0322");
    artistDto.setFirstName("fname");
    artistDto.setGender("M");
    artistDto.setId(1L);
    artistDto.setLastName("lname");
    return artistDto;
  }

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders.standaloneSetup(artistController)
        .setMessageConverters(new MappingJackson2HttpMessageConverter())
        .setCustomArgumentResolvers(pageableArgumentResolver).build();
  }

  /**
   * Test case for createArtist
   *
   * @throws Exception
   */
  @Test
  public void test_createArtist_success() throws Exception {
    final int firstObject = 0;

    Artist artist = getArtist();
    ArtistDTO artistDto = getArtistDto();

    when(artistModelMapper.mapToEntity(artistDto)).thenReturn(artist);
    when(artistService.createArtist(artist, getUser(), false)).thenReturn(artist);
    when(artistModelMapper.map(artist, ArtistDTO.class)).thenReturn(artistDto);
    when(categoryService.findByCategoryNameAndParentCategoryName(
        artist.getCategories().get(firstObject).getName(), null))
            .thenReturn(artist.getCategories().get(firstObject));
    when(countryRepository
        .findByCountryName(artist.getCountries().get(firstObject).getCountryName()))
            .thenReturn(artist.getCountries().get(firstObject));

    mockMvc
        .perform(post("/artists").requestAttr("requestUser", getUser())
            .contentType(MediaType.APPLICATION_JSON).content(asJsonString(artistDto)))
        .andExpect(status().isCreated())
        .andExpect(header().string("Location", containsString("/artists/")));
    verify(artistService, times(1)).createArtist(artist, getUser(), false);
    verifyNoMoreInteractions(artistService);
  }

  /**
   * Test case for createCatalogueRaisoneesForArtist notFound scenario
   *
   * @throws Exception
   */
  // TO Do -fix
  @Ignore
  @Test(expected = NotFoundException.class)
  public void test_createCatalogueRaisoneesForArtist_NotFound() throws Exception {
    Artist artist = getArtist();
    when(artistController.getArtistByExternalId(artist.getExternalId()))
        .thenThrow(new NotFoundException());
    when(artistService.findArtistByExternalId(artist.getExternalId())).thenReturn(null);
    mockMvc.perform(get("/artist/{externalId}/catalogue-raisonee", artist.getExternalId()))
        .andDo(print()).andExpect(status().isNotFound())
        .andExpect(jsonPath("$.error").value("No artist found with ID=1"));
  }

  /**
   * Test case for Catalogue raisonees for artist
   *
   * @throws Exception
   */
  @Test
  public void test_createCatalogueRaisoneesForArtist_success() throws Exception {
    final int firstObject = 0;

    Artist artist = getArtist();
    ArtistDTO artistDto = getArtistDto();
    List<ArtistCatalogueRaisonee> artistCatalogueRaisoneeObj = artist.getArtistCatalogueRaisonees();
    List<ArtistCatalogueRaisoneeDTO> artistCatalogueRaisoneeDTOObj =
        artistDto.getArtistCatalogueRaisonees();

    when(artistModelMapper.mapToEntity(artistCatalogueRaisoneeDTOObj))
        .thenReturn(artistCatalogueRaisoneeObj);
    when(artistCatalogueRaisoneeService.saveArtistCataloguesRaisoneesForArtistByExternalId(
        artist.getExternalId(), artistCatalogueRaisoneeObj, getUser()))
            .thenReturn(artistCatalogueRaisoneeObj);
    when(artistModelMapper.mapToDTO(artistCatalogueRaisoneeObj))
        .thenReturn(artistCatalogueRaisoneeDTOObj);
    when(categoryService.findByCategoryNameAndParentCategoryName(
        artist.getCategories().get(firstObject).getName(), null))
            .thenReturn(artist.getCategories().get(firstObject));
    when(countryRepository
        .findByCountryName(artist.getCountries().get(firstObject).getCountryName()))
            .thenReturn(artist.getCountries().get(firstObject));

    mockMvc
        .perform(post("/artists/{externalId}/catalogues-raisonees", artist.getExternalId())
            .requestAttr("requestUser", getUser()).contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(artistCatalogueRaisoneeDTOObj)))
        .andExpect(status().isCreated());
    verify(artistCatalogueRaisoneeService, times(1))
        .saveArtistCataloguesRaisoneesForArtistByExternalId(artist.getExternalId(),
            artistCatalogueRaisoneeObj, getUser());
    verifyNoMoreInteractions(artistCatalogueRaisoneeService);
  }

  /**
   * Test case for deleteArtistById
   *
   * @throws Exception
   */
  @Test
  public void test_deleteArtistById_success() throws Exception {
    Artist artist = getArtist();
    when(artistService.findArtistByExternalId(artist.getExternalId())).thenReturn(artist);
    doNothing().when(artistService).deleteArtist(artist.getExternalId());
    mockMvc.perform(delete("/artists/{externalId}", artist.getExternalId()))
        .andExpect(status().isNoContent());
    verify(artistService, times(1)).deleteArtist(artist.getExternalId());
  }

  /**
   * To delete catalogueRaisonee from artist based on given catalogueRaisoneeIdId-Failure
   *
   * @throws Exception
   */
  // TO Do -fix
  @Ignore
  @Test(expected = NotFoundException.class)
  public void test_deleteCatalogueRaisoneesForArtistById_failure() throws Exception {
    Artist artist = getArtist();
    when(artistController.getArtistByExternalId(artist.getExternalId()))
        .thenThrow(new NotFoundException());
    when(artistService.findArtistByExternalId(artist.getExternalId())).thenReturn(null);
    mockMvc
        .perform(get("/artist/{externalId}/catalogue-raisonee/{catalogueRaisoneeId}",
            artist.getExternalId()))
        .andDo(print()).andExpect(status().isInternalServerError())
        .andExpect(jsonPath("$.error").value("Artist id not found..."));
  }

  /**
   * To delete catalogueRaisonee from artist based on given catalogueRaisoneeIdId-Not found scenario
   *
   * @throws Exception
   */
  // TO Do -fix
  @Ignore
  @Test(expected = NotFoundException.class)
  public void test_deleteCatalogueRaisoneesForArtistById_NotFound() throws Exception {
    Artist artist = getArtist();
    when(artistService.findArtistById(artist.getId())).thenReturn(artist);
    when(artistController.deleteCatalogueRaisoneesForArtistById(artist.getExternalId(), 0L,
        getUser())).thenThrow(NotFoundException.class);
    mockMvc.perform(delete("/artists/{externalId}/catalogues-raisonees/{catalogueRaisoneeId}",
        artist.getExternalId(), 0)).andExpect(status().isNoContent());
    verify(artistService, times(1)).findArtistById(artist.getId());
    verifyNoMoreInteractions(artistService);
  }

  /**
   * To delete catalogueRaisonee from artist based on given catalogueRaisoneeIdId
   *
   * @throws Exception
   */
  @Test
  public void test_deleteCatalogueRaisoneesForArtistById_success() throws Exception {
    Artist artist = getArtist();
    ArtistCatalogueRaisonee artistCatalogueRaisonee = artist.getArtistCatalogueRaisonees().get(0);
    doNothing().when(artistCatalogueRaisoneeService).deleteCatalogueRaisoneeForArtistByExternalId(
        artist.getExternalId(), artistCatalogueRaisonee.getId(), getUser());
    mockMvc.perform(delete("/artists/{externalId}/catalogues-raisonees/{catalogueRaisoneeId}",
        artist.getExternalId(), artistCatalogueRaisonee.getId()).requestAttr("requestUser",
            getUser()))
        .andExpect(status().isNoContent());
    verify(artistCatalogueRaisoneeService, times(1)).deleteCatalogueRaisoneeForArtistByExternalId(
        artist.getExternalId(), artistCatalogueRaisonee.getId(), getUser());
    verifyNoMoreInteractions(artistCatalogueRaisoneeService);
  }

  /**
   * Test case for getAllArtists failure scenario
   *
   * @throws Exception
   */
  @Test
  @Ignore
  public void test_getAllArtists_NotFound() throws Exception {
    List<Artist> artistList = new ArrayList<>(0);
    List<ArtistDTO> artistDtoList = new ArrayList<>(0);

    when(artistService.findAllArtists()).thenReturn(artistList);
    when(artistModelMapper.mapToArtistDTOs(artistList)).thenReturn(artistDtoList);

    mockMvc.perform(get("/artists")).andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(0)));
  }

  /**
   * Test case for getAllArtists
   *
   * @throws Exception
   */
  @Test
  @Ignore
  public void test_getAllArtists_success() throws Exception {
    List<Artist> artistsList = new ArrayList<Artist>();
    Artist artist1 = getArtist();
    Artist artist2 = getArtist();
    artistsList.add(artist1);
    artistsList.add(artist2);
    List<ArtistDTO> artistDtoList = new ArrayList<ArtistDTO>();
    ArtistDTO artistDto1 = getArtistDto();
    ArtistDTO artistDto2 = getArtistDto();
    artistDtoList.add(artistDto1);
    artistDtoList.add(artistDto2);

    when(artistService.findAllArtists()).thenReturn(artistsList);
    when(artistModelMapper.mapToArtistDTOs(artistsList)).thenReturn(artistDtoList);

    mockMvc.perform(get("/artists")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].firstName", is("fname"))).andExpect(jsonPath("$[1].id", is(1)))
        .andExpect(jsonPath("$[1].firstName", is("fname")));

    verify(artistService, times(1)).findAllArtists();
    verifyNoMoreInteractions(artistService);
  }

  /**
   * Test case for findArtistById notFound scenario
   *
   * @throws Exception
   */
  // TO Do -fix
  @Ignore
  @Test(expected = NotFoundException.class)
  public void test_getArtistById_NotFound() throws Exception {
    Artist artist = getArtist();
    when(artistController.getArtistByExternalId(artist.getExternalId()))
        .thenThrow(new NotFoundException());
    when(artistService.findArtistByExternalId(artist.getExternalId())).thenReturn(null);
    mockMvc.perform(get("/artist/{externalId}", artist.getExternalId())).andDo(print())
        .andExpect(status().isInternalServerError())
        .andExpect(jsonPath("$.error").value("Artist id not found..."));
  }

  /**
   * Test case for findArtistById
   *
   * @throws Exception
   */
  @Test
  public void test_getArtistById_success() throws Exception {
    Artist artist = getArtist();
    ArtistDTO artistDto = getArtistDto();

    when(artistService.findArtistByExternalId(artist.getExternalId())).thenReturn(artist);
    when(artistModelMapper.map(artist, ArtistDTO.class)).thenReturn(artistDto);
    mockMvc.perform(get("/artists/{externalId}", "00000002")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.firstName", is(artist.getFirstName()))).andDo(print());
    verify(artistService, times(1)).findArtistByExternalId(artist.getExternalId());
    verifyNoMoreInteractions(artistService);

  }

  /**
   * Test case for createArtist
   *
   * @throws Exception
   */

  @Test
  @Ignore
  public void test_searchArtist_success() throws Exception {
    Artist artist = getArtist();
    List<Artist> artistlist = new ArrayList<Artist>();
    artistlist.add(artist);
    Page<Artist> artists = new PageImpl<Artist>(artistlist);

    ArtistSearchInputDTO artistDto = getArtistSearchInputDto();
    Pageable pageRequestI = new PageRequest(0, 50);
    PageRequest pageRequest = new PageRequest(0, 50);

    when(artistService.findAllArtists(pageRequest)).thenReturn(artists);

    SearchCriteria criteria =
        new SearchCriteria("id", PropertyOperation.getSimpleOperation(':'), "1");
    Page<ArtistSearchDTO> artistdtos =
        new PageImpl<ArtistSearchDTO>(getArtistSearchDto(), pageRequest, 0L);
    Specification<Artist> spec = new ArtistSpecification(criteria);

    when(artistSearchBuilder.createSpecificationBuilder(artistDto))
        .thenReturn(artistSpecificationBuilder);
    when(artistService.findBySearchTerm(spec, pageRequestI)).thenReturn(artists);
    when(artistModelMapper.mapEntityPageIntoDTOPageWithIdsOnly(pageRequestI, artists))
        .thenReturn(artistdtos);

    mockMvc.perform(get("/artists", artistDto, pageRequest).contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(artistDto))).andExpect(status().isOk());

    verify(artistService, times(1)).findAllArtists(pageRequest);
    verify(artistService, times(1)).findBySearchTerm(spec, pageRequest);
    verifyNoMoreInteractions(artistService);
  }

  /**
   * Test case for updateArtist
   *
   * @throws Exception
   */
  @Test
  public void test_updateArtist_success() throws Exception {
    Artist artist = getArtist();
    ArtistDTO artistDto = getArtistDto();

    when(artistModelMapper.map(artistDto, Artist.class)).thenReturn(artist);
    when(artistService.updateArtist(artist.getId(), artist, getUser())).thenReturn(artist);
    when(artistService.findArtistByExternalId(artist.getExternalId())).thenReturn(artist);

    when(artistModelMapper.map(artist, ArtistDTO.class)).thenReturn(artistDto);

    mockMvc.perform(
        put("/artists/{externalId}", artist.getExternalId()).requestAttr("requestUser", getUser())
            .contentType(MediaType.APPLICATION_JSON).content(asJsonString(artistDto)))
        .andExpect(status().isOk());
    verify(artistService, times(1)).updateArtistByExternalId(artist.getExternalId(), artist,
        getUser());
  }

  /**
   * Test case for update Catalogue raisonees for artist
   */
  @Test
  public void test_updateCatalogueRaisoneesForArtist_success() throws Exception {
    Artist artist = getArtist();
    ArtistDTO artistDto = getArtistDto();
    ArtistCatalogueRaisonee artistCatalogueRaisoneeList =
        artist.getArtistCatalogueRaisonees().get(0);
    ArtistCatalogueRaisoneeDTO artistCatalogueRaisoneeDTOList =
        artistDto.getArtistCatalogueRaisonees().get(0);
    ArtistCatalogueRaisoneeDTO artistCatalogueRaisoneeDTOObj =
        artistDto.getArtistCatalogueRaisonees().get(0);
    ArtistCatalogueRaisonee artistCatalogueRaisoneeObj =
        artist.getArtistCatalogueRaisonees().get(0);

    when(artistModelMapper.mapToEntity(artistCatalogueRaisoneeDTOObj))
        .thenReturn(artistCatalogueRaisoneeObj);
    when(artistCatalogueRaisoneeService.updateCatalogueRaisoneeForArtistByExternalId(
        artist.getExternalId(), artistCatalogueRaisoneeObj, artistCatalogueRaisoneeObj.getId(),
        getUser())).thenReturn(artistCatalogueRaisoneeList);
    when(artistModelMapper.mapToDTO(artistCatalogueRaisoneeList))
        .thenReturn(artistCatalogueRaisoneeDTOList);

    mockMvc
        .perform(put("/artists/{externalId}/catalogues-raisonees/{catalogueRaisoneeId}",
            artist.getExternalId(), artistCatalogueRaisoneeObj.getId())
                .requestAttr("requestUser", getUser()).contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(artistCatalogueRaisoneeDTOObj)))
        .andExpect(status().isOk());
    verify(artistCatalogueRaisoneeService, times(1)).updateCatalogueRaisoneeForArtistByExternalId(
        artist.getExternalId(), artistCatalogueRaisoneeObj, artistCatalogueRaisoneeObj.getId(),
        getUser());
    verifyNoMoreInteractions(artistCatalogueRaisoneeService);
  }
  
 

  /**
   * Test case for exportArtists
   *
   * @throws Exception
   */
  @Test
  public void test_exportArtists_success() throws Exception {
    String exportId ="64300571-0e04-4785-8afc-8da2b508a358";
    List<Artist> artistsList = new ArrayList<Artist>();
    Artist artist1 = getArtist();
    Artist artist2 = getArtist();
    artistsList.add(artist1);
    artistsList.add(artist2);
    List<ArtistDTO> artistDtoList = new ArrayList<ArtistDTO>();
    ArtistDTO artistDto1 = getArtistDto();
    ArtistDTO artistDto2 = getArtistDto();
    artistDtoList.add(artistDto1);
    artistDtoList.add(artistDto2);
    when(artistService.exportArtistData()).thenReturn(exportId);
    when(artistModelMapper.mapToArtistDTOs(artistsList)).thenReturn(artistDtoList);
    mockMvc
    .perform(post("/artists/export").requestAttr("requestUser", getUser())
        .contentType(MediaType.APPLICATION_JSON))
    .andExpect(status().isAccepted());
    verify(artistService, times(1)).exportArtistData();
    verifyNoMoreInteractions(artistService);
  }

  /**
   * Test case for exportIdStatus 
   *
   * @throws Exception
   */
  @Test
  public void test_exportIdStatus_success() throws Exception {
    String exportId ="64300571-0e04-4785-8afc-8da2b508a358";
    when(exportService.findExportStatus(exportId)).thenReturn(PollStatus.Completed);
    mockMvc.perform(get("/artists/export/{id}/status",exportId)).andExpect(status().isOk())
    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    verify(exportService, times(1)).findExportStatus(exportId);
    verifyNoMoreInteractions(exportService);
  }
  
  /**
   * Test case for exportIdStatus failure scenario
   *
   * @throws Exception
   */
  @Test(expected = NotFoundException.class)
  public void test_exportIdStatus_NotFound() throws Exception {
    String exportId ="64300571-0e04-4785-8afc-8da2b508a358";
    when(artistController.exportIdStatus(exportId))
    .thenThrow(new NotFoundException());
    when(exportService.findExportStatus(exportId)).thenReturn(PollStatus.Error);
    mockMvc.perform(get("/artists/export/{id}/status",exportId))
    .andDo(print()).andExpect(status().isNotFound())
    .andExpect(jsonPath("$.error").value("No export id found with ID="+exportId));
  }
  
}
