/**
 * 
 */
package com.sothebys.propertydb.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.ImageGenderDTO;
import com.sothebys.propertydb.mapper.ImageGenderModelMapper;
import com.sothebys.propertydb.model.ImageGender;
import com.sothebys.propertydb.service.ImageGenderService;

/**
 * @author aneesha.l
 *
 */
public class ImageGenderControllerTest {

	@InjectMocks
	private ImageGenderController imageGenderController;

	@Mock
	private ImageGenderModelMapper imageGenderModelMapper;

	@Mock
	private ImageGenderService imageGenderService;

	@InjectMocks
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	private MockMvc mockMvc;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		mockMvc = MockMvcBuilders.standaloneSetup(imageGenderController)
				.setMessageConverters(new MappingJackson2HttpMessageConverter())
				.setCustomArgumentResolvers(pageableArgumentResolver).build();

	}

	/**
	 * @return ImageGender
	 */
	public ImageGender getImageGender() {
		ImageGender imageGender = new ImageGender();
		imageGender.setId(new Long(1));
		imageGender.setName("Female");

		return imageGender;
	}

	/**
	 * @return ImageGender list
	 */
	public List<ImageGender> getImageGenderList() {
		List<ImageGender> imageGenderList = new ArrayList<ImageGender>();

		ImageGender imageGender = new ImageGender();
		imageGender.setId(new Long(1));
		imageGender.setName("Female");
		imageGenderList.add(imageGender);

		imageGender = new ImageGender();
		imageGender.setId(new Long(2));
		imageGender.setName("Male");
		imageGenderList.add(imageGender);

		return imageGenderList;
	}

	/**
	 * @return ImageGenderDTO
	 */
	public ImageGenderDTO getImageGenderDTO() {
		ImageGenderDTO imageGenderDTO = new ImageGenderDTO();
		imageGenderDTO.setId(new Long(1));
		imageGenderDTO.setName("Female");

		return imageGenderDTO;
	}

	/**
	 * @return ImageGenderDTO list
	 */
	public List<ImageGenderDTO> getImageGenderDTOList() {
		List<ImageGenderDTO> imageGenderDTOList = new ArrayList<ImageGenderDTO>();

		ImageGenderDTO imageGenderDTO = new ImageGenderDTO();
		imageGenderDTO.setId(new Long(1));
		imageGenderDTO.setName("Female");
		imageGenderDTOList.add(imageGenderDTO);

		imageGenderDTO = new ImageGenderDTO();
		imageGenderDTO.setId(new Long(2));
		imageGenderDTO.setName("Male");
		imageGenderDTOList.add(imageGenderDTO);

		return imageGenderDTOList;
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Test case for getting all genders
	 *
	 */
	@Test
	public void test_findAllImageGenders() throws Exception {
		List<ImageGenderDTO> imageGenderDTOs = getImageGenderDTOList();
		List<ImageGender> imageGenders = getImageGenderList();

		when(imageGenderService.findAllImageGenders()).thenReturn(imageGenders);
		when(imageGenderModelMapper.mapToImageGenderDTOs(imageGenders)).thenReturn(imageGenderDTOs);

		mockMvc.perform(get("/imageGenders").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)));
		;

		verify(imageGenderService, times(1)).findAllImageGenders();
		verifyNoMoreInteractions(imageGenderService);

	}

}
