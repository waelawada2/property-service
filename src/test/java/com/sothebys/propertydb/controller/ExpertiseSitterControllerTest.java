/**
 * 
 */
package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.ExpertiseSitterDTO;
import com.sothebys.propertydb.mapper.ExpertiseSitterModelMapper;
import com.sothebys.propertydb.model.ExpertiseSitter;
import com.sothebys.propertydb.search.ExpertiseSitterSearchBuilder;
import com.sothebys.propertydb.search.ExpertiseSitterSearchInputDTO;
import com.sothebys.propertydb.search.ExpertiseSitterSpecificationBuilder;
import com.sothebys.propertydb.search.PropertyOperation;
import com.sothebys.propertydb.service.ExpertiseSitterService;

/**
 * @author aneesha.l
 *
 */
public class ExpertiseSitterControllerTest {

	@InjectMocks
	private ExpertiseSitterController expertiseSitterController;

	@Mock
	private ExpertiseSitterService expertiseSitterService;

	@Mock
	private ExpertiseSitterModelMapper expertiseSitterModelMapper;

	@InjectMocks
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Mock
	private ExpertiseSitterSearchBuilder expertiseSitterSearchBuilder;

	@InjectMocks
	private ExpertiseSitterSpecificationBuilder expertiseSitterSpecificationBuilder;

	private MockMvc mockMvc;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		mockMvc = MockMvcBuilders.standaloneSetup(expertiseSitterController)
				.setMessageConverters(new MappingJackson2HttpMessageConverter())
				.setCustomArgumentResolvers(pageableArgumentResolver).build();

	}

	/**
	 * Test case for create expertise sitter
	 *
	 */
	@Test
	public void test_createExpertiseSitter() throws Exception {
		ExpertiseSitterDTO expertiseSitterDTO = getExpertiseSitterDTO();
		ExpertiseSitter expertiseSitter = getExpertiseSitter();
		when(expertiseSitterModelMapper.map(expertiseSitterDTO, ExpertiseSitter.class)).thenReturn(expertiseSitter);
		when(expertiseSitterService.createExpertiseSitter(expertiseSitter)).thenReturn(expertiseSitter);
		when(expertiseSitterModelMapper.map(expertiseSitter, ExpertiseSitterDTO.class)).thenReturn(expertiseSitterDTO);

		mockMvc.perform(
				post("/sitters").contentType(MediaType.APPLICATION_JSON).content(asJsonString(expertiseSitterDTO)))
				.andExpect(status().isCreated()).andExpect(header().string("Location", containsString("/sitters/")));
		verify(expertiseSitterService, times(1)).createExpertiseSitter(expertiseSitter);
		verifyNoMoreInteractions(expertiseSitterService);

	}

	/**
	 * Test case for searching the expertise sitter with search criteria
	 *
	 */
	@Test
	public void test_findAllExpertiseSitters_with_search_inputs() throws Exception {
		ExpertiseSitterSearchInputDTO expertiseSitterSearchInputDTO = getExpertiseSitterSearchInputDTO();
		List<ExpertiseSitterDTO> expertiseSitterDTOs = getExpertiseSitterDTOs();
		List<ExpertiseSitter> expertiseSitters = getExpertiseSitters();

		expertiseSitterSpecificationBuilder.with("firstName", PropertyOperation.Operator.LIKEJ.getOp(), "Rob");

		when(expertiseSitterSearchBuilder.createSpecificationBuilder(Mockito.any()))
				.thenReturn(expertiseSitterSpecificationBuilder);
		when(expertiseSitterService.findBySearchTerm(Mockito.any())).thenReturn(expertiseSitters);
		when(expertiseSitterModelMapper.mapToExpertiseSitterDTOs(expertiseSitters)).thenReturn(expertiseSitterDTOs);

		mockMvc.perform(get("/sitters?firstName=Rob").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(expertiseSitterSearchInputDTO))).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)));

		verify(expertiseSitterService, times(1)).findBySearchTerm(Mockito.any());
		verifyNoMoreInteractions(expertiseSitterService);

	}

	/**
	 * Test case for searching the expertise sitter
	 *
	 */
	@Test
	public void test_findAllExpertiseSitters() throws Exception {
		List<ExpertiseSitterDTO> expertiseSitterDTOs = getExpertiseSitterDTOs();
		List<ExpertiseSitter> expertiseSitters = getExpertiseSitters();

		when(expertiseSitterService.findAllExpertiseSitters()).thenReturn(expertiseSitters);
		when(expertiseSitterModelMapper.mapToExpertiseSitterDTOs(expertiseSitters)).thenReturn(expertiseSitterDTOs);

		mockMvc.perform(get("/sitters").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)));
		;

		verify(expertiseSitterService, times(1)).findAllExpertiseSitters();
		verifyNoMoreInteractions(expertiseSitterService);

	}

	/**
	 * Test case for deleting the expertise sitter by id
	 *
	 */
	@Test
	public void test_deleteExpertiseSitterById() throws Exception {
		ExpertiseSitter expertiseSitter = getExpertiseSitter();
		doNothing().when(expertiseSitterService).deleteExpertiseSitter(expertiseSitter.getId());
		mockMvc.perform(delete("/sitters/{id}", expertiseSitter.getId())).andExpect(status().isNoContent());
		verify(expertiseSitterService, times(1)).deleteExpertiseSitter(expertiseSitter.getId());

	}

	/**
	 * Test case for updateExpertiseSitter
	 *
	 * @throws Exception
	 */
	@Test
	public void test_updateExpertiseSitter() throws Exception {
		ExpertiseSitter expertiseSitter = getExpertiseSitter();
		ExpertiseSitterDTO expertiseSitterDTO = getExpertiseSitterDTO();

		when(expertiseSitterModelMapper.map(expertiseSitterDTO, ExpertiseSitter.class)).thenReturn(expertiseSitter);
		when(expertiseSitterService.updateExpertiseSitter(expertiseSitter)).thenReturn(expertiseSitter);

		when(expertiseSitterModelMapper.map(expertiseSitter, ExpertiseSitterDTO.class)).thenReturn(expertiseSitterDTO);

		mockMvc.perform(put("/sitters").requestAttr("expertiseSitterDTO", expertiseSitterDTO)
				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(expertiseSitterDTO)))
				.andExpect(status().isOk());
		verify(expertiseSitterService, times(1)).updateExpertiseSitter(expertiseSitter);
	}

	private ExpertiseSitter getExpertiseSitter() {
		ExpertiseSitter expertiseSitter = new ExpertiseSitter();
		expertiseSitter.setId(new Long(1));
		expertiseSitter.setFirstName("Bob");
		expertiseSitter.setLastName("Vein");
		return expertiseSitter;
	}

	private ExpertiseSitterDTO getExpertiseSitterDTO() {
		ExpertiseSitterDTO expertiseSitterDTO = new ExpertiseSitterDTO();
		expertiseSitterDTO.setId(new Long(1));
		expertiseSitterDTO.setFirstName("Bob");
		expertiseSitterDTO.setLastName("Vein");
		return expertiseSitterDTO;
	}

	private ExpertiseSitterSearchInputDTO getExpertiseSitterSearchInputDTO() {
		ExpertiseSitterSearchInputDTO expertiseSitterSearchInputDTO = new ExpertiseSitterSearchInputDTO();
		expertiseSitterSearchInputDTO.setFirstName("Bob");
		expertiseSitterSearchInputDTO.setLastName("Vein");
		expertiseSitterSearchInputDTO.setName("Bob Vein");
		return expertiseSitterSearchInputDTO;
	}

	private List<ExpertiseSitterDTO> getExpertiseSitterDTOs() {
		List<ExpertiseSitterDTO> expertiseSitterDTOs = new ArrayList<ExpertiseSitterDTO>();
		ExpertiseSitterDTO expertiseSitterDTO = new ExpertiseSitterDTO();
		expertiseSitterDTO.setId(new Long(1));
		expertiseSitterDTO.setFirstName("Bob");
		expertiseSitterDTO.setLastName("Vein");
		expertiseSitterDTOs.add(expertiseSitterDTO);

		expertiseSitterDTO = new ExpertiseSitterDTO();
		expertiseSitterDTO.setId(new Long(2));
		expertiseSitterDTO.setFirstName("Rob");
		expertiseSitterDTO.setLastName("Van");
		expertiseSitterDTOs.add(expertiseSitterDTO);

		return expertiseSitterDTOs;
	}

	private List<ExpertiseSitter> getExpertiseSitters() {
		List<ExpertiseSitter> expertiseSitters = new ArrayList<ExpertiseSitter>();
		ExpertiseSitter expertiseSitter = new ExpertiseSitter();
		expertiseSitter.setId(new Long(1));
		expertiseSitter.setFirstName("Bob");
		expertiseSitter.setLastName("Vein");
		expertiseSitters.add(expertiseSitter);

		expertiseSitter = new ExpertiseSitter();
		expertiseSitter.setId(new Long(2));
		expertiseSitter.setFirstName("Rob");
		expertiseSitter.setLastName("Van");
		expertiseSitters.add(expertiseSitter);

		return expertiseSitters;
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
