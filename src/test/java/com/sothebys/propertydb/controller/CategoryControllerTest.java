package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.sothebys.propertydb.dto.request.CategoryRequestDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.CategoryDTO;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.mapper.CategoryModelMapper;
import com.sothebys.propertydb.model.Category;
import com.sothebys.propertydb.service.CategoryService;

public class CategoryControllerTest {

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @InjectMocks
  private CategoryController categoryController;

  @Mock
  CategoryModelMapper categoryMapper;

  @Mock
  private CategoryService categoryService;

  private MockMvc mockMvc;

  /**
   * @return category
   */
  public Category getCategory() {
    Category category = new Category();
    category.setId(1L);
    category.setName("category");
    category.setDescription("category discription");
    return category;
  }

  /**
   * @return categoryDTO
   */
  public CategoryDTO getCategoryDTO() {
    CategoryDTO categoryDTO = new CategoryDTO();
    categoryDTO.setId(1L);
    categoryDTO.setName("category");
    categoryDTO.setDescription("category discription");
    return categoryDTO;
  }

  /**
   * @return categoryRequestDto
   */
  public CategoryRequestDTO getCategoryRequestDto() {
    CategoryRequestDTO categoryDTO = new CategoryRequestDTO();
    categoryDTO.setName("category");
    categoryDTO.setDescription("category discription");
    return categoryDTO;
  }

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders.standaloneSetup(categoryController)
        .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
  }

  /**
   * Test case for createParentCategory Success scenario
   *
   * @throws Exception
   */
  @Test
  public void test_createCategory_success() throws Exception {
    Category category1 = getCategory();
    CategoryDTO categoryDto = getCategoryDTO();
    CategoryRequestDTO categoryRequestDTO = getCategoryRequestDto();

    when(categoryMapper.map(categoryRequestDTO, Category.class)).thenReturn(category1);
    when(categoryService.createParentCategory(category1)).thenReturn(category1);
    when(categoryMapper.map(category1, CategoryDTO.class)).thenReturn(categoryDto);

    mockMvc
        .perform(post("/categories", category1).contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(category1)))
        .andExpect(status().isCreated())
        .andExpect(header().string("Location", containsString(String.format("/categories"))));
    verify(categoryService, times(1)).createParentCategory(category1);
    verifyNoMoreInteractions(categoryService);

  }

  /**
   * Test case for createCategoryForParent Success scenario
   *
   * @throws Exception
   */
  @Test
  public void test_createSubCategory_success() throws Exception {
    Category category1 = getCategory();
    CategoryDTO categoryDto = getCategoryDTO();
    CategoryRequestDTO categoryRequestDTO = getCategoryRequestDto();

    when(categoryMapper.map(categoryRequestDTO, Category.class)).thenReturn(category1);
    when(categoryService.createCategoryForParent(category1.getId(), category1))
        .thenReturn(category1);
    when(categoryMapper.map(category1, CategoryDTO.class)).thenReturn(categoryDto);

    mockMvc
        .perform(post("/categories/{category-id}/children", category1.getId(), categoryDto)
            .contentType(MediaType.APPLICATION_JSON).content(asJsonString(categoryDto)))
        .andExpect(status().isCreated()).andExpect(header().string("Location", containsString(
            String.format("/categories/%s/children", category1.getId(), categoryDto))));
    verify(categoryService, times(1)).createCategoryForParent(category1.getId(), category1);
    verifyNoMoreInteractions(categoryService);

  }

  /**
   * Test case for deleteCategory success scenario
   *
   * @throws Exception
   */
  public void test_deleteChildByIdforParentId_failure() throws Exception {
    Category category = getCategory();
    doThrow(new NotFoundException("Category not found")).when(categoryService)
        .deleteParentCategory(category.getId());
    mockMvc.perform(get("/categories/{category-id}", category.getId(), category.getId()))
        .andDo(print()).andExpect(status().isNotFound())
        .andExpect(jsonPath("$.error").value(contains("Category not found")));
  }

  /**
   * Test case for deleteCategory success scenario
   */
  @Test
  public void test_deleteChildByIdforParentId_success() throws Exception {
    Category category = getCategory();
    mockMvc.perform(delete("/categories/{category-id}", category.getId(), category.getId()))
        .andExpect(status().isNoContent());
    verify(categoryService, times(1)).deleteParentCategory(category.getId());
    verifyNoMoreInteractions(categoryService);
  }

  /**
   * Test case for getAllCategories failure scenario
   *
   * @throws Exception
   */

  @Test
  public void test_findAllCategories_NotFound() throws Exception {
    Category category = getCategory();
    when(categoryService.getAllParentCategories()).thenReturn(Collections.EMPTY_LIST);
    mockMvc.perform(get("/categories", category.getId())).andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(0)));
  }

  /**
   * Test case for findAllChildCategories failure scenario
   */
  public void test_findAllChildCategories_NotFound() throws Exception {
    Category category = getCategory();
    when(categoryService.getAllChildCategories(category.getId()))
        .thenReturn(Collections.EMPTY_LIST);
    mockMvc.perform(get("/categories", category.getId())).andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(0)));
  }

  /**
   * Test case for findAllChildCategories
   *
   * @throws Exception
   */
  @Test
  public void test_findAllChildCategories_success() throws Exception {
    List<Category> categoryList = new ArrayList<Category>();
    Category category1 = getCategory();
    Category category2 = getCategory();
    categoryList.add(category1);
    categoryList.add(category2);
    CategoryDTO categoryDto = getCategoryDTO();

    when(categoryService.getAllChildCategories(category1.getId())).thenReturn(categoryList);
    when(categoryMapper.map(category1, CategoryDTO.class)).thenReturn(categoryDto);

    mockMvc.perform(get("/categories/{category-id}/children", 1)).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].name", is("category"))).andExpect(jsonPath("$[1].id", is(1)))
        .andExpect(jsonPath("$[1].name", is("category")));

    verify(categoryService, times(1)).getAllChildCategories(category1.getId());
    verifyNoMoreInteractions(categoryService);
  }

  /**
   * Test case for findAllParentCategories
   *
   * @throws Exception
   */
  @Test
  public void test_findAllParentCategories_success() throws Exception {
    List<Category> categories = Arrays.asList(new Category[] {getCategory(), getCategory()});
    List<CategoryDTO> categoryDTOs =
        Arrays.asList(new CategoryDTO[] {getCategoryDTO(), getCategoryDTO()});

    when(categoryService.getAllParentCategories()).thenReturn(categories);
    when(categoryMapper.mapToDTO(categories)).thenReturn(categoryDTOs);

    mockMvc.perform(get("/categories?type=main")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].name", is("category"))).andExpect(jsonPath("$[1].id", is(1)))
        .andExpect(jsonPath("$[1].name", is("category")));

    verify(categoryService, times(1)).getAllParentCategories();
    verifyNoMoreInteractions(categoryService);
  }

  /**
   * Test case for getCategoryById notFound scenario
   *
   * @throws Exception
   */
  @Test(expected = NotFoundException.class)
  public void test_getCategoryById_NotFound() throws Exception {
    Category category = getCategory();
    when(categoryController.getCategoryById(category.getId()))
        .thenThrow(new NotFoundException("Category not found"));
    mockMvc.perform(get("/categories/{categoryId}", category.getId())).andDo(print())
        .andExpect(status().isInternalServerError())
        .andExpect(jsonPath("$.error").value("Category not found"));
  }

  /**
   * Test case for getCategoryById
   *
   * @throws Exception
   */
  @Test
  public void test_getCategoryById_success() throws Exception {
    Category category = getCategory();
    CategoryDTO categoryDto = getCategoryDTO();

    when(categoryService.findParentCategory(category.getId())).thenReturn(category);
    when(categoryMapper.map(category, CategoryDTO.class)).thenReturn(categoryDto);

    mockMvc.perform(get("/categories/{categoryId}", 1)).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.name", is(category.getName()))).andDo(print());
    verify(categoryService, times(1)).findParentCategory(category.getId());
    verifyNoMoreInteractions(categoryService);

  }

  /**
   * Test case for updateCategory
   *
   * @throws Exception
   */
  @Test
  public void test_updateCategory_success() throws Exception {
    Category category = getCategory();
    CategoryDTO categoryDto = getCategoryDTO();
    CategoryRequestDTO categoryRequestDTO = getCategoryRequestDto();

    when(categoryMapper.map(categoryRequestDTO, Category.class)).thenReturn(category);
    when(categoryService.updateParentCategoryById(category.getId(), category)).thenReturn(category);
    when(categoryMapper.map(category, CategoryDTO.class)).thenReturn(categoryDto);

    mockMvc
        .perform(put("/categories/{categoryId}", category.getId())
            .contentType(MediaType.APPLICATION_JSON).content(asJsonString(category)))
        .andExpect(status().isOk());
    verify(categoryService, times(1)).updateParentCategoryById(category.getId(), category);
    verifyNoMoreInteractions(categoryService);
  }

}
