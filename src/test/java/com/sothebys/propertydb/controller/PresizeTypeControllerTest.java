package com.sothebys.propertydb.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.PresizeTypeDTO;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.exception.PropertyServiceException;
import com.sothebys.propertydb.model.PresizeType;
import com.sothebys.propertydb.service.PresizeTypeService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class PresizeTypeControllerTest {

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private MockMvc mockMvc;

  @InjectMocks
  private PresizeTypeController presizeTypeController;

  @Mock
  ModelMapper presizeTypesMapper = new ModelMapper();

  @Mock
  private PresizeTypeService presizeTypeService;

  /**
   * @return presizeTypes
   */
  public PresizeType getPresizeTypes() {
    PresizeType presizeType = new PresizeType();
    presizeType.setId(1L);
    presizeType.setName("presizeTypes");
    presizeType.setDescription("presizeTypes discription");
    return presizeType;
  }

  /**
   * @return presizeTypesDTO
   */
  public PresizeTypeDTO getPresizeTypesDTO() {
    PresizeTypeDTO presizeTypeDTO = new PresizeTypeDTO();
    presizeTypeDTO.setId(1L);
    presizeTypeDTO.setName("presizeTypes");
    presizeTypeDTO.setDescription("presizeTypes discription");
    return presizeTypeDTO;
  }

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders.standaloneSetup(presizeTypeController)
        .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
  }

  /**
   * Test case for createPresizeType
   *
   * @throws Exception
   */
  @Test
  public void test_createPresizeType_success() throws Exception {
    PresizeType presizeType = getPresizeTypes();
    PresizeTypeDTO presizeTypeDto = getPresizeTypesDTO();

    when(presizeTypesMapper.map(presizeTypeDto, PresizeType.class)).thenReturn(presizeType);
    when(presizeTypeService.createPresizeType(presizeType)).thenReturn(presizeType);
    when(presizeTypesMapper.map(presizeType, PresizeTypeDTO.class)).thenReturn(presizeTypeDto);

    mockMvc
        .perform(post("/presize-types").contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(presizeType)))
        .andExpect(status().isCreated())
        .andExpect(header().string("Location", containsString("/presize-types/")));;
    verify(presizeTypeService, times(1)).createPresizeType(presizeType);
    verifyNoMoreInteractions(presizeTypeService);
  }

  /**
   * Test case for getAllPresizeTypes failure scenario
   *
   * @throws Exception
   */
  public void test_findAllPresizeTypes_NotFound() throws Exception {
    when(presizeTypeService.getAllPresizeTypes()).thenReturn(new ArrayList<>(0));

    mockMvc.perform(get("/presize-types")).andDo(print())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(0)));

    verify(presizeTypeService, times(1)).getAllPresizeTypes();
    verifyNoMoreInteractions(presizeTypeService);
  }

  /**
   * Test case for findAllPresizeTypes
   *
   * @throws Exception
   */
  @Test
  public void test_findAllPresizeTypes_success() throws Exception {
    List<PresizeType> presizeTypeList = new ArrayList<PresizeType>();
    PresizeType presizeType1 = getPresizeTypes();
    PresizeType presizeType2 = getPresizeTypes();
    presizeTypeList.add(presizeType1);
    presizeTypeList.add(presizeType2);
    PresizeTypeDTO presizeTypeDto = getPresizeTypesDTO();

    when(presizeTypeService.getAllPresizeTypes()).thenReturn(presizeTypeList);
    when(presizeTypesMapper.map(presizeType1, PresizeTypeDTO.class)).thenReturn(presizeTypeDto);

    mockMvc.perform(get("/presize-types")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].name", is("presizeTypes"))).andExpect(jsonPath("$[1].id", is(1)))
        .andExpect(jsonPath("$[1].name", is("presizeTypes")));

    verify(presizeTypeService, times(1)).getAllPresizeTypes();
    verifyNoMoreInteractions(presizeTypeService);
  }

  /**
   * Test case for getPresizeTypeById
   *
   * @throws Exception
   */
  @Test
  public void test_getgetPresizeTypeById_success() throws Exception {
    PresizeType presizeType = getPresizeTypes();
    PresizeTypeDTO presizeTypeDto = getPresizeTypesDTO();

    when(presizeTypeService.getPresizeTypeById(presizeType.getId())).thenReturn(presizeType);
    when(presizeTypesMapper.map(presizeType, PresizeTypeDTO.class)).thenReturn(presizeTypeDto);

    mockMvc.perform(get("/presize-types/{presizeTypeId}", 1)).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.name", is(presizeType.getName()))).andDo(print());
    verify(presizeTypeService, times(1)).getPresizeTypeById(presizeType.getId());
    verifyNoMoreInteractions(presizeTypeService);

  }

  /**
   * Test case for getPresizeTypeById notFound scenario
   *
   * @throws Exception
   */
  @Test(expected = NotFoundException.class)
  public void test_getPresizeTypeById_NotFound() throws Exception {
    PresizeType presizeType = getPresizeTypes();
    when(presizeTypeController.getPresizeTypeById(presizeType.getId()))
        .thenThrow(new PropertyServiceException("PresizeTypes id not found"));
    mockMvc.perform(get("/presize-types/{presizeTypeId}", presizeType.getId())).andDo(print())
        .andExpect(status().isInternalServerError())
        .andExpect(jsonPath("$.error").value("PresizeTypes id not found..."));

    doThrow(new PropertyServiceException("PresizeTypes id not found"))
        .when(presizeTypeService).getPresizeTypeById(eq(presizeType.getId()));

  }

  /**
   * Test case for updatePresizeTypes
   *
   * @throws Exception
   */
  @Test
  public void test_updatePresizeType_success() throws Exception {
    PresizeType presizeType = getPresizeTypes();
    PresizeTypeDTO presizeTypeDto = getPresizeTypesDTO();

    when(presizeTypesMapper.map(presizeTypeDto, PresizeType.class)).thenReturn(presizeType);
    when(presizeTypeService.updatePresizeType(presizeType)).thenReturn(presizeType);
    when(presizeTypesMapper.map(presizeType, PresizeTypeDTO.class)).thenReturn(presizeTypeDto);

    mockMvc
        .perform(put("/presize-types/{presizeTypeId}", presizeType.getId())
            .contentType(MediaType.APPLICATION_JSON).content(asJsonString(presizeType)))
        .andExpect(status().isOk());
    verify(presizeTypeService, times(1)).updatePresizeType(presizeType);
    verifyNoMoreInteractions(presizeTypeService);
  }

  @Test
  public void test_deletePresizeType_success() throws Exception {
    mockMvc.perform(delete("/presize-types/{presizeTypeId}", 1L)).andExpect(status().isNoContent());
    verify(presizeTypeService, times(1)).deletePresizeType(1L);
    verifyNoMoreInteractions(presizeTypeService);
  }

}
