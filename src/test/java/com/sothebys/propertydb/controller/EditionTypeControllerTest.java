package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.sothebys.propertydb.dto.EditionTypeDTO;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.mapper.EditionTypeModelMapper;
import com.sothebys.propertydb.model.EditionType;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.service.EditionTypeService;

public class EditionTypeControllerTest {

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @InjectMocks
  private EditionTypeController editionTypeController;

  @Mock
  private EditionTypeService editionTypeService;

  private MockMvc mockMvc;

  @Mock
  ModelMapper modelMapper = new ModelMapper();

  @Mock
  EditionTypeModelMapper editionTypeModelMapper;

  /**
   * @return category
   */
  public EditionType getEditionType() {
    EditionType editionType = new EditionType();
    editionType.setId(1L);
    editionType.setSortCode(1L);
    editionType.setName("EditionType");
    editionType.setDescription("description");
    return editionType;
  }

  /**
   * @return categoryDTO
   */
  public EditionTypeDTO getEditionTypeDTO() {
    EditionTypeDTO editionTypeDTO = new EditionTypeDTO();
    editionTypeDTO.setId(1L);
    editionTypeDTO.setSortCode(1L);
    editionTypeDTO.setName("EditionType");
    editionTypeDTO.setDescription("description");
    return editionTypeDTO;
  }

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders.standaloneSetup(editionTypeController)
        .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
  }

  /**
   * Test case for createParentCategory
   *
   * @throws Exception
   */
  @Test
  public void test_createEditionType_success() throws Exception {
    EditionType editionType = getEditionType();
    EditionTypeDTO editionTypeDto = getEditionTypeDTO();

    when(editionTypeModelMapper.map(editionTypeDto, EditionType.class)).thenReturn(editionType);
    when(editionTypeService.createEditionType(editionType)).thenReturn(editionType);
    when(editionTypeModelMapper.map(editionType, EditionTypeDTO.class)).thenReturn(editionTypeDto);
    mockMvc
        .perform(post("/edition-types").contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(editionType)))
        .andExpect(status().isCreated())
        .andExpect(header().string("Location", containsString("/edition-types/")));;
    verify(editionTypeService, times(1)).createEditionType(editionType);
    verifyNoMoreInteractions(editionTypeService);
  }

  /**
   * Test case for findAllCategories
   */
  @Test
  public void test_findAllCategories_success() throws Exception {
    List<EditionType> editionTypes =
        Arrays.asList(new EditionType[] {getEditionType(), getEditionType()});
    List<EditionTypeDTO> editionTypeDTOs =
        Arrays.asList(new EditionTypeDTO[] {getEditionTypeDTO(), getEditionTypeDTO()});

    when(editionTypeService.getAllEditionTypes()).thenReturn(editionTypes);
    when(editionTypeModelMapper.mapToDTO(editionTypes)).thenReturn(editionTypeDTOs);

    // @formatter:off
    mockMvc.perform(get("/edition-types")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].name", is("EditionType"))).andExpect(jsonPath("$[1].id", is(1)))
        .andExpect(jsonPath("$[1].name", is("EditionType")));
    // @formatter:on

    verify(editionTypeService, times(1)).getAllEditionTypes();
    verifyNoMoreInteractions(editionTypeService);
  }

  /**
   * Test case for getAllCategories failure scenario
   *
   * @throws Exception
   */
  public void test_findAllEditionType_NotFound() throws Exception {
    when(editionTypeService.getAllEditionTypes()).thenReturn(new ArrayList<>(0));

    mockMvc.perform(get("/edition-types")).andDo(print())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(0)));
  }

  /**
   * Test case for geEditionTypeById notFound scenario
   *
   * @throws Exception
   */
  @Test(expected = NotFoundException.class)
  public void test_getEditionTypeById_NotFound() throws Exception {
    EditionType editionType = getEditionType();
    when(editionTypeController.getEditionTypeById(editionType.getId()))
        .thenThrow(new NotFoundException("EditionType id not found..."));
    mockMvc.perform(get("/edition-types/{editionTypeId}", editionType.getId())).andDo(print())
        .andExpect(status().isInternalServerError())
        .andExpect(jsonPath("$.error").value("EditionType id not found..."));
  }

  /**
   * Test case for geEditionTypeById
   *
   * @throws Exception
   */
  @Test
  public void test_geEditionTypeById_success() throws Exception {
    EditionType editionType = getEditionType();
    EditionTypeDTO editionTypeDto = getEditionTypeDTO();

    when(editionTypeService.getEditionTypeById(editionType.getId())).thenReturn(editionType);
    when(editionTypeModelMapper.map(editionType, EditionTypeDTO.class)).thenReturn(editionTypeDto);

    mockMvc.perform(get("/edition-types/{editiontypeId}", 1)).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.name", is(editionType.getName()))).andDo(print());
    verify(editionTypeService, times(1)).getEditionTypeById(editionType.getId());
    verifyNoMoreInteractions(editionTypeService);

  }

  /**
   * Test case for updateCategory
   *
   * @throws Exception
   */
  @Test
  public void test_updateEditionType_success() throws Exception {
    EditionType editionType = getEditionType();
    EditionTypeDTO editionTypeDto = getEditionTypeDTO();

    when(editionTypeModelMapper.map(editionTypeDto, EditionType.class)).thenReturn(editionType);
    when(editionTypeService.updateEditionType(editionType.getId(),editionType)).thenReturn(editionType);
    when(editionTypeModelMapper.map(editionType, EditionTypeDTO.class)).thenReturn(editionTypeDto);

    mockMvc
        .perform(put("/edition-types/{editiontypeId}", editionType.getId())
            .contentType(MediaType.APPLICATION_JSON).content(asJsonString(editionType)))
        .andExpect(status().isOk());
    verify(editionTypeService, times(1)).updateEditionType(editionType.getId(),editionType);
    verifyNoMoreInteractions(editionTypeService);
  }

}
