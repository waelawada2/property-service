/**
 * 
 */
package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.ImageAnimalDTO;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.mapper.ImageAnimalModelMapper;
import com.sothebys.propertydb.model.ImageAnimal;
import com.sothebys.propertydb.service.ImageAnimalService;

/**
 * @author aneesha.l
 *
 */
public class ImageAnimalControllerTest {

	@InjectMocks
	private ImageAnimalController imageAnimalController;

	@Mock
	private ImageAnimalModelMapper imageAnimalModelMapper;

	@Mock
	private ImageAnimalService imageAnimalService;

	@InjectMocks
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	private MockMvc mockMvc;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		mockMvc = MockMvcBuilders.standaloneSetup(imageAnimalController)
				.setMessageConverters(new MappingJackson2HttpMessageConverter())
				.setCustomArgumentResolvers(pageableArgumentResolver).build();

	}

	/**
	 * @return ImageAnimal
	 */
	public ImageAnimal getImageAnimal() {
		ImageAnimal imageAnimal = new ImageAnimal();
		imageAnimal.setId(1L);
		imageAnimal.setName("Tiger");

		return imageAnimal;
	}

	/**
	 * @return ImageAnimal list
	 */
	public List<ImageAnimal> getImageAnimalList() {
		List<ImageAnimal> imageAnimalList = new ArrayList<ImageAnimal>();

		ImageAnimal imageAnimal = new ImageAnimal();
		imageAnimal.setId(new Long(1));
		imageAnimal.setName("Lion");
		imageAnimalList.add(imageAnimal);

		imageAnimal = new ImageAnimal();
		imageAnimal.setId(new Long(2));
		imageAnimal.setName("Tiger");
		imageAnimalList.add(imageAnimal);

		imageAnimal = new ImageAnimal();
		imageAnimal.setId(new Long(3));
		imageAnimal.setName("Fox");
		imageAnimalList.add(imageAnimal);

		imageAnimal = new ImageAnimal();
		imageAnimal.setId(new Long(4));
		imageAnimal.setName("Wolf");
		imageAnimalList.add(imageAnimal);

		return imageAnimalList;
	}

	/**
	 * @return ImageAnimalDTO
	 */
	public ImageAnimalDTO getImageAnimalDTO() {
		ImageAnimalDTO imageAnimalDTO = new ImageAnimalDTO();
		imageAnimalDTO.setId(1L);
		imageAnimalDTO.setName("name 1");

		return imageAnimalDTO;
	}

	/**
	 * @return ImageAnimalDTO list
	 */
	public List<ImageAnimalDTO> getImageAnimalDTOList() {
		List<ImageAnimalDTO> imageAnimalDTOList = new ArrayList<ImageAnimalDTO>();

		ImageAnimalDTO imageAnimalDTO = new ImageAnimalDTO();
		imageAnimalDTO.setId(new Long(1));
		imageAnimalDTO.setName("Lion");
		imageAnimalDTOList.add(imageAnimalDTO);

		imageAnimalDTO = new ImageAnimalDTO();
		imageAnimalDTO.setId(new Long(2));
		imageAnimalDTO.setName("Tiger");
		imageAnimalDTOList.add(imageAnimalDTO);

		imageAnimalDTO = new ImageAnimalDTO();
		imageAnimalDTO.setId(new Long(3));
		imageAnimalDTO.setName("Fox");
		imageAnimalDTOList.add(imageAnimalDTO);

		imageAnimalDTO = new ImageAnimalDTO();
		imageAnimalDTO.setId(new Long(4));
		imageAnimalDTO.setName("Wolf");
		imageAnimalDTOList.add(imageAnimalDTO);

		return imageAnimalDTOList;
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Test case for createImageAnimal
	 *
	 * @throws Exception
	 */

	@Test
	public void test_createImageAnimal_success() throws Exception {
		ImageAnimal imageAnimal = getImageAnimal();
		ImageAnimalDTO imageAnimalDTO = getImageAnimalDTO();

		when(imageAnimalModelMapper.map(imageAnimalDTO, ImageAnimal.class)).thenReturn(imageAnimal);
		when(imageAnimalService.createImageAnimal(imageAnimal)).thenReturn(imageAnimal);
		when(imageAnimalModelMapper.map(imageAnimal, ImageAnimalDTO.class)).thenReturn(imageAnimalDTO);

		mockMvc.perform(
				post("/imageAnimals").contentType(MediaType.APPLICATION_JSON).content(asJsonString(imageAnimalDTO)))
				.andExpect(status().isCreated())
				.andExpect(header().string("Location", containsString("/imageAnimals/")));
		verify(imageAnimalService, times(1)).createImageAnimal(imageAnimal);
		verifyNoMoreInteractions(imageAnimalService);
	}

	/**
	 * Test case for deleteImageAnimalById
	 *
	 * @throws Exception
	 */

	@Test
	public void test_deleteImageAnimalById_success() throws Exception {
		ImageAnimal imageAnimal = getImageAnimal();
		doNothing().when(imageAnimalService).deleteImageAnimal(imageAnimal.getId());
		mockMvc.perform(delete("/imageAnimals/{id}", imageAnimal.getId())).andExpect(status().isNoContent());
		verify(imageAnimalService, times(1)).deleteImageAnimal(imageAnimal.getId());
	}

	/**
	 * Test case for findAllImageAnimals
	 *
	 * @throws Exception
	 */
	@Test
	public void test_findAllImageAnimals_success() throws Exception {
		List<ImageAnimal> imageAnimalList = getImageAnimalList();
		List<ImageAnimalDTO> imageAnimalDTOList = getImageAnimalDTOList();
		when(imageAnimalService.findAllImageAnimals()).thenReturn(imageAnimalList);
		when(imageAnimalModelMapper.mapToImageAnimalDTOs(imageAnimalList)).thenReturn(imageAnimalDTOList);
		mockMvc.perform(get("/imageAnimals")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", hasSize(4))).andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("Lion"))).andExpect(jsonPath("$[3].id", is(4)))
				.andExpect(jsonPath("$[3].name", is("Wolf")));
		verify(imageAnimalService, times(1)).findAllImageAnimals();
		verifyNoMoreInteractions(imageAnimalService);

	}

	/**
	 * Test case for findAllImageAnimals by name
	 *
	 * @throws Exception
	 */
	@Test
	public void test_findAllImageAnimals_byName_success() throws Exception {
		List<ImageAnimal> imageAnimalList = getImageAnimalList();
		List<ImageAnimalDTO> imageAnimalDTOList = getImageAnimalDTOList();
		String name = "Lion";
		when(imageAnimalService.findAllImageAnimalsByName(name)).thenReturn(imageAnimalList);
		when(imageAnimalModelMapper.mapToImageAnimalDTOs(imageAnimalList)).thenReturn(imageAnimalDTOList);
		mockMvc.perform(get("/imageAnimals/" + name).param("name", name)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", hasSize(4))).andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("Lion")));
		verify(imageAnimalService, times(1)).findAllImageAnimalsByName(name);
		verifyNoMoreInteractions(imageAnimalService);
	}

	/**
	 * Test case for updateImageAnimal success
	 *
	 * @throws Exception
	 */
	@Test
	public void test_updateImageAnimal_success() throws Exception {
		ImageAnimal imageAnimal = getImageAnimal();
		ImageAnimalDTO imageAnimalDTO = getImageAnimalDTO();

		when(imageAnimalModelMapper.map(imageAnimalDTO, ImageAnimal.class)).thenReturn(imageAnimal);
		when(imageAnimalService.updateImageAnimal(imageAnimal)).thenReturn(imageAnimal);

		when(imageAnimalModelMapper.map(imageAnimal, ImageAnimalDTO.class)).thenReturn(imageAnimalDTO);

		mockMvc.perform(put("/imageAnimals").requestAttr("imageAnimalDTO", imageAnimalDTO)
				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(imageAnimalDTO)))
				.andExpect(status().isOk());
		verify(imageAnimalService, times(1)).updateImageAnimal(imageAnimal);
	}

	/**
	 * Test case for updateImageAnimal - Animal Not Found
	 *
	 * @throws Exception
	 */
	@Test(expected = Exception.class)
	public void test_updateImageAnimal_notFound() throws Exception {
		ImageAnimal imageAnimal = getImageAnimal();
		ImageAnimalDTO imageAnimalDTO = getImageAnimalDTO();
		when(imageAnimalModelMapper.map(imageAnimalDTO, ImageAnimal.class)).thenReturn(imageAnimal);
		when(imageAnimalService.updateImageAnimal(imageAnimal)).thenThrow(
				new NotFoundException(String.format("Animal object with Id %s doesn't exist", imageAnimal.getId())));

		when(imageAnimalModelMapper.map(imageAnimal, ImageAnimalDTO.class)).thenReturn(imageAnimalDTO);

		mockMvc.perform(put("/imageAnimals").requestAttr("imageAnimalDTO", imageAnimalDTO)
				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(imageAnimalDTO))).andDo(print())// .andExpect(status().isNotFound())
				.andExpect(jsonPath("$.error")
						.value(String.format("Animal object with Id %s doesn't exist", imageAnimal.getId())));
		verify(imageAnimalService, times(1)).updateImageAnimal(imageAnimal);

	}

}
