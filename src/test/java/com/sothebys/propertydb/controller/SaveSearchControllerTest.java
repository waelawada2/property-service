package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.SaveSearchCriteriaDTO;
import com.sothebys.propertydb.dto.SaveSearchDTO;
import com.sothebys.propertydb.dto.request.SaveSearchRequestDTO;
import com.sothebys.propertydb.exception.ForbiddenException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.mapper.SaveSearchModelMapper;
import com.sothebys.propertydb.model.SaveSearch;
import com.sothebys.propertydb.model.SaveSearchCriteria;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.service.SaveSearchService;

public class SaveSearchControllerTest {

  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @InjectMocks
  private SaveSearchController saveSearchController;

  @Mock
  SaveSearchModelMapper saveSearchModelMapper;

  @Mock
  private SaveSearchService saveSearchService;

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  private MockMvc mockMvc;

  /**
   * @return saveSearch
   */
  private SaveSearch getSaveSearch() {
    SaveSearch saveSearch = new SaveSearch();
    saveSearch.setId(1L);
    saveSearch.setName("ram");
    saveSearch.setNotes("notes");
    saveSearch.setExternalId("00000002");
    saveSearch.setUserId(1L);
    List<SaveSearchCriteria> searchCriteriaList = new ArrayList<SaveSearchCriteria>();
    SaveSearchCriteria searchCriteria = new SaveSearchCriteria();
    searchCriteria.setId(1L);
    searchCriteria.setParamName("fname");
    searchCriteria.setParamValue("ram");
    saveSearch.setSearchType("properties");
    searchCriteriaList.add(searchCriteria);
    saveSearch.setSearchCriterias(searchCriteriaList);
    return saveSearch;
  }

  /**
   * @return countryDTO
   */
  private SaveSearchDTO getSaveSearchDTO() {
    SaveSearchDTO saveSearchDTO = new SaveSearchDTO();
    saveSearchDTO.setId("00000002");
    saveSearchDTO.setName("ram");
    saveSearchDTO.setNotes("notes");
    List<SaveSearchCriteriaDTO> searchCriteriaDTOList = new ArrayList<SaveSearchCriteriaDTO>();
    SaveSearchCriteriaDTO searchCriteriaDTO = new SaveSearchCriteriaDTO();
    searchCriteriaDTO.setId(1L);
    searchCriteriaDTO.setParamName("fname");
    searchCriteriaDTO.setParamValue("ram");
    saveSearchDTO.setSearchType("properties");
    searchCriteriaDTOList.add(searchCriteriaDTO);
    saveSearchDTO.setSearchCriterias(searchCriteriaDTOList);
    return saveSearchDTO;
  }

  /**
   * @return SaveSearchDTO
   */
  private SaveSearchRequestDTO getSaveSearchRequestDTO() {
    SaveSearchRequestDTO saveSearchDTO = new SaveSearchRequestDTO();
    saveSearchDTO.setName("ram");
    saveSearchDTO.setNotes("notes");
    List<SaveSearchCriteriaDTO> searchCriteriaDTOList = new ArrayList<SaveSearchCriteriaDTO>();
    SaveSearchCriteriaDTO searchCriteriaDTO = new SaveSearchCriteriaDTO();
    searchCriteriaDTO.setId(1L);
    searchCriteriaDTO.setParamName("fname");
    searchCriteriaDTO.setParamValue("ram");
    saveSearchDTO.setSearchType("properties");
    searchCriteriaDTOList.add(searchCriteriaDTO);
    saveSearchDTO.setSearchCriterias(searchCriteriaDTOList);
    return saveSearchDTO;
  }

  public User getUser() {
    User user = new User();
    user.setKeycloakUserId("123456-7890123-456789");
    user.setFirstName("Test");
    user.setLastName("User");
    user.setUserName("ram");
    user.setEmail("test.user@sothebys.com");
    user.setId(1L);
    return user;
  }

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders.standaloneSetup(saveSearchController)
        .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
  }

  /**
   * Test case for findByUserIdandExternalId
   *
   * @throws Exception
   */
  @Test
  public void test_findByUserIdandExternalId_success() throws Exception {
    SaveSearch saveSearch = getSaveSearch();
    SaveSearchDTO saveSearchDto = getSaveSearchDTO();

    when(saveSearchService.findSaveSearchesByExternalId(saveSearch.getName(),
        saveSearch.getExternalId())).thenReturn(saveSearch);
    when(saveSearchModelMapper.map(saveSearch, SaveSearchDTO.class)).thenReturn(saveSearchDto);
    mockMvc.perform(get("/users/{user-name}/searches/{external-id}", saveSearch.getName(),
        saveSearch.getExternalId()).requestAttr("requestUser", getUser())
            .content(asJsonString(saveSearchDto)))
        .andExpect(status().isOk());
    verify(saveSearchService, times(1)).findSaveSearchesByExternalId(saveSearch.getName(),
        saveSearch.getExternalId());
    verifyNoMoreInteractions(saveSearchService);
  }

  /**
   * Test case for findByUserIdandExternalId notFound scenario
   *
   * @throws Exception
   */
  @Test
  public void test_findByUserIdandExternalId_NotFound() throws Exception {
    SaveSearch saveSearch = getSaveSearch();
    thrown.expect(NotFoundException.class);
    thrown.expectMessage(
        String.format("No save search found with ID=%s", saveSearch.getExternalId()));
    when(saveSearchController.findSaveSearchesByExternalId(saveSearch.getName(),
        saveSearch.getExternalId()));
  }

  /**
   * Test case for createSaveSearch
   *
   * @throws Exception
   */
  @Test
  public void test_createSaveSearch_success() throws Exception {
    SaveSearch saveSearch = getSaveSearch();
    SaveSearchRequestDTO saveSearchRequestDTO = getSaveSearchRequestDTO();
    SaveSearchDTO saveSearchDTO = getSaveSearchDTO();

    when(saveSearchModelMapper.map(saveSearchRequestDTO, SaveSearch.class)).thenReturn(saveSearch);
    when(saveSearchService.createSaveSearch(saveSearch, getUser().getId())).thenReturn(saveSearch);
    when(saveSearchModelMapper.mapToDTO(saveSearch)).thenReturn(saveSearchDTO);

    mockMvc.perform(post("/users/{user-name}/searches", saveSearch.getName())
        .contentType(MediaType.APPLICATION_JSON).requestAttr("requestUser", getUser())
        .content(asJsonString(saveSearchRequestDTO))).andExpect(status().isCreated());

    verify(saveSearchService, times(1)).createSaveSearch(saveSearch, getUser().getId());
    verifyNoMoreInteractions(saveSearchService);
  }

  /**
   * Test case for createSaveSearch name doesn't match with user object name scenario
   *
   * @throws Exception
   */
  @Test
  public void test_createSaveSearch_failure() throws Exception {
    SaveSearch saveSearch = getSaveSearch();
    saveSearch.setName("testUser");
    SaveSearchRequestDTO saveSearchRequestDTO = getSaveSearchRequestDTO();

    thrown.expect(ForbiddenException.class);
    thrown.expectMessage(String.format(
        "userName '%s' doesn't match with user object name %s, hence create save search failed",
        saveSearch.getName(), getUser().getUserName()));
    when(saveSearchController.createSaveSearch(saveSearch.getName(), saveSearchRequestDTO,
        getUser()));
  }

  /**
   * Test case for delete SaveSearch success scenario
   */
  @Test
  public void test_deleteSaveSearchListByExternalId_success() throws Exception {
    SaveSearch saveSearch = getSaveSearch();
    mockMvc
        .perform(delete("/users/{user-name}/searches/{external-id}", saveSearch.getName(),
            saveSearch.getExternalId()).requestAttr("requestUser", getUser()))
        .andExpect(status().isNoContent());

    verify(saveSearchService, times(1)).deleteByUserIdAndExternalId(saveSearch.getId(),
        saveSearch.getExternalId());
    verifyNoMoreInteractions(saveSearchService);
  }

  /**
   * Test case for updateSavesearch
   *
   * @throws Exception
   */
  @Test
  public void test_updateSavedSearchByExternalId_success() throws Exception {
    SaveSearch saveSearch = getSaveSearch();
    SaveSearchRequestDTO saveSearchRequestDTO = getSaveSearchRequestDTO();
    SaveSearchDTO saveSearchDTO = getSaveSearchDTO();

    when(saveSearchModelMapper.map(saveSearchRequestDTO, SaveSearch.class)).thenReturn(saveSearch);
    when(saveSearchService.updateByUserIdAndExternalId(saveSearch.getId(),
        saveSearch.getExternalId(), saveSearch)).thenReturn(saveSearch);
    when(saveSearchModelMapper.map(saveSearch, SaveSearch.class)).thenReturn(saveSearchDTO);

    mockMvc
        .perform(put("/users/{user-name}/searches/{external-id}", saveSearch.getName(),
            saveSearch.getExternalId()).requestAttr("requestUser", getUser())
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(saveSearchRequestDTO)))
        .andExpect(status().isOk());

    verify(saveSearchService, times(1)).updateByUserIdAndExternalId(saveSearch.getId(),
        saveSearch.getExternalId(), saveSearch);
    verifyNoMoreInteractions(saveSearchService);
  }

  /**
   * Test case for updateSaveSearch, name doesn't match with user object name scenario
   *
   * @throws Exception
   */
  @Test
  public void test_updateSaveSearch_failure() throws Exception {
    SaveSearch saveSearch = getSaveSearch();
    saveSearch.setName("testUser");
    SaveSearchRequestDTO saveSearchRequestDTO = getSaveSearchRequestDTO();

    thrown.expect(ForbiddenException.class);
    thrown.expectMessage(String.format(
        "userName '%s' doesn't match with user object name %s, hence update save search failed",
        saveSearch.getName(), getUser().getUserName()));
    when(saveSearchController.updateSavedSearchByExternalId(saveSearchRequestDTO,
        saveSearch.getName(), saveSearch.getExternalId(), getUser()));
  }

}
