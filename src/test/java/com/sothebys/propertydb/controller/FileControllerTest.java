package com.sothebys.propertydb.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.PropertyDTO;
import com.sothebys.propertydb.model.ArchiveNumber;
import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.Authenticity;
import com.sothebys.propertydb.model.Category;
import com.sothebys.propertydb.model.Edition;
import com.sothebys.propertydb.model.EditionType;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.PropertyCatalogueRaisonee;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class FileControllerTest {
  public static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @InjectMocks
  private FileController fileUploadController;

  private MockMvc mockMvc;

  /**
   * @return category
   */
  public Property getPropertyObject() {
    Property property = new Property();
    List<ArchiveNumber> archiveNumberList = new ArrayList<>();
    List<PropertyCatalogueRaisonee> catalogueRaisoneeList = new ArrayList<>();

    ArchiveNumber archiveNumber = new ArchiveNumber();
    archiveNumber.setArchiveNumber("1");
    archiveNumberList.add(archiveNumber);
    property.setArchiveNumbers(archiveNumberList);

    PropertyCatalogueRaisonee catalogueRaisonee = new PropertyCatalogueRaisonee();
    catalogueRaisonee.setVolume("5");
    catalogueRaisonee.setNumber("6");
    catalogueRaisoneeList.add(catalogueRaisonee);
    property.setPropertyCatalogueRaisonees(catalogueRaisoneeList);

    Category category = new Category();
    category.setDescription("Painting");
    property.setCategory(category);

    Edition edition = new Edition();
    EditionType editionType = new EditionType();
    editionType.setDescription("Edition Type");
    edition.setEditionType(editionType);
    property.setEdition(edition);

    property.setId(1L);
    Artist artist = new Artist();
    artist.setLastName("Picasso");
    artist.setId(1L);
    property.setArtists(Arrays.asList(artist));

    Authenticity authenticity = new Authenticity();
    authenticity.setAuthentication(true);
    property.setAuthenticity(authenticity);
    property.setImagePath("2017321055254");

    return property;
  }

  /**
   * @return categoryDTO
   **/

  public PropertyDTO getPropertyObjectDto() {
    PropertyDTO propertyDTO = new PropertyDTO();
    propertyDTO.setId("1");
    propertyDTO.setAuthentication(true);
    propertyDTO.setImagePath("2017321055254");
    return propertyDTO;
  }

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders.standaloneSetup(fileUploadController)
        .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

  }

  /**
   * Test case for empty file upload_failure
   *
   * @throws Exception
   */
  @Test
  public void test_fileUpload_Failure() throws Exception {

    MockMultipartFile multipartFile =
        new MockMultipartFile("file", "", "multipart/form-data", "".getBytes());

    MockHttpServletRequestBuilder requestBuilder =
        MockMvcRequestBuilders.fileUpload("/fileUpload").file(multipartFile);
    mockMvc.perform(requestBuilder).andExpect(status().isBadRequest()).andReturn();

  }

  /**
   * Test case for file upload_success
   *
   * @throws Exception
   */
  @Test
  @Ignore //Ignore for now, need to fix this
  public void test_fileUpload_success() throws Exception {

    MockMultipartFile multipartFile =
        new MockMultipartFile("file", "", "multipart/form-data", "Spring Framework".getBytes());

    MockHttpServletRequestBuilder requestBuilder =
        MockMvcRequestBuilders.fileUpload("/fileUpload").file(multipartFile);
    mockMvc.perform(requestBuilder).andExpect(status().isOk()).andReturn();

  }

}
