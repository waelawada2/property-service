/**
 * 
 */
package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.ImageSubjectDTO;
import com.sothebys.propertydb.mapper.ImageSubjectModelMapper;
import com.sothebys.propertydb.model.ImageSubject;
import com.sothebys.propertydb.service.ImageSubjectService;

/**
 * @author aneesha.l
 *
 */
public class ImageSubjectControllerTest {

	@InjectMocks
	private ImageSubjectController imageSubjectController;

	@Mock
	private ImageSubjectModelMapper imageSubjectModelMapper;

	@Mock
	private ImageSubjectService imageSubjectService;

	@InjectMocks
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	private MockMvc mockMvc;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		mockMvc = MockMvcBuilders.standaloneSetup(imageSubjectController)
				.setMessageConverters(new MappingJackson2HttpMessageConverter())
				.setCustomArgumentResolvers(pageableArgumentResolver).build();

	}

	/**
	 * @return ImageSubject
	 */
	public ImageSubject getImageSubject() {
		ImageSubject imageSubject = new ImageSubject();
		imageSubject.setId(1L);
		imageSubject.setName("Red");

		return imageSubject;
	}

	/**
	 * @return ImageSubject list
	 */
	public List<ImageSubject> getImageSubjectList() {
		List<ImageSubject> imageSubjectList = new ArrayList<ImageSubject>();

		ImageSubject imageSubject = new ImageSubject();
		imageSubject.setId(new Long(1));
		imageSubject.setName("Red");
		imageSubjectList.add(imageSubject);

		imageSubject = new ImageSubject();
		imageSubject.setId(new Long(2));
		imageSubject.setName("Blue");
		imageSubjectList.add(imageSubject);

		imageSubject = new ImageSubject();
		imageSubject.setId(new Long(3));
		imageSubject.setName("Black");
		imageSubjectList.add(imageSubject);

		imageSubject = new ImageSubject();
		imageSubject.setId(new Long(4));
		imageSubject.setName("White");
		imageSubjectList.add(imageSubject);

		return imageSubjectList;
	}

	/**
	 * @return ImageSubjectDTO
	 */
	public ImageSubjectDTO getImageSubjectDTO() {
		ImageSubjectDTO imageSubjectDTO = new ImageSubjectDTO();
		imageSubjectDTO.setId(1L);
		imageSubjectDTO.setName("Red");

		return imageSubjectDTO;
	}

	/**
	 * @return ImageSubjectDTO list
	 */
	public List<ImageSubjectDTO> getImageSubjectDTOList() {
		List<ImageSubjectDTO> imageSubjectDTOList = new ArrayList<ImageSubjectDTO>();

		ImageSubjectDTO imageSubjectDTO = new ImageSubjectDTO();
		imageSubjectDTO.setId(new Long(1));
		imageSubjectDTO.setName("Red");
		imageSubjectDTOList.add(imageSubjectDTO);

		imageSubjectDTO = new ImageSubjectDTO();
		imageSubjectDTO.setId(new Long(2));
		imageSubjectDTO.setName("Blue");
		imageSubjectDTOList.add(imageSubjectDTO);

		imageSubjectDTO = new ImageSubjectDTO();
		imageSubjectDTO.setId(new Long(3));
		imageSubjectDTO.setName("Black");
		imageSubjectDTOList.add(imageSubjectDTO);

		imageSubjectDTO = new ImageSubjectDTO();
		imageSubjectDTO.setId(new Long(4));
		imageSubjectDTO.setName("White");
		imageSubjectDTOList.add(imageSubjectDTO);

		return imageSubjectDTOList;
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Test case for createImageSubject
	 *
	 * @throws Exception
	 */

	@Test
	public void test_createImageSubject_success() throws Exception {
		ImageSubject imageSubject = getImageSubject();
		ImageSubjectDTO imageSubjectDTO = getImageSubjectDTO();

		when(imageSubjectModelMapper.map(imageSubjectDTO, ImageSubject.class)).thenReturn(imageSubject);
		when(imageSubjectService.createImageSubject(imageSubject)).thenReturn(imageSubject);
		when(imageSubjectModelMapper.map(imageSubject, ImageSubjectDTO.class)).thenReturn(imageSubjectDTO);

		mockMvc.perform(
				post("/imageSubjects").contentType(MediaType.APPLICATION_JSON).content(asJsonString(imageSubjectDTO)))
				.andExpect(status().isCreated())
				.andExpect(header().string("Location", containsString("/imageSubjects/")));
		verify(imageSubjectService, times(1)).createImageSubject(imageSubject);
		verifyNoMoreInteractions(imageSubjectService);
	}

	/**
	 * Test case for deleteImageSubjectById
	 *
	 * @throws Exception
	 */

	@Test
	public void test_deleteImageSubjectById_success() throws Exception {
		ImageSubject imageSubject = getImageSubject();
		doNothing().when(imageSubjectService).deleteImageSubjectById(imageSubject.getId());
		mockMvc.perform(delete("/imageSubjects/{id}", imageSubject.getId())).andExpect(status().isNoContent());
		verify(imageSubjectService, times(1)).deleteImageSubjectById(imageSubject.getId());
	}

	/**
	 * Test case for updateImageSubject success
	 *
	 * @throws Exception
	 */
	@Test
	public void test_updateImageSubject_success() throws Exception {
		ImageSubject imageSubject = getImageSubject();
		ImageSubjectDTO imageSubjectDTO = getImageSubjectDTO();

		when(imageSubjectModelMapper.map(imageSubjectDTO, ImageSubject.class)).thenReturn(imageSubject);
		when(imageSubjectService.updateImageSubjectById(imageSubjectDTO.getId(), imageSubject))
				.thenReturn(imageSubject);

		when(imageSubjectModelMapper.map(imageSubject, ImageSubjectDTO.class)).thenReturn(imageSubjectDTO);

		mockMvc.perform(
				put("/imageSubjects/" + imageSubjectDTO.getId()).requestAttr("imageSubjectDTO", imageSubjectDTO)
						.contentType(MediaType.APPLICATION_JSON).content(asJsonString(imageSubjectDTO)))
				.andExpect(status().isOk());
		verify(imageSubjectService, times(1)).updateImageSubjectById(imageSubject.getId(), imageSubject);
	}

	/**
	 * Test case for getting all Subjects
	 *
	 */
	@Test
	public void test_findAllImageSubjects() throws Exception {
		List<ImageSubjectDTO> imageSubjectDTOs = getImageSubjectDTOList();
		List<ImageSubject> imageSubjects = getImageSubjectList();

		when(imageSubjectService.findAllImageSubjects()).thenReturn(imageSubjects);
		when(imageSubjectModelMapper.mapToImageSubjectDTOs(imageSubjects)).thenReturn(imageSubjectDTOs);

		mockMvc.perform(get("/imageSubjects").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(4)));
		;

		verify(imageSubjectService, times(1)).findAllImageSubjects();
		verifyNoMoreInteractions(imageSubjectService);

	}

}
