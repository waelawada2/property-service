/**
 * 
 */
package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.OtherTagDTO;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.mapper.OtherTagModelMapper;
import com.sothebys.propertydb.model.OtherTag;
import com.sothebys.propertydb.service.OtherTagService;

/**
 * @author aneesha.l
 *
 */
public class OtherTagControllerTest {

	@InjectMocks
	private OtherTagController otherTagController;

	@Mock
	private OtherTagModelMapper otherTagModelMapper;

	@Mock
	private OtherTagService otherTagService;

	@InjectMocks
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	private MockMvc mockMvc;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		mockMvc = MockMvcBuilders.standaloneSetup(otherTagController)
				.setMessageConverters(new MappingJackson2HttpMessageConverter())
				.setCustomArgumentResolvers(pageableArgumentResolver).build();

	}

	/**
	 * @return OtherTag
	 */
	public OtherTag getOtherTag() {
		OtherTag otherTag = new OtherTag();
		otherTag.setId(1L);
		otherTag.setName("name 1");

		return otherTag;
	}

	/**
	 * @return OtherTag list
	 */
	public List<OtherTag> getOtherTagList() {
		List<OtherTag> otherTagList = new ArrayList<OtherTag>();

		OtherTag otherTag = new OtherTag();
		otherTag.setId(new Long(1));
		otherTag.setName("Tag 1");
		otherTagList.add(otherTag);

		otherTag = new OtherTag();
		otherTag.setId(new Long(2));
		otherTag.setName("Tag 2");
		otherTagList.add(otherTag);

		otherTag = new OtherTag();
		otherTag.setId(new Long(3));
		otherTag.setName("New Tag");
		otherTagList.add(otherTag);

		otherTag = new OtherTag();
		otherTag.setId(new Long(4));
		otherTag.setName("New Value");
		otherTagList.add(otherTag);

		return otherTagList;
	}

	/**
	 * @return OtherTagDTO
	 */
	public OtherTagDTO getOtherTagDTO() {
		OtherTagDTO otherTagDTO = new OtherTagDTO();
		otherTagDTO.setId(1L);
		otherTagDTO.setName("name 1");

		return otherTagDTO;
	}

	/**
	 * @return OtherTagDTO list
	 */
	public List<OtherTagDTO> getOtherTagDTOList() {
		List<OtherTagDTO> otherTagDTOList = new ArrayList<OtherTagDTO>();

		OtherTagDTO otherTagDTO = new OtherTagDTO();
		otherTagDTO.setId(new Long(1));
		otherTagDTO.setName("Tag 1");
		otherTagDTOList.add(otherTagDTO);

		otherTagDTO = new OtherTagDTO();
		otherTagDTO.setId(new Long(2));
		otherTagDTO.setName("Tag 2");
		otherTagDTOList.add(otherTagDTO);

		otherTagDTO = new OtherTagDTO();
		otherTagDTO.setId(new Long(3));
		otherTagDTO.setName("New Tag");
		otherTagDTOList.add(otherTagDTO);

		otherTagDTO = new OtherTagDTO();
		otherTagDTO.setId(new Long(4));
		otherTagDTO.setName("New Value");
		otherTagDTOList.add(otherTagDTO);

		return otherTagDTOList;
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Test case for createOtherTag
	 *
	 * @throws Exception
	 */

	@Test
	public void test_createOtherTag_success() throws Exception {
		OtherTag otherTag = getOtherTag();
		OtherTagDTO otherTagDTO = getOtherTagDTO();

		when(otherTagModelMapper.map(otherTagDTO, OtherTag.class)).thenReturn(otherTag);
		when(otherTagService.createOtherTag(otherTag)).thenReturn(otherTag);
		when(otherTagModelMapper.map(otherTag, OtherTagDTO.class)).thenReturn(otherTagDTO);

		mockMvc.perform(post("/otherTags").contentType(MediaType.APPLICATION_JSON).content(asJsonString(otherTagDTO)))
				.andExpect(status().isCreated()).andExpect(header().string("Location", containsString("/otherTags/")));
		verify(otherTagService, times(1)).createOtherTag(otherTag);
		verifyNoMoreInteractions(otherTagService);
	}

	/**
	 * Test case for deleteOtherTagById
	 *
	 * @throws Exception
	 */

	@Test
	public void test_deleteOtherTagById_success() throws Exception {
		OtherTag otherTag = getOtherTag();
		doNothing().when(otherTagService).deleteOtherTag(otherTag.getId());
		mockMvc.perform(delete("/otherTags/{id}", otherTag.getId())).andExpect(status().isNoContent());
		verify(otherTagService, times(1)).deleteOtherTag(otherTag.getId());
	}

	/**
	 * Test case for findAllOtherTags by search Criteria
	 *
	 * @throws Exception
	 */
	@Test
	public void test_findAllOtherTags_success() throws Exception {
		List<OtherTag> otherTagList = getOtherTagList();
		List<OtherTagDTO> otherTagDTOList = getOtherTagDTOList();
		when(otherTagService.findAllOtherTags()).thenReturn(otherTagList);
		when(otherTagModelMapper.mapToOtherTagDTOs(otherTagList)).thenReturn(otherTagDTOList);
		mockMvc.perform(get("/otherTags")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", hasSize(4))).andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("Tag 1"))).andExpect(jsonPath("$[3].id", is(4)))
				.andExpect(jsonPath("$[3].name", is("New Value")));
		verify(otherTagService, times(1)).findAllOtherTags();
		verifyNoMoreInteractions(otherTagService);

	}

	/**
	 * Test case for findAllOtherTags by name
	 *
	 * @throws Exception
	 */
	@Test
	@Ignore
	public void test_findAllOtherTags_byName_success() throws Exception {
		List<OtherTag> otherTagList = getOtherTagList();
		List<OtherTagDTO> otherTagDTOList = getOtherTagDTOList();
		String name = "Tag";
		when(otherTagService.findAllOtherTagsByName(name)).thenReturn(otherTagList);
		when(otherTagModelMapper.mapToOtherTagDTOs(otherTagList)).thenReturn(otherTagDTOList);
		mockMvc.perform(get("/otherTags?name="+ name)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", hasSize(4))).andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("Tag 1")));
		verify(otherTagService, times(1)).findAllOtherTagsByName(name);
		verifyNoMoreInteractions(otherTagService);
	}

	/**
	 * Test case for updateOtherTag success
	 *
	 * @throws Exception
	 */
	@Test
	public void test_updateOtherTag_success() throws Exception {
		OtherTag otherTag = getOtherTag();
		OtherTagDTO otherTagDTO = getOtherTagDTO();

		when(otherTagModelMapper.map(otherTagDTO, OtherTag.class)).thenReturn(otherTag);
		when(otherTagService.updateOtherTag(otherTag)).thenReturn(otherTag);

		when(otherTagModelMapper.map(otherTag, OtherTagDTO.class)).thenReturn(otherTagDTO);

		mockMvc.perform(put("/otherTags").requestAttr("otherTagDTO", otherTagDTO)
				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(otherTagDTO))).andExpect(status().isOk());
		verify(otherTagService, times(1)).updateOtherTag(otherTag);
	}

	/**
	 * Test case for updateOtherTag - Tag Not Found
	 *
	 * @throws Exception
	 */
	@Test(expected = Exception.class)
	public void test_updateOtherTag_notFound() throws Exception {
		OtherTag otherTag = getOtherTag();
		OtherTagDTO otherTagDTO = getOtherTagDTO();
		when(otherTagModelMapper.map(otherTagDTO, OtherTag.class)).thenReturn(otherTag);
		when(otherTagService.updateOtherTag(otherTag)).thenThrow(new NotFoundException());

		when(otherTagModelMapper.map(otherTag, OtherTagDTO.class)).thenReturn(otherTagDTO);

		mockMvc.perform(put("/otherTags").requestAttr("otherTagDTO", otherTagDTO)
				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(otherTagDTO))).andDo(print())
				.andExpect(status().isNotFound());
		verify(otherTagService, times(1)).updateOtherTag(otherTag);

	}

}
