/**
 * 
 */
package com.sothebys.propertydb.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.sothebys.propertydb.model.Orientation;
import com.sothebys.propertydb.model.Scale;
import com.sothebys.propertydb.service.AutomatedTagService;

/**
 * @author aneesha.l
 *
 */
public class AutomatedTagControllerTest {

	@InjectMocks
	private AutomatedTagController automatedTagController;

	@Mock
	private AutomatedTagService automatedTagService;

	@InjectMocks
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	private MockMvc mockMvc;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		mockMvc = MockMvcBuilders.standaloneSetup(automatedTagController)
				.setMessageConverters(new MappingJackson2HttpMessageConverter())
				.setCustomArgumentResolvers(pageableArgumentResolver).build();

	}

	/**
	 * Test case for get All Orientations
	 *
	 */
	@Test
	public void test_getAllOrientations() throws Exception {
		when(automatedTagService.getAllOrientation()).thenReturn(Orientation.values());
		mockMvc.perform(get("/automatedTags/orientations")).andExpect(status().isOk())
		.andExpect(content().string(containsString("VERTICAL")));
		verify(automatedTagService, times(1)).getAllOrientation();
		verifyNoMoreInteractions(automatedTagService);

	}
	
	/**
	 * Test case for get All Scales
	 *
	 */
	@Test
	public void test_getAllScales() throws Exception {
		when(automatedTagService.getAllScales()).thenReturn(Scale.values());
		mockMvc.perform(get("/automatedTags/scales")).andExpect(status().isOk())
		.andExpect(content().string(containsString("M")));
		verify(automatedTagService, times(1)).getAllScales();
		verifyNoMoreInteractions(automatedTagService);

	}

}
