package com.sothebys.propertydb.util;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.sothebys.propertydb.DummyModelHelper;
import com.sothebys.propertydb.constants.PropertyServiceConstants;
import com.sothebys.propertydb.dto.PropertyDTO;

@RunWith(MockitoJUnitRunner.class)
public class ExportDataTest {
  
  private static final String CSV_DELIMITER = ",";
  private PropertyDTO property;
  @InjectMocks
  private ExportData exportData;
  
  private String tmpPath;
  private String fileName;
  
  
  @Before
  public void setUp() {
    property = DummyModelHelper.createPropertyDTO();
    fileName = "property-file-test.csv";
    tmpPath = System.getProperty(PropertyServiceConstants.TEMP_PATH);
    if (!tmpPath.endsWith(File.separator)) {
      tmpPath = tmpPath + File.separator;
    }
  }
  
  @Test
  public void catalogueRaisoneesValuesTestByPipe() {
    List<PropertyDTO> propertyDtos = Collections.singletonList(property);
    File exportCSVDataFile = null;
    try {
      exportData.buildPropertyCsvDocument(propertyDtos, fileName);
      exportCSVDataFile = new File(new File(tmpPath), fileName);
      BufferedReader reader = new BufferedReader(new FileReader(exportCSVDataFile));
      reader.readLine();
      String line = "";
      while ((line = reader.readLine()) != null) {
        String[] rowValues = line.split(CSV_DELIMITER);
        
        assertEquals("\"vol1||vol3\"", rowValues[6]);
        assertEquals("\"num1||num3\"", rowValues[7]);
      }
      reader.close();
      exportCSVDataFile.delete();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
