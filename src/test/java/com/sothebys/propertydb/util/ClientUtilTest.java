package com.sothebys.propertydb.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.sothebys.propertydb.search.ClientSearchInputDTO;
import com.sothebys.propertydb.search.ClientUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author jsilva.
 */
@RunWith(MockitoJUnitRunner.class)
public class ClientUtilTest {

  private static final String CLIENT_CITY = "NYC";
  private static final String CLIENT_STATE = "New York";
  private static final String CLIENT_COUNTRY = "United States";
  private static final String CLIENT_INDIVIDUAL_FIRST_NAME = "Jhon";
  private static final String CLIENT_INDIVIDUAL_LAST_NAME = "Doe";
  private static final String CLIENT_KEY_CLIENT_MANAGER_EXTERNAL_ID = "EOS-12345678";
  private static final String CLIENT_BIRTH_DATE = "06-12-1986";
  private static final int CLIENT_LEVEL = 10;
  private ClientSearchInputDTO inputTest;

  @Before
  public void setup() {
    arrangeInputDTO();
  }

  private void arrangeInputDTO() {
    inputTest = new ClientSearchInputDTO();
    inputTest.setClientCity(CLIENT_CITY);
    inputTest.setClientState(CLIENT_STATE);
    inputTest.setClientCountry(CLIENT_COUNTRY);
    inputTest.setClientIndividualFirstName(CLIENT_INDIVIDUAL_FIRST_NAME);
    inputTest.setClientIndividualLastName(CLIENT_INDIVIDUAL_LAST_NAME);
    inputTest.setClientKeyClientManagerExternalId(CLIENT_KEY_CLIENT_MANAGER_EXTERNAL_ID);
    inputTest.setClientLevel(CLIENT_LEVEL);
    Date birthDate = new Date();
    try {
      birthDate = new SimpleDateFormat("MM-dd-yyyy").parse(CLIENT_BIRTH_DATE);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    inputTest.setClientIndividualBirthDate(birthDate);
  }

  @Test
  public void test_ValidateQueryParamMapBuilder() {
    final MultiValueMap<String, String> map = ClientUtil
        .validateClientSearchInputDTO(inputTest);
    assertNotNull(map);
    assertEquals(map.get("city"), Collections.singletonList(CLIENT_CITY));
    assertEquals(map.get("state"), Collections.singletonList(CLIENT_STATE));
    assertEquals(map.get("country"), Collections.singletonList(CLIENT_COUNTRY));
  }


  @Test
  public void test_ClientSearchInputUrlBuilder() {
    final MultiValueMap<String, String> clientSearchQueryParams = ClientUtil
        .validateClientSearchInputDTO(inputTest);
    String urlQueryParams = UriComponentsBuilder
        .fromHttpUrl("https://sothebys.com/property-service/clients?projection=idOnly&size=1000")
        .queryParams(clientSearchQueryParams).toUriString();
    assertTrue(urlQueryParams.contains("&city=NYC"));
    assertTrue(urlQueryParams.contains("&state=New%20York"));
    assertTrue(urlQueryParams.contains("&country=United%20States"));
    assertTrue(urlQueryParams
        .contains("&individual.firstName=" + CLIENT_INDIVIDUAL_FIRST_NAME));
    assertTrue(urlQueryParams
        .contains("&individual.lastName=" + CLIENT_INDIVIDUAL_LAST_NAME));
    assertTrue(urlQueryParams.contains("individual.birthDate=" + new SimpleDateFormat("MM-dd-yyyy")
        .format(inputTest.getClientIndividualBirthDate())));
  }

  @Test
  public void test_EmptyClientSearchInputUrlBuilder() {
    assertEquals(ClientUtil.validateClientSearchInputDTO(getEmptyClientSearchInputDTO()),
        new LinkedMultiValueMap<>());
  }

  private ClientSearchInputDTO getEmptyClientSearchInputDTO() {
    return new ClientSearchInputDTO("", "", "", "", "", "", null, "", "", "", "", null, "", null,
        "", "", "", "", "");
  }
}
