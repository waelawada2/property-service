package com.sothebys.propertydb.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Created by wael.awada on 3/9/17.
 */
public class AlphaNumericIdGeneratorTest {

  @Test
  public void testAlphaNumericGenerator() {
    String alphaNumbericId = AlphaNumericIdGenerator.getAlphaNumbericId(809234809L, "OO-");
    assertEquals(11, alphaNumbericId.length());
  }

}
