package com.sothebys.propertydb.functional;


import static io.restassured.RestAssured.config;
import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.sothebys.propertydb.dto.KeyClientManagerDto;
import com.sothebys.propertydb.functional.utils.JsonHelper;
import io.restassured.RestAssured;
import io.restassured.config.LogConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseBodyData;
import io.restassured.specification.RequestSpecification;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@DirtiesContext
public class ApiTestBase {

  @LocalServerPort
  private int port;

  protected KeyClientManagerDto dummyKeyClientManagerDto;
  protected KeyClientManagerDto secondDummyKeyClientManagerDto;

  protected String baseUrl;
  protected String accessToken;


  public String getAccessToken() {

    RequestSpecification requestSpec = RestAssured.with();

    ResponseBodyData response = requestSpec.given().baseUri("https://keycloakqa.aws.sothebys.com")
        .port(443).contentType(ContentType.URLENC).formParam("grant_type", "password")
        .formParam("username", "testuser@sothebys.com").formParam("password", "test1234")
        .formParam("client_id", "property-database-ui").formParam("userid", "testuser")
        .formParam("shareduserid", "savelistshareduser")
        .post("/auth/realms/Core Applications/protocol/openid-connect/token").getBody();

    String responseMsg = response.asString();

    Assert.assertTrue("Missing access token", responseMsg.contains("access_token"));

    DocumentContext doc = JsonPath.parse(responseMsg);
    String accessToken = doc.read("access_token");

    System.out.println(">> doc.read access_token = " + accessToken);

    Assert.assertNotNull(accessToken);

    return accessToken;
  }

  @BeforeClass
  public static void configureRestAssured() {
    config = config().logConfig(new LogConfig());
  }

  @Before
  public void setupBaseUrl() {

    String basePath = System.getProperty("server.base");
    if (StringUtils.isBlank(basePath)) {
      basePath = "/property-service/v1";
    }


    String baseHost = System.getProperty("server.host");
    if (StringUtils.isBlank(baseHost)) {
      baseHost = "http://localhost";
    }

    this.baseUrl = baseHost + ":" + this.port + basePath;
  }


  protected RequestSpecification getRequestSpecification() {
    RequestSpecification requestSpec = RestAssured.with();
    return requestSpec.baseUri(this.baseUrl).given()
        .header("Authorization", "Bearer " + this.accessToken).contentType(ContentType.JSON);

  }


  private void setupKeyClientManagerDependency(KeyClientManagerDto dto) throws IOException {


    KeyClientManagerDto dummyKCM = dto;
    String kcmJson = null;
    try {
      kcmJson = JsonHelper.convertToJson(dummyKCM);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }

    Assert.assertNotNull("Dummy Key Client Manager Json value shouldn't be null", kcmJson);
    Response responseKCMPost = getRequestSpecification().body(kcmJson)
        .post("auth/realms/Core " + "Applications/protocol/openid-connect/token");

    if (responseKCMPost.getStatusCode() == 201) {

      dummyKeyClientManagerDto =
          JsonHelper.convertToObject(responseKCMPost.asString(), KeyClientManagerDto.class);

      Assert.assertNotNull("Failed to create Dummy Key Client Manager", dummyKeyClientManagerDto);
      Assert.assertEquals("Failed to create Dummy Key Client Manager according to Request",
          dummyKCM, dummyKeyClientManagerDto);
    } else {
      dummyKeyClientManagerDto = dto;
    }
  }



}
