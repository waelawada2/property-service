package com.sothebys.propertydb.functional;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertNotNull;

import com.sothebys.propertydb.dto.request.SaveAllPropertiesRequestDto;
import com.sothebys.propertydb.search.PropertySearchInputDTO;
import java.io.IOException;
import java.util.Collections;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.sothebys.propertydb.dto.request.SaveAllPropertiesFromSavelistRequestDto;
import com.sothebys.propertydb.functional.utils.JsonHelper;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

@ActiveProfiles("test")
@DirtiesContext
public class SavedListApiTest extends ApiTestBase {


  private String createdSaveListId = null;
  private String createdSaveListId2 = null;


  @Before
  public void setup() throws IOException {
    this.accessToken = getAccessToken();

  }

  @Test
  public void createSaveListTest() {

    RequestSpecification requestSpec = getRequestSpecification();

    String expectedLocationUrl = this.baseUrl + "/users/testuser/lists/";



    Response response = requestSpec.when()
        .body("{\n" + "   \"date\": \"2017-07-14T16:19:48.596Z\",\n"
            + "  \"name\": \"srinivas updated save list\",\n" + "  \"notes\": \"sreenotes\",\n"
            + "  \"openSaveList\": true, \n" + "  \"propertyIds\": [],\n"
            + " \"sharedUsersList\": []\n" + "}")
        .post("/users/testuser/lists").then().assertThat().statusCode(201)
        .body("id", notNullValue())
        .header("Location", responseData -> equalTo(expectedLocationUrl + responseData.path("id")))
        .extract().response();

    this.createdSaveListId = response.path("id");

    Assert.assertTrue("Response Json is valid.",
        JsonHelper.isValidJSON(response.getBody().asString()));

  }


  private void createSaveListDependencies() {

    RequestSpecification requestSpec = getRequestSpecification();

    Response response = requestSpec.when()
        .body("{\n" + "   \"date\": \"2017-07-14T16:19:48.596Z\",\n"
            + "  \"name\": \"srinivas updated save list 1\",\n" + "  \"notes\": \"sreenotes\",\n"
            + "  \"openSaveList\": true, \n" + "  \"propertyIds\": [],\n"
            + " \"sharedUsersList\": []\n" + "}")
        .post("/users/testuser/lists").then().assertThat().statusCode(201)
        .body("id", notNullValue()).extract().response();

    this.createdSaveListId = response.path("id");


    response = requestSpec.when()
        .body("{\n" + "   \"date\": \"2017-07-14T16:19:48.596Z\",\n"
            + "  \"name\": \"srinivas updated save list 2\",\n" + "  \"notes\": \"sreenotes\",\n"
            + "  \"openSaveList\": true, \n" + "  \"propertyIds\": [],\n"
            + " \"sharedUsersList\": []\n" + "}")
        .post("/users/testuser/lists").then().assertThat().statusCode(201)
        .body("id", notNullValue()).extract().response();

    this.createdSaveListId2 = response.path("id");
  }


  @Test
  public void addPropertiesToSaveListFromOtherSaveListTest() throws JsonProcessingException {
    System.out.println(">> Executing test: 'Add Properties to Save List from other Save List'.");

    if (this.createdSaveListId == null || this.createdSaveListId2 == null) {
      this.createSaveListDependencies();
    }

    assertNotNull(this.createdSaveListId);
    assertNotNull(this.createdSaveListId2);

    RequestSpecification requestSpec = getRequestSpecification();

    SaveAllPropertiesFromSavelistRequestDto requestDto =
        new SaveAllPropertiesFromSavelistRequestDto();
    requestDto.setFromSavedListId(this.createdSaveListId);
    requestDto.setIgnoredIds(Collections.emptyList());

    Response response =
        requestSpec
            .when()
            .log().all()
            .body(JsonHelper.convertToJson(requestDto))
            .post("/users/testuser/lists/" + this
                .createdSaveListId2+"/properties?fromSavedList=true").then()
            .assertThat()
            .log().all()
            .statusCode(200)
            .body("totalFound", notNullValue())
            .body("totalInserted", notNullValue())
            .extract().response();


    System.out
        .println(">> Successfully executed 'Add Properties to Save List from other Save List'.");
  }


  @Test
  public void addAllPropertiesToSaveList() throws JsonProcessingException {
    System.out.println(">> Executing test: 'Add Properties to Save List from other Save List'.");

    if (this.createdSaveListId == null || this.createdSaveListId2 == null) {
      this.createSaveListDependencies();
    }

    assertNotNull(this.createdSaveListId);


    RequestSpecification requestSpec = getRequestSpecification();

    SaveAllPropertiesRequestDto requestDto = new SaveAllPropertiesRequestDto();
    requestDto.setSearchCriteria(new PropertySearchInputDTO());
    requestDto.setIgnoredIds(Collections.emptyList());
    requestDto.getSearchCriteria().setAuthentication(true);


    Response response =
        requestSpec
            .when()
            .log().all()
            .body(JsonHelper.convertToJson(requestDto))
            .post("/users/testuser/lists/" + this
                .createdSaveListId+"/properties?updateAll=true").then()
            .assertThat()
            .log().all()
            .statusCode(200)
            .body("totalFound", notNullValue())
            .body("totalInserted", notNullValue())
            .extract().response();


    System.out
        .println(">> Successfully executed 'Add Properties to Save List from other Save List'.");
  }
}
