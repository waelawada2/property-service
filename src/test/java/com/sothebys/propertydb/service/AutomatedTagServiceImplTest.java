package com.sothebys.propertydb.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.sothebys.propertydb.model.Orientation;
import com.sothebys.propertydb.model.Scale;

@RunWith(MockitoJUnitRunner.class)
public class AutomatedTagServiceImplTest {

	@InjectMocks
	private AutomatedTagServiceImpl automatedTagService;

	@Test
	public void getAllOrientation() {
		Orientation[] response = automatedTagService.getAllOrientation();
		assertEquals(3, response.length);
	}
	
	@Test
	public void getAllScales() {
		Scale[] response = automatedTagService.getAllScales();
		assertEquals(7, response.length);
	}


}