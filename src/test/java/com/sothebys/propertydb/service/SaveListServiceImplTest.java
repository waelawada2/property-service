package com.sothebys.propertydb.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.sothebys.propertydb.DummyModelHelper;
import com.sothebys.propertydb.exception.ForbiddenException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.AddPropertiesToSavelistResult;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.SaveList;
import com.sothebys.propertydb.model.User;
import com.sothebys.propertydb.repository.PropertyRepository;
import com.sothebys.propertydb.repository.SaveListRepository;
import com.sothebys.propertydb.repository.UserRepository;
import com.sothebys.propertydb.search.PropertySpecification;
import com.sothebys.propertydb.search.SearchCriteria;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.jpa.domain.Specification;

/**
 * Created by sankeerth on 1/16/2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class SaveListServiceImplTest {

  @InjectMocks
  private SaveListServiceImpl saveListService;

  @Mock
  private SaveListRepository saveListRepository;

  @Mock
  private UserRepository userRepository;

  @Mock
  private PropertyRepository propertyRepository;

  private User user;

  private User sharedUser;

  private SaveList saveList;

  private SaveList sharedSaveList;

  private Property property;

  private Specification<Property> specification;

  private List<String> propertyIds;
  private List<String> ignoredIds;

  /**
   * Initializes the model objects
   */
  @Before
  public void setUp() {
    user = DummyModelHelper.createUserModel();
    sharedUser = DummyModelHelper.createSharedUserModel();
    saveList = DummyModelHelper.createSaveListModel();
    sharedSaveList = DummyModelHelper.createSaveListModelWithSharedUser();
    propertyIds = Collections.singletonList("OO-123");
    specification = new PropertySpecification(new SearchCriteria());

    ignoredIds = Collections.emptyList();

    property = DummyModelHelper.createPropertyModel();

  }


  @Test
  public void addAllPropertiesToSaveList() {

    when(saveListRepository.findByUserAndExternalId(user, "SL-00000002")).thenReturn
        (saveList);
    when(userRepository.findByUserName(user.getUserName())).thenReturn(user);
    when(propertyRepository.findAll(specification)).thenReturn(Arrays.asList(property));
    doNothing().when(saveListRepository).addPropertyToSaveList(saveList.getId(), property.getId());
    saveListService.addPropertiesToSaveList("SL-00000002", specification, user, ignoredIds);

    verify(saveListRepository, times(1)).addPropertiesToSaveList(eq(saveList.getId()),
        any(List.class));

  }

  @Test(expected = NotFoundException.class)
  public void addAllPropertiesToSaveListNoSavelistFound() {

    when(saveListRepository.findByExternalId("SL-00000002")).thenReturn(null);
    when(userRepository.findByUserName(user.getUserName())).thenReturn(user);
    when(propertyRepository.findByExternalId("00000002")).thenReturn(property);
    doNothing().when(saveListRepository).addPropertyToSaveList(saveList.getId(), property.getId());
    saveListService.addPropertiesToSaveList("SL-00000002", specification, user, ignoredIds);


  }

  /**
   * Case for when requested user is not a shared user, user is not in savelist and opensavelist is flase
   */
  @Test(expected = ForbiddenException.class)
  public void testAddPropertyToSaveListTestUserIsNotValid() {

    List<User> sharedUsers = new ArrayList<>();
    sharedUsers.add(sharedUser);
    sharedSaveList.setSharedUsers(sharedUsers);
    sharedSaveList.setOpenSaveList(false);

    saveListService.validateUserPermission(user, sharedSaveList, "SL-00009T83");

  }

  @Test
  public void findSaveListByUserNameAndExternalId() {
    when(userRepository.findByUserName("ram")).thenReturn(user);
    when(saveListRepository.findByUserAndExternalId(user, "SL-00000002")).thenReturn(saveList);

    SaveList response = saveListService.findSaveListByUserNameAndExternalId("ram", "SL-00000002");

    assertEquals("SL-00000002", response.getExternalId());
  }

  @Test
  public void findSaveListByUserNameAndExternalIdShared() {
    when(userRepository.findByUserName("ram")).thenReturn(user);
    when(saveListRepository.findByUserAndExternalId(user, "SL-00000002")).thenReturn(null);
    when(saveListRepository.findByExternalIdAndSharedUsers("SL-00000002", user))
        .thenReturn(saveList);

    SaveList response = saveListService.findSaveListByUserNameAndExternalId("ram", "SL-00000002");

    assertEquals("SL-00000002", response.getExternalId());
  }

  @Test(expected = NotFoundException.class)
  public void findSaveListByUserNameAndExternalIdNotFound() {
    when(userRepository.findByUserName("ram")).thenReturn(user);
    when(saveListRepository.findByUserAndExternalId(user, "SL-00000002")).thenReturn(null);
    when(saveListRepository.findByExternalIdAndSharedUsers("SL-00000002", user)).thenReturn(null);

    saveListService.findSaveListByUserNameAndExternalId("ram", "SL-00000002");
  }

  @Test
  public void addPropertiesToSaveList() {
    when(saveListRepository.findOne(1L)).thenReturn(saveList);
    doNothing().when(saveListRepository).refresh(saveList);
    when(saveListRepository.addPropertiesToSaveList(saveList.getId(), propertyIds)).thenReturn(1);
    AddPropertiesToSavelistResult response = saveListService
        .addPropertiesToSaveList(saveList.getId(), propertyIds);

    assertEquals("SL-00000002", response.getSaveList().getExternalId());
    assertEquals(1, response.getTotalFound());
    assertEquals(1, response.getTotalInserted());
    verify(saveListRepository).addPropertiesToSaveList(1L, propertyIds);
  }

  @Test
  public void addPropertiesToSaveListEmpty() {
    when(saveListRepository.findOne(1L)).thenReturn(saveList);
    doNothing().when(saveListRepository).refresh(saveList);

    AddPropertiesToSavelistResult response = saveListService
        .addPropertiesToSaveList(1L, Collections.emptyList());

    assertEquals("SL-00000002", response.getSaveList().getExternalId());
    assertEquals(0, response.getTotalFound());
    assertEquals(0, response.getTotalInserted());
    verify(saveListRepository, never()).addPropertiesToSaveList(1L, Collections.emptyList());
  }

}
