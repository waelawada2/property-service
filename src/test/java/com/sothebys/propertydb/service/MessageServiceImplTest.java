package com.sothebys.propertydb.service;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sothebys.propertydb.dto.SnsMessageDto;
import com.sothebys.propertydb.dto.SnsMessageOperation;
import com.sothebys.propertydb.exception.MessageServiceException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceImplTest {

  @InjectMocks
  private MessageServiceImpl messageService;
  
  @Mock
  private ObjectMapper objectMapper;

  @Mock
  private AmazonSNSClient snsClient;


  private PublishResult publishResult;
  private SnsMessageDto messageDto;

  @Before
  public void setup() {
    publishResult = new PublishResult();
    publishResult.setMessageId("1");
    messageDto = new SnsMessageDto();
    messageDto.setId("OO-123456");
  }

  @Test(expected = MessageServiceException.class)
  public void testSendMessageFailed() {
    messageService.sendSnsMessage(messageDto, SnsMessageOperation.CREATE);
    Mockito.verify(snsClient, times(1))
        .publish(any(PublishRequest.class));
  }


  @Test
  public void testSendMessage() {
    Mockito.when(snsClient.publish(any(PublishRequest.class))).thenReturn(this.publishResult);
    messageService.sendSnsMessage(messageDto, SnsMessageOperation.CREATE);
    Mockito.verify(snsClient, times(1))
        .publish(any(PublishRequest.class));
  }
}