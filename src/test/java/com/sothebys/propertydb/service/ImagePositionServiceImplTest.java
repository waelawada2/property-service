package com.sothebys.propertydb.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;

import com.sothebys.propertydb.DummyModelHelper;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ImagePosition;
import com.sothebys.propertydb.repository.ImagePositionRepository;

@RunWith(MockitoJUnitRunner.class)
public class ImagePositionServiceImplTest {

  @InjectMocks
  private ImagePositionServiceImpl imagePositionService;

  @Mock
  private ImagePositionRepository imagePositionRepository;

  private ImagePosition imagePosition;

	private List<ImagePosition> imagePositionList;

  @Before
  public void setUp() {
    imagePosition = DummyModelHelper.createImagePositionModel();
		imagePositionList = DummyModelHelper.imagePositionList();
  }

  @Test
  public void findByName() {
    when(imagePositionRepository.findByName("Standing")).thenReturn(imagePosition);

    ImagePosition response = imagePositionService.findByName("Standing");

    assertEquals(new Long(123), response.getId());
    assertEquals("Standing", response.getName());
  }

  @Test(expected = NotFoundException.class)
  public void findByNameNotFound() {
    when(imagePositionRepository.findByName("Standing")).thenReturn(null);
    imagePositionService.findByName("Standing");
  }

	@Test
	public void findAllImagePositions() {
		when(imagePositionRepository.findAllByOrderByNameAsc()).thenReturn(imagePositionList);

		List<ImagePosition> response = imagePositionService.findAllImagePositions();

		assertEquals(2, response.size());
		verify(imagePositionRepository, times(1)).findAllByOrderByNameAsc();
	}

	@Test
	public void updateImagePositionById_success() {
		when(imagePositionRepository.findOne(Mockito.anyLong())).thenReturn(imagePosition);
		when(imagePositionRepository.save(imagePosition)).thenReturn(imagePosition);

		ImagePosition response = imagePositionService.updateImagePositionById(imagePosition.getId(), imagePosition);

		assertEquals(new Long(123), response.getId());
		assertEquals("Standing", response.getName());
		verify(imagePositionRepository, times(1)).findOne(Mockito.anyLong());
		verify(imagePositionRepository, times(1)).save(imagePosition);
	}
	
	@Test(expected = CannotPerformOperationException.class)
	public void updateImagePositionById_failure_1() {
		when(imagePositionRepository.findOne(Mockito.anyLong())).thenReturn(imagePosition);
		when(imagePositionRepository.save(imagePosition)).thenReturn(imagePosition);

		imagePositionService.updateImagePositionById(111L, imagePosition);

		verify(imagePositionRepository, times(1)).findOne(Mockito.anyLong());
	}

	@Test(expected = CannotPerformOperationException.class)
	public void updateImagePositionById_failure_2() {
		when(imagePositionRepository.findOne(Mockito.anyLong())).thenReturn(null);

		imagePositionService.updateImagePositionById(imagePosition.getId(), imagePosition);

		verify(imagePositionRepository, times(1)).findOne(Mockito.anyLong());
	}
	
	@Test(expected = CannotPerformOperationException.class)
	public void updateImagePositionById_failure_3() {
		when(imagePositionRepository.findOne(Mockito.anyLong())).thenReturn(imagePosition);
		when(imagePositionRepository.save(imagePosition)).thenReturn(imagePosition);

		imagePositionService.updateImagePositionById(null, imagePosition);

		verify(imagePositionRepository, times(1)).findOne(Mockito.anyLong());
	}

	@Test
	public void deleteImagePositionById_success() {
		when(imagePositionRepository.findOne(Mockito.anyLong())).thenReturn(imagePosition);
		when(imagePositionRepository.findByProperty(Mockito.anyLong())).thenReturn(null);
		doNothing().when(imagePositionRepository).delete(Mockito.anyLong());

		imagePositionService.deleteImagePositionById(imagePosition.getId());

		verify(imagePositionRepository, times(1)).findOne(Mockito.anyLong());
		verify(imagePositionRepository, times(1)).findByProperty(Mockito.anyLong());
		verify(imagePositionRepository, times(1)).delete(Mockito.anyLong());

	}

	@Test(expected = NotFoundException.class)
	public void deleteImagePositionById_failure_1() {
		when(imagePositionRepository.findOne(Mockito.anyLong())).thenReturn(null);

		imagePositionService.deleteImagePositionById(imagePosition.getId());

		verify(imagePositionRepository, times(1)).findOne(Mockito.anyLong());

	}

	@Test(expected = NotFoundException.class)
	public void deleteImagePositionById_failure_2() {
		when(imagePositionRepository.findOne(Mockito.anyLong())).thenReturn(imagePosition);
		when(imagePositionRepository.findByProperty(Mockito.anyLong())).thenReturn(imagePosition);

		imagePositionService.deleteImagePositionById(imagePosition.getId());

		verify(imagePositionRepository, times(1)).findOne(Mockito.anyLong());
		verify(imagePositionRepository, times(1)).findByProperty(Mockito.anyLong());

	}

	@Test
	public void createImagePosition_success() {
		when(imagePositionRepository.save(imagePosition)).thenReturn(imagePosition);

		ImagePosition response = imagePositionService.createImagePosition(imagePosition);

		assertEquals(new Long(123), response.getId());
		assertEquals("Standing", response.getName());
		verify(imagePositionRepository, times(1)).save(imagePosition);

	}

	@Test(expected = CannotPerformOperationException.class)
	public void createImagePosition_failure() {
		doThrow(new DataIntegrityViolationException("ImagePosition name already exists.")).when(imagePositionRepository)
				.save(imagePosition);

		imagePositionService.createImagePosition(imagePosition);

		verify(imagePositionRepository, times(1)).save(imagePosition);

	}

}