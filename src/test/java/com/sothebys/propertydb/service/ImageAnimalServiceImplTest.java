package com.sothebys.propertydb.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.CannotAcquireLockException;

import com.sothebys.propertydb.DummyModelHelper;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ImageAnimal;
import com.sothebys.propertydb.repository.ImageAnimalRepository;

@RunWith(MockitoJUnitRunner.class)
public class ImageAnimalServiceImplTest {

	@InjectMocks
	private ImageAnimalServiceImpl imageAnimalService;

	@Mock
	private ImageAnimalRepository imageAnimalRepository;

	private ImageAnimal imageAnimal;

	@Before
	public void setUp() {
		imageAnimal = DummyModelHelper.createImageAnimalModel();
	}

	@Test
	public void findByName() {
		when(imageAnimalRepository.findByName("Lion")).thenReturn(imageAnimal);

		ImageAnimal response = imageAnimalService.findByName("Lion");

		assertEquals(new Long(123), response.getId());
		assertEquals("Lion", response.getName());
	}

	@Test(expected = NotFoundException.class)
	public void findByNameNotFound() {
		when(imageAnimalRepository.findByName("Lion")).thenReturn(null);
		imageAnimalService.findByName("Lion");
	}

	@Test
	public void findByNameCreateIfNotFound() {
		when(imageAnimalRepository.findByName("Lion")).thenReturn(imageAnimal);

		ImageAnimal response = imageAnimalService.findByNameCreateIfNotFound("Lion");

		assertEquals(new Long(123), response.getId());
		assertEquals("Lion", response.getName());
	}

	@Test
	public void findByNameCreateIfNotFoundNotFound() {
		ImageAnimal newImageAnimal = new ImageAnimal();
		newImageAnimal.setName("Lion");

		when(imageAnimalRepository.findByName("Lion")).thenReturn(null);
		when(imageAnimalRepository.save(newImageAnimal)).thenReturn(imageAnimal);

		ImageAnimal response = imageAnimalService.findByNameCreateIfNotFound("Lion");

		assertEquals(new Long(123), response.getId());
		assertEquals("Lion", response.getName());
	}

	@Test
	public void test_createImageAnimal_success() {
		ImageAnimal newImageAnimal = new ImageAnimal();
		newImageAnimal.setName("Lion");

		when(imageAnimalRepository.findAllByName(Mockito.anyString())).thenReturn(null);
		when(imageAnimalRepository.save(newImageAnimal)).thenReturn(imageAnimal);

		ImageAnimal response = imageAnimalService.createImageAnimal(newImageAnimal);

		assertEquals(new Long(123), response.getId());
		assertEquals("Lion", response.getName());
	}

	@Test
	public void test_createImageAnimal_success_2() {
		ImageAnimal newImageAnimal = new ImageAnimal();
		newImageAnimal.setName("Lion");
		List<ImageAnimal> imageAnimalList = new ArrayList<>();
		when(imageAnimalRepository.findAllByName(Mockito.anyString())).thenReturn(imageAnimalList);
		when(imageAnimalRepository.save(newImageAnimal)).thenReturn(imageAnimal);

		ImageAnimal response = imageAnimalService.createImageAnimal(newImageAnimal);

		assertEquals(new Long(123), response.getId());
		assertEquals("Lion", response.getName());
	}

	@Test(expected = Exception.class)
	public void test_createImageAnimal_failure() {
		ImageAnimal newImageAnimal = new ImageAnimal();
		newImageAnimal.setId(new Long(123));
		newImageAnimal.setName("Lion");
		List<ImageAnimal> imageAnimalList = new ArrayList<>();
		imageAnimalList.add(newImageAnimal);
		when(imageAnimalRepository.findAllByName(Mockito.anyString())).thenReturn(imageAnimalList);

		imageAnimalService.createImageAnimal(newImageAnimal);

	}

	@Test
	public void findAllImageAnimals() {
		List<ImageAnimal> imageAnimalList = new ArrayList<>();
		ImageAnimal newImageAnimal = new ImageAnimal();
		newImageAnimal.setId(new Long(123));
		newImageAnimal.setName("Lion");
		imageAnimalList.add(newImageAnimal);

		newImageAnimal = new ImageAnimal();
		newImageAnimal.setId(new Long(124));
		newImageAnimal.setName("Fox");
		imageAnimalList.add(newImageAnimal);
		when(imageAnimalRepository.findAllByOrderByNameAsc()).thenReturn(imageAnimalList);
		List<ImageAnimal> response = imageAnimalService.findAllImageAnimals();
		assertEquals(2, response.size());
	}

	@Test
	public void findAllImageAnimalsByName() {
		List<ImageAnimal> imageAnimalList = new ArrayList<ImageAnimal>();
		ImageAnimal newImageAnimal = new ImageAnimal();
		newImageAnimal.setId(123L);
		newImageAnimal.setName("Lion");
		imageAnimalList.add(newImageAnimal);

		newImageAnimal = new ImageAnimal();
		newImageAnimal.setId(124L);
		newImageAnimal.setName("Fox");
		imageAnimalList.add(newImageAnimal);
		when(imageAnimalService.findAllImageAnimalsByName(Mockito.anyString())).thenReturn(imageAnimalList);
		List<ImageAnimal> response = imageAnimalService.findAllImageAnimalsByName("Lion");
		assertEquals(2, response.size());
	}

	@Test
	public void deleteImageAnimal_success() {
		when(imageAnimalRepository.findById(Mockito.anyLong())).thenReturn(imageAnimal);
		doNothing().when(imageAnimalRepository).delete(Mockito.anyLong());
		imageAnimalService.deleteImageAnimal(123L);
		verify(imageAnimalRepository, times(1)).delete(123L);
	}

	@Test(expected = NotFoundException.class)
	public void deleteImageAnimal_failure_1() {

		when(imageAnimalRepository.findById(Mockito.anyLong())).thenReturn(null);
		// doNothing().when(imageAnimalRepository).delete(Mockito.anyLong());
		imageAnimalService.deleteImageAnimal(123L);
		verify(imageAnimalRepository, times(1)).delete(123L);
	}

	@Test(expected = CannotPerformOperationException.class)
	public void deleteImageAnimal_failure_2() {

		when(imageAnimalRepository.findById(Mockito.anyLong())).thenReturn(imageAnimal);
		doThrow(new CannotAcquireLockException("Lock couldn't be acquired on table, please try again later"))
				.when(imageAnimalRepository).delete(Mockito.anyLong());
		imageAnimalService.deleteImageAnimal(123L);
		verify(imageAnimalRepository, times(1)).delete(123L);
	}

	@Test
	public void updateImageAnimal_success() {
		when(imageAnimalRepository.findById(Mockito.anyLong())).thenReturn(imageAnimal);
		when(imageAnimalRepository.save(imageAnimal)).thenReturn(imageAnimal);
		ImageAnimal response = imageAnimalService.updateImageAnimal(imageAnimal);
		verify(imageAnimalRepository, times(1)).save(imageAnimal);
		assertEquals(new Long(123), response.getId());
		assertEquals("LION", response.getName());
	}

	@Test(expected = NotFoundException.class)
	public void updateImageAnimal_failure_1() {
		when(imageAnimalRepository.findById(Mockito.anyLong())).thenReturn(null);
		imageAnimalService.updateImageAnimal(imageAnimal);
		verify(imageAnimalRepository, times(1)).findById(Mockito.anyLong());
	}

	@Test(expected = CannotPerformOperationException.class)
	public void updateImageAnimal_failure_2() {
		when(imageAnimalRepository.findById(Mockito.anyLong())).thenReturn(imageAnimal);
		doThrow(new CannotAcquireLockException("Lock couldn't be acquired on table, please try again later"))
				.when(imageAnimalRepository).save(imageAnimal);
		imageAnimalService.updateImageAnimal(imageAnimal);
		verify(imageAnimalRepository, times(1)).findById(Mockito.anyLong());
		verify(imageAnimalRepository, times(1)).save(imageAnimal);
	}
}
