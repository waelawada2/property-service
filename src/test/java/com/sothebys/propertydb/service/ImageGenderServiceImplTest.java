package com.sothebys.propertydb.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.sothebys.propertydb.DummyModelHelper;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ImageGender;
import com.sothebys.propertydb.repository.ImageGenderRepository;

@RunWith(MockitoJUnitRunner.class)
public class ImageGenderServiceImplTest {

  @InjectMocks
  private ImageGenderServiceImpl imageGenderService;

  @Mock
  private ImageGenderRepository imageGenderRepository;

  private ImageGender imageGender;

  @Before
  public void setUp() {
    imageGender = DummyModelHelper.createImageGenderModel();
  }

  @Test
  public void findByName() {
    when(imageGenderRepository.findByName("Male")).thenReturn(imageGender);

    ImageGender response = imageGenderService.findByName("Male");

    assertEquals(new Long(123), response.getId());
    assertEquals("Male", response.getName());
  }

  @Test(expected = NotFoundException.class)
  public void findByNameNotFound() {
    when(imageGenderRepository.findByName("Male")).thenReturn(null);
    imageGenderService.findByName("Male");
  }
  
  @Test
  public void findAllImageGenders() {
	List<ImageGender> imageGenderList = new ArrayList<ImageGender>();
	ImageGender newImageGender = new ImageGender();
	newImageGender.setId(123L);
	newImageGender.setName("Male");
	imageGenderList.add(newImageGender);
    
	newImageGender = new ImageGender();
	newImageGender.setId(124L);
	newImageGender.setName("Female");
	imageGenderList.add(newImageGender);
    when(imageGenderRepository.findAllByOrderByNameAsc()).thenReturn(imageGenderList);
    List<ImageGender> response = imageGenderService.findAllImageGenders();
    assertEquals(2, response.size());
  }
}