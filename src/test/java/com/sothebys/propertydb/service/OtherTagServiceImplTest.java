package com.sothebys.propertydb.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.CannotAcquireLockException;

import com.sothebys.propertydb.DummyModelHelper;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.OtherTag;
import com.sothebys.propertydb.repository.OtherTagRepository;

@RunWith(MockitoJUnitRunner.class)
public class OtherTagServiceImplTest {

  @InjectMocks
  private OtherTagServiceImpl otherTagService;

  @Mock
  private OtherTagRepository otherTagRepository;

  private OtherTag otherTag;

	private List<OtherTag> otherTagList;

  @Before
  public void setUp() {
    otherTag = DummyModelHelper.createOtherTagModel();
		otherTagList = DummyModelHelper.otherTagList();
  }

  @Test
  public void findByName() {
    when(otherTagRepository.findByName("Tag")).thenReturn(otherTag);

    OtherTag response = otherTagService.findByName("Tag");

    assertEquals(new Long(123), response.getId());
    assertEquals("Tag", response.getName());
  }

  @Test(expected = NotFoundException.class)
  public void findByNameNotFound() {
    when(otherTagRepository.findByName("Tag")).thenReturn(null);
    otherTagService.findByName("Tag");
  }

  @Test
  public void findByNameCreateIfNotFound() {
    when(otherTagRepository.findByName("Tag")).thenReturn(otherTag);

    OtherTag response = otherTagService.findByNameCreateIfNotFound("Tag");

    assertEquals(new Long(123), response.getId());
    assertEquals("Tag", response.getName());
  }

  @Test
  public void findByNameCreateIfNotFoundNotFound() {
    OtherTag newOtherTag = new OtherTag();
    newOtherTag.setName("Tag");

    when(otherTagRepository.findByName("Tag")).thenReturn(null);
    when(otherTagRepository.save(newOtherTag)).thenReturn(otherTag);

    OtherTag response = otherTagService.findByNameCreateIfNotFound("Tag");

    assertEquals(new Long(123), response.getId());
    assertEquals("Tag", response.getName());
  }

	@Test
	public void createOtherTag_success_1() {

		when(otherTagRepository.findAllByName(Mockito.anyString())).thenReturn(null);
		when(otherTagRepository.save(otherTag)).thenReturn(otherTag);

		OtherTag response = otherTagService.createOtherTag(otherTag);

		verify(otherTagRepository, times(1)).findAllByName(Mockito.anyString());
		verify(otherTagRepository, times(1)).save(otherTag);
		assertEquals(new Long(123), response.getId());
		assertEquals("Tag", response.getName());
	}

	@Test
	public void createOtherTag_success_2() {
		List<OtherTag> searchOtherTag = new ArrayList<OtherTag>();
		when(otherTagRepository.findAllByName(Mockito.anyString())).thenReturn(searchOtherTag);
		when(otherTagRepository.save(otherTag)).thenReturn(otherTag);

		OtherTag response = otherTagService.createOtherTag(otherTag);

		verify(otherTagRepository, times(1)).findAllByName(Mockito.anyString());
		verify(otherTagRepository, times(1)).save(otherTag);
		assertEquals(new Long(123), response.getId());
		assertEquals("Tag", response.getName());
	}

	@Test(expected = CannotPerformOperationException.class)
	public void createOtherTag_failure() {

		when(otherTagRepository.findAllByName(Mockito.anyString())).thenReturn(otherTagList);
		otherTagService.createOtherTag(otherTag);
		verify(otherTagRepository, times(1)).findAllByName(Mockito.anyString());
	}

	@Test
	public void deleteOtherTag_success() {
		when(otherTagRepository.findById(Mockito.anyLong())).thenReturn(otherTag);
		doNothing().when(otherTagRepository).delete(Mockito.anyLong());
		otherTagService.deleteOtherTag(123L);
		verify(otherTagRepository, times(1)).findById(Mockito.anyLong());
		verify(otherTagRepository, times(1)).delete(Mockito.anyLong());
	}

	@Test(expected = NotFoundException.class)
	public void deleteOtherTag_failure_1() {
		when(otherTagRepository.findById(Mockito.anyLong())).thenReturn(null);
		doNothing().when(otherTagRepository).delete(Mockito.anyLong());
		otherTagService.deleteOtherTag(123L);
		verify(otherTagRepository, times(1)).findById(Mockito.anyLong());
	}

	@Test(expected = CannotPerformOperationException.class)
	public void deleteOtherTag_failure_2() {
		when(otherTagRepository.findById(Mockito.anyLong())).thenReturn(otherTag);
		doThrow(new CannotAcquireLockException("Lock couldn't be acquired on table, please try again later"))
				.when(otherTagRepository).delete(Mockito.anyLong());
		otherTagService.deleteOtherTag(123L);
		verify(otherTagRepository, times(1)).findById(Mockito.anyLong());
		verify(otherTagRepository, times(1)).delete(Mockito.anyLong());
	}

	@Test
	public void updateOtherTag_success() {

		when(otherTagRepository.findById(Mockito.anyLong())).thenReturn(otherTag);
		when(otherTagRepository.save(otherTag)).thenReturn(otherTag);

		OtherTag response = otherTagService.updateOtherTag(otherTag);

		verify(otherTagRepository, times(1)).findById(Mockito.anyLong());
		verify(otherTagRepository, times(1)).save(otherTag);
		assertEquals(new Long(123), response.getId());
		assertEquals("Tag", response.getName());
	}

	@Test(expected = NotFoundException.class)
	public void updateOtherTag_failure_1() {

		when(otherTagRepository.findById(Mockito.anyLong())).thenReturn(null);

		otherTagService.updateOtherTag(otherTag);

		verify(otherTagRepository, times(1)).findById(Mockito.anyLong());
	}

	@Test(expected = CannotPerformOperationException.class)
	public void updateOtherTag_failure_2() {

		when(otherTagRepository.findById(Mockito.anyLong())).thenReturn(otherTag);
		doThrow(new CannotAcquireLockException("Lock couldn't be acquired on table, please try again later"))
				.when(otherTagRepository).save(otherTag);
		otherTagService.updateOtherTag(otherTag);

		verify(otherTagRepository, times(1)).findById(Mockito.anyLong());
		verify(otherTagRepository, times(1)).save(otherTag);
	}

	@Test
	public void findAllOtherTags() {

		when(otherTagRepository.findAllByOrderByNameAsc()).thenReturn(otherTagList);

		List<OtherTag> response = otherTagService.findAllOtherTags();

		verify(otherTagRepository, times(1)).findAllByOrderByNameAsc();
		assertEquals(2, response.size());
}

	@Test
	public void findAllOtherTagsByName() {

		when(otherTagRepository.findByNameContaining(Mockito.anyString())).thenReturn(otherTagList);

		List<OtherTag> response = otherTagService.findAllOtherTagsByName("Tag");

		verify(otherTagRepository, times(1)).findByNameContaining(Mockito.anyString());
		assertEquals(2, response.size());
	}

}
