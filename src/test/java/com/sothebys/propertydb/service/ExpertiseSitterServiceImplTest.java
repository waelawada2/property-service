package com.sothebys.propertydb.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.dao.DataIntegrityViolationException;

import com.sothebys.propertydb.DummyModelHelper;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.model.ExpertiseSitter;
import com.sothebys.propertydb.repository.ExpertiseSitterRepository;

@RunWith(MockitoJUnitRunner.class)
public class ExpertiseSitterServiceImplTest {

  @InjectMocks
  private ExpertiseSitterServiceImpl expertiseSitterService;

  @Mock
  private ExpertiseSitterRepository expertiseSitterRepository;

  private ExpertiseSitter expertiseSitter;

	private List<ExpertiseSitter> expertiseSitterList;

  @Before
  public void setUp() {
    expertiseSitter = DummyModelHelper.createExpertiseSitterModel();
		expertiseSitterList = DummyModelHelper.expertiseSitterList();
  }

  @Test
	public void findByName_success_1() {
		when(expertiseSitterRepository.findByLastNameAndFirstNameIn(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(expertiseSitter);

    ExpertiseSitter response = expertiseSitterService.findByName("John Longlegs");

    assertEquals(new Long(123), response.getId());
    assertEquals("John", response.getFirstName());
    assertEquals("Longlegs", response.getLastName());
		verify(expertiseSitterRepository, times(1)).findByLastNameAndFirstNameIn(Mockito.anyString(),
				Mockito.anyString());
  }

	@Test
	public void findByName_success_2() {
		when(expertiseSitterRepository.findByLastNameAndFirstNameIn(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(expertiseSitter);

		ExpertiseSitter response = expertiseSitterService.findByName("John");

		assertEquals(new Long(123), response.getId());
		assertEquals("John", response.getFirstName());
		assertEquals("Longlegs", response.getLastName());
		verify(expertiseSitterRepository, times(1)).findByLastNameAndFirstNameIn(Mockito.anyString(),
				Mockito.anyString());
	}

	@Test
	public void findByNameCreateIfNotFound_success_1() {
		when(expertiseSitterRepository.findByLastNameAndFirstNameIn(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(null);
		when(expertiseSitterRepository.save(Mockito.any(ExpertiseSitter.class))).thenReturn(expertiseSitter);

    ExpertiseSitter response = expertiseSitterService.findByNameCreateIfNotFound("John Longlegs");

    assertEquals(new Long(123), response.getId());
    assertEquals("John", response.getFirstName());
    assertEquals("Longlegs", response.getLastName());
		verify(expertiseSitterRepository, times(1)).findByLastNameAndFirstNameIn(Mockito.anyString(),
				Mockito.anyString());
		verify(expertiseSitterRepository, times(1)).save(Mockito.any(ExpertiseSitter.class));
  }

  @Test
	public void findByNameCreateIfNotFound_success_2() {
		when(expertiseSitterRepository.findByLastNameAndFirstNameIn(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(null);
		when(expertiseSitterRepository.save(Mockito.any(ExpertiseSitter.class))).thenReturn(expertiseSitter);

		ExpertiseSitter response = expertiseSitterService.findByNameCreateIfNotFound("John");

		assertEquals(new Long(123), response.getId());
		assertEquals("John", response.getFirstName());
		assertEquals("Longlegs", response.getLastName());
		verify(expertiseSitterRepository, times(1)).findByLastNameAndFirstNameIn(Mockito.anyString(),
				Mockito.anyString());
		verify(expertiseSitterRepository, times(1)).save(Mockito.any(ExpertiseSitter.class));
	}

	@Test
	public void findByNameCreateIfNotFound_success_3() {
		when(expertiseSitterRepository.findByLastNameAndFirstNameIn(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(expertiseSitter);
		when(expertiseSitterRepository.save(Mockito.any(ExpertiseSitter.class))).thenReturn(expertiseSitter);

		ExpertiseSitter response = expertiseSitterService.findByNameCreateIfNotFound("John");

		assertEquals(new Long(123), response.getId());
		assertEquals("John", response.getFirstName());
		assertEquals("Longlegs", response.getLastName());
		verify(expertiseSitterRepository, times(1)).findByLastNameAndFirstNameIn(Mockito.anyString(),
				Mockito.anyString());
		verify(expertiseSitterRepository, times(0)).save(Mockito.any(ExpertiseSitter.class));
	}

	@Test
	public void createExpertiseSitter_success_1() {
		List<ExpertiseSitter> sitterList = new ArrayList<ExpertiseSitter>();
		when(expertiseSitterRepository.findDistinctExpertiseSitterByLastNameAndFirstName(Mockito.anyString(),
				Mockito.anyString())).thenReturn(sitterList);
		when(expertiseSitterRepository.save(Mockito.any(ExpertiseSitter.class))).thenReturn(expertiseSitter);

		ExpertiseSitter response = expertiseSitterService.createExpertiseSitter(expertiseSitter);

		assertEquals(new Long(123), response.getId());
		assertEquals("John", response.getFirstName());
		assertEquals("Longlegs", response.getLastName());
		verify(expertiseSitterRepository, times(1))
				.findDistinctExpertiseSitterByLastNameAndFirstName(Mockito.anyString(), Mockito.anyString());
		verify(expertiseSitterRepository, times(1)).save(Mockito.any(ExpertiseSitter.class));
	}

	@Test
	public void createExpertiseSitter_success_2() {
		List<ExpertiseSitter> sitterList = new ArrayList<ExpertiseSitter>();
		when(expertiseSitterRepository.findDistinctExpertiseSitterByLastNameAndFirstName(Mockito.anyString(),
				Mockito.anyString())).thenReturn(sitterList);
		when(expertiseSitterRepository.save(Mockito.any(ExpertiseSitter.class))).thenReturn(expertiseSitter);
		ExpertiseSitter sitter = new ExpertiseSitter();
		sitter.setLastName("John");
		ExpertiseSitter response = expertiseSitterService.createExpertiseSitter(sitter);

    assertEquals(new Long(123), response.getId());
    assertEquals("John", response.getFirstName());
    assertEquals("Longlegs", response.getLastName());
		verify(expertiseSitterRepository, times(1))
				.findDistinctExpertiseSitterByLastNameAndFirstName(Mockito.anyString(), Mockito.anyString());
		verify(expertiseSitterRepository, times(1)).save(Mockito.any(ExpertiseSitter.class));
	}

	@Test(expected = CannotPerformOperationException.class)
	public void createExpertiseSitter_failure_1() {
		List<ExpertiseSitter> sitterList = new ArrayList<ExpertiseSitter>();
		when(expertiseSitterRepository.findDistinctExpertiseSitterByLastNameAndFirstName(Mockito.anyString(),
				Mockito.anyString())).thenReturn(sitterList);
		when(expertiseSitterRepository.save(Mockito.any(ExpertiseSitter.class))).thenReturn(expertiseSitter);
		ExpertiseSitter sitter = new ExpertiseSitter();
		expertiseSitterService.createExpertiseSitter(sitter);

		verify(expertiseSitterRepository, times(1))
				.findDistinctExpertiseSitterByLastNameAndFirstName(Mockito.anyString(), Mockito.anyString());
		verify(expertiseSitterRepository, times(1)).save(Mockito.any(ExpertiseSitter.class));
	}

	@Test(expected = CannotPerformOperationException.class)
	public void createExpertiseSitter_failure_2() {
		List<ExpertiseSitter> sitterList = new ArrayList<ExpertiseSitter>();
		when(expertiseSitterRepository.findDistinctExpertiseSitterByLastNameAndFirstName(Mockito.anyString(),
				Mockito.anyString())).thenReturn(sitterList);
		when(expertiseSitterRepository.save(Mockito.any(ExpertiseSitter.class)))
				.thenThrow(new DataIntegrityViolationException("ExpertiseSitter name already exists."));

		ExpertiseSitter sitter = new ExpertiseSitter();
		sitter.setLastName("John");
		expertiseSitterService.createExpertiseSitter(sitter);

		verify(expertiseSitterRepository, times(1))
				.findDistinctExpertiseSitterByLastNameAndFirstName(Mockito.anyString(), Mockito.anyString());
		verify(expertiseSitterRepository, times(1)).save(Mockito.any(ExpertiseSitter.class));
	}
	
	@Test(expected = CannotPerformOperationException.class)
	public void createExpertiseSitter_failure_3() {
		List<ExpertiseSitter> sitterList = new ArrayList<ExpertiseSitter>();
		when(expertiseSitterRepository.findDistinctExpertiseSitterByLastNameAndFirstName(Mockito.anyString(),
				Mockito.anyString())).thenReturn(sitterList);
		when(expertiseSitterRepository.save(Mockito.any(ExpertiseSitter.class)))
				.thenThrow(new CannotAcquireLockException("Lock couldn't be acquired on table, please try again later"));

		ExpertiseSitter sitter = new ExpertiseSitter();
		sitter.setLastName("John");
		expertiseSitterService.createExpertiseSitter(sitter);

		verify(expertiseSitterRepository, times(1))
				.findDistinctExpertiseSitterByLastNameAndFirstName(Mockito.anyString(), Mockito.anyString());
		verify(expertiseSitterRepository, times(1)).save(Mockito.any(ExpertiseSitter.class));
	}
	
	@Test(expected = CannotPerformOperationException.class)
	public void createExpertiseSitter_failure_4() {
		when(expertiseSitterRepository.findDistinctExpertiseSitterByLastNameAndFirstName(Mockito.anyString(),
				Mockito.anyString())).thenReturn(expertiseSitterList);
		when(expertiseSitterRepository.save(Mockito.any(ExpertiseSitter.class)))
				.thenThrow(new CannotAcquireLockException("Lock couldn't be acquired on table, please try again later"));

		ExpertiseSitter sitter = new ExpertiseSitter();
		sitter.setLastName("John");
		expertiseSitterService.createExpertiseSitter(sitter);

		verify(expertiseSitterRepository, times(1))
				.findDistinctExpertiseSitterByLastNameAndFirstName(Mockito.anyString(), Mockito.anyString());
		verify(expertiseSitterRepository, times(0)).save(Mockito.any(ExpertiseSitter.class));
	}

}