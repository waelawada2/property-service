package com.sothebys.propertydb.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;

import com.sothebys.propertydb.DummyModelHelper;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ImageMainColour;
import com.sothebys.propertydb.repository.ImageMainColourRepository;

@RunWith(MockitoJUnitRunner.class)
public class ImageMainColourServiceImplTest {

  @InjectMocks
  private ImageMainColourServiceImpl imageMainColourService;

  @Mock
  private ImageMainColourRepository imageMainColourRepository;

  private ImageMainColour imageMainColour;

	private List<ImageMainColour> imageMainColourList;

  @Before
  public void setUp() {
    imageMainColour = DummyModelHelper.createImageMainColourModel();
		imageMainColourList = DummyModelHelper.imageMainColourList();
  }

  @Test
  public void findByName() {
    when(imageMainColourRepository.findByName("White")).thenReturn(imageMainColour);

    ImageMainColour response = imageMainColourService.findByName("White");

    assertEquals(new Long(123), response.getId());
    assertEquals("White", response.getName());
  }

  @Test(expected = NotFoundException.class)
  public void findByNameNotFound() {
    when(imageMainColourRepository.findByName("White")).thenReturn(null);
    imageMainColourService.findByName("White");
  }

	@Test
	public void findAllImageMainColours() {
		when(imageMainColourRepository.findAllByOrderByNameAsc()).thenReturn(imageMainColourList);
		List<ImageMainColour> response = imageMainColourService.findAllImageMainColours();
		assertEquals(2, response.size());
		verify(imageMainColourRepository, times(1)).findAllByOrderByNameAsc();
	}

	@Test
	public void getImageMainColourById_success() {
		when(imageMainColourRepository.findOne(Mockito.anyLong())).thenReturn(imageMainColour);
		ImageMainColour response = imageMainColourService.getImageMainColourById(imageMainColour.getId());
		assertEquals(new Long(123), response.getId());
		assertEquals("White", response.getName());
		verify(imageMainColourRepository, times(1)).findOne(Mockito.anyLong());
	}

	@Test(expected = NotFoundException.class)
	public void getImageMainColourById_failure() {
		when(imageMainColourRepository.findOne(Mockito.anyLong())).thenReturn(null);
		imageMainColourService.getImageMainColourById(imageMainColour.getId());
		verify(imageMainColourRepository, times(1)).findOne(Mockito.anyLong());
	}

	@Test
	public void createImageMainColour_success() {
		when(imageMainColourRepository.save(imageMainColour)).thenReturn(imageMainColour);
		ImageMainColour response = imageMainColourService.createImageMainColour(imageMainColour);
		verify(imageMainColourRepository, times(1)).save(imageMainColour);
		assertEquals(new Long(123), response.getId());
		assertEquals("White", response.getName());
	}

	@Test(expected = CannotPerformOperationException.class)
	public void createImageMainColour_failure() {
		when(imageMainColourRepository.save(imageMainColour))
				.thenThrow(new DataIntegrityViolationException("ImageMainColour name already exists."));
		imageMainColourService.createImageMainColour(imageMainColour);
		verify(imageMainColourRepository, times(1)).save(imageMainColour);
	}
	
	@Test
	public void deleteImageMainColourById_success() {
		when(imageMainColourRepository.findOne(Mockito.anyLong())).thenReturn(imageMainColour);
		when(imageMainColourRepository.findByProperty(Mockito.anyLong())).thenReturn(null);
		doNothing().when(imageMainColourRepository).delete(Mockito.anyLong());
		imageMainColourService.deleteImageMainColourById(123L);
		verify(imageMainColourRepository, times(1)).findOne(123L);
		verify(imageMainColourRepository, times(1)).findByProperty(123L);
		verify(imageMainColourRepository, times(1)).delete(123L);
	}

	@Test(expected = NotFoundException.class)
	public void deleteImageMainColourById_failure_1() {
		when(imageMainColourRepository.findOne(Mockito.anyLong())).thenReturn(null);
		imageMainColourService.deleteImageMainColourById(123L);
		verify(imageMainColourRepository, times(1)).findOne(123L);
	}

	@Test(expected = NotFoundException.class)
	public void deleteImageMainColourById_failure_2() {
		when(imageMainColourRepository.findOne(Mockito.anyLong())).thenReturn(imageMainColour);
		when(imageMainColourRepository.findByProperty(Mockito.anyLong())).thenReturn(imageMainColour);
		imageMainColourService.deleteImageMainColourById(123L);
		verify(imageMainColourRepository, times(1)).delete(123L);
	}
	
	@Test
	public void updateImageMainColourById_success() {
		when(imageMainColourRepository.findOne(Mockito.anyLong())).thenReturn(imageMainColour);
		when(imageMainColourRepository.save(imageMainColour)).thenReturn(imageMainColour);
		ImageMainColour response = imageMainColourService.updateImageMainColourById(imageMainColour.getId(), imageMainColour);
		verify(imageMainColourRepository, times(1)).findOne(123L);
		verify(imageMainColourRepository, times(1)).save(imageMainColour);
		assertEquals(new Long(123), response.getId());
		assertEquals("White", response.getName());
	}
	
	@Test(expected = CannotPerformOperationException.class)
	public void updateImageMainColourById_failure() {
		when(imageMainColourRepository.findOne(Mockito.anyLong())).thenReturn(null);
		imageMainColourService.updateImageMainColourById(imageMainColour.getId(), imageMainColour);
		verify(imageMainColourRepository, times(1)).findOne(123L);
	}

}