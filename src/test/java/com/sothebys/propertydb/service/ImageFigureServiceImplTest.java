package com.sothebys.propertydb.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.sothebys.propertydb.DummyModelHelper;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ImageAnimal;
import com.sothebys.propertydb.model.ImageFigure;
import com.sothebys.propertydb.repository.ImageFigureRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ImageFigureServiceImplTest {

  @InjectMocks
  private ImageFigureServiceImpl imageFigureService;

  @Mock
  private ImageFigureRepository imageFigureRepository;

  private ImageFigure imageFigure;

  @Before
  public void setUp() {
    imageFigure = DummyModelHelper.createImageFigureModel();
  }

  @Test
  public void findByName() {
    when(imageFigureRepository.findByName("20+")).thenReturn(imageFigure);

    ImageFigure response = imageFigureService.findByName("20+");

    assertEquals(new Long(123), response.getId());
    assertEquals("20+", response.getName());
  }

  @Test(expected = NotFoundException.class)
  public void findByNameNotFound() {
    when(imageFigureRepository.findByName("20+")).thenReturn(null);
    imageFigureService.findByName("20+");
  }
  
  @Test
  public void findAllImageFigures() {
	List<ImageFigure> imageFigureList = new ArrayList<ImageFigure>();
	ImageFigure newImageFigure = new ImageFigure();
	newImageFigure.setId(123L);
	newImageFigure.setName("19");
	imageFigureList.add(newImageFigure);
    
	newImageFigure = new ImageFigure();
	newImageFigure.setId(124L);
	newImageFigure.setName("20+");
	imageFigureList.add(newImageFigure);
    when(imageFigureRepository.findAllByOrderByNameAsc()).thenReturn(imageFigureList);
    List<ImageFigure> response = imageFigureService.findAllImageFigures();
    assertEquals(2, response.size());
  }
}