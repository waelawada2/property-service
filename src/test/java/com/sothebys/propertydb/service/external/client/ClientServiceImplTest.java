package com.sothebys.propertydb.service.external.client;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;

import com.sothebys.propertydb.search.ClientSearchInputDTO;
import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.KeycloakSecurityContext;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * @author jsilva.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(ClientServiceImpl.class)
@Slf4j
public class ClientServiceImplTest {

  public static final String CLIENT_IDS_SEARCH_EXTERNAL_SERVICE = "clientIdsSearchExternalService";

  @Test
  public void test_clientIdSearch() throws Exception {
    ClientSearchInputDTO csi = new ClientSearchInputDTO("USA", "NYC", "New York", "", "", "", null,
        "", "", "", "", null, "", null,
        "", "", "", "", "");
    ClientServiceImpl clientService = PowerMockito.spy(new ClientServiceImpl(new RestTemplateBuilder()));
    PowerMockito.doReturn(Arrays.asList("EOS-123456", "EOS-1234598"))
        .when(clientService, CLIENT_IDS_SEARCH_EXTERNAL_SERVICE, anyObject(), anyString());
    ReflectionTestUtils.setField(clientService,"CLIENT_SERVICE_URL","http://sothebys.com/clients");
    List<String> result= clientService.clientIdsSearch(csi,new KeycloakSecurityContext());
    verifyPrivate(clientService, times(1)).invoke(CLIENT_IDS_SEARCH_EXTERNAL_SERVICE, anyObject(), anyString());
    assertEquals(2,result.size());
  }
}
