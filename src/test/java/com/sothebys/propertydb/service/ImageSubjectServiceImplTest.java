package com.sothebys.propertydb.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;

import com.sothebys.propertydb.DummyModelHelper;
import com.sothebys.propertydb.exception.CannotPerformOperationException;
import com.sothebys.propertydb.exception.NotFoundException;
import com.sothebys.propertydb.model.ImageSubject;
import com.sothebys.propertydb.repository.ImageSubjectRepository;

@RunWith(MockitoJUnitRunner.class)
public class ImageSubjectServiceImplTest {

  @InjectMocks
  private ImageSubjectServiceImpl imageSubjectService;

  @Mock
  private ImageSubjectRepository imageSubjectRepository;

  private ImageSubject imageSubject;

	private List<ImageSubject> imageSubjectList;

  @Before
  public void setUp() {
    imageSubject = DummyModelHelper.createImageSubjectModel();
		imageSubjectList = DummyModelHelper.imageSubjectList();
  }

  @Test
  public void findByName() {
    when(imageSubjectRepository.findByName("Architecture")).thenReturn(imageSubject);

    ImageSubject response = imageSubjectService.findByName("Architecture");

    assertEquals(new Long(123), response.getId());
    assertEquals("Architecture", response.getName());
  }

  @Test(expected = NotFoundException.class)
  public void findByNameNotFound() {
    when(imageSubjectRepository.findByName("Architecture")).thenReturn(null);
    imageSubjectService.findByName("Architecture");
  }

	@Test
	public void findByNameCreateIfNotFound_success() {
		when(imageSubjectRepository.findByName(Mockito.anyString())).thenReturn(null);
		when(imageSubjectRepository.save(Mockito.any(ImageSubject.class))).thenReturn(imageSubject);

		ImageSubject response = imageSubjectService.findByNameCreateIfNotFound("Architecture");

		assertEquals(new Long(123), response.getId());
		assertEquals("Architecture", response.getName());
		verify(imageSubjectRepository, times(1)).findByName(Mockito.anyString());
		verify(imageSubjectRepository, times(1)).save(Mockito.any(ImageSubject.class));
	}

	@Test
	public void findByNameCreateIfNotFound_failure() {
		when(imageSubjectRepository.findByName(Mockito.anyString())).thenReturn(imageSubject);

		ImageSubject response = imageSubjectService.findByNameCreateIfNotFound("Architecture");

		assertEquals("Architecture", response.getName());
		verify(imageSubjectRepository, times(1)).findByName(Mockito.anyString());
		verify(imageSubjectRepository, times(0)).save(Mockito.any(ImageSubject.class));
	}

	@Test
	public void findAllImageSubjects() {
		when(imageSubjectRepository.findAllByOrderByNameAsc()).thenReturn(imageSubjectList);

		List<ImageSubject> response = imageSubjectService.findAllImageSubjects();

		assertEquals(2, response.size());
		verify(imageSubjectRepository, times(1)).findAllByOrderByNameAsc();
	}

	@Test
	public void findAllImageSubjectsByName() {
		when(imageSubjectRepository.findByNameContaining(Mockito.anyString())).thenReturn(imageSubjectList);

		List<ImageSubject> response = imageSubjectService.findAllImageSubjectsByName("Architecture");

		assertEquals(2, response.size());
		verify(imageSubjectRepository, times(1)).findByNameContaining(Mockito.anyString());
	}

	@Test
	public void updateImageSubjectById_success() {
		when(imageSubjectRepository.findOne(Mockito.anyLong())).thenReturn(imageSubject);
		when(imageSubjectRepository.save(Mockito.any(ImageSubject.class))).thenReturn(imageSubject);

		ImageSubject response = imageSubjectService.updateImageSubjectById(imageSubject.getId(), imageSubject);

		assertEquals(new Long(123), response.getId());
		assertEquals("Architecture", response.getName());
		verify(imageSubjectRepository, times(1)).findOne(Mockito.anyLong());
		verify(imageSubjectRepository, times(1)).save(Mockito.any(ImageSubject.class));
	}

	@Test(expected = CannotPerformOperationException.class)
	public void updateImageSubjectById_failure_1() {
		when(imageSubjectRepository.findOne(Mockito.anyLong())).thenReturn(imageSubject);
		when(imageSubjectRepository.save(Mockito.any(ImageSubject.class))).thenReturn(imageSubject);

		ImageSubject response = imageSubjectService.updateImageSubjectById(null, imageSubject);

		assertEquals(new Long(123), response.getId());
		assertEquals("Architecture", response.getName());
		verify(imageSubjectRepository, times(1)).findOne(Mockito.anyLong());
		verify(imageSubjectRepository, times(0)).save(Mockito.any(ImageSubject.class));
	}

	@Test(expected = CannotPerformOperationException.class)
	public void updateImageSubjectById_failure_2() {
		when(imageSubjectRepository.findOne(Mockito.anyLong())).thenReturn(imageSubject);
		when(imageSubjectRepository.save(Mockito.any(ImageSubject.class))).thenReturn(imageSubject);

		ImageSubject response = imageSubjectService.updateImageSubjectById(111L, imageSubject);

		assertEquals(new Long(123), response.getId());
		assertEquals("Architecture", response.getName());
		verify(imageSubjectRepository, times(1)).findOne(Mockito.anyLong());
		verify(imageSubjectRepository, times(0)).save(Mockito.any(ImageSubject.class));
	}

	@Test(expected = CannotPerformOperationException.class)
	public void updateImageSubjectById_failure_3() {
		when(imageSubjectRepository.findOne(Mockito.anyLong())).thenReturn(null);
		when(imageSubjectRepository.save(Mockito.any(ImageSubject.class))).thenReturn(imageSubject);

		ImageSubject response = imageSubjectService.updateImageSubjectById(111L, imageSubject);

		assertEquals(new Long(123), response.getId());
		assertEquals("Architecture", response.getName());
		verify(imageSubjectRepository, times(1)).findOne(Mockito.anyLong());
		verify(imageSubjectRepository, times(0)).save(Mockito.any(ImageSubject.class));
	}

	@Test
	public void deleteImageSubjectById_success() {
		when(imageSubjectRepository.findOne(Mockito.anyLong())).thenReturn(imageSubject);
		when(imageSubjectRepository.findByProperty(Mockito.anyLong())).thenReturn(null);
		doNothing().when(imageSubjectRepository).delete(Mockito.anyLong());
		imageSubjectService.deleteImageSubjectById(123L);

		verify(imageSubjectRepository, times(1)).findOne(Mockito.anyLong());
		verify(imageSubjectRepository, times(1)).findByProperty(Mockito.anyLong());
		verify(imageSubjectRepository, times(1)).delete(Mockito.anyLong());
	}

	@Test(expected = NotFoundException.class)
	public void deleteImageSubjectById_failure_1() {
		when(imageSubjectRepository.findOne(Mockito.anyLong())).thenReturn(null);
		when(imageSubjectRepository.findByProperty(Mockito.anyLong())).thenReturn(null);
		doNothing().when(imageSubjectRepository).delete(Mockito.anyLong());
		imageSubjectService.deleteImageSubjectById(123L);

		verify(imageSubjectRepository, times(1)).findOne(Mockito.anyLong());
		verify(imageSubjectRepository, times(0)).findByProperty(Mockito.anyLong());
		verify(imageSubjectRepository, times(0)).delete(Mockito.anyLong());
	}

	@Test(expected = NotFoundException.class)
	public void deleteImageSubjectById_failure_2() {
		when(imageSubjectRepository.findOne(Mockito.anyLong())).thenReturn(imageSubject);
		when(imageSubjectRepository.findByProperty(Mockito.anyLong())).thenReturn(imageSubject);
		doNothing().when(imageSubjectRepository).delete(Mockito.anyLong());
		imageSubjectService.deleteImageSubjectById(123L);

		verify(imageSubjectRepository, times(1)).findOne(Mockito.anyLong());
		verify(imageSubjectRepository, times(1)).findByProperty(Mockito.anyLong());
		verify(imageSubjectRepository, times(0)).delete(Mockito.anyLong());
	}

	@Test
	public void createImageSubject_success() {
		when(imageSubjectRepository.save(Mockito.any(ImageSubject.class))).thenReturn(imageSubject);

		ImageSubject response = imageSubjectService.createImageSubject(imageSubject);

		assertEquals(new Long(123), response.getId());
		assertEquals("Architecture", response.getName());
		verify(imageSubjectRepository, times(1)).save(Mockito.any(ImageSubject.class));
	}

	@Test(expected = CannotPerformOperationException.class)
	public void createImageSubject_failure() {
		when(imageSubjectRepository.save(Mockito.any(ImageSubject.class)))
				.thenThrow(new DataIntegrityViolationException("ImageSubject name already exists."));

		imageSubjectService.createImageSubject(imageSubject);

		verify(imageSubjectRepository, times(1)).save(Mockito.any(ImageSubject.class));
	}

}