package com.sothebys.propertydb.mapper.converter;

import static org.junit.Assert.*;

import com.sothebys.propertydb.DummyModelHelper;
import com.sothebys.propertydb.dto.TagsDTO;
import com.sothebys.propertydb.model.Orientation;
import com.sothebys.propertydb.model.Scale;
import com.sothebys.propertydb.model.Tags;
import java.math.BigDecimal;
import java.util.Collections;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TagsToDTOConverterTest {

  @InjectMocks
  private TagsToDTOConverter tagsToDTOConverter;

  private Tags tags;

  @Before
  public void setUp() {
    tags = DummyModelHelper.createTagsModel();
  }

  @Test
  public void convert() {
    TagsDTO tagsDTO = tagsToDTOConverter.convert(tags);

    assertEquals("20+", tagsDTO.getImageFigure());
    assertEquals("Standing", tagsDTO.getImagePositions().get(0));
    assertEquals("Architecture", tagsDTO.getImageSubjects().get(0));
    assertEquals("some location", tagsDTO.getExpertiseLocation());
    assertEquals("some text", tagsDTO.getImageText());
    assertEquals("Male", tagsDTO.getImageGenders().get(0));
    assertEquals("John", tagsDTO.getExpertiseSitters().get(0).getFirstName());
    assertEquals("Longlegs", tagsDTO.getExpertiseSitters().get(0).getLastName());
    assertEquals("Lion", tagsDTO.getImageAnimals().get(0));
    assertEquals("White", tagsDTO.getImageMainColours().get(0));
    assertEquals("Tag", tagsDTO.getOtherTags().get(0));
    assertEquals(Orientation.HORIZONTAL, tagsDTO.getAutoOrientation());
    assertEquals(true, tagsDTO.isAutoImage());
    assertEquals(Scale.XS, tagsDTO.getAutoScale());
    assertEquals(new BigDecimal(1020.45), tagsDTO.getAutoVolume());
  }

  @Test
  public void convertNullTags() {
    TagsDTO tagsDTO = tagsToDTOConverter.convert((Tags) null);
    assertNull(tagsDTO);
  }

  @Test
  public void convertEmptyTags() {
    TagsDTO tagsDTO = tagsToDTOConverter.convert(new Tags());

    assertNull(tagsDTO.getImageFigure());
    assertTrue(tagsDTO.getImagePositions().isEmpty());
    assertTrue(tagsDTO.getImageSubjects().isEmpty());
    assertNull(tagsDTO.getExpertiseLocation());
    assertNull(tagsDTO.getImageText());
    assertTrue(tagsDTO.getImageGenders().isEmpty());
    assertTrue(tagsDTO.getExpertiseSitters().isEmpty());
    assertTrue(tagsDTO.getImageAnimals().isEmpty());
    assertTrue(tagsDTO.getImageMainColours().isEmpty());
    assertTrue(tagsDTO.getOtherTags().isEmpty());
    assertNull(tagsDTO.getAutoOrientation());
    assertFalse(tagsDTO.isAutoImage());
    assertNull(tagsDTO.getAutoScale());
    assertNull(tagsDTO.getAutoVolume());
  }



}