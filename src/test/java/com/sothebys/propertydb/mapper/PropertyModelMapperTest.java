package com.sothebys.propertydb.mapper;

import com.sothebys.propertydb.dto.request.PropertyAddRequestCatalogueRaisoneeDTO;
import com.sothebys.propertydb.dto.request.PropertyAddRequestDTO;
import com.sothebys.propertydb.mapper.PropertyModelMapper;
import com.sothebys.propertydb.model.ArtistCatalogueRaisonee;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by sankeerth on 1/13/2018.
 */
public class PropertyModelMapperTest {

  private PropertyModelMapper propertyModelMapper;

  /**
   * Intializes PropertyModelMapper
   */
  @Before
  public void createpropertyModelMapper() {
    propertyModelMapper = new PropertyModelMapper(null, null, null, null, null, null, null,
        null, null, null, null, null, null, null, null, null,null);
  }

  /**
   * Success case when Number and Volume are Not Empty
   */
  @Test
  public void mapCatalogueRaisoneesTest() {

    PropertyAddRequestDTO propertyAddRequestDTO = propertyModelMapper.mapCatalogueRaisonees(
        getPropertyAddRequestDto(getpropertyAddRequestCatalogueRaisoneeDTO()));

    assertNotNull(propertyAddRequestDTO.getPropertyCatalogueRaisonees());
    assertEquals(1, propertyAddRequestDTO.getPropertyCatalogueRaisonees().size());

  }

  /**
   * Success case when Number and Volume are Not Empty for list of CatalogueRaisonees
   */
  @Test
  public void mapCatalogueRaisoneesListTest() {

    List<PropertyAddRequestCatalogueRaisoneeDTO> propertyCatalogueRaisoneeList = new ArrayList<>();
    propertyCatalogueRaisoneeList.add(getpropertyAddRequestCatalogueRaisoneeDTO().get(0));
    propertyCatalogueRaisoneeList
        .add(getpropertyAddRequestCatalogueRaisoneeDTOWithNullValues().get(0));

    PropertyAddRequestDTO propertyAddRequestDTO = propertyModelMapper
        .mapCatalogueRaisonees(getPropertyAddRequestDto(propertyCatalogueRaisoneeList));

    assertEquals(1, propertyAddRequestDTO.getPropertyCatalogueRaisonees().size());

  }

  /**
   * Failure case when Number and Volume are Empty
   */
  @Test
  public void mapCatalogueRaisoneesTestNullNumberAndVolume() {

    PropertyAddRequestDTO propertyAddRequestDTO = propertyModelMapper.mapCatalogueRaisonees(
        getPropertyAddRequestDto(getpropertyAddRequestCatalogueRaisoneeDTOWithNullValues()));

    assertEquals(0, propertyAddRequestDTO.getPropertyCatalogueRaisonees().size());
  }

  /**
   * @return PropertyAddRequestDTO
   */
  private PropertyAddRequestDTO getPropertyAddRequestDto(
      List<PropertyAddRequestCatalogueRaisoneeDTO> propertCatalogueRaisoneeList) {
    List<String> artistList = new ArrayList<>();
    ArtistCatalogueRaisonee artistCatalogueRaisonee = new ArtistCatalogueRaisonee();
    artistCatalogueRaisonee.setId(1L);
    artistList.add("1");
    PropertyAddRequestDTO propertyDTO = new PropertyAddRequestDTO();
    propertyDTO.setArtistIds(artistList);
    propertyDTO.setAuthentication(true);
    propertyDTO.setCategory("category");
    propertyDTO.setEditionName("edition name");
    propertyDTO.setPresizeName("presize name");
    propertyDTO.setPropertyCatalogueRaisonees(propertCatalogueRaisoneeList);
    return propertyDTO;
  }

  /**
   * Populates PropertyAddRequestCatalogueRaisoneeDTO list with Number and Volume as not null
   * values
   *
   * @return List<PropertyAddRequestCatalogueRaisoneeDTO>
   */
  private List<PropertyAddRequestCatalogueRaisoneeDTO> getpropertyAddRequestCatalogueRaisoneeDTO() {
    PropertyAddRequestCatalogueRaisoneeDTO propertyCatalogueRaisonee =
        new PropertyAddRequestCatalogueRaisoneeDTO();
    propertyCatalogueRaisonee.setId(1L);
    propertyCatalogueRaisonee.setNumber("Test");
    propertyCatalogueRaisonee.setVolume("Test");
    propertyCatalogueRaisonee.setArtistCatalogueRaisoneeId(1L);
    List<PropertyAddRequestCatalogueRaisoneeDTO> propertyCatalogueRaisoneeList = new ArrayList<>();
    propertyCatalogueRaisoneeList.add(propertyCatalogueRaisonee);
    return propertyCatalogueRaisoneeList;
  }

  /**
   * Populates PropertyAddRequestCatalogueRaisoneeDTO list with Number and Volume as null values
   *
   * @return List<PropertyAddRequestCatalogueRaisoneeDTO>
   */
  private List<PropertyAddRequestCatalogueRaisoneeDTO> getpropertyAddRequestCatalogueRaisoneeDTOWithNullValues() {
    PropertyAddRequestCatalogueRaisoneeDTO propertyCatalogueRaisonee =
        new PropertyAddRequestCatalogueRaisoneeDTO();
    propertyCatalogueRaisonee.setId(1L);
    propertyCatalogueRaisonee.setNumber("");
    propertyCatalogueRaisonee.setVolume("");
    propertyCatalogueRaisonee.setArtistCatalogueRaisoneeId(1L);
    List<PropertyAddRequestCatalogueRaisoneeDTO> propertyCatalogueRaisoneeList = new ArrayList<>();
    propertyCatalogueRaisoneeList.add(propertyCatalogueRaisonee);
    return propertyCatalogueRaisoneeList;
  }

}
