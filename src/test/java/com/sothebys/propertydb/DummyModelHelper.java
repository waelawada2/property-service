package com.sothebys.propertydb;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.accept.ContentNegotiationManagerFactoryBean;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;

import com.sothebys.propertydb.configuration.views.CsvViewResolver;
import com.sothebys.propertydb.controller.support.AdditionalMediaType;
import com.sothebys.propertydb.dto.ArtistCatalogueRaisoneeDTO;
import com.sothebys.propertydb.dto.ArtistDTO;
import com.sothebys.propertydb.dto.PropertyCatalogueRaisoneeDTO;
import com.sothebys.propertydb.dto.PropertyDTO;
import com.sothebys.propertydb.dto.SaveListDTO;
import com.sothebys.propertydb.dto.request.SaveListRequestDTO;
import com.sothebys.propertydb.model.Artist;
import com.sothebys.propertydb.model.Authenticity;
import com.sothebys.propertydb.model.ExpertiseSitter;
import com.sothebys.propertydb.model.ImageAnimal;
import com.sothebys.propertydb.model.ImageFigure;
import com.sothebys.propertydb.model.ImageGender;
import com.sothebys.propertydb.model.ImageMainColour;
import com.sothebys.propertydb.model.ImagePosition;
import com.sothebys.propertydb.model.ImageSubject;
import com.sothebys.propertydb.model.Orientation;
import com.sothebys.propertydb.model.OtherTag;
import com.sothebys.propertydb.model.Property;
import com.sothebys.propertydb.model.SaveList;
import com.sothebys.propertydb.model.Scale;
import com.sothebys.propertydb.model.Tags;
import com.sothebys.propertydb.model.User;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DummyModelHelper {

  public static ContentNegotiatingViewResolver createContentNegotiatingViewResolver() {
    ContentNegotiationManagerFactoryBean factory = new ContentNegotiationManagerFactoryBean();
    factory.setDefaultContentType(MediaType.APPLICATION_JSON);
    factory.setIgnoreAcceptHeader(true);
    factory.addMediaType("json", MediaType.APPLICATION_JSON);
    factory.addMediaType("csv", AdditionalMediaType.TEXT_CSV);
    factory.setUseJaf(false);
    factory.setFavorPathExtension(true);
    factory.setFavorParameter(false);
    factory.afterPropertiesSet();
    ContentNegotiationManager manager = factory.getObject();

    CsvViewResolver csvViewResolver = new CsvViewResolver();

    ContentNegotiatingViewResolver contentNegotiatingViewResolver = new ContentNegotiatingViewResolver();
    contentNegotiatingViewResolver.setViewResolvers(Collections.singletonList(csvViewResolver));
    contentNegotiatingViewResolver.setContentNegotiationManager(manager);

    return contentNegotiatingViewResolver;
  }

  public static ExpertiseSitter createExpertiseSitterModel() {
    final ExpertiseSitter expertiseSitter = new ExpertiseSitter();
    expertiseSitter.setId(123L);
    expertiseSitter.setFirstName("John");
    expertiseSitter.setLastName("Longlegs");
    return expertiseSitter;
  }

	public static List<ExpertiseSitter> expertiseSitterList() {
		List<ExpertiseSitter> expertiseSitterList = new ArrayList<ExpertiseSitter>();

		ExpertiseSitter expertiseSitter = new ExpertiseSitter();
		expertiseSitter.setId(123L);
		expertiseSitter.setFirstName("John");
		expertiseSitter.setLastName("Longlegs");
		expertiseSitterList.add(expertiseSitter);

		expertiseSitter = new ExpertiseSitter();
		expertiseSitter.setId(124L);
		expertiseSitter.setFirstName("Sean");
		expertiseSitter.setLastName("Paul");
		expertiseSitterList.add(expertiseSitter);

		return expertiseSitterList;
	}

  public static ImageAnimal createImageAnimalModel() {
    final ImageAnimal imageAnimal = new ImageAnimal();
    imageAnimal.setId(123L);
    imageAnimal.setName("Lion");
    return imageAnimal;
  }

  public static OtherTag createOtherTagModel() {
    final OtherTag otherTag = new OtherTag();
    otherTag.setId(123L);
    otherTag.setName("Tag");
    return otherTag;
  }

  public static List<OtherTag> otherTagList() {
	  List<OtherTag> otherTagList = new ArrayList<OtherTag>();
	  OtherTag otherTag = new OtherTag();
	  otherTag.setId(123L);
	  otherTag.setName("Tag");
	  otherTagList.add(otherTag);
	  
	  otherTag = new OtherTag();
	  otherTag.setId(124L);
	  otherTag.setName("Other Tag");
	  otherTagList.add(otherTag);
	  
	  return otherTagList;
  }

  public static ImageFigure createImageFigureModel() {
    final ImageFigure imageFigure = new ImageFigure();
    imageFigure.setId(123L);
    imageFigure.setName("20+");
    return imageFigure;
  }

  public static ImageGender createImageGenderModel() {
    final ImageGender imageGender = new ImageGender();
    imageGender.setId(123L);
    imageGender.setName("Male");
    return imageGender;
  }

  public static ImageMainColour createImageMainColourModel() {
    final ImageMainColour imageMainColour = new ImageMainColour();
    imageMainColour.setId(123L);
    imageMainColour.setName("White");
    return imageMainColour;
  }

  public static List<ImageMainColour> imageMainColourList() {
	  List<ImageMainColour> imageMainColourList = new ArrayList<ImageMainColour>();
	  ImageMainColour imageMainColour = new ImageMainColour();
	  imageMainColour.setId(123L);
	  imageMainColour.setName("White");
	  imageMainColourList.add(imageMainColour);
	  
	  imageMainColour = new ImageMainColour();
	  imageMainColour.setId(124L);
	  imageMainColour.setName("Black");
	  imageMainColourList.add(imageMainColour);
	  
	  return imageMainColourList;
  }

  public static ImagePosition createImagePositionModel() {
    final ImagePosition imagePosition = new ImagePosition();
    imagePosition.setId(123L);
    imagePosition.setName("Standing");
    return imagePosition;
  }

  public static List<ImagePosition> imagePositionList() {
	    List<ImagePosition> imagePositionList = new ArrayList<ImagePosition>();
	    
	    ImagePosition imagePosition = new ImagePosition();
	    imagePosition.setId(123L);
	    imagePosition.setName("Standing");
	    imagePositionList.add(imagePosition);
	    
	    imagePosition = new ImagePosition();
	    imagePosition.setId(124L);
	    imagePosition.setName("Sitting");
	    imagePositionList.add(imagePosition);
	    
	    return imagePositionList;
	  }

  public static ImageSubject createImageSubjectModel() {
    final ImageSubject imageSubject = new ImageSubject();
    imageSubject.setId(123L);
    imageSubject.setName("Architecture");
    return imageSubject;
  }

	public static List<ImageSubject> imageSubjectList() {
		List<ImageSubject> imageSubjectList = new ArrayList<ImageSubject>();
		
		ImageSubject imageSubject = new ImageSubject();
		imageSubject.setId(123L);
		imageSubject.setName("Architecture");
		imageSubjectList.add(imageSubject);
		
		imageSubject = new ImageSubject();
		imageSubject.setId(124L);
		imageSubject.setName("Abstract");
		imageSubjectList.add(imageSubject);
		
		return imageSubjectList;
	}

  public static Tags createTagsModel() {
    final Tags tags = new Tags();
    tags.setImageFigure(createImageFigureModel());
    tags.setImagePositions(Collections.singletonList(createImagePositionModel()));
    tags.setImageSubjects(Collections.singletonList(createImageSubjectModel()));
    tags.setExpertiseLocation("some location");
    tags.setImageText("some text");
    tags.setImageGenders(Collections.singletonList(createImageGenderModel()));
    tags.setExpertiseSitters(Collections.singletonList(createExpertiseSitterModel()));
    tags.setImageAnimals(Collections.singletonList(createImageAnimalModel()));
    tags.setImageMainColours(Collections.singletonList(createImageMainColourModel()));
    tags.setOtherTags(Collections.singletonList(createOtherTagModel()));
    tags.setAutoOrientation(Orientation.HORIZONTAL);
    tags.setAutoImage(true);
    tags.setAutoScale(Scale.XS);
    tags.setAutoVolume(new BigDecimal(1020.45));
    return tags;
  }

  public static Property createPropertyModel() {
    Property property = new Property();
    property.setId(1L);
    Artist artist = new Artist();
    artist.setLastName("Picasso");
    artist.setId(1L);
    property.setArtists(Collections.singletonList(artist));
    Authenticity authenticity = new Authenticity();
    authenticity.setAuthentication(true);
    property.setAuthenticity(authenticity);
    property.setImagePath("2017321055254");
    property.setExternalId("00000002");
    return property;
  }

  public static PropertyDTO createPropertyDTO() {
    PropertyDTO propertyDTO = new PropertyDTO();
    propertyDTO.setArtists(Collections.singletonList(createArtistDTO()));
    propertyDTO.setTitle("Test Property");
    propertyDTO.setImagePath("2017321055254");
    propertyDTO.setId("OO-00000002");
    propertyDTO.setAuthentication(true);
    propertyDTO.setPropertyCatalogueRaisonees(createPropertyCatalogueRaisonees());
    return propertyDTO;
  }

  public static ArtistDTO createArtistDTO() {
    ArtistDTO artistDTO = new ArtistDTO();
    artistDTO.setLastName("Picasso");
    artistDTO.setId("00000002");
    artistDTO.setArtistCatalogueRaisonees(createArtistCatalogueRaisonees());
    return artistDTO;
  }
  
  public static List<PropertyCatalogueRaisoneeDTO> createPropertyCatalogueRaisonees() {
    List<PropertyCatalogueRaisoneeDTO> catalogueRaisoneeDTOList = new ArrayList<>();
    
    PropertyCatalogueRaisoneeDTO catalogueRaisoneeDTO1 = new PropertyCatalogueRaisoneeDTO();
    catalogueRaisoneeDTO1.setArtistCatalogueRaisoneeId(new Long("111"));
    catalogueRaisoneeDTO1.setNumber("num1");
    catalogueRaisoneeDTO1.setVolume("vol1");
    
    PropertyCatalogueRaisoneeDTO catalogueRaisoneeDTO2 = new PropertyCatalogueRaisoneeDTO();
    catalogueRaisoneeDTO2.setArtistCatalogueRaisoneeId(new Long("222"));
    catalogueRaisoneeDTO2.setNumber("");
    catalogueRaisoneeDTO2.setVolume("");
    
    PropertyCatalogueRaisoneeDTO catalogueRaisoneeDTO3 = new PropertyCatalogueRaisoneeDTO();
    catalogueRaisoneeDTO3.setArtistCatalogueRaisoneeId(new Long("333"));
    catalogueRaisoneeDTO3.setNumber("num3");
    catalogueRaisoneeDTO3.setVolume("vol3");
    
    catalogueRaisoneeDTOList.add(catalogueRaisoneeDTO1);
    catalogueRaisoneeDTOList.add(catalogueRaisoneeDTO2);
    catalogueRaisoneeDTOList.add(catalogueRaisoneeDTO3);
    
    return catalogueRaisoneeDTOList;
  }

  public static List<ArtistCatalogueRaisoneeDTO> createArtistCatalogueRaisonees() {
    List<ArtistCatalogueRaisoneeDTO> artistCatalogueRaisoneeDTOList = new ArrayList<>();
    
    ArtistCatalogueRaisoneeDTO artistCatalogueRaisoneeDTO1 = new ArtistCatalogueRaisoneeDTO();
    artistCatalogueRaisoneeDTO1.setAuthor("author1");
    artistCatalogueRaisoneeDTO1.setId(new Long("111"));
    artistCatalogueRaisoneeDTO1.setSortId(1);
    
    ArtistCatalogueRaisoneeDTO artistCatalogueRaisoneeDTO2 = new ArtistCatalogueRaisoneeDTO();
    artistCatalogueRaisoneeDTO2.setAuthor("author3");
    artistCatalogueRaisoneeDTO2.setId(new Long("222"));
    artistCatalogueRaisoneeDTO2.setSortId(2);
    
    ArtistCatalogueRaisoneeDTO artistCatalogueRaisoneeDTO3 = new ArtistCatalogueRaisoneeDTO();
    artistCatalogueRaisoneeDTO3.setAuthor("author3");
    artistCatalogueRaisoneeDTO3.setId(new Long("333"));
    artistCatalogueRaisoneeDTO3.setSortId(3);
    
    artistCatalogueRaisoneeDTOList.add(artistCatalogueRaisoneeDTO1);
    artistCatalogueRaisoneeDTOList.add(artistCatalogueRaisoneeDTO2);
    artistCatalogueRaisoneeDTOList.add(artistCatalogueRaisoneeDTO3);
    
    return artistCatalogueRaisoneeDTOList;
        
  }

  public static User createUserModel() {
    User user = new User();
    user.setKeycloakUserId("123456-7890123-456789");
    user.setFirstName("Test");
    user.setLastName("User");
    user.setUserName("ram");
    user.setEmail("test.user@sothebys.com");
    user.setId(1L);
    return user;
  }

  public static User createSharedUserModel() {
    User sharedUser = new User();
    sharedUser.setKeycloakUserId("123456-7890123-456789");
    sharedUser.setFirstName("Test");
    sharedUser.setLastName("User");
    sharedUser.setUserName("sankeerth");
    sharedUser.setEmail("test.user@sothebys.com");
    sharedUser.setId(1L);
    return sharedUser;
  }

  public static SaveList createSaveListModel() {
    SaveList saveList = new SaveList();
    saveList.setId(1L);
    saveList.setName("ram");
    saveList.setNotes("notes");
    saveList.setExternalId("SL-00000002");
    saveList.setUser(createUserModel());
    saveList.setProperties(Collections.singletonList(createPropertyModel()));
    return saveList;
  }

  public static SaveList createSaveListModelWithSharedUser() {
    SaveList saveList = new SaveList();
    saveList.setId(1L);
    saveList.setName("ram");
    saveList.setNotes("notes");
    saveList.setExternalId("SL-00000002");
    saveList.setUser(createSharedUserModel());
    saveList.setProperties(Collections.singletonList(createPropertyModel()));
    return saveList;
  }

  public static SaveListDTO createSaveListDTO() {
    SaveListDTO saveListDTO = new SaveListDTO();
    saveListDTO.setId("SL-00000002");
    saveListDTO.setName("ram");
    saveListDTO.setNotes("notes");
    List<String> propertyIds = new ArrayList<String>();
    propertyIds.add("00000002");
    saveListDTO.setPropertyIds(propertyIds);
    return saveListDTO;
  }

  public static SaveListRequestDTO createSaveListRequestDTO() {
    SaveListRequestDTO saveListDTO = new SaveListRequestDTO();
    saveListDTO.setName("ram");
    saveListDTO.setNotes("notes");
    List<String> propertyIds = new ArrayList<String>();
    propertyIds.add("00000002");
    saveListDTO.setPropertyIds(propertyIds);
    return saveListDTO;
  }

}
