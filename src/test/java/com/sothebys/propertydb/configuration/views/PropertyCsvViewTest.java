package com.sothebys.propertydb.configuration.views;

import com.sothebys.propertydb.DummyModelHelper;
import com.sothebys.propertydb.dto.PropertyDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PropertyCsvViewTest {

  private PropertyDTO propertyDto;

  @Before
  public void setup() {
    this.propertyDto = DummyModelHelper.createPropertyDTO();
  }

  @Test
  public void exportPropertyDataNoNullValuesTest() {
    String[] csvValues = PropertyCsvView.exportPropertyData(this.propertyDto);

    for(int i = 0; i< csvValues.length; i++) {
      Assert.assertNotEquals("null", csvValues[i]);
    }
  }
}
