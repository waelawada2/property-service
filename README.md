# Property Service

This service exposes REST resources for the management of properties

## AWS Resources

All AWS resources needed by Property Service are managed by the CloudFormation template [stb-propertyservice.cf.yml](aws/stb-propertyservice.cf.yml).

The stack should be called `stb-stk-{ENV}-eos-propertyservice` where `{ENV}` is the logical environment name, e.g. `stb-stk-dev-eos-propertyservice`.

*Please Note*: The CloudFormation template creates an IAM user for Property Service. (The IAM username is printed in the CloudFormation Outputs.) 
Credentials must be created manually and added to the appropriate configuration files in the project's [config repository](https://bitbucket.org/sothebys/property-service-config).
