FROM 368978185270.dkr.ecr.us-east-1.amazonaws.com/stb-oraclejdk-overops:latest

COPY target/property-service*.jar app.jar
COPY docker/app.sh /
RUN chmod +x /app.sh

EXPOSE 9990

ENV PORT=9990
ENV JAVA_OPTS="-Xmx1536M -Djava.security.egd=file:/dev/./urandom -Dsun.java2d.cmm=sun.java2d.cmm.kcms.KcmsServiceProvider"

ENTRYPOINT [ "/app.sh" ]
